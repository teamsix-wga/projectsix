﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using ProgressBar;

public class Mine : MonoBehaviour {
    GameObject lumbArea, minePopUp, mineEffect;
    float maxArea;
    public List<GameObject> workingMiners;
    public float workingRange;
    bool closedPopUp;
    TextMesh countLumbers;
    GameObject minePopupGO;
    GameObject mineEffectGO;
    public float goldLimit, startGoldLimit;
    GameObject FoW;
    GameObject percentOfEmpty;

    void Awake () {
        FoW = GameObject.FindWithTag("FoWProjector");
        mineEffectGO = Resources.Load("MineEffect") as GameObject;
        minePopupGO = Resources.Load("MinePopUp") as GameObject;
        goldLimit = startGoldLimit = 50000;
        closedPopUp = false;
        workingMiners = new List<GameObject>();
        workingRange = 0;
        maxArea = 45f;
        mineEffect = Instantiate(mineEffectGO, new Vector3(transform.position.x, GetComponent<Collider>().bounds.max.y + 13f, transform.position.z), Quaternion.identity) as GameObject;
        mineEffect.SetActive(false);
    }
	
	void Update ()
    {
        if(!FoW.GetComponent<RealFoW>()
            .IsBlackPlace(new Vector2(GetComponent<Collider>().bounds.min.x, GetComponent<Collider>().bounds.min.z),
                        new Vector2(GetComponent<Collider>().bounds.max.x, GetComponent<Collider>().bounds.max.z)))
        {
            if (!mineEffect.activeInHierarchy)
            {
                mineEffect.SetActive(true);
            }


            if (!closedPopUp && Vector3.Distance(Camera.main.transform.position, transform.position) <= maxArea + 30f)
            {
                if (!minePopUp)
                {
                    minePopUp =
                        Instantiate(minePopupGO, transform.position + new Vector3(0, 10f, 0), Quaternion.identity) as
                            GameObject;
                    Button[] btns = minePopUp.GetComponentsInChildren<Button>();
                    btns[0].onClick.RemoveAllListeners();
                    btns[0].onClick.AddListener(AddLumberjacks);
                    btns[1].onClick.RemoveAllListeners();
                    btns[1].onClick.AddListener(DeleteLumberjack);
                    btns[2].onClick.RemoveAllListeners();
                    btns[2].onClick.AddListener(ClosePopUp);
                    countLumbers = minePopUp.transform.FindChild("Working/Text").GetComponent<TextMesh>();
                    countLumbers.text = "Шахтёров нет";
                    percentOfEmpty = minePopUp.transform.FindChild("Progress/ProgressBar").gameObject;
                    percentOfEmpty.GetComponent<ProgressBarBehaviour>().SetFillerSizeAsPercentage(100*goldLimit/startGoldLimit);
                    percentOfEmpty.transform.FindChild("Label").GetComponent<Text>().text = goldLimit.ToString();
                }
                if(!minePopUp.activeInHierarchy)
                    minePopUp.SetActive(true);
                minePopUp.transform.LookAt(Camera.main.transform);
                minePopUp.transform.localScale = new Vector3(-1, 1, 1);
            }
            else if (Vector3.Distance(Camera.main.transform.position, transform.position) > maxArea + 30f)
            {
                closedPopUp = false;
                if (minePopUp && minePopUp.activeInHierarchy)
                    minePopUp.SetActive(false);
            }
        }
    }
    
    public void StartMine(GameObject miner){
        StartCoroutine(MineMine(miner));
    }
    
    public IEnumerator MineMine(GameObject miner){
        miner.SetActive(false);   
        while (miner.GetComponent<Lumberjack>().resourcesLimit != 0)
        {
            miner.GetComponent<Lumberjack>().resourcesLimit -= miner.GetComponent<Parameters>().damage/10;
            miner.GetComponent<Lumberjack>().carryGold += miner.GetComponent<Parameters>().damage/10;
            goldLimit -= miner.GetComponent<Parameters>().damage/10;
            if (minePopUp)
            {
                percentOfEmpty.GetComponent<ProgressBarBehaviour>().SetFillerSizeAsPercentage(100 * goldLimit / startGoldLimit);
                percentOfEmpty.transform.FindChild("Label").GetComponent<Text>().text = goldLimit.ToString();
            }
            yield return new WaitForSeconds(0.1f);    
        }
        miner.SetActive(true);   
        miner.GetComponent<Lumberjack>().CarryResources();
        if(!miner.GetComponent<Lumberjack>().working)
            miner.GetComponent<Lumberjack>().GoBack();
        yield break;
    }
    
    public void AddLumberjacks()
    {
        GameObject lumber = Camera.main.GetComponent<Camera3dPerson>().lumberjacks.Find((GameObject lumb) => (!lumb.GetComponent<Lumberjack>().working));

        if (lumber)
        {
            lumber.GetComponent<Lumberjack>().state = LumberState.Miner;
            lumber.GetComponent<Lumberjack>().GoToWork(transform.GetChild(0).position, gameObject);
            workingMiners.Add(lumber);
            SetCountWorkers();
        }
    }

    public void DeleteLumberjack()
    {
        if (workingMiners.Count > 0)
        {
            if(workingMiners[workingMiners.Count - 1].GetComponent<Lumberjack>().resourcesLimit != 0){
                workingMiners[workingMiners.Count - 1].GetComponent<Lumberjack>().resourcesLimit = 0;
                
            }
            workingMiners[workingMiners.Count - 1].SetActive(true);
            workingMiners[workingMiners.Count - 1].GetComponent<Lumberjack>().GoBack();
            workingMiners.RemoveAt(workingMiners.Count - 1);
            SetCountWorkers();
        }
    }

    public void DeleteLumberjack(GameObject lumb)
    {
        if (workingMiners.Count > 0)
        {
            lumb.GetComponent<Lumberjack>().GoBack();
            workingMiners.Remove(lumb);
            if (minePopUp)
                SetCountWorkers();
        }
    }

    void SetCountWorkers()
    {
        if (workingMiners.Count > 0)
            countLumbers.text = "Сейчас работа" + WordWorkEnding(workingMiners.Count) + "\n" + workingMiners.Count + " шахтёр" + WordCountEnding(workingMiners.Count);
        else
            countLumbers.text = "Шахтёров нет";
    }

    string WordCountEnding(int count)
    {
        var num = 0;
        if (count > 0 && count < 10)
            num = count;
        else
            num = count % 10;

        if (num == 1 && count != 11)
            return "";
        if (count >= 2 && count <= 4)
            return "а";
        return "ов";
    }

    string WordWorkEnding(int count)
    {
        var num = 0;
        if (count > 0 && count < 10)
            num = count;
        else
            num = count % 10;

        if (num == 1 && count != 11)
            return "ет";
        else
            return "ют";
        return "";
    }

    public void ClosePopUp()
    {
        Destroy(minePopUp);
        closedPopUp = true;
    }

    void OnMouseDown()
    {
        closedPopUp = false;
    }
}
