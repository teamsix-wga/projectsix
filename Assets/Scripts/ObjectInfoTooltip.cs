﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ObjectInfoTooltip : MonoBehaviour {
    private string data;
    private GameObject tooltip;
    GameObject dataGO;
    GameObject HPBar;
    float openTimer;
    float timeToOpen;
    private GameObject hero;
    bool act = false;

    void Start()
    {
        hero = GameObject.FindWithTag("Player");
        timeToOpen = 0.5f;
        tooltip = GameObject.Find("Tooltip").transform.FindChild("TooltipBar").gameObject;
        tooltip.SetActive(false);
    }

    void Update() {
        if(openTimer > 0)
            openTimer -= Time.deltaTime;
        if(openTimer < 0) {
            openTimer = 0;
            if(act)
                tooltip.SetActive(true);
        }
        if(act) {
            if(dataGO != null)
                ConstructDataString(dataGO);

            tooltip.transform.position = Input.mousePosition + new Vector3(tooltip.GetComponent<RectTransform>().sizeDelta.x / 2, tooltip.GetComponent<RectTransform>().sizeDelta.y / 2);
            if (tooltip.transform.position.x < 0)
                tooltip.transform.position += new Vector3(tooltip.transform.position.x * 2, 0, 0);
            float byY = tooltip.GetComponent<RectTransform>().position.y - tooltip.GetComponent<RectTransform>().rect.height;
            if (byY < 0)
            {
                tooltip.transform.position = new Vector3(tooltip.transform.position.x, tooltip.transform.position.y - byY, tooltip.transform.position.z);
            }
            float byX = tooltip.GetComponent<RectTransform>().position.x + tooltip.GetComponent<RectTransform>().rect.width;
            if (byX > Screen.width)
            {
                tooltip.transform.position = new Vector3(Screen.width - tooltip.GetComponent<RectTransform>().rect.width, tooltip.transform.position.y, tooltip.transform.position.z);
            }

            byY = tooltip.GetComponent<RectTransform>().position.y + tooltip.GetComponent<RectTransform>().rect.height;
            if (byY > Screen.height)
            {
                tooltip.transform.position = new Vector3(tooltip.transform.position.x, Screen.height - tooltip.GetComponent<RectTransform>().rect.height, tooltip.transform.position.z);
            }
            byX = tooltip.GetComponent<RectTransform>().position.x - tooltip.GetComponent<RectTransform>().rect.width;
            if (byX < 0)
            {
                tooltip.transform.position = new Vector3(tooltip.GetComponent<RectTransform>().position.x + byX, tooltip.transform.position.y, tooltip.transform.position.z);
            }
        }
    }
    public void Activate(GameObject infoGO)
    {
        if((infoGO.CompareTag("FriendBuild") && hero.GetComponent<Builder>().building) || tooltip.activeInHierarchy)
            return;
        openTimer = timeToOpen;
        dataGO = infoGO;
        ConstructDataString(infoGO);
        act = true;
    }

    public void Deactivate()
    {
        tooltip.SetActive(false);
        act = false;
    }

    public void ConstructDataString(GameObject infoGO)
    {
        data = "";
        if (infoGO.CompareTag("Enemy") || infoGO.CompareTag("EnemyBuild"))
        {
            data = "<color=#FF0000>"+infoGO.GetComponent<Parameters>().objectName + "\t\t\t"+infoGO.GetComponent<Parameters>().health+"/"+infoGO.GetComponent<Parameters>().maxHealth+"</color>";
        }else
        if(infoGO.CompareTag("TownHall")){
            data = "<color=#00D000>"+ infoGO.GetComponent<Parameters>().objectName + "</color>";
        }else 
        if (infoGO.CompareTag("Player") || infoGO.CompareTag("Unit") || infoGO.CompareTag("Lumberjack"))
        {
            data = "<color=#00D000>" + infoGO.GetComponent<Parameters>().objectName + "\t\t\t" + infoGO.GetComponent<Parameters>().health + "/" + infoGO.GetComponent<Parameters>().maxHealth + "</color>\n" +
                   "<size=10>Нажмите I, чтобы посмотреть снаряжение</size>";
        }
        else 
            data = "<color=#00D000>"+ infoGO.GetComponent<Parameters>().objectName + "\t"+infoGO.GetComponent<Parameters>().health+"/"+infoGO.GetComponent<Parameters>().maxHealth+"</color>"; 
        tooltip.transform.GetChild(0).GetComponent<Text>().text = data;
    }
}
