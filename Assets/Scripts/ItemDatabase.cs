﻿using UnityEngine;
using System.Collections.Generic;
using LitJson;
using System.IO;

public class ItemDatabase : MonoBehaviour {
    public List<Item> database = new List<Item>();
    private JsonData itemData;
    
    void Start()
    {
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath+ "/StreamingAssets/ItemDB/Items.json"));
        ConstructItemDB();
    }
    
    void ConstructItemDB()
    {
        for(int i = 0; i < itemData.Count; i++)
            database.Add(new Item(
                (int)itemData[i]["id"],
                itemData[i]["name"].ToString(),
                itemData[i]["description"].ToString(),
                (int)itemData[i]["damage"],
                (float)((double)itemData[i]["concentration"]),
                (int)itemData[i]["health"],
                (int)itemData[i]["defence"],
                (float)((double)itemData[i]["movingSpeed"]),
                (int)itemData[i]["itemLevel"],
                (Item.ItemType)((int)itemData[i]["itemType"]),
                (Item.ItemQuality)((int)itemData[i]["itemQuality"]),
                (bool)itemData[i]["stackable"],
                itemData[i]["iconName"].ToString(),
                itemData[i]["modelName"].ToString(),
                (int)itemData[i]["sellCost"],
                (int)itemData[i]["buyCost"],
                itemData[i]["skillFileName"].ToString()));
    }

    public Item FetchItemByID(int id)
    {
        return database.Find(it => it.ID == id);
    }

    public List<Item> FetchItemsForGenerator(Item.ItemQuality quality, Item.ItemType type)
    {
        return database.FindAll(it => it.quality == quality && it.type == type);
    }

    public List<Item> FetchItemsForShop(Item.ItemQuality quality)
    {
        return database.FindAll(it => it.quality == quality && it.type != Item.ItemType.QuestItem);
    }

    public static void SpawnItem(Vector3 where, int itemID)
    {
        Item spawnedItem = GameObject.Find("Inventory").GetComponent<ItemDatabase>().FetchItemByID(itemID);

        GameObject it = Instantiate(spawnedItem.model, @where, Quaternion.identity) as GameObject;
        it.name = spawnedItem.model.name;
        it.tag = "LyingItem";
        it.layer = 8;
        it.AddComponent<Rigidbody>();
        it.AddComponent<DroppedItem>();
        it.SendMessage("SetItem", spawnedItem);
        it.GetComponent<SphereCollider>().enabled = true;
        it.GetComponent<BoxCollider>().enabled = true;
    }
}

[System.Serializable]
public class Item
{ 
    public enum ItemType
    {
        None = -1,
        Gloves = 6, // перчатки
        Armor = 7, // броня
        Boots = 8, // ботинки
        Weapon = 9, // оружие
        SecondWeapon = 10, // второстепенное оружие
        Potion = 11, // зелье
        WorkersToolHammer = 12, // орудие труда (молоток)
        WorkersToolAxe = 13, // орудие труда (топор)
        WorkersToolPickaxe = 14, // орудие труда (кирка)
        WorkersToolScythe = 15, // орудие труда (коса)
        QuestItem = 16
    }

    public enum ItemQuality
    {
        None = -1,
        Broken = 0,
        Damaged = 1,
        Common = 2,
        Good = 3,
        Rare = 4
    }

    public int ID;
    public string Name;
    public string Description;
    public int Damage;
    public float Concentration;
    public int Health;
    public int Defence;
    public float MovingSpeed;
    public int ItemLevel;
    public bool Stackable;
    public string iconName;
    public ItemType type;
    public ItemQuality quality;
    public Sprite sprite;
    public string modelName;
    public Object model;
    public int slotID; 
    public int sellCost;
    public int buyCost;
    public string skillFileName;

    public List<Skill> skills; 

    public Item(int id, string name, string descr, int damage, float con, int hlth, int def, float movSpeed,
        int lvl, ItemType type, ItemQuality quality, bool stackable, string iconName, string modelName, int sellCost, int buyCost, string fileName)
    {
        this.ID = id;
        this.Name = name;
        this.Description = descr;
        this.Damage = damage;
        this.Concentration = con;
        this.Health = hlth;
        this.Defence = def;
        this.MovingSpeed = movSpeed;
        this.ItemLevel = lvl;
        this.type = type;
        this.quality = quality;
        this.Stackable = stackable;
        this.iconName = iconName;
        this.modelName = modelName;
        this.model = Resources.Load<Object>("ItemModels/" + modelName);
        this.sprite = Resources.Load<Sprite>("ItemIcons/" + iconName);
        this.sellCost = sellCost;
        this.buyCost = buyCost;
        this.slotID = -1;
        skillFileName = fileName;
        skills = GameObject.Find("SkillsManager").GetComponent<SkillsLoader>().LoadSkills(fileName);
    }

    public Item()
    {
        this.ID = -1;
        this.iconName = "";
        skills = new List<Skill>();
    }

    public static Item CopyItem(Item refItem)
    {
        return new Item(
                    refItem.ID,
                    refItem.Name,
                    refItem.Description,
                    refItem.Damage,
                    refItem.Concentration,
                    refItem.Health,
                    refItem.Defence,
                    refItem.MovingSpeed,
                    refItem.ItemLevel,
                    refItem.type,
                    refItem.quality,
                    refItem.Stackable,
                    refItem.iconName,
                    refItem.modelName,
                    refItem.sellCost,
                    refItem.buyCost,
                    refItem.skillFileName);
    }
}
