﻿using UnityEngine;
using System.Collections.Generic;

public class EnemyCheckSphere : MonoBehaviour {
    public List<GameObject> enemiesHere = new List<GameObject>();

    void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player") || collider.CompareTag("FriendBuild") || collider.CompareTag("Lumberjack") || collider.CompareTag("Unit"))
        {
            enemiesHere.Add(collider.gameObject);
            transform.parent.gameObject.SendMessage("EnemyDetected", collider.gameObject);
        }
    }

    void OnTriggerExit(Collider collider)
    {
        enemiesHere.Remove(collider.gameObject);
    }

    void OnCollisionEnter(Collision collider)
    {
        if (collider.gameObject.CompareTag("Player") || collider.gameObject.CompareTag("FriendBuild") || collider.gameObject.CompareTag("Lumberjack") || collider.gameObject.CompareTag("Unit"))
        {
            enemiesHere.Add(collider.gameObject);
            transform.parent.gameObject.SendMessage("EnemyDetected", collider.gameObject);
        }
    }

    void Update()
    {
        if (transform.parent.GetComponent<Parameters>().health <= 0)
            GetComponent<Collider>().enabled = false;
        
        if (enemiesHere.Count > 0 && !transform.parent.gameObject.GetComponent<Enemy>().Opponent)
        {
            var enemy = enemiesHere.Find(en => en != null);
            if(enemy)
                transform.parent.gameObject.SendMessage("EnemyDetected", enemy);
        }
    }
}
