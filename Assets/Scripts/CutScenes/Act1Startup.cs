﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions.Comparers;
using UnityEngine.EventSystems;
using UnityStandardAssets.ImageEffects;

public class Act1Startup : MonoBehaviour {
    GameObject tomTalking;
    public static GameObject fadeScreen;
    public static bool animPlaying = false;
    private bool cutsceneOnePlaying = false;
    private string playingCutscene;
    private GameObject hero;
    private bool dialogueClosed = false;
    private GameObject shaman;
    public static bool ring = true;
    private Color orange = new Color(255, 165, 0, 1);
    private GameObject lostWorkers, lostVillageWorkers, swampWorkers;
    private GameObject[] savedWorkers;
    private bool heroIsSleeping = false;
    List<GameObject> whoComeToGG;
    GameObject rocksCollapse;
    int basiliskChoose = 0;
    int numSquadHide = 1;
    public static bool ruinsPlaying;
    GameObject uiCanBeTurnedOff;
    GameObject minimapUI;

    void Start ()
    {
        minimapUI = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/MinimapUI").gameObject;
        uiCanBeTurnedOff = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff").gameObject;
        whoComeToGG = new List<GameObject>();
        rocksCollapse = GameObject.Find("RocksCollapse");
        rocksCollapse.SetActive(false);
        if (!MainMenu.loaded)
        {
            savedWorkers = GameObject.FindGameObjectsWithTag("SavedWorker");
            hero = GameObject.FindGameObjectWithTag("Player");
            fadeScreen = GameObject.Find("FadeScreen");
            fadeScreen.GetComponent<Image>().enabled = false;
            tomTalking = GameObject.Find("TalkingTom");
            GameObject.Find("Squad2").GetComponent<Squad>().ChangeSquadActiveStatus(false);
            StartCoroutine(StartCutscene("1_1"));
            shaman = Instantiate(Resources.Load("Shaman"), Vector3.zero, Quaternion.identity) as GameObject;
            shaman.name = "Shaman";

            lostWorkers = GameObject.Find("LostWorkers");
            for (int i = 0; i < lostWorkers.transform.childCount; i++)
            {
                foreach (Renderer r in lostWorkers.transform.GetChild(i).GetComponentsInChildren<Renderer>())
                    foreach (Material material in r.materials)
                    {
                        material.shader = Shader.Find("Standard");
                        material.SetFloat("_Mode", 3);
                        material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                        material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                        material.SetInt("_ZWrite", 0);
                        material.DisableKeyword("_ALPHATEST_ON");
                        material.DisableKeyword("_ALPHABLEND_ON");
                        material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                        material.renderQueue = 3000;
                        material.SetColor("_Color", new Color(0, 0, 0.5f, 0.2f));
                    }
            }
            lostWorkers.SetActive(false);

            lostVillageWorkers = GameObject.Find("WorkersInLostVillage");
            lostVillageWorkers.SetActive(false);

            swampWorkers = GameObject.Find("WorkersInSwamp");
            swampWorkers.SetActive(false);
        }
    }

    void Update()
    {
        if (cutsceneOnePlaying && Input.GetKeyDown(KeyCode.Space))
        {
            cutsceneOnePlaying = false;
            StopAllCoroutines();
            FadeText.text.Remove("Акт 1.\n");
            GameObject.Find(playingCutscene).GetComponent<QuickCutsceneController>().EndCutscene();
            tomTalking.GetComponent<NavMeshAgent>().Warp(GameObject.Find("NPCPositionAfterCutscene1").transform.position);
            StartCoroutine(AfterCutsceneOne());
        }
    }

    IEnumerator StartCutscene(string num)
    {
        Camera3dPerson.canMoveCam = false;
        uiCanBeTurnedOff.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        GameObject.Find("Squad3").GetComponent<Squad>().GoToFlag(GameObject.Find("Squad3TargetStartup").transform.position, FlagType.Free);
        cutsceneOnePlaying = animPlaying = true;
        GameObject cutscene = GameObject.Find("Cutscene"+num);
        playingCutscene = cutscene.name;
        
        foreach (var lumber in Camera.main.GetComponent<Camera3dPerson>().lumberjacks)
        {
            lumber.transform.LookAt(cutscene.transform);
        }

        fadeScreen.GetComponent<Image>().enabled = true;
        yield return StartCoroutine(FadeText.FadeTxt("Акт 1.\n", Color.red, 3));

        cutscene.GetComponent<QuickCutsceneController>().ActivateCutscene(); // запустили кат-сцену
        yield return StartCoroutine(FadeScreenToWhite(1.5f)); // "открываем глаза"
        
        
        while (cutscene.GetComponent<QuickCutsceneController>().playingCutscene) // ожидаем окончания кат-сцены
        {
            yield return  new WaitForEndOfFrame();
        }

        yield return StartCoroutine(FadeScreenToBlack(1.5f)); // "закрываем глаза"
        yield return StartCoroutine(FadeScreenToWhite(1.5f)); // "открываем глаза"
        yield return StartCoroutine(FadeScreenToBlack(2f)); // "закрываем глаза"
        yield return StartCoroutine(AfterCutsceneOne());
        cutsceneOnePlaying = false;
        Camera3dPerson.canMoveCam = true;
    }

    IEnumerator AfterCutsceneOne()
    {
        Camera.main.GetComponent<BloomOptimized>().enabled = false; // выключаем размытие 
        
        hero.GetComponent<NavMeshAgent>().Warp(GameObject.Find("Cutscene1_1").transform.position);
        hero.GetComponent<ClickToMove>().waitPosition = hero.transform.position;
        hero.transform.LookAt(tomTalking.transform);
        tomTalking.transform.LookAt(hero.transform);
        GameObject posCamera = GameObject.Find("PositionCameraAfterCutscene1");
        Camera.main.transform.position = posCamera.transform.position;
        Camera.main.transform.localEulerAngles = posCamera.transform.localEulerAngles;

        yield return StartCoroutine(FadeScreenToWhite(2f)); // "открываем глаза"

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        
        tomTalking.GetComponent<DialoguesTalker>().OpenDialogue();
        minimapUI.SetActive(false);
        animPlaying = false;
        hero.GetComponent<Builder>().buildingUI.GetComponent<BuildingMenu>().AddBuildToList(new List<string>() { "TownHall" });
        hero.GetComponent<Builder>().buildingUI.GetComponent<BuildingMenu>().RemoveBuildFromList(new List<string> { "Kazarma", "Church", "Shop", "DefenceTower" });
        yield return null;
    }

    void Act1Scene1BuildTH()
    {
        StartCoroutine(CorAct1Scene1BuildTH());
    }

    IEnumerator CorAct1Scene1BuildTH()
    {
        yield return StartCoroutine(hero.GetComponent<BubbleDialogue>().Say(
        "Мысли: \"<i>Что бы ни произошло, необходимо действовать. Действия смогут направить мысли в нужное русло. Строительство требует от меня ресурсов. " +
        "Стоит отправить за мост отряд для разведки местности</i>\""
        , 11.2f, "Dialogue2", 0));
        hero.GetComponent<QuestGiver>().AcceptQuest(8);

    }

    public static IEnumerator FadeScreenToBlack(float speed)
    {
        fadeScreen.GetComponent<Image>().enabled = true;
        fadeScreen.GetComponent<CanvasGroup>().alpha = 0;
        while (fadeScreen.GetComponent<CanvasGroup>().alpha <= 0.99f)
        {
            fadeScreen.GetComponent<CanvasGroup>().alpha += speed * Time.deltaTime;
            yield return null;
        }
        yield return null;
    }

    public static IEnumerator FadeScreenToWhite(float speed)
    {
        fadeScreen.GetComponent<Image>().enabled = true;
        fadeScreen.GetComponent<CanvasGroup>().alpha = 1;
        while (fadeScreen.GetComponent<CanvasGroup>().alpha >= 0.01f)
        {
            fadeScreen.GetComponent<CanvasGroup>().alpha -= speed * Time.deltaTime;
            yield return null;
        }
        fadeScreen.GetComponent<Image>().enabled = false;
        yield return null;
    }

    // вызывается из катсцены
    void NPCGoesAway()
    {
        tomTalking.GetComponent<NavMeshAgent>().SetDestination(GameObject.Find("NPCPositionAfterCutscene1").transform.position);
    }

    void Act1Scene1ExploreComplete()
    {
        StartCoroutine(CorAct1Scene1ExploreComplete());
    }

    IEnumerator CorAct1Scene1ExploreComplete()
    {
        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        var squad = GameObject.Find("Squad2").GetComponent<Squad>();

        squad.ChangeSquadActiveStatus(true);
        whoComeToGG = new List<GameObject>();
        foreach (var unit in squad.units)
        {
            unit.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(unit, hero.transform.position, 8));
            unit.GetComponent<NPC>().SetTarget(hero, true);
            whoComeToGG.Add(unit);
        }
        foreach (var sqd in Squads.squads)
            if (Vector3.Distance(sqd[0].GetComponent<NavMeshAgent>().destination, new Vector3(1093.3f, 556.34f, 867.9f)) < 25)
            {
                StartCoroutine(SquadSleep(sqd[0].GetComponent<FriendUnit>().numSquad));
                break;
            }

        yield return StartCoroutine(FadeScreenToWhite(5));

        hero.GetComponent<DialoguesTalker>().dialID = 3;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;
        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;

        hero.GetComponent<Builder>().buildingUI.GetComponent<BuildingMenu>().AddBuildToList(new List<string> { "Kazarma"});
    }

    IEnumerator SquadSleep(int num)
    {
        numSquadHide = num;
        var squadOne = GameObject.Find("Squad"+num);
        bool squadSleeping = false;
        while (!heroIsSleeping)
        {
            if (!squadSleeping && squadOne.GetComponent<Squad>().isComingBack)
            {
                squadSleeping = true;
                squadOne.GetComponent<Squad>().ChangeSquadActiveStatus(false);
            }
            yield return new WaitForEndOfFrame();
        }
        squadOne.GetComponent<Squad>().ChangeSquadActiveStatus(true);
        yield return null;
    }

    void HeroInRuins()
    {
        ruinsPlaying = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        StartCoroutine(FadeScreenToBlack(4));

        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(new Vector3(388.5f, 556.34f, 1190.4f), 70);
        StartCoroutine(HeroInRuinsCutscene());
    }

    IEnumerator HeroInRuinsCutscene()
    {
        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        uiCanBeTurnedOff.SetActive(false);
        GameObject ruinsCamera = GameObject.Find("RuinsCamera");
        Camera.main.transform.position = ruinsCamera.transform.position;
        Camera.main.transform.rotation = ruinsCamera.transform.rotation;

        GameObject hero = GameObject.FindGameObjectWithTag("Player");
        GameObject entrance = GameObject.Find("RuinEntrance");

        float heroSpeed = hero.GetComponent<NavMeshAgent>().speed;
        hero.GetComponent<NavMeshAgent>().speed = 5;


        hero.GetComponent<ClickToMove>().waitPosition = entrance.transform.position;
        hero.GetComponent<NavMeshAgent>().Warp(GameObject.Find("HeroInRuinsPosition").transform.position);
        hero.GetComponent<NavMeshAgent>().SetDestination(entrance.transform.position);

        StartCoroutine(FadeScreenToWhite(150));

        while (Vector3.Distance(hero.transform.position, entrance.transform.position) > hero.GetComponent<NavMeshAgent>().stoppingDistance)
            yield return new WaitForEndOfFrame();
        
        GameObject nearestWarrior = whoComeToGG[0];

        foreach (var unit in whoComeToGG)
            if (Vector3.Distance(nearestWarrior.transform.position, hero.transform.position) > Vector3.Distance(unit.transform.position, hero.transform.position))
                nearestWarrior = unit;

        var warWait = GameObject.Find("Ruins_WarriorsWaitHere");
        foreach (var unit in whoComeToGG)
            if (unit != nearestWarrior)
            {
                unit.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(unit, warWait.transform.position, 9));
                unit.GetComponent<NPC>().SetTarget(warWait, true);
            }

        nearestWarrior.GetComponent<NavMeshAgent>().Warp(GameObject.Find("RuinsWarriorNearHero").transform.position);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        ruinsPlaying = false;

        hero.GetComponent<DialoguesTalker>().dialID = 6;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;
        hero.GetComponent<DialoguesTalker>().OpenDialogue();
        minimapUI.SetActive(true);
        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera3dPerson.canMoveCam = true;

        GameObject.Find("Ruins").transform.FindChild("HeroExitRuins").gameObject.SetActive(true);

        hero.GetComponent<NavMeshAgent>().speed = heroSpeed;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;
    }

    public void HeroExitRuins()
    {
        foreach (var unit in whoComeToGG)
            unit.GetComponent<NPC>().SetTarget(hero, true);
    }

    void TalkWithSquadAboutVillage()
    {
        hero.GetComponent<QuestGiver>().AcceptQuest(12);
    }

    void Act1Scene3CameBackToVillage()
    {
        StartCoroutine(CorAct1Scene3CameBackToVillage());
    }

    IEnumerator CorAct1Scene3CameBackToVillage()
    {
        foreach (var unit in whoComeToGG)
            unit.GetComponent<NPC>().SetTarget(null, false);
        whoComeToGG = new List<GameObject>();

        var kazarma = hero.GetComponent<Builder>().listBuild.Find(bld => bld.name == "Kazarma");
        if (kazarma)
        {
            if (kazarma.GetComponent<Construction>().builded)
            {
                TalkWithSquadAboutVillage();
                Act1Scene3KazarmaBuilded();
            }
            else
                Act1Scene3KazarmaPlaced();
            yield break;
        }

        uiCanBeTurnedOff.SetActive(false);

        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;

        yield return StartCoroutine(FadeScreenToBlack(5));

        GameObject tom = GameObject.Find("TalkingTom");

        Vector3 nearPointToHero = FindNearestFreePos(tom, hero.transform.position, 8);

        if (nearPointToHero != Vector3.zero)
            tom.GetComponent<NavMeshAgent>().Warp(nearPointToHero);

        hero.GetComponent<ClickToMove>().waitPosition = hero.GetComponent<NavMeshAgent>().destination = hero.transform.position;

        tom.transform.LookAt(hero.transform.position);
        hero.transform.LookAt(tom.transform.position);

        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        yield return StartCoroutine(FadeScreenToWhite(5));

        tom.GetComponent<DialoguesTalker>().dialID = 8;
        tom.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        tom.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;
    }

    public static Vector3 FindNearestFreePos(GameObject who, Vector3 center, float targetDistance)
    {
        var hero = GameObject.FindGameObjectWithTag("Player");
        Vector3 randPoint = Random.onUnitSphere * targetDistance + center;
        randPoint.y = hero.transform.position.y;
        for (int i = 0; i < 10000; i++)
        {
            NavMeshPath path = new NavMeshPath();
            who.GetComponent<NavMeshAgent>().CalculatePath(randPoint, path);
            if (path.status == NavMeshPathStatus.PathInvalid || path.status == NavMeshPathStatus.PathPartial)
            {
                randPoint = Random.onUnitSphere * targetDistance + center;
                randPoint.y = hero.transform.position.y;
                continue;
            }
            else
                return randPoint;
        }
        return center;
    }

    void Act1Scene2Fishers(GameObject[] fishers)
    {
        StartCoroutine(CorAct1Scene2Fishers(fishers));
    }

    IEnumerator CorAct1Scene2Fishers(GameObject[] fishers)
    {
        Camera3dPerson.canMoveCam = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        foreach (var unit in whoComeToGG)
        {
            unit.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(unit, hero.transform.position, 5));
            unit.transform.LookAt(fishers[Random.Range(0, fishers.Length)].transform);
        }

        yield return StartCoroutine(FadeScreenToWhite(4));

        yield return StartCoroutine(fishers[0].GetComponent<BubbleDialogue>().Say("Не ходи туда, добрый человек, места те страшные, темные! Злые дела творятся в тех руинах, ох, злые дела! Страх и страдание найдешь ты там, добрый человек!", 13f, "Dialogue4", 0));
        yield return StartCoroutine(whoComeToGG[0].GetComponent<BubbleDialogue>().Say("Закройте рты! Ненавижу, когда всякая чернь открывает рот и сеет смуту!", 5.2f, "Dialogue4", 1));

        GameObject.Find("TriggerForCutscene2").SendMessage("DialogueEnd");

        StartCoroutine(hero.GetComponent<BubbleDialogue>().Say("Мысли: \"<i>Откуда они узнали, что мы направляемся в руины? Какая-то чертовщина!</i>\"", 5.5f, "Dialogue4an", 0));
        Camera3dPerson.canMoveCam = true;
        uiCanBeTurnedOff.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    void Act1Scene3KazarmaPlaced()
    {
        StartCoroutine(CorAct1Scene3KazarmaPlaced());
    }

    IEnumerator CorAct1Scene3KazarmaPlaced()
    {
        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        var squad = Camera3dPerson.squadsGO.Find(sqd => sqd.GetComponent<Squad>().isSquadReady);
        GameObject warrior = squad.GetComponent<Squad>().units[0];

        warrior.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(warrior, hero.transform.position, 7));

        warrior.transform.LookAt(hero.transform.position);
        hero.transform.LookAt(warrior.transform.position);
        hero.GetComponent<ClickToMove>().waitPosition = hero.GetComponent<NavMeshAgent>().destination = hero.transform.position;

        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        yield return StartCoroutine(FadeScreenToWhite(5));

        hero.GetComponent<DialoguesTalker>().dialID = 7;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        warrior.GetComponent<NavMeshAgent>().destination = warrior.GetComponent<FriendUnit>().cameFrom;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;
    }

    void Act1Scene3KazarmaBuilded()
    {
        StartCoroutine(CorAct1Scene3KazarmaBuilded());
    }

    IEnumerator CorAct1Scene3KazarmaBuilded()
    {
        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        GameObject lumber = Camera.main.GetComponent<Camera3dPerson>().lumberjacks.Find(lumb => !lumb.GetComponent<Lumberjack>().working);

        lumber.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(lumber, hero.transform.position, 7));

        hero.GetComponent<Builder>().buildingUI.GetComponent<BuildingMenu>().AddBuildToList(new List<string> { "Church", "Shop", "DefenceTower" });

        lumber.transform.LookAt(hero.transform.position);
        hero.transform.LookAt(lumber.transform.position);
        hero.GetComponent<ClickToMove>().waitPosition = hero.GetComponent<NavMeshAgent>().destination = hero.transform.position;

        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        yield return StartCoroutine(FadeScreenToWhite(5));

        hero.GetComponent<DialoguesTalker>().dialID = 9;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;
    }

    void Act1Scene3SquadCreated()
    {
        StartCoroutine(CorAct1Scene3SquadCreated());
    }

    IEnumerator CorAct1Scene3SquadCreated()
    {
        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        foreach (var unit in GameObject.Find("Squad"+ Squads.squads.Count).GetComponent<Squad>().units)
        {
            whoComeToGG.Add(unit);
            unit.GetComponent<NPC>().SetTarget(hero, true);
            unit.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(unit, hero.transform.position, 9));
        }

        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        yield return StartCoroutine(FadeScreenToWhite(5));

        hero.GetComponent<DialoguesTalker>().dialID = 10;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;
    }

    void Act1Scene3HeroInVillage()
    {
        StartCoroutine(CorAct1Scene3HeroInVillage());
    }

    IEnumerator CorAct1Scene3HeroInVillage()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Camera3dPerson.canMoveCam = false;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        hero.GetComponent<ClickToMove>().waitPosition = hero.GetComponent<NavMeshAgent>().destination = hero.transform.position;

        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(GameObject.Find("VillageCenter").transform.position, 150);

        var vilResidents = GameObject.FindGameObjectsWithTag("VillageResident");
        GameObject nearestResident = vilResidents[0];
        foreach (var res in vilResidents)
            if (Vector3.Distance(hero.transform.position, res.transform.position) < Vector3.Distance(hero.transform.position, nearestResident.transform.position))
                nearestResident = res;
        nearestResident.GetComponent<NavMeshAgent>().speed = 10;

        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        Vector3 shamanPos = hero.transform.position + new Vector3(0,0,-20);
        shaman.GetComponent<NavMeshAgent>().Warp(shamanPos);
        shaman.GetComponent<NavMeshAgent>().destination = hero.transform.position;

        yield return StartCoroutine(FadeScreenToWhite(5));

        shaman.GetComponent<Animator>().Play("RM_Spear_run");
        while (Vector3.Distance(shaman.transform.position, hero.transform.position) > shaman.GetComponent<NavMeshAgent>().stoppingDistance)
            yield return new WaitForEndOfFrame();

        shaman.transform.LookAt(hero.transform);
        hero.transform.LookAt(shaman.transform);
        shaman.GetComponent<Animator>().Play("Spear_thrust_mid");
        yield return new WaitForSeconds(shaman.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length);
        shaman.GetComponent<Animator>().Play("IDLE");
        yield return StartCoroutine(shaman.GetComponent<BubbleDialogue>().Say("Карл, как бодр ты в этот солнечный день! Не так страшна моя стрела, как та на поле боя?", 7.3f, "Booble_Karl_Kak_bodr_ti", 0));
        shaman.GetComponent<Animator>().CrossFade("RM_Spear_run", 0.005f);
        StartCoroutine(ShamanHider());

        NavMeshAgent residentAgent = nearestResident.GetComponent<NavMeshAgent>();
        float stoppDistResident = residentAgent.stoppingDistance;
        Vector3 residentPos = residentAgent.transform.position;
        float residentSpeed = residentAgent.speed;
        residentAgent.destination = hero.transform.position;
        residentAgent.stoppingDistance = 5;
        while (Vector3.Distance(nearestResident.transform.position, hero.transform.position) > residentAgent.stoppingDistance)
            yield return new WaitForEndOfFrame();

        hero.GetComponent<DialoguesTalker>().dialID = 11;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();

        dialogueClosed = false;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;

        residentAgent.stoppingDistance = stoppDistResident;
        residentAgent.destination = residentPos;
        residentAgent.speed = residentSpeed;

        GameObject edelvulf = GameObject.Find("Edelvulf");
        edelvulf.GetComponent<DialoguesTalker>().dialID = 12;
        edelvulf.GetComponent<DialoguesTalker>().ReloadDialogue();

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Camera3dPerson.canMoveCam = true;
    }

    IEnumerator ShamanHider()
    {
        shaman.GetComponent<Animator>().Play("RM_Spear_run");
        shaman.GetComponent<NavMeshAgent>().destination = new Vector3(185.9415f, 556.3422f, 529.8742f);
        yield return new WaitForSeconds(20);
        shaman.GetComponent<NavMeshAgent>().Warp(Vector3.zero);
        yield return null;
    }

    void Act1Scene3FindedScoutsInForest()
    {
        StartCoroutine(CorAct1Scene3FindedScoutsInForest());
    }

    IEnumerator CorAct1Scene3FindedScoutsInForest()
    {
        GameObject scoutsRoot = GameObject.Find("Triggers").transform.FindChild("EdelvulfScoutsInForest").gameObject;
        scoutsRoot.SetActive(true);

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(scoutsRoot.transform.FindChild("Campfire").position, 25);

        Vector3 heroPos = scoutsRoot.transform.FindChild("HeroHere").position;

        hero.GetComponent<NavMeshAgent>().Warp(heroPos);
        hero.GetComponent<NavMeshAgent>().SetDestination(heroPos);
        hero.GetComponent<ClickToMove>().waitPosition = heroPos;
        hero.transform.LookAt(scoutsRoot.transform.FindChild("Campfire"));

        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        yield return StartCoroutine(FadeScreenToWhite(5));
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        hero.GetComponent<DialoguesTalker>().dialID = 13;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();

        dialogueClosed = false;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;

        Destroy(scoutsRoot.transform.FindChild("Campfire").gameObject);

        GameObject village = GameObject.Find("Village");

        int warNum = 1;
        foreach(var unit in scoutsRoot.GetComponentsInChildren<NavMeshAgent>())
        {
            unit.destination = village.transform.FindChild("VillageWarPos" + warNum).position;
            unit.speed = 12;
            warNum++;
        }
    }

    void DialogueClosed()
    {
        dialogueClosed = true;
    }

    void Act1Scene3PrivalInForest()
    {
        StartCoroutine(CorAct1Scene3PrivalInForest());
    }

    IEnumerator CorAct1Scene3PrivalInForest()
    {
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        
        hero.GetComponent<ClickToMove>().waitPosition = hero.GetComponent<NavMeshAgent>().destination = hero.transform.position;

        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        hero.transform.LookAt(whoComeToGG[0].transform);
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        yield return StartCoroutine(FadeScreenToWhite(5));

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        hero.GetComponent<DialoguesTalker>().dialID = 14;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;

        foreach (var unit in whoComeToGG)
            unit.GetComponent<NPC>().SetTarget(unit, true);

        StartCoroutine(RingRing());

        float curDist = Vector3.Distance(hero.transform.position, new Vector3(365, 556.34f, 800));
        float multiplier = orange.g/curDist;
        float orangeG = orange.g;

        while (ring)
        {
            if (Vector3.Distance(hero.transform.position, new Vector3(365, 556.34f, 800)) > curDist)
                orange.g = orangeG;
            else
            {
                orange = new Color(orange.r , 
                                    255 / ((curDist -
                                            Mathf.FloorToInt(curDist -
                                            Vector3.Distance(hero.transform.position, new Vector3(365, 556.34f, 800)))) *
                                            multiplier), 
                                    orange.b);
            }

            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator RingRing()
    {
        GameObject ringUI = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/MinimapUI/Minimap_Border").gameObject;
        Image ringImage = ringUI.GetComponent<Image>();

        Color white = Color.white;

        ringUI.GetComponent<Image>().color = white;

        bool whiteCol = true;
        while (ring)
        {
            if(whiteCol)
                ringImage.color = Color.Lerp(ringImage.color, orange, Time.deltaTime * 2);
            else
                ringImage.color = Color.Lerp(ringImage.color, white, Time.deltaTime * 2);

            if (whiteCol && FloatComparer.AreEqual(ringImage.color.r, orange.r, 1) && FloatComparer.AreEqual(ringImage.color.g, orange.g, 1) &&
                FloatComparer.AreEqual(ringImage.color.b, orange.b, 1) && FloatComparer.AreEqual(ringImage.color.a, orange.a, 1))
                whiteCol = false;
            else
            if (!whiteCol && FloatComparer.AreEqual(ringImage.color.r, white.r, 1) && FloatComparer.AreEqual(ringImage.color.g, white.g, 1) &&
                FloatComparer.AreEqual(ringImage.color.b, white.b, 1) && FloatComparer.AreEqual(ringImage.color.a, white.a, 1))
                whiteCol = true;

            // разобрались с цветом, теперь разберёмся с "вибрацией"

            var randX = Random.Range(-1f, 1f);
            randX = (FloatComparer.AreEqual(0, randX, 0.1f)) ? 1 : randX;

            var randY = Random.Range(-1f, 1f);
            randY = (FloatComparer.AreEqual(0, randY, 0.1f)) ? 1 : randY;

            ringUI.transform.position = new Vector3(120f + Mathf.PingPong(Time.time, 3)* randX, 120f + Mathf.PingPong(Time.time, 3)*randY, 0);
            yield return new WaitForEndOfFrame();
        }
        ringUI.transform.position = new Vector3(120, 120, 0);
    }

    void Act1Scene3FoundWorkers()
    {
        StartCoroutine(CorAct1Scene3FoundWorkers());
    }

    IEnumerator CorAct1Scene3FoundWorkers()
    {
        ring = false;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        lostWorkers.SetActive(true);
        for (int i = 0; i < lostWorkers.transform.childCount; i++)
        {
            lostWorkers.transform.GetChild(i).transform.LookAt(hero.transform);
        }

        hero.GetComponent<DialoguesTalker>().dialID = 15;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        yield return StartCoroutine(FadeScreenToWhite(5));

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;

        shaman.GetComponent<NavMeshAgent>().Warp(GameObject.Find("ShamanHidesHere").transform.position);
        shaman.GetComponent<NavMeshAgent>().destination = transform.position;
        shaman.GetComponent<DialoguesTalker>().dialID = 17;
        shaman.GetComponent<DialoguesTalker>().ReloadDialogue();
        shaman.GetComponent<Animator>().Play("IDLE");

        yield return null;
    }

    void Act1Scene3ShamanFinded()
    {
        StartCoroutine(CorAct1Scene3ShamanFinded());
    }

    IEnumerator CorAct1Scene3ShamanFinded()
    {
        StartCoroutine(ShamanHider());

        yield return null;
    }

    void Act1Scene3DisenchantWorkers()
    {
        StartCoroutine(CorAct1Scene3DisenchantWorkers());
    }

    IEnumerator CorAct1Scene3DisenchantWorkers()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        hero.GetComponent<ClickToMove>().waitPosition =
            hero.GetComponent<NavMeshAgent>().destination = hero.transform.position;

        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        hero.GetComponent<DialoguesTalker>().dialID = 18;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        yield return StartCoroutine(FadeScreenToWhite(5));

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;
        Camera3dPerson.canMoveCam = false;
        uiCanBeTurnedOff.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        for (int i = 0; i < lostWorkers.transform.childCount; i++)
        {
            foreach (var renderer in lostWorkers.transform.GetChild(i).GetComponentsInChildren<Renderer>())
            {
                foreach (var mat in renderer.materials)
                {
                    mat.shader = Shader.Find("Legacy Shaders/Diffuse");
                    mat.SetColor("_Color", Color.white);
                }
            }
            lostWorkers.transform.GetChild(i).gameObject.GetComponent<NavMeshAgent>().speed = 12;
            yield return new WaitForSeconds(1.5f);
        }

        hero.GetComponent<DialoguesTalker>().dialID = 19;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;
        Camera3dPerson.canMoveCam = true;
        GameObject villageCenter = GameObject.Find("VillageCenter");

        for (int i = 0; i < lostWorkers.transform.childCount; i++)
        {
            Vector2 randPoint = Random.insideUnitCircle * 15;
            lostWorkers.transform.GetChild(i).GetComponent<NavMeshAgent>().destination =
                new Vector3(villageCenter.transform.position.x + randPoint.x, villageCenter.transform.position.y,
                    villageCenter.transform.position.z + randPoint.y);
        }

        foreach (var unit in whoComeToGG)
            unit.GetComponent<NPC>().SetTarget(hero, true);
    }

    void Act1Scene3CameToAbandonedVillage()
    {
        StartCoroutine(CorAct1Scene3CameToAbandonedVillage());
    }

    IEnumerator CorAct1Scene3CameToAbandonedVillage()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        hero.GetComponent<ClickToMove>().waitPosition =
            hero.GetComponent<NavMeshAgent>().destination = hero.transform.position;

        lostVillageWorkers.SetActive(true);

        for (int i = 0; i < lostVillageWorkers.transform.childCount; i++)
            lostVillageWorkers.transform.GetChild(i).LookAt(hero.transform);

        hero.transform.LookAt(lostVillageWorkers.transform.GetChild(0));

        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        hero.GetComponent<DialoguesTalker>().dialID = 20;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        GameObject abandonedHouse = GameObject.Find("AbandonedHouse");
        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(abandonedHouse, 40);

        yield return StartCoroutine(FadeScreenToWhite(5));

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;

        ItemDatabase.SpawnItem(GameObject.Find("PlaceAxesHere").transform.position, 22);
    }

    public void WarriorsWaitOutsideHouse()
    {
        GameObject waitHere = GameObject.Find("WarriorsWaitOutside");
        foreach (var unit in whoComeToGG)
            unit.GetComponent<NPC>().SetTarget(waitHere, true);
    }

    public void HeroExitAbandonedHouse()
    {
        foreach (var unit in whoComeToGG)
            unit.GetComponent<NPC>().SetTarget(hero, true);
    }

    void Act1Scene3ReturnAxe()
    {
        StartCoroutine(CorAct1Scene3ReturnAxe());
    }

    IEnumerator CorAct1Scene3ReturnAxe()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        hero.GetComponent<ClickToMove>().waitPosition =
            hero.GetComponent<NavMeshAgent>().destination = hero.transform.position;

        for (int i = 0; i < lostVillageWorkers.transform.childCount; i++)
            lostVillageWorkers.transform.GetChild(i).LookAt(hero.transform);

        hero.transform.LookAt(lostVillageWorkers.transform.GetChild(0));

        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        hero.GetComponent<DialoguesTalker>().dialID = 21;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        yield return StartCoroutine(FadeScreenToWhite(5));

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;

        GameObject villageCenter = GameObject.Find("VillageCenter");

        for (int i = 0; i < lostVillageWorkers.transform.childCount; i++)
        {
            Vector2 randPoint = Random.insideUnitCircle * 15;
            lostVillageWorkers.transform.GetChild(i).GetComponent<NavMeshAgent>().destination =
                new Vector3(villageCenter.transform.position.x + randPoint.x, villageCenter.transform.position.y,
                    villageCenter.transform.position.z + randPoint.y);
        }
    }

    void Act1Scene3WorkersInSwamp()
    {
        StartCoroutine(CorAct1Scene3WorkersInSwamp());
    }

    IEnumerator CorAct1Scene3WorkersInSwamp()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Camera3dPerson.canMoveCam = false;

        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        hero.GetComponent<ClickToMove>().waitPosition =
            hero.GetComponent<NavMeshAgent>().destination = hero.transform.position;

        swampWorkers.SetActive(true);

        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        GameObject cameraHere = GameObject.Find("SwampCamera");
        Camera.main.transform.position = cameraHere.transform.position;
        Camera.main.transform.rotation = cameraHere.transform.rotation;

        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(swampWorkers.transform.GetChild(0).gameObject, 40);

        yield return StartCoroutine(FadeScreenToWhite(5));

        yield return StartCoroutine(swampWorkers.transform.GetChild(0).gameObject.GetComponent<BubbleDialogue>().Say("Помогите! Помогите! Помогите!", 2.3f, "Booble_Pomogite", 0));
        yield return StartCoroutine(swampWorkers.transform.GetChild(1).gameObject.GetComponent<BubbleDialogue>().Say("Не кричи! Только силы зря тратишь, мы в глухом лесу и никто нас не слышит!", 4.9f, "Booble_Pomogite", 1));
        yield return StartCoroutine(swampWorkers.transform.GetChild(2).gameObject.GetComponent<BubbleDialogue>().Say("Будь у нас веревка, мы могли бы спастись.", 2.5f, "", 0));

        yield return StartCoroutine(FadeScreenToBlack(5));

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;

        yield return StartCoroutine(FadeScreenToWhite(5));

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Camera3dPerson.canMoveCam = true;
        uiCanBeTurnedOff.SetActive(true);
        hero.GetComponent<QuestGiver>().AcceptQuest(24);
        ItemDatabase.SpawnItem(GameObject.Find("PlaceAxesHere").transform.position, 21);
        yield return null;
    }

    void Act1Scene3SaveWorkersFromSwamp()
    {
        StartCoroutine(CorAct1Scene3SaveWorkersFromSwamp());
    }

    IEnumerator CorAct1Scene3SaveWorkersFromSwamp()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Camera3dPerson.canMoveCam = false;

        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        hero.GetComponent<ClickToMove>().waitPosition =
            hero.GetComponent<NavMeshAgent>().destination = hero.transform.position;

        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        for (int i = 0; i < swampWorkers.transform.childCount; i++)
        {
            swampWorkers.transform.GetChild(i).gameObject.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(swampWorkers.transform.GetChild(i).gameObject, hero.transform.position, 10));
            swampWorkers.transform.GetChild(i).gameObject.GetComponent<NPC>().SetTarget(hero, true);
        }

        yield return StartCoroutine(FadeScreenToWhite(5));

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        hero.GetComponent<DialoguesTalker>().dialID = 22;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();

        dialogueClosed = false;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;

        GameObject edelvulf = GameObject.Find("Edelvulf");
        edelvulf.GetComponent<DialoguesTalker>().dialID = 23;
        edelvulf.GetComponent<DialoguesTalker>().ReloadDialogue();
    }

    void Act1Scene3TalkWithEdelvulf()
    {
        foreach (var worker in savedWorkers)
        {
            if (!worker.GetComponent<NPC>())
                worker.AddComponent<NPC>();
            worker.GetComponent<NPC>().SetTarget(hero, true);
        }
    }

    void Act1Scene3Ending()
    {
        StartCoroutine(CorAct1Scene3Ending());
    }

    IEnumerator mayWorkersBeOurs()
    {
        foreach (var worker in savedWorkers)
        {
            if(worker.GetComponent<LostWorkers>())
                Destroy(worker.GetComponent<LostWorkers>());

            if(!worker.GetComponent<Parameters>())
                worker.AddComponent<Parameters>();
            worker.GetComponent<Parameters>().name = "Рабочий";
            worker.GetComponent<Parameters>().health = worker.GetComponent<Parameters>().maxHealth = 300;

            worker.transform.FindChild("TextAbove").GetComponent<TextMesh>().text = "Рабочий";
            if (!worker.GetComponent<IDGetSet>())
                worker.AddComponent<IDGetSet>();
            worker.GetComponent<IDGetSet>().creatureID = 2;

            if (!worker.GetComponent<InventoryUnit>())
                worker.AddComponent<InventoryUnit>();

            if (!worker.GetComponent<RealFoWUnit>())
                worker.AddComponent<RealFoWUnit>();

            if (!worker.GetComponent<Lumberjack>())
                worker.AddComponent<Lumberjack>();

            if(worker.GetComponent<NPC>())
                Destroy(worker.GetComponent<NPC>());

            worker.GetComponent<Lumberjack>().agent = worker.GetComponent<NavMeshAgent>();
            worker.GetComponent<Lumberjack>().SetTownhall(Camera3dPerson.townhall);
            worker.GetComponent<Lumberjack>().hand = worker.transform.Find("WeaponSlot");
            worker.GetComponent<Lumberjack>().run = worker.GetComponent<Animation>()["Walk"].clip;
            worker.GetComponent<Lumberjack>().stop = worker.GetComponent<Animation>()["Idle"].clip;
            worker.GetComponent<Lumberjack>().lumbering = worker.GetComponent<Animation>()["Lumbering"].clip;
            worker.tag = "Lumberjack";
            worker.GetComponent<Lumberjack>().comingBack = true;

            worker.GetComponent<Parameters>().speed = worker.GetComponent<NavMeshAgent>().speed = 10;
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator CorAct1Scene3Ending()
    {
        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Camera3dPerson.canMoveCam = false;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        StartCoroutine(mayWorkersBeOurs());

        foreach (var worker in savedWorkers)
            worker.GetComponent<NPC>().SetTarget(null, false);

        var squadOne = GameObject.Find("Squad"+ numSquadHide);
        squadOne.GetComponent<Squad>().ChangeSquadActiveStatus(true);
        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(GameObject.FindGameObjectWithTag("Mine"), 35);
        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(GameObject.FindGameObjectWithTag("Quarry"), 35);
        float[] squadSpeed = new float[squadOne.GetComponent<Squad>().units.Count];
        for (int i = 0; i < squadOne.GetComponent<Squad>().units.Count; i++)
        {
            squadSpeed[i] = squadOne.GetComponent<Squad>().units[i].GetComponent<NavMeshAgent>().speed;
            squadOne.GetComponent<Squad>().units[i].GetComponent<NavMeshAgent>().speed = 40;
            squadOne.GetComponent<Squad>().SetDestinationForSquad(squadOne.GetComponent<Squad>().units[0].GetComponent<FriendUnit>().cameFrom);
        }
        
        heroIsSleeping = true;
        GameObject cutsceneOne = GameObject.Find("Scene3LastCutscene_1"), cutsceneTwo = GameObject.Find("Scene3LastCutscene_2");
        cutsceneOne.transform.position = cutsceneTwo.transform.position = hero.transform.position + new Vector3(0,15,0);
        GameObject sky = GameObject.Find("UsualSkyDome");
        float skySpeed = sky.GetComponent<TOD_Time>().DayLengthInMinutes;
        sky.GetComponent<TOD_Time>().DayLengthInMinutes = 0.15f;
        
        cutsceneOne.GetComponent<QuickCutsceneController>().ActivateCutscene(); // запустили кат-сцену
        yield return StartCoroutine(FadeScreenToWhite(5));
        if (sky.GetComponent<TOD_Sky>().Cycle.Hour >= 0 && sky.GetComponent<TOD_Sky>().Cycle.Hour < 7)
            while (sky.GetComponent<TOD_Sky>().Cycle.Hour < 7)
                yield return new WaitForEndOfFrame();
        else
        {
            var day = sky.GetComponent<TOD_Sky>().Cycle.Day;
            while (sky.GetComponent<TOD_Sky>().Cycle.Day != day + 1 || sky.GetComponent<TOD_Sky>().Cycle.Hour < 7)
                yield return new WaitForEndOfFrame();
        }

        cutsceneOne.GetComponent<QuickCutsceneController>().EndCutscene();

        sky.GetComponent<TOD_Time>().DayLengthInMinutes = skySpeed;

        cutsceneTwo.GetComponent<QuickCutsceneController>().ActivateCutscene();

        while(cutsceneTwo.GetComponent<QuickCutsceneController>().playingCutscene)
            yield return new WaitForEndOfFrame();

        for (int i = 0; i < squadOne.GetComponent<Squad>().units.Count; i++)
            squadOne.GetComponent<Squad>().units[i].GetComponent<NavMeshAgent>().speed = squadSpeed[i];


        yield return StartCoroutine(FadeScreenToBlack(5));


        foreach (var war in GameObject.FindGameObjectsWithTag("EdelvulfWarrior"))
            Destroy(war);

        GameObject lumber = Camera.main.GetComponent<Camera3dPerson>().lumberjacks.Find(lumb => !lumb.GetComponent<Lumberjack>().working);

        lumber.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(lumber, hero.transform.position, 7));

        lumber.transform.LookAt(hero.transform.position);
        hero.transform.LookAt(lumber.transform.position);

        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        yield return StartCoroutine(FadeScreenToWhite(5));

        hero.GetComponent<DialoguesTalker>().dialID = 24;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        uiCanBeTurnedOff.SetActive(false);

        yield return StartCoroutine(FadeScreenToBlack(5));
        whoComeToGG = new List<GameObject>();
        foreach (var unit in GameObject.Find("Squad" + Squads.squads.Count).GetComponent<Squad>().units)
        {
            whoComeToGG.Add(unit);
            unit.GetComponent<NPC>().SetTarget(hero, true);
            unit.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(unit, hero.transform.position, 9));
        }

        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;

        yield return StartCoroutine(FadeScreenToWhite(5));
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Camera3dPerson.canMoveCam = true;

        GameObject edelvulf = GameObject.Find("Edelvulf");
        edelvulf.GetComponent<DialoguesTalker>().dialID = 25;
        edelvulf.GetComponent<DialoguesTalker>().ReloadDialogue();
        uiCanBeTurnedOff.SetActive(true);
    }

    void Act1Scene4AfterTalkWithEdelvulf()
    {
        foreach (var unit in whoComeToGG)
        {
            unit.GetComponent<NPC>().SetTarget(null, false);
        }
        whoComeToGG[0].GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().GoToFlag(new Vector3(327, 556.34f, 700), FlagType.Free); // отряд идёт в деревню
        var center = FindNearestFreePos(whoComeToGG[0], GameObject.Find("HeroVillageCenter").transform.position, 15);
        foreach (var unit in whoComeToGG)
            unit.GetComponent<FriendUnit>().cameFrom = FindNearestFreePos(unit, center + Random.insideUnitSphere*5, 5);
        whoComeToGG = new List<GameObject>();
    }

    void Act1Scene4HeroCameBackToVillage()
    {
        StartCoroutine(CorAct1Scene4HeroCameBackToVillage());
    }

    IEnumerator CorAct1Scene4HeroCameBackToVillage()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        var freeSquad =
            Squads.squads.Find(sqd => sqd[0].GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().isSquadReady);

        foreach (var unit in freeSquad)
        {
            whoComeToGG.Add(unit);
            unit.GetComponent<NPC>().SetTarget(hero, true);
            unit.GetComponent<NPC>().distance = 15;
            unit.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(unit, hero.transform.position, 9));
        }

        hero.GetComponent<ClickToMove>().waitPosition = hero.GetComponent<NavMeshAgent>().destination = hero.transform.position;
        hero.transform.LookAt(whoComeToGG[0].transform);
        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        hero.GetComponent<DialoguesTalker>().dialID = 26;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;

        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        yield return StartCoroutine(FadeScreenToWhite(5));

        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;
        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;
        DungeoTrigger.canEnter = true;
    }

    void Act1Scene4NearCave()
    {
        hero.GetComponent<QuestGiver>().AcceptQuest(30);
        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(hero.transform.position, 100);
    }

    void CaveRocksFailing()
    {
        StartCoroutine(CorCaveRocksFailing());
    }

    IEnumerator CorCaveRocksFailing()
    {
        rocksCollapse.SetActive(true);
        var warpHeroHere = GameObject.Find("CaveWarpHeroHere");
        var warStopHere = GameObject.Find("WarriorsStopHere");
        foreach (var unit in whoComeToGG)
            unit.GetComponent<NPC>().SetTarget(warStopHere, true);
        yield return StartCoroutine(FadeScreenToBlack(6));
        hero.GetComponent<NavMeshAgent>().Warp(warpHeroHere.transform.position);
        hero.GetComponent<ClickToMove>().waitPosition = hero.transform.position;
        yield return StartCoroutine(FadeScreenToWhite(6));
    }

    void CaveDialogueWithBasilisk()
    {
        StartCoroutine(CorCaveDialogueWithBasilisk());
    }

    IEnumerator CorCaveDialogueWithBasilisk()
    {
        Camera3dPerson.isometric = false;

        var basilisk = GameObject.Find("Basilisk");

        Time.timeScale = 0;
        
        while (Vector3.Angle(Camera.main.transform.forward, (basilisk.transform.position - Camera.main.transform.position).normalized) >= 5)
        {
            Camera.main.transform.position += (basilisk.transform.position - hero.transform.position).normalized;
            yield return null;
        }

        hero.GetComponent<DialoguesTalker>().dialID = 27;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;
        hero.GetComponent<DialoguesTalker>().OpenDialogue();
        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        Time.timeScale = 0;

        while (Vector3.Angle(Camera.main.transform.forward, (hero.transform.position - Camera.main.transform.position).normalized) >= 5)
        {
            Camera.main.transform.position += (hero.transform.position - basilisk.transform.position).normalized;
            yield return null;
        }

        Time.timeScale = 1;

        Camera3dPerson.isometric = true;
    }

    void BasiliskDialogueChooseOne()
    {
        basiliskChoose = 0;
    }
    void BasiliskDialogueChooseTwo()
    {
        basiliskChoose = 1;
    }
    void BasiliskDialogueChooseThree()
    {
        basiliskChoose = 2;
    }
    void BasiliskDialogueChooseFour()
    {
        basiliskChoose = 3;
    }
    void BasiliskDialogueChooseFive()
    {
        basiliskChoose = 4;
    }

    void Act1Scene4KilledBasilisk()
    {
        StartCoroutine(CorAct1Scene4KilledBasilisk());
        Destroy(rocksCollapse);
        foreach (var unit in whoComeToGG)
            unit.GetComponent<NPC>().SetTarget(hero, true);
    }

    IEnumerator CorAct1Scene4KilledBasilisk()
    {
        hero.GetComponent<DialoguesTalker>().dialID = 28 + basiliskChoose;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();

        dialogueClosed = false;
        hero.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;
        
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        Destroy(GameObject.Find("Basilisk"));
        foreach (var unit in whoComeToGG)
        {
            unit.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(unit, hero.transform.position, 8));
            unit.GetComponent<NPC>().distance = 10;
        }

        hero.GetComponent<DialoguesTalker>().dialID = 33;
        hero.GetComponent<DialoguesTalker>().ReloadDialogue();

        yield return StartCoroutine(FadeScreenToWhite(5));
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        hero.GetComponent<DialoguesTalker>().OpenDialogue();
    }

    void Act1Scene4ReturnToVillageAfterBasilisk()
    {
        StartCoroutine(CorAct1Scene4ReturnToVillageAfterBasilisk());
    }

    IEnumerator CorAct1Scene4ReturnToVillageAfterBasilisk()
    {
        var villageCenter = GameObject.Find("VillageCenter");
        var villagers = GameObject.FindGameObjectsWithTag("VillageResident");

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Camera3dPerson.canMoveCam = false;
        uiCanBeTurnedOff.SetActive(false);
        yield return StartCoroutine(FadeScreenToBlack(5));

        hero.GetComponent<NavMeshAgent>().Warp(villageCenter.transform.position);
        hero.GetComponent<ClickToMove>().waitPosition = villageCenter.transform.position;

        GameObject edelvulf = GameObject.Find("Edelvulf");
        GameObject wife = GameObject.Find("Matilda");

        edelvulf.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(edelvulf, hero.transform.position, 3));
        wife.GetComponent<NavMeshAgent>().Warp(edelvulf.transform.position + Vector3.left*5);

        edelvulf.GetComponent<NPC>().canWalk = wife.GetComponent<NPC>().canWalk = false;

        hero.transform.LookAt(edelvulf.transform);
        edelvulf.transform.LookAt(hero.transform);
        wife.transform.LookAt(hero.transform);

        foreach (var villager in villagers)
        {
            villager.GetComponent<NavMeshAgent>().Warp(FindNearestFreePos(edelvulf, edelvulf.transform.position, 8));
            villager.transform.LookAt(hero.transform);
        }

        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        yield return StartCoroutine(FadeScreenToWhite(5));

        yield return StartCoroutine(edelvulf.GetComponent<BubbleDialogue>().Say("Ты одержал великую победу, храбрый воин, и достоин самой щедрой награды! Бери моих крестьян и знай, что двери моего дома в любое время открыты для тебя! Ура, великому карлу и его бравым воинам! Ура!", 6f, "", 0));

        for (int i = 0; i < villagers.Length - 1; i++)
        {
            StartCoroutine(villagers[i].GetComponent<BubbleDialogue>().Say("Ура! Ура! Ура!", 3.7f, "", 0));
        }
        yield return StartCoroutine(villagers[villagers.Length - 1].GetComponent<BubbleDialogue>().Say("Ура! Ура! Ура!", 3.7f, "Uraaa", 0));
        yield return StartCoroutine(hero.GetComponent<BubbleDialogue>().Say("Где мне найти шамана?", 2.5f, "Booble_Gde_Mne_Nayti_Shamana", 0));
        yield return StartCoroutine(wife.GetComponent<BubbleDialogue>().Say("Никто не знает, о, храбрый Карл. Он гуляет свободно, как южный ветер. Бродит меж низких земель и высоких скал, баюкает колосья пшеницы и молит небо о дождях. Прислушайся к себе, и ты найдешь дорогу. Но если хочешь, я поищу вместе с тобой…", 6f, "", 0));
        edelvulf.GetComponent<NPC>().canWalk = wife.GetComponent<NPC>().canWalk = true;
        hero.GetComponent<QuestGiver>().AcceptQuest(32);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Camera3dPerson.canMoveCam = true;
        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;
        uiCanBeTurnedOff.SetActive(true);
    }

    public void Act1Ending()
    {
        StartCoroutine(CorAct1Ending());
    }

    IEnumerator CorAct1Ending()
    {
        foreach (var unit in whoComeToGG)
            unit.GetComponent<NPC>().SetTarget(null, false);
        whoComeToGG = new List<GameObject>();
        uiCanBeTurnedOff.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        heroIsSleeping = true;
        Camera3dPerson.canMoveCam = false;

        GameObject cutsceneOne = GameObject.Find("Scene3LastCutscene_1"), cutsceneTwo = GameObject.Find("Scene3LastCutscene_2");
        cutsceneOne.transform.position = cutsceneTwo.transform.position = hero.transform.position + new Vector3(0, 15, 0);
        GameObject sky = GameObject.Find("UsualSkyDome");
        float skySpeed = sky.GetComponent<TOD_Time>().DayLengthInMinutes;
        sky.GetComponent<TOD_Time>().DayLengthInMinutes = 0.15f;

        cutsceneOne.GetComponent<QuickCutsceneController>().ActivateCutscene(); // запустили кат-сцену
        yield return StartCoroutine(FadeScreenToWhite(5));
        if (sky.GetComponent<TOD_Sky>().Cycle.Hour >= 0 && sky.GetComponent<TOD_Sky>().Cycle.Hour < 7)
        {
            bool madeFog = false;
            while (sky.GetComponent<TOD_Sky>().Cycle.Hour < 7)
            {
                if (!madeFog && sky.GetComponent<TOD_Sky>().Cycle.Hour >= 3 && sky.GetComponent<TOD_Sky>().Cycle.Hour < 7)
                {
                    yield return StartCoroutine(FadeScreenToBlack(5));
                    GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().MakeFog();
                    madeFog = true;
                }
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            var day = sky.GetComponent<TOD_Sky>().Cycle.Day;
            bool madeFog = false;
            while (sky.GetComponent<TOD_Sky>().Cycle.Day != day + 1 || sky.GetComponent<TOD_Sky>().Cycle.Hour < 7)
            {
                if (!madeFog && sky.GetComponent<TOD_Sky>().Cycle.Hour >= 3 && sky.GetComponent<TOD_Sky>().Cycle.Hour < 7)
                {
                    yield return StartCoroutine(FadeScreenToBlack(5));
                    GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().MakeFog();
                    madeFog = true;
                }
                yield return new WaitForEndOfFrame();
            }
        }

        cutsceneOne.GetComponent<QuickCutsceneController>().EndCutscene();

        yield return StartCoroutine(FadeScreenToWhite(5));

        sky.GetComponent<TOD_Time>().DayLengthInMinutes = skySpeed;

        cutsceneTwo.GetComponent<QuickCutsceneController>().ActivateCutscene();

        while (cutsceneTwo.GetComponent<QuickCutsceneController>().playingCutscene)
            yield return new WaitForEndOfFrame();
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Camera3dPerson.canMoveCam = true;

        hero.GetComponent<Parameters>().health = hero.GetComponent<Parameters>().maxHealth;

        uiCanBeTurnedOff.SetActive(true);
        SendMessage("Act2Started");
    }

    void HeroNearDungeon()
    {
        foreach (var squad in Squads.squads)
        {
            squad[0].GetComponent<FriendUnit>()
                .squadGO.GetComponent<Squad>()
                .squadButton.GetComponent<Button>()
                .interactable = false;
        }

        GameObject dungeonStart = GameObject.Find("DungeonHeroStart");
        hero.GetComponent<NavMeshAgent>().Warp(dungeonStart.transform.position);

        Camera.main.transform.localEulerAngles = new Vector3(30, -45, 0);
        Camera.main.transform.position = hero.transform.position;
        Camera3dPerson.isometric = true;
        Camera.main.orthographic = true;
        Camera.main.orthographicSize = 20;
        Camera.main.farClipPlane = 100;
        Camera.main.nearClipPlane = -275;

        hero.GetComponent<NavMeshAgent>().destination =
            hero.GetComponent<ClickToMove>().waitPosition = hero.transform.position;
        Squad.canSquadBeReady = false;
        foreach (var unit in whoComeToGG)
        {
            unit.GetComponent<NavMeshAgent>().Warp(dungeonStart.transform.position);
        }
    }

    void HeroWantToExitDungeon()
    {
        foreach (var squad in Squads.squads)
        {
            squad[0].GetComponent<FriendUnit>()
                .squadGO.GetComponent<Squad>()
                .squadButton.GetComponent<Button>()
                .interactable = squad[0].GetComponent<FriendUnit>()
                                .squadGO.GetComponent<Squad>().squadActive;
        }

        GameObject dungeonExit = GameObject.Find("DungeonEntrance");
        hero.GetComponent<NavMeshAgent>().Warp(dungeonExit.transform.position);
        foreach (var unit in whoComeToGG)
        {
            unit.GetComponent<NavMeshAgent>().Warp(dungeonExit.transform.position);
        }

        hero.GetComponent<NavMeshAgent>().destination =
        hero.GetComponent<ClickToMove>().waitPosition = hero.transform.position;
        Camera.main.transform.position = hero.transform.FindChild("CaveCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("CaveCamera").rotation;

        Camera3dPerson.isometric = false;
        Camera.main.orthographic = false;
        Camera.main.farClipPlane = 586;
        Camera.main.nearClipPlane = 0.3f;
        Squad.canSquadBeReady = true;
    }
}
