﻿using UnityEngine;
using System.Collections;

public class HeroExitRuinsTrigger : MonoBehaviour {
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            GameObject.Find("CutScenesManager").SendMessage("HeroExitRuins");
            Destroy(gameObject);
        }
    }
}
