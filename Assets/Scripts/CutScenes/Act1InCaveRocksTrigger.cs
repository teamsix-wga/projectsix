﻿using UnityEngine;
using System.Collections;

public class Act1InCaveRocksTrigger : MonoBehaviour {
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            GameObject.Find("CutScenesManager").SendMessage("CaveRocksFailing");
            Destroy(gameObject);
        }
    }
}
