﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cutscene2Trigger : MonoBehaviour
{
    public static bool cutscenePlaying;

    private bool talked = false;
    private GameObject fisherGameObject;
    private GameObject fadeScreen;
    private GameObject[] fishers;
    GameObject book;
    void Start()
    {
        fisherGameObject = Resources.Load("Fisher") as GameObject;
        fadeScreen = GameObject.Find("FadeScreen");
        fishers = new GameObject[3];
        for (int i = 0; i < 3; i++) // спавним "рыбаков"
        {
            fishers[i] = Instantiate(fisherGameObject, Vector3.zero, Quaternion.identity) as GameObject;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!talked && other.CompareTag("Player") && other.gameObject.GetComponent<QuestReceiver>().quests.Find(q => q.ID == 9) != null) // если это герой и взят квест на руины
        {
            StartCoroutine(Scene(other.gameObject));
        }
    }

    IEnumerator Scene(GameObject hero)
    {
        cutscenePlaying = true;
        talked = true;

        float heroSpeed = hero.GetComponent<NavMeshAgent>().speed;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        GameObject.Find("UI").transform.FindChild("CanBeTurnedOff").gameObject.SetActive(false);
        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(3));

        // герой идёт к рыбакам
        hero.GetComponent<ClickToMove>().waitPosition = hero.GetComponent<NavMeshAgent>().destination = transform.FindChild("PlayerPositionCutscene2").position;
        hero.GetComponent<NavMeshAgent>().speed = 4.5f;

        GameObject whereFishersLooking = GameObject.Find("FishersLooksHere"); // куда смотрят рыбаки
        GameObject fisherTalkingCameraPos = GameObject.Find("FishersTalkingCamera"); // где камера

        book = Instantiate(Resources.Load("FireBook"), whereFishersLooking.transform.position, Quaternion.identity) as GameObject;

        for (int i = 0; i < 3; i++)
        {
            fishers[i].transform.GetChild(0).gameObject.AddComponent<Rigidbody>();
            fishers[i].transform.GetChild(0).gameObject.GetComponent<Rigidbody>().useGravity = false;
            fishers[i].transform.GetChild(0).gameObject.GetComponent<Rigidbody>().isKinematic = true;
            fishers[i].GetComponent<NavMeshAgent>().Warp(transform.FindChild("Fisher" + (i + 1).ToString()).position);
            fishers[i].transform.LookAt(whereFishersLooking.transform);
            fishers[i].transform.GetChild(0).gameObject.AddComponent<RealFoWUnit>();
            fishers[i].transform.GetChild(0).gameObject.GetComponent<RealFoWUnit>().friend = false;
        }
        GameObject.FindWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(fishers[0], 100);

        Camera.main.transform.position = fisherTalkingCameraPos.transform.position;
        Camera.main.transform.rotation = fisherTalkingCameraPos.transform.rotation;

        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(3));

        yield return StartCoroutine(fishers[0].GetComponent<BubbleDialogue>().Say("Зря вы это всё затеяли, ой, зря!", 3.7f, "Dialog3", 0));
        yield return StartCoroutine(fishers[1].GetComponent<BubbleDialogue>().Say("Ты помоги нам лучше, а не причитай, как моя бабка на смертном одре!", 4.2f, "Dialog3", 1));
        yield return StartCoroutine(fishers[2].GetComponent<BubbleDialogue>().Say("Думаю, за такую диковинку можно раздобыть хорошего рома.", 3.8f, "Dialog3", 2));
        yield return StartCoroutine(fishers[0].GetComponent<BubbleDialogue>().Say("А я говорю зря, нам нечего здесь делать и лучше уходить!", 4.9f, "Dialog3", 3));
        yield return StartCoroutine(fishers[2].GetComponent<BubbleDialogue>().Say("Нет! Мы достанем её! Я бежал за ней чуть ли не от истока реки!", 4.2f, "Dialog3", 4));
        yield return StartCoroutine(fishers[2].GetComponent<BubbleDialogue>().Say("Как она только туда попала?", 1.8f, "Dialog3", 7));
        yield return StartCoroutine(fishers[0].GetComponent<BubbleDialogue>().Say("Вся эта чертовщина добром не кончится!", 3.9f, "Dialog3", 5));
        yield return StartCoroutine(fishers[0].GetComponent<BubbleDialogue>().Say("Недаром её не уносит течением возле руин, как будто к камню привязана!", 5.5f, "Dialog3", 8));

        fishers[1].transform.LookAt(transform.FindChild("PlayerPositionCutscene2").position);
        yield return StartCoroutine(fishers[1].GetComponent<BubbleDialogue>().Say("Глядите!", 1.5f, "Dialog3", 6));

        fishers[2].transform.LookAt(transform.FindChild("PlayerPositionCutscene2").position);
        fishers[0].transform.LookAt(transform.FindChild("PlayerPositionCutscene2").position);

        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(4));

        hero.GetComponent<NavMeshAgent>().Warp(transform.FindChild("PlayerPositionCutscene2").position); // перемещаем героя
        hero.GetComponent<ClickToMove>().waitPosition = hero.GetComponent<NavMeshAgent>().destination = transform.FindChild("PlayerPositionCutscene2").position;
        
        hero.transform.LookAt(fishers[1].transform);
        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(hero, hero.GetComponent<RealFoWUnit>().radius);

        GameObject.Find("CutScenesManager").SendMessage("Act1Scene2Fishers", fishers);
        
        cutscenePlaying = false;
        hero.GetComponent<NavMeshAgent>().speed = heroSpeed;
    }

    public void DialogueEnd()
    {
        GameObject villageCenter = GameObject.Find("VillageCenter");

        for (int i = 0; i < fishers.Length; i++)
        {
            Vector2 randPoint = Random.insideUnitCircle * 15;
            fishers[i].GetComponent<NavMeshAgent>().destination =
                new Vector3(villageCenter.transform.position.x + randPoint.x, villageCenter.transform.position.y,
                    villageCenter.transform.position.z + randPoint.y);
        }
        Destroy(book);
        Destroy(gameObject);
    }
}
