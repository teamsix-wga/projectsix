﻿using UnityEngine;
using System.Collections;

public class Act2HeroFindMatildaAndEva : MonoBehaviour
{
    bool played = false;

    void OnTriggerStay(Collider col)
    {
        if (!played && col.CompareTag("Player") && col.GetComponent<QuestReceiver>().quests.Exists(q => q.ID == 34))
        {
            played = true;
            GameObject.Find("CutScenesManager").SendMessage("Act2HeroFindEvaAndMatilda");
        }
    }
}
