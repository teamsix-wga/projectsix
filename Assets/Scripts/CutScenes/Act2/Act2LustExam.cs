﻿using UnityEngine;
using System.Collections;

public class Act2LustExam : MonoBehaviour {
    GameObject lustTerr;

    void Start()
    {
        lustTerr = GameObject.Find("Terrains").transform.FindChild("LustTerrain").gameObject;
    }

    void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("Player") && col.GetComponent<QuestReceiver>().quests.Exists(q => q.ID == 34) && lustTerr.activeInHierarchy)
        {
            GameObject.Find("CutScenesManager").SendMessage("LustPassed");
            Destroy(gameObject);
        }
    }
}
