﻿using UnityEngine;

public class Act2HeroDreamAfterWizard : MonoBehaviour {
    void OnTriggerStay(Collider col)
    {
        if (col.CompareTag("Player") && col.GetComponent<QuestReceiver>().quests.Exists(q => q.ID == 35))
        {
            GameObject.Find("CutScenesManager").SendMessage("Act2DreamHeroAfterWizard");
            Destroy(gameObject);
        }
    }
}
