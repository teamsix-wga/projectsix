﻿using UnityEngine;
using System.Collections;

public class Act2TreeTrigger : MonoBehaviour
{
    public GameObject girl;
    public GameObject billboard;
    GameObject hero;
    GameObject lustTerr;
    string[] poolPhrases;
    bool said = false;
    string[] animPool;

    void Start()
    {
        animPool = new[] {"S_Kiss", "S_waving", "S_warning" };
        lustTerr = GameObject.Find("Terrains").transform.FindChild("LustTerrain").gameObject;
        poolPhrases = new []{"О, Карл!", "Приди ко мне, Карл!", "Я жажду тебя, Карл!", "Возьми меня, Карл!", "Оооооо!", "Ааааа!", "Позволь мне победить твоего змея, Карл!", "Вонзи в меня свой меч!"};
        hero = GameObject.FindWithTag("Player");
        billboard = transform.parent.FindChild("Conifer_Desktop_Billboard").gameObject;
        girl = transform.parent.FindChild("girl_biped").gameObject;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player") && col.GetComponent<QuestReceiver>().quests.Exists(q => q.ID == 34) && lustTerr.activeInHierarchy && !said)
        {
            billboard.SetActive(false);
            girl.SetActive(true);
            var phr = poolPhrases[Random.Range(0, poolPhrases.Length)];
            StartCoroutine(girl.GetComponent<BubbleDialogue>().Say(phr, 3, "", 0));
            girl.GetComponent<DialoguesTalker>().dialID = 36;
            girl.GetComponent<DialoguesTalker>().ReloadDialogue();
            girl.GetComponent<DialoguesTalker>().rootPhrase.phrase = phr;
            Act2.girlsTrees.Add(gameObject);
            girl.GetComponent<Animator>().Play(animPool[Random.Range(0, animPool.Length)]);
        }
    }

    void Update()
    {
        if (girl.activeInHierarchy)
        {
            var len = girl.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length;
            var time = girl.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).normalizedTime*len;
            if (time > len - 0.3f && time < len - 0.04f)
            {
                girl.GetComponent<Animator>().Play(animPool[Random.Range(0, animPool.Length)]);
            }
            girl.transform.LookAt(hero.transform);
        }
    }
}
