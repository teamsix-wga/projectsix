﻿using UnityEngine;
using System.Collections;

public class Act2LastFightTrigger : MonoBehaviour {
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player") && col.GetComponent<QuestReceiver>().quests.Exists(q => q.ID == 40))
        {
            Debug.Log("start fight");
            GameObject.Find("CutScenesManager").SendMessage("Act2StartLastFight");
            Destroy(gameObject);
        }
    }
}
