﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Act2 : MonoBehaviour
{
    GameObject hero;
    List<GameObject> whoComeToGG;
    GameObject uiCanBeTurnedOff;
    public static GameObject actTwo;
    GameObject eva, matilda;
    GameObject usualTerrain, usualSkyDome, lustTerrain, lustSkyDome, hellTerrain, hellSkyDome;
    bool dialogueClosed = true;
    public static List<GameObject> girlsTrees = new List<GameObject>();
    bool escort = false;
    GameObject koldyn;
    public static bool endlessBattle = false;
    public static bool lastBattle = false;
    public static bool needToReturnCamera = false;
    GameObject mainEnemy;

    IEnumerator Start ()
    {
        mainEnemy = GameObject.Find("MainEnemy");
        mainEnemy.SetActive(false);
        koldyn = GameObject.Find("Koldyn");
        koldyn.SetActive(false);
        usualTerrain = GameObject.Find("Terrain");
        lustTerrain = GameObject.Find("Terrains").transform.FindChild("LustTerrain").gameObject;
        hellTerrain = GameObject.Find("Terrains").transform.FindChild("HellTerrain").gameObject;
        usualSkyDome = GameObject.Find("UsualSkyDome");
        lustSkyDome = GameObject.Find("LustSkyDome");
        lustSkyDome.SetActive(false);
        hellSkyDome = GameObject.Find("HellSkyDome");
        hellSkyDome.SetActive(false);
        eva = GameObject.Find("Eva");
        matilda = GameObject.Find("Matilda");
        actTwo = GameObject.Find("Act2");
        actTwo.SetActive(false);
	    hero = GameObject.FindWithTag("Player");
        whoComeToGG = new List<GameObject>();
        uiCanBeTurnedOff = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff").gameObject;
        yield return StartCoroutine(Minimap.SetMinimap());
    }

    void Lock()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Camera3dPerson.canMoveCam = false;
        uiCanBeTurnedOff.SetActive(false);
    }

    void Unlock()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Camera3dPerson.canMoveCam = true;
        uiCanBeTurnedOff.SetActive(true);
    }

    void DialogueClosed()
    {
        dialogueClosed = true;
    }

    void StopHero()
    {
        hero.GetComponent<NavMeshAgent>().SetDestination(hero.transform.position);
        hero.GetComponent<ClickToMove>().waitPosition = hero.transform.position;
    }

    IEnumerator WaitForDialogueStop(GameObject who, int dialNum)
    {
        who.GetComponent<DialoguesTalker>().dialID = dialNum;
        who.GetComponent<DialoguesTalker>().ReloadDialogue();
        dialogueClosed = false;
        who.GetComponent<DialoguesTalker>().OpenDialogue();

        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;
    }

    void Act2Started()
    {
        actTwo.SetActive(true);
        hero.GetComponent<QuestGiver>().AcceptQuest(33);
        var ed =  GameObject.Find("Edelvulf");
        ed.GetComponent<DialoguesTalker>().dialID = 34;
        ed.GetComponent<DialoguesTalker>().ReloadDialogue();
        matilda.SetActive(false);
    }

    void Act2TriggerHeroEnterVillage()
    {
        StartCoroutine(CorAct2TriggerHeroEnterVillage());
    }

    IEnumerator CorAct2TriggerHeroEnterVillage()
    {
        var villagers = GameObject.FindGameObjectsWithTag("VillageResident");
        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;

        Lock();

        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(5));

        hero.GetComponent<NavMeshAgent>().Warp(GameObject.Find("EdelvulfVillageEntrance").transform.position);
        StopHero();
        hero.transform.LookAt(GameObject.Find("VillageCenter").transform);

        for (int i = 0; i < 4; i++)
        {
            villagers[i].GetComponent<NavMeshAgent>()
                .Warp(Act1Startup.FindNearestFreePos(villagers[i], hero.transform.position + Vector3.right*10, 7));
            villagers[i].transform.LookAt(hero.transform);
        }
        hero.transform.LookAt(villagers[0].transform);
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;
        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(GameObject.Find("VillageCenter").transform.position, 150);
        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(5));
        yield return StartCoroutine(hero.GetComponent<BubbleDialogue>().Say("Вы видели шамана?", 2.5f, "", 0));
        yield return StartCoroutine(villagers[0].GetComponent<BubbleDialogue>().Say("Доброго здравия Вам, о, доблестный господин!", 4f, "", 0));
        yield return StartCoroutine(villagers[1].GetComponent<BubbleDialogue>().Say("Слава великому герою!", 2.5f, "", 0));
        yield return StartCoroutine(villagers[2].GetComponent<BubbleDialogue>().Say("Долгих лет храброму воину!", 3f, "", 0));
        yield return StartCoroutine(villagers[3].GetComponent<BubbleDialogue>().Say("Дорогу храбрейшему из храбрейших, сильнейшему из сильнейших!", 5f, "", 0));

        Unlock();
        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(5));
        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;
        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(5));
    }

    void Act2DialogueWithEdelvulf()
    {
        StartCoroutine(CorAct2DialogueWithEdelvulf());
    }

    IEnumerator CorAct2DialogueWithEdelvulf()
    {
        yield return StartCoroutine(hero.GetComponent<BubbleDialogue>().Say("Мысли: \"<i>Творится какая-то чертовщина. Шаман, как в воду канул. Мне нужно отправиться в заброшенную деревню и попытать счастье там.</i>\"", 6f, "", 0));
        hero.GetComponent<QuestGiver>().AcceptQuest(34);
        var edelvulf = GameObject.Find("Edelvulf");
        edelvulf.GetComponent<DialoguesTalker>().dialID = 1;
        edelvulf.GetComponent<DialoguesTalker>().ReloadDialogue();
    }

    void Act2HeroFindEvaAndMatilda()
    {
        StartCoroutine(CorAct2HeroFindEvaAndMatilda());
    }

    IEnumerator CorAct2HeroFindEvaAndMatilda()
    {
        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Lock();

        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(5));

        eva.GetComponent<NavMeshAgent>().Warp(GameObject.Find("HeroFindEvaHere").transform.position);
        eva.GetComponent<NavMeshAgent>().destination = eva.transform.position;
        matilda.SetActive(true);
        matilda.GetComponent<NavMeshAgent>().Warp(GameObject.Find("HeroFindMatildaHere").transform.position);
        matilda.GetComponent<NavMeshAgent>().destination = matilda.transform.position;

        eva.GetComponent<NPC>().canWalk = false;
        matilda.GetComponent<NPC>().canWalk = false;

        hero.GetComponent<NavMeshAgent>().Warp(GameObject.Find("HeroFindAndStandHere").transform.position);
        StopHero();
        GameObject.FindWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(hero.transform.position, 50);
        hero.transform.LookAt((matilda.transform.position + eva.transform.position)/2);
        eva.transform.LookAt(hero.transform);
        matilda.transform.LookAt(hero.transform);
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;
        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(5));
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        yield return StartCoroutine(WaitForDialogueStop(hero, 35));

        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(5));
        usualTerrain.SetActive(false);
        usualTerrain.layer = LayerMask.NameToLayer("SecretPlace");
        hellTerrain.SetActive(false);
        hellTerrain.layer = LayerMask.NameToLayer("SecretPlace");
        lustTerrain.SetActive(true);
        lustTerrain.layer = LayerMask.NameToLayer("Default");
        lustTerrain.transform.position = new Vector3(0, 19.2f, 0);
        hellSkyDome.SetActive(false);
        lustSkyDome.SetActive(true);
        usualSkyDome.SetActive(false);
        Camera.main.GetComponent<TOD_Camera>().sky = lustSkyDome.GetComponent<TOD_Sky>();
        yield return StartCoroutine(Minimap.SetMinimap());
        Unlock();
        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;
        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(5));
    }

    public void LustEnd()
    {
        StartCoroutine(CorLustEnd());
    }

    IEnumerator CorLustEnd()
    {
        Lock();
        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;
        StopHero();
        yield return new WaitForEndOfFrame();
        hero.GetComponent<Animator>().Play("Spear_Knocked_backward_onPlace");
        yield return new WaitForSeconds(1.35f);
        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(5));

        var quest = hero.GetComponent<QuestReceiver>().quests.Find(q => q.ID == 34);
        hero.GetComponent<QuestReceiver>().quests.Remove(quest);
        quest.questDone = true;
        hero.GetComponent<QuestReceiver>().doneQuests.Add(quest);
        hero.GetComponent<QuestGiver>().GiveReward(34);
        var questExplore = GameObject.FindGameObjectsWithTag("QuestExplore");
        foreach (var checker in questExplore)
            if (checker.transform.FindChild("Checker").GetComponent<QuestWhereToGoCheck>().quest.ID == 34)
            {
                Destroy(checker);
                break;
            }

        foreach (var girl in girlsTrees)
        {
            Destroy(girl.GetComponent<Act2TreeTrigger>().girl);
            girl.GetComponent<Act2TreeTrigger>().billboard.SetActive(true);
            Destroy(girl);
        }
        usualTerrain.SetActive(true);
        usualTerrain.layer = LayerMask.NameToLayer("Default");
        lustTerrain.SetActive(false);
        lustTerrain.layer = LayerMask.NameToLayer("SecretPlace");
        hellTerrain.SetActive(false);
        hellTerrain.layer = LayerMask.NameToLayer("SecretPlace");
        lustSkyDome.SetActive(false);
        hellSkyDome.SetActive(false);
        usualSkyDome.SetActive(true);
        Camera.main.GetComponent<TOD_Camera>().sky = usualSkyDome.GetComponent<TOD_Sky>();
        yield return StartCoroutine(Minimap.SetMinimap());

        var camPos = GameObject.Find("CameraHereAfterLust");
        Camera.main.transform.position = camPos.transform.position;
        Camera.main.transform.rotation = camPos.transform.rotation;
        koldyn.SetActive(true);
        hero.GetComponent<NavMeshAgent>().Warp(GameObject.Find("HeroHereAfterLust").transform.position);
        StopHero();
        koldyn.transform.LookAt(hero.transform);
        hero.transform.LookAt(koldyn.transform);
        foreach (Renderer ren in koldyn.GetComponentsInChildren<Renderer>())
            ren.enabled = true;
        StartCoroutine(ChangesAfterTalkWithWizard());
        GameObject.FindWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(hero.transform.position, 50);
        GameObject.Find("AbandonedHouse").transform.FindChild("First_floor").SendMessage("HeroEnterColliderPorch", hero);
        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(5));
        hero.GetComponent<Animator>().Play("Spear_Recover_from_knocked_backward");
        yield return new WaitForSeconds(1.333f);
        hero.GetComponent<Animator>().Play("IDLE");
        yield return StartCoroutine(WaitForDialogueStop(hero, 37));
        Lock();

        hero.GetComponent<NavMeshAgent>().destination =
            hero.GetComponent<ClickToMove>().waitPosition = GameObject.Find("WarriorsWaitOutside").transform.position;

        while (Vector3.Distance(hero.GetComponent<ClickToMove>().waitPosition, hero.transform.position) > hero.GetComponent<NavMeshAgent>().stoppingDistance)
            yield return null;

        Unlock();
        koldyn.SetActive(false);
    }

    IEnumerator ChangesAfterTalkWithWizard()
    {
        foreach (var villager in GameObject.FindGameObjectsWithTag("VillageResident"))
            Destroy(villager);
        Destroy(GameObject.Find("Edelvulf"));

        yield return null;

        var keys = new List<GameObject>(GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().FoWUnits.Keys);
        foreach (var unit in keys)
            unit.GetComponent<RealFoWUnit>().friend = false;
        hero.GetComponent<RealFoWUnit>().friend = true;

        matilda.SetActive(false);
        eva.SetActive(false);
        Squad.canSquadBeReady = false;

        foreach (var squad in Squads.squads)
        {
            squad[0].GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().isGoingToFlag = false;
            squad[0].GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().isComingBack = false;
            squad[0].GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().isSquadReady = false;
            foreach (var unit in squad)
            {
                var where = unit.GetComponent<FriendUnit>().cameFrom;
                unit.GetComponent<NavMeshAgent>().Warp(where);
                unit.GetComponent<NavMeshAgent>().destination = where;
            }
        }

        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().MakeFog();

        yield return null;
    }

    void Act2DreamHeroAfterWizard()
    {
        StartCoroutine(CorAct2DreamHeroAfterWizard());
    }

    IEnumerator CorAct2DreamHeroAfterWizard()
    {
        var savedCameraPos = Camera.main.transform.position;
        var savedCameraRot = Camera.main.transform.rotation;
        Lock();

        Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
        Camera.main.transform.rotation = hero.transform.FindChild("HeroCamera").rotation;

        StopHero();
        hero.GetComponent<Animator>().Play("Spear_Knocked_backward_onPlace");
        yield return new WaitForSeconds(1.35f);
        StartCoroutine(FadeText.FadeTxt("Здесь будет сон героя. Пока его нет :(", Color.yellow, 3));
        hero.GetComponent<Animator>().Play("Spear_Recover_from_knocked_backward");
        yield return new WaitForSeconds(1.333f);
        hero.GetComponent<Animator>().Play("IDLE");

        Unlock();
        Camera.main.transform.position = savedCameraPos;
        Camera.main.transform.rotation = savedCameraRot;
    }

    void Act2EmptyVillage()
    {
        StartCoroutine(CorAct2EmptyVillage());
    }

    IEnumerator CorAct2EmptyVillage()
    {
        GameObject.FindWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(GameObject.Find("EdelvulfVillageEntrance"), 150); 
        yield return StartCoroutine(hero.GetComponent<BubbleDialogue>().Say("Мысли: \"<i>Никого нет... Надеюсь, они не добрались до моей деревни!</i>\"", 2f, "", 0));
        hero.GetComponent<QuestGiver>().AcceptQuest(36);
        GameObject.Find("TownHall").SendMessage("AttackAlarm");
        GameObject.Find("TalkingTom").SetActive(false);
        GameObject.Find("WarBalancer").GetComponent<WarBalancer>().StartWork();
        
        foreach (var enemy in GameObject.Find("WarBalancer").GetComponent<WarBalancer>().spawnedEnemies)
            enemy.GetComponent<Enemy>().canAttack = false;

        foreach (var unit in GameObject.FindGameObjectsWithTag("Unit"))
            unit.GetComponent<FriendUnit>().canAttack = false;
    }

    void Act2HeroFoundVillageAtWar()
    {
        GameObject.FindWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(GameObject.Find("HeroVillageCenter"), 150);
        foreach (var enemy in GameObject.Find("WarBalancer").GetComponent<WarBalancer>().spawnedEnemies)
            enemy.GetComponent<Enemy>().canAttack = true;

        var units = GameObject.FindGameObjectsWithTag("Unit");
        foreach (var unit in units)
            unit.GetComponent<FriendUnit>().canAttack = true;

        List<string> phrasesPool = new List<string>
        {
            "Не щадите никого! Они хотят разграбить наши земли, обесчестить наших женщин и оставить сиротами наших детей!",
            "Не пожалеем же сил и жизни ради спасения нашей деревни!",
            "Вместе с нами сражается Карл! Теперь мы непобедимы!",
            "Ура нашему господину!",
            "Господин с нами! Теперь мы победим!",
            "Атакуйте!",
            "Получи!",
            "За нашего господина!"
        };
        for (int i = 0; i < Random.Range(0, units.Length); i++)
        {
            var phrase = phrasesPool[Random.Range(0, phrasesPool.Count)];
            StartCoroutine(units[i].GetComponent<BubbleDialogue>().Say(phrase, 2.5f, "", 0));
            phrasesPool.Remove(phrase);
        }
    }

    void Act2KilledOneUnit()
    {
        StartCoroutine(hero.GetComponent<BubbleDialogue>().Say("Мои солдаты ослабли, да и сам я долго не продержусь, что же делать?", 4.35f, "Booble_Moi_soldati_oslabli", 0));
    }

    void Act2KilledThreeUnit()
    {
        StartCoroutine(CorAct2KilledThreeUnit());
    }

    IEnumerator CorAct2KilledThreeUnit()
    {
        var units = GameObject.FindGameObjectsWithTag("Unit");
        int iterNum = 0;
        float minDistance = 60000;
        for (int i = 0; i < units.Length; i++)
        {
            if (minDistance > Vector3.Distance(units[0].transform.position, units[i].transform.position))
            {
                minDistance = Vector3.Distance(units[0].transform.position, units[i].transform.position);
                iterNum = i;
            }
        }
        yield return StartCoroutine(units[iterNum].GetComponent<BubbleDialogue>().Say("Жаль, враги не исчезают по взмаху волшебной палочки.", 4.8f, "Booble_Skromnitsa", 0));
        yield return StartCoroutine(units[0].GetComponent<BubbleDialogue>().Say("Не скромничай, твой меч лучше любой магии!", 2.7f, "Booble_Skromnitsa", 1));
    }

    void Act2KilledSixUnit()
    {
        StartCoroutine(CorAct2KilledSixUnit());
    }

    IEnumerator CorAct2KilledSixUnit()
    {
        yield return StartCoroutine(hero.GetComponent<BubbleDialogue>().Say("Необходимо достать книгу.", 1.4f, "Booble_I_need_your_book", 0));
        yield return StartCoroutine(WaitForDialogueStop(hero, 38));
        yield return new WaitForSeconds(0.1f);

        var enemies = GameObject.Find("WarBalancer").GetComponent<WarBalancer>();
        if (escort)
        {
            enemies.canAttack = false;
            foreach (var enemy in enemies.spawnedEnemies)
                enemy.GetComponent<Enemy>().canAttack = false;

            foreach (var unit in GameObject.FindGameObjectsWithTag("Unit"))
                unit.GetComponent<FriendUnit>().canAttack = false;

            hero.GetComponent<Fighter>().canAttack = false;

            for (int i = 1; i < 4; i++)
            {
                enemies.spawnedEnemies[i].GetComponent<NavMeshAgent>()
                    .Warp(Act1Startup.FindNearestFreePos(enemies.spawnedEnemies[i], hero.transform.position, 9));
                enemies.spawnedEnemies[i].GetComponent<NPC>().SetTarget(hero, true);
                whoComeToGG.Add(enemies.spawnedEnemies[i]);
            }
        }
        else
        {
            endlessBattle = true;
        }

        hero.GetComponent<QuestGiver>().AcceptQuest(37);
    }

    void Act2HeroRunToRuins()
    {
        escort = false;
    }

    void Act2HeroRunToRuinsWithEscort()
    {
        escort = true;
    }

    void Act2HeroFindBook()
    {
        StartCoroutine(CorAct2HeroFindBook());
    }

    IEnumerator CorAct2HeroFindBook()
    {
        Lock();
        StopHero();
        hero.GetComponent<Animator>().Play("Spear_Knocked_backward_onPlace");
        yield return new WaitForSeconds(1.35f);
        StartCoroutine(FadeText.FadeTxt("Здесь будет сон героя. Пока его нет :(", Color.yellow, 3));
        hero.GetComponent<Animator>().Play("Spear_Recover_from_knocked_backward");
        yield return new WaitForSeconds(1.333f);
        hero.GetComponent<Animator>().Play("IDLE");

        hero.GetComponent<InventoryUnit>().items.Add(GameObject.Find("Inventory").GetComponent<ItemDatabase>().database.Find(it => it.ID == 23)); // добавили книгу
        var itemInSlot = hero.GetComponent<InventoryUnit>().items.Find(it => it.slotID == hero.GetComponent<InventoryUnit>().slotsCount + 3); // ищем предмет в слоте
        if (itemInSlot != null)
        {
            itemInSlot.slotID = hero.GetComponent<InventoryUnit>().FindFreeSlot();
            InventorySlot.UpdateParameters(hero, itemInSlot, -1, true);
            if (itemInSlot.slotID == -1)
                hero.GetComponent<InventoryUnit>().DropItem(itemInSlot, hero);
        }
        hero.GetComponent<InventoryUnit>().items.Find(it => it.ID == 23).slotID = hero.GetComponent<InventoryUnit>().slotsCount + 3; 
        InventorySlot.UpdateParameters(hero, hero.GetComponent<InventoryUnit>().items.Find(it => it.ID == 23), 1, true);

        Unlock();

        if (escort)
            StartCoroutine(WaitForDialogueStop(hero, 39));
        else
        {
            hero.GetComponent<QuestGiver>().AcceptQuest(38);
            koldyn.SetActive(true);
            foreach (Renderer ren in koldyn.GetComponentsInChildren<Renderer>())
                ren.enabled = true;
            koldyn.GetComponent<DialoguesTalker>().dialID = 40;
            koldyn.GetComponent<DialoguesTalker>().ReloadDialogue();
        }
        hero.GetComponent<Fighter>().canAttack = true;
    }

    void Act2HeroKillEscort()
    {
        foreach (var unit in whoComeToGG)
        {
            unit.SendMessage("GetHit", new AttackStructure()
            {
                damage = 200000,
                whoHit = hero
            });
        }
        hero.GetComponent<QuestGiver>().AcceptQuest(38);
        koldyn.SetActive(true);
        foreach (Renderer ren in koldyn.GetComponentsInChildren<Renderer>())
            ren.enabled = true;
        koldyn.GetComponent<DialoguesTalker>().dialID = 40;
        koldyn.GetComponent<DialoguesTalker>().ReloadDialogue();
    }

    void Act2HeroDefuseEscort()
    {
        foreach (var unit in whoComeToGG)
        {
            GameObject.Find("WarBalancer").GetComponent<WarBalancer>().spawnedEnemies.Remove(unit);
            unit.tag = "Untagged";
            unit.GetComponent<NPC>().SetTarget(null, false);
            unit.GetComponent<Parameters>().knockedBack = true;
            Destroy(unit.transform.FindChild("Checker").gameObject);
            Destroy(unit.GetComponent<Enemy>());
            unit.GetComponent<Animator>().Play("S_Sleeping");
            StartCoroutine(DefuseEnemy(unit));
            if(unit.transform.FindChild("VisibleSelection"))
                unit.transform.FindChild("VisibleSelection").GetComponent<Renderer>().material.SetColor("_OutlineColor", Color.green);
        }
        
        hero.GetComponent<QuestGiver>().AcceptQuest(38);
        koldyn.SetActive(true);
        foreach (Renderer ren in koldyn.GetComponentsInChildren<Renderer>())
            ren.enabled = true;
        koldyn.GetComponent<DialoguesTalker>().dialID = 40;
        koldyn.GetComponent<DialoguesTalker>().ReloadDialogue();
    }

    IEnumerator DefuseEnemy(GameObject ene)
    {
        yield return new WaitForSeconds(1);
        ene.GetComponent<Animator>().Stop();
    }

    void Act2KoldynIsEnemy()
    {
        StartCoroutine(CorAct2KoldynIsEnemy());
    }

    IEnumerator CorAct2KoldynIsEnemy()
    {
        dialogueClosed = false;
        while (!dialogueClosed)
            yield return new WaitForEndOfFrame();
        dialogueClosed = false;

        koldyn.GetComponent<Enemy>().enabled = true;
        koldyn.tag = "Enemy";
        koldyn.GetComponent<Enemy>().Opponent = hero;
        koldyn.GetComponent<DialoguesTalker>().canOpenDialogueByClick = false;
        if(koldyn.transform.FindChild("VisibleSelection"))
            koldyn.transform.FindChild("VisibleSelection").GetComponent<Renderer>().material.SetColor("_OutlineColor", Color.red);
    }

    void Act2HeroEndWithKoldyn()
    {
        StartCoroutine(WaitForDialogueStop(hero, 41));
    }

    void Act2HeroKilledKoldyn()
    {
        HeroDesidions.killedWizard = true;
        Destroy(koldyn);
        Act2AngerSleep();
    }

    void Act2HeroDefuseKoldyn()
    {
        HeroDesidions.killedWizard = false;
        StartCoroutine(CorAct2HeroDefuseKoldyn());
    }

    IEnumerator CorAct2HeroDefuseKoldyn()
    {
        Lock();
        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(5));
        hero.GetComponent<NavMeshAgent>().Warp(GameObject.Find("HeroHereAfterLust").transform.position);
        StopHero();
        var shaman = GameObject.Find("Shaman");
        shaman.GetComponent<NavMeshAgent>().Warp(koldyn.transform.position);
        koldyn.GetComponent<NavMeshAgent>().Warp(Vector3.zero);
        shaman.transform.LookAt(hero.transform);
        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(5));
        yield return StartCoroutine(shaman.GetComponent<BubbleDialogue>().Say("Ты не осквернил свое сердце злобой. За это ты будешь вознагражден.", 4.5f, "42-1", 0));
        yield return StartCoroutine(hero.GetComponent<BubbleDialogue>().Say("Случилась беда, и я не знаю, как всех спасти.", 3f, "", 0));
        yield return StartCoroutine(shaman.GetComponent<BubbleDialogue>().Say("Ты должен довериться себе.", 1.9f, "", 0));
        Act2AngerSleep();
        Destroy(koldyn);
    }

    void Act2AngerSleep()
    {
        needToReturnCamera = true;
        StartCoroutine(CorAct2AngerSleep());
    }

    IEnumerator CorAct2AngerSleep()
    {
        Lock();
        StopHero();
        hero.GetComponent<Animator>().Play("Spear_Knocked_backward_onPlace");
        StartCoroutine(FadeText.FadeTxt("Здесь будет сон героя. Пока его нет :(", Color.yellow, 3));
        yield return new WaitForSeconds(1.35f);
        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(5));

        usualTerrain.SetActive(false);
        usualTerrain.layer = LayerMask.NameToLayer("SecretPlace");
        hellTerrain.SetActive(true);
        hellTerrain.layer = LayerMask.NameToLayer("Default");
        hellTerrain.transform.position = new Vector3(0, 19.2f, 0);
        hellSkyDome.transform.FindChild("Atmosphere").gameObject.layer = LayerMask.NameToLayer("SecretPlace");
        hellSkyDome.SetActive(true);
        usualSkyDome.SetActive(false);
        Camera.main.GetComponent<TOD_Camera>().sky = hellSkyDome.GetComponent<TOD_Sky>();
        yield return StartCoroutine(Minimap.SetMinimap());

        var sleepingPoint = GameObject.Find("HeroSleepHeroAfterKillKoldyn");
        hero.GetComponent<NavMeshAgent>().Warp(sleepingPoint.transform.position);
        StopHero();
        var house = GameObject.Find("AbandonedHouse").transform.FindChild("First_floor");
        house.SendMessage("Exit", hero);
        house.GetComponent<Act1Scene3HouseInAbandonedVillage>().inHouse = false;
        matilda.SetActive(true);
        matilda.GetComponent<NavMeshAgent>().Warp(sleepingPoint.transform.FindChild("MatildaInSwampHere").position);
        matilda.GetComponent<NavMeshAgent>().destination =
            sleepingPoint.transform.FindChild("MatildaInSwampHere").position;

        eva.SetActive(true);
        eva.GetComponent<NavMeshAgent>().Warp(sleepingPoint.transform.FindChild("EvaInSwampHere").position);
        eva.GetComponent<NavMeshAgent>().destination =
            sleepingPoint.transform.FindChild("EvaInSwampHere").position;
        GameObject.FindWithTag("FoWProjector").GetComponent<RealFoW>().UnfogPlace(hero, 100);
        matilda.transform.LookAt(hero.transform);
        eva.transform.LookAt(hero.transform);
        hero.transform.LookAt(matilda.transform);
        hero.GetComponent<Animator>().Play("IDLE");
        Camera3dPerson.cameraLooksAtPlayer = false;
        var camPos = GameObject.Find("CameraSleepingHere");
        Camera.main.transform.position = camPos.transform.position;
        Camera.main.transform.rotation = camPos.transform.rotation;
        Camera3dPerson.cameraLooksAtPlayer = false;
        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(5));
        hero.GetComponent<Animator>().Play("Spear_Recover_from_knocked_backward");
        yield return new WaitForSeconds(1.333f);
        hero.GetComponent<Animator>().Play("IDLE");

        Unlock();
        StartCoroutine(WaitForDialogueStop(hero, 43));
    }

    void Act2HelpGirls()
    {
        StartCoroutine(CorAct2HelpGirls());
    }

    IEnumerator CorAct2HelpGirls()
    {
        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(5));
        HeroDesidions.pridePassed = true;

        matilda.name = "Angel";
        matilda.tag = "Unit";
        matilda.transform.FindChild("Checker").GetComponent<UnitsSphere>().enabled = true;
        matilda.GetComponent<FriendUnit>().enabled = true;
        matilda.GetComponent<FriendUnit>().skills = GameObject.Find("SkillsManager").GetComponent<SkillsLoader>().LoadSkills(matilda.name);
        matilda.GetComponent<Parameters>().health = matilda.GetComponent<Parameters>().maxHealth = 1000;
        matilda.GetComponent<DialoguesTalker>().canOpenDialogueByClick = false;
        matilda.GetComponent<FriendUnit>().ResetSkills();
        matilda.AddComponent<InventoryUnit>();
        if (matilda.transform.FindChild("VisibleSelection"))
            matilda.transform.FindChild("VisibleSelection").GetComponent<Renderer>().material.SetColor("_OutlineColor", Color.green);
        matilda.GetComponent<NPC>().SetTarget(hero, true);

        eva.name = "Angel";
        eva.transform.FindChild("Checker").GetComponent<UnitsSphere>().enabled = true;
        eva.tag = "Unit";
        eva.GetComponent<FriendUnit>().enabled = true;
        eva.GetComponent<FriendUnit>().skills = GameObject.Find("SkillsManager").GetComponent<SkillsLoader>().LoadSkills(eva.name);
        eva.GetComponent<FriendUnit>().ResetSkills();
        eva.AddComponent<InventoryUnit>();
        eva.GetComponent<Parameters>().health = eva.GetComponent<Parameters>().maxHealth = 1000;
        eva.GetComponent<DialoguesTalker>().canOpenDialogueByClick = false;
        if (eva.transform.FindChild("VisibleSelection"))
            eva.transform.FindChild("VisibleSelection").GetComponent<Renderer>().material.SetColor("_OutlineColor", Color.green);
        eva.GetComponent<NPC>().SetTarget(hero, true);

        Destroy(eva.transform.FindChild("Checker").GetComponent<EnemyCheckSphere>());
        Destroy(matilda.transform.FindChild("Checker").GetComponent<EnemyCheckSphere>());
        Destroy(eva.GetComponent<Enemy>());
        Destroy(matilda.GetComponent<Enemy>());
        eva.transform.FindChild("Checker").GetComponent<Collider>().enabled = true;
        matilda.transform.FindChild("Checker").GetComponent<Collider>().enabled = true;

        matilda.GetComponent<FriendUnit>().Opponent = null;
        eva.GetComponent<FriendUnit>().Opponent = null;

        Camera.main.GetComponent<Camera3dPerson>().UnitList.Add(matilda);
        Camera.main.GetComponent<Camera3dPerson>().UnitList.Add(eva);

        eva.GetComponent<NavMeshAgent>().Warp(hero.transform.position);
        matilda.GetComponent<NavMeshAgent>().Warp(hero.transform.position);

        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(5));

        hero.GetComponent<QuestGiver>().AcceptQuest(40);
    }

    void Act2LeftGirls()
    {
        StartCoroutine(CorAct2LeftGirls());
    }

    IEnumerator CorAct2LeftGirls()
    {
        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(5));
        HeroDesidions.pridePassed = false;

        matilda.name = "Demonitsa";
        matilda.transform.FindChild("Checker").GetComponent<EnemyCheckSphere>().enabled = true;
        matilda.GetComponent<Parameters>().health = matilda.GetComponent<Parameters>().maxHealth = 1000;
        matilda.GetComponent<Enemy>().enabled = true;
        matilda.GetComponent<Enemy>().skills = GameObject.Find("SkillsManager").GetComponent<SkillsLoader>().LoadSkills(matilda.name);
        matilda.tag = "Enemy";
        matilda.GetComponent<Enemy>().ResetSkills();
        matilda.GetComponent<DialoguesTalker>().canOpenDialogueByClick = false;
        if (matilda.transform.FindChild("VisibleSelection"))
            matilda.transform.FindChild("VisibleSelection").GetComponent<Renderer>().material.SetColor("_OutlineColor", Color.red);
        matilda.SetActive(false);

        eva.name = "Demonitsa";
        eva.transform.FindChild("Checker").GetComponent<EnemyCheckSphere>().enabled = true;
        eva.GetComponent<Enemy>().enabled = true;
        eva.GetComponent<Enemy>().skills = GameObject.Find("SkillsManager").GetComponent<SkillsLoader>().LoadSkills(eva.name);
        eva.GetComponent<Enemy>().ResetSkills();
        eva.GetComponent<Parameters>().health = eva.GetComponent<Parameters>().maxHealth = 1000;
        eva.tag = "Enemy";
        eva.GetComponent<DialoguesTalker>().canOpenDialogueByClick = false;
        if (eva.transform.FindChild("VisibleSelection"))
            eva.transform.FindChild("VisibleSelection").GetComponent<Renderer>().material.SetColor("_OutlineColor", Color.red);
        eva.SetActive(false);
        Destroy(eva.GetComponent<FriendUnit>());
        Destroy(matilda.GetComponent<FriendUnit>());
        Destroy(eva.transform.FindChild("Checker").GetComponent<UnitsSphere>());
        Destroy(matilda.transform.FindChild("Checker").GetComponent<UnitsSphere>());
        eva.transform.FindChild("Checker").GetComponent<Collider>().enabled = true;
        matilda.transform.FindChild("Checker").GetComponent<Collider>().enabled = true;

        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(5));

        hero.GetComponent<QuestGiver>().AcceptQuest(40);
    }

    void Act2StartLastFight()
    {
        endlessBattle = false;
        WarBalancer.working = false;
        lastBattle = true;

        var center = GameObject.Find("HeroVillageCenter");
        mainEnemy.SetActive(true);
        mainEnemy.GetComponent<NavMeshAgent>().Warp(Act1Startup.FindNearestFreePos(mainEnemy, center.transform.position + Random.onUnitSphere * 20, 20));
        var balancer = GameObject.Find("WarBalancer");
        balancer.GetComponent<WarBalancer>().canAttack = true;
        foreach (var enemy in balancer.GetComponent<WarBalancer>().spawnedEnemies)
            enemy.GetComponent<Enemy>().canAttack = true;

        var units = GameObject.FindGameObjectsWithTag("Unit");
        foreach (var unit in units)
            unit.GetComponent<FriendUnit>().canAttack = true;

        if (!HeroDesidions.pridePassed)
        {
            matilda.SetActive(true);
            matilda.GetComponent<NavMeshAgent>().Warp(Act1Startup.FindNearestFreePos(matilda, center.transform.position + Random.onUnitSphere*10, 20));
            eva.SetActive(true);
            eva.GetComponent<NavMeshAgent>().Warp(Act1Startup.FindNearestFreePos(eva, center.transform.position + Random.onUnitSphere * 15, 20));
        }
    }

    void HeroWinBattle()
    {
        StartCoroutine(CorHeroWinBattle());
    }

    IEnumerator CorHeroWinBattle()
    {
        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(5));
        hero.GetComponent<Fighter>().enabled = false;
        hero.GetComponent<ClickToMove>().enabled = false;
        GameObject.FindWithTag("FoWProjector").GetComponent<RealFoW>().Unfog();
        StopHero();
        Fighter.Opponent = null;
        Lock();
        var cor = StartCoroutine(SaveCameraPosition());
        
        foreach (var enemy in GameObject.Find("WarBalancer").GetComponent<WarBalancer>().spawnedEnemies)
            enemy.SetActive(false);

        foreach (var unit in GameObject.FindGameObjectsWithTag("Unit"))
            unit.SetActive(false);

        foreach (var unit in GameObject.FindGameObjectsWithTag("Lumberjack"))
            unit.SetActive(false);
        
        Destroy(mainEnemy.GetComponent<Enemy>());
        mainEnemy.GetComponent<NavMeshAgent>().enabled = true;
        mainEnemy.GetComponent<NavMeshAgent>().Warp(hero.transform.position + Vector3.forward * 10);
        mainEnemy.GetComponent<NavMeshAgent>().speed = 3;
        mainEnemy.GetComponent<NavMeshAgent>().destination = mainEnemy.transform.position;
        mainEnemy.GetComponent<Animator>().Play("IDLE");
        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(5));
        hero.GetComponent<Animator>().Play("IDLE");
        mainEnemy.transform.LookAt(hero.transform);
        hero.transform.LookAt(mainEnemy.transform);
        hero.GetComponent<NavMeshAgent>().speed = 3;
        StopHero();
        hero.GetComponent<ClickToMove>().waitPosition = hero.GetComponent<NavMeshAgent>().destination = mainEnemy.transform.position;
        hero.GetComponent<Animator>().Play("Spear_Casual_walk");
        hero.GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.wrapMode = WrapMode.Loop;
        hero.GetComponent<NavMeshAgent>().stoppingDistance = 2f;
        while (Vector3.Distance(mainEnemy.transform.position, hero.transform.position) >
               hero.GetComponent<NavMeshAgent>().stoppingDistance + 1.2f)
        {
            hero.GetComponent<Animator>().Play("Spear_Casual_walk"); 
            yield return null;
        }
        hero.transform.LookAt(mainEnemy.transform);

        hero.GetComponent<Animator>().Play("IDLE");
        hero.GetComponent<Animator>().Play("Spear_Deep_Trust");
        yield return new WaitForSeconds(0.4f);
        mainEnemy.transform.FindChild("Blood").GetChild(0).GetComponent<ParticleSystem>().Play();
        mainEnemy.transform.FindChild("Blood").GetChild(0).GetChild(0).GetComponent<ParticleSystem>().Play();
        hero.GetComponent<Animator>().CrossFade("IDLE", 0.2f);
        mainEnemy.GetComponent<Animator>().Play("Spear_Knocked_backward_onPlace");
        yield return new WaitForSeconds(1.4f);
        mainEnemy.GetComponent<Animator>().Play("Spear_Down_back");
        yield return new WaitForSeconds(1.5f);

        StopCoroutine(cor);

        var eyes = hero.transform.GetComponentsInChildren<Transform>().ToList().Find(t => t.name == "HeroEyes");
        Camera.main.transform.position = eyes.position;
        Camera.main.transform.rotation = eyes.rotation;

        var mask = mainEnemy.transform.GetComponentsInChildren<Transform>().ToList().Find(t => t.name == "Mask");

        var savedRot = Camera.main.transform.rotation;
        Camera.main.transform.LookAt(mask);

        var rotAngle = Camera.main.transform.rotation;
        Camera.main.transform.rotation = savedRot;
        
        eyes.GetComponent<Camera>().enabled = false;
        while (Quaternion.Angle(Camera.main.transform.rotation, rotAngle) > 1)
        {
            Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, rotAngle, 0.1f);
            yield return null;
        }

        while (mask.GetComponent<Renderer>().material.color.a > 0.1f)
        {
            var color = mask.GetComponent<Renderer>().material.color;
            mask.GetComponent<Renderer>().material.color = color - new Color(0, 0, 0, 0.05f);
            mask.GetComponent<Renderer>().material.SetFloat("_Metallic", mask.GetComponent<Renderer>().material.GetFloat("_Metallic") + 0.35f * 0.05f);
            yield return null;
        }
        mask.gameObject.SetActive(false);

        yield return new WaitForSeconds(1);
        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(2));
        Unlock();
        GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/EscMenu").GetComponent<Canvas>().enabled = true;
        Time.timeScale = 0;

        Debug.Log("Победа:)");
    }

    void HeroLostBattle()
    {
        StartCoroutine(CorHeroLostBattle());
    }

    IEnumerator CorHeroLostBattle()
    {
        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(5));
        GameObject.FindWithTag("FoWProjector").GetComponent<RealFoW>().Unfog();
        StopHero();
        Fighter.Opponent = null;
        Lock();
        var cor = StartCoroutine(SaveCameraPosition());

        var enemies = GameObject.Find("WarBalancer").GetComponent<WarBalancer>().spawnedEnemies;
        
        var angle = 360 * Mathf.Deg2Rad;

        var point = hero.transform.position;
        for (int i = 0; i < enemies.Count; i++)
        {
            float _z = hero.transform.position.z + Mathf.Cos(angle / enemies.Count * i) * 15;
            float _x = hero.transform.position.x + Mathf.Sin(angle / enemies.Count * i) * 15;
            point.x = _x;
            point.z = _z;
            enemies[i].GetComponent<NavMeshAgent>().Warp(point);
            enemies[i].GetComponent<NavMeshAgent>().destination = point;
        }

        foreach (var enemy in enemies)
        {
            if (enemy.GetComponent<Animation>())
                enemy.GetComponent<Animation>().Play(enemy.GetComponent<Enemy>().stop.name);
            else
                enemy.GetComponent<Animator>().Play(enemy.GetComponent<Enemy>().stop.name);
            enemy.GetComponent<Enemy>().enabled = false;
            enemy.transform.LookAt(hero.transform);
        }

        foreach (var unit in GameObject.FindGameObjectsWithTag("Unit"))
            unit.SetActive(false);

        foreach (var unit in GameObject.FindGameObjectsWithTag("Lumberjack"))
            unit.SetActive(false);

        hero.transform.LookAt(enemies[0].transform);

        mainEnemy.GetComponent<Animator>().Play("IDLE");
        Destroy(mainEnemy.transform.FindChild("Checker").GetComponent<EnemyCheckSphere>());
        Destroy(mainEnemy.GetComponent<Enemy>());
        mainEnemy.GetComponent<NavMeshAgent>().Warp(hero.transform.position + Vector3.forward * 10);
        mainEnemy.GetComponent<NavMeshAgent>().speed = 3;
        mainEnemy.GetComponent<NavMeshAgent>().destination = mainEnemy.transform.position;

        mainEnemy.GetComponent<Animator>().Play("Spear_Casual_walk"); 

        yield return StartCoroutine(Act1Startup.FadeScreenToWhite(5));

        mainEnemy.GetComponent<NavMeshAgent>().destination = hero.GetComponent<Collider>().ClosestPointOnBounds(mainEnemy.transform.position);

        while (Vector3.Distance(mainEnemy.transform.position, hero.transform.position) >
               mainEnemy.GetComponent<NavMeshAgent>().stoppingDistance + 1.8f)
        {
            mainEnemy.transform.LookAt(hero.transform);
            yield return null;
        }
        mainEnemy.GetComponent<Animator>().Play("IDLE");
        mainEnemy.GetComponent<NavMeshAgent>().destination = mainEnemy.transform.position;
        mainEnemy.GetComponent<Animator>().Play("Spear_Deep_Trust");
        yield return new WaitForSeconds(0.4f);
        hero.transform.FindChild("Blood").GetChild(0).GetComponent<ParticleSystem>().Play();
        hero.transform.FindChild("Blood").GetChild(0).GetChild(0).GetComponent<ParticleSystem>().Play();
        mainEnemy.GetComponent<Animator>().CrossFade("IDLE", 0.2f);
        hero.GetComponent<Animator>().Play("Spear_Knocked_backward_onPlace");
        yield return new WaitForSeconds(1.4f);
        hero.GetComponent<Animator>().Play("Spear_Down_back");
        yield return new WaitForSeconds(1.5f);

        StopCoroutine(cor);

        var mask = mainEnemy.transform.GetComponentsInChildren<Transform>().ToList().Find(t => t.name == "Mask");
        var eyes = hero.transform.GetComponentsInChildren<Transform>().ToList().Find(t => t.name == "HeroEyes");
        Camera.main.transform.position = eyes.transform.position;
        Camera.main.transform.rotation = eyes.transform.rotation;
        eyes.GetComponent<Camera>().depth = -200;
        var savedRot = Camera.main.transform.rotation;
        Camera.main.transform.LookAt(mask);

        var rotAngle = Camera.main.transform.rotation;
        Camera.main.transform.rotation = savedRot;

        while (Quaternion.Angle(Camera.main.transform.rotation, rotAngle) > 1)
        {
            Camera.main.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, rotAngle, 0.1f);
            yield return null;
        }

        while (mask.GetComponent<Renderer>().material.color.a > 0.1f)
        {
            var color = mask.GetComponent<Renderer>().material.color;
            mask.GetComponent<Renderer>().material.color = color - new Color(0,0,0,0.05f);
            mask.GetComponent<Renderer>().material.SetFloat("_Metallic", mask.GetComponent<Renderer>().material.GetFloat("_Metallic")+0.35f*0.05f);
            yield return null;
        }
        mask.gameObject.SetActive(false);
        yield return new WaitForSeconds(1);
        yield return StartCoroutine(Act1Startup.FadeScreenToBlack(2));
        Unlock();
        GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/EscMenu").GetComponent<Canvas>().enabled = true;
        Time.timeScale = 0;
    }

    IEnumerator SaveCameraPosition()
    {
        var eyes = hero.transform.GetComponentsInChildren<Transform>().ToList().Find(t => t.name == "HeroEyes");
        Camera.main.enabled = true;
        eyes.GetComponent<Camera>().enabled = true;
        while (true)
        {
            yield return null;
        }
    }
}
