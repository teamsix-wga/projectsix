﻿using UnityEngine;
using System.Collections;

public class Act2HeroEnterVillageTrigger : MonoBehaviour {
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            GameObject.FindGameObjectWithTag("CutsceneManager").SendMessage("Act2TriggerHeroEnterVillage");
            Destroy(gameObject);
        }
    }
}
