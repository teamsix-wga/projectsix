﻿using UnityEngine;
using System.Collections;

public class Act1Scene3HouseInAbandonedVillage : MonoBehaviour
{
    bool said = false;
    public Vector3 savedCameraPos, savedCameraEulAngles;
    GameObject houseCamera;
    public bool inHouse, heroWantExit, heroEnterHouse;

    void Start()
    {
        houseCamera = GameObject.Find("CameraInAbandonedHouse");
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            if (!said)
            {
                said = true;
                StartCoroutine(col.GetComponent<BubbleDialogue>().Say("Cколько здесь разного добра! Нужно будет послать отряд за всем этим.", 3.5f, "", 0));
            }
            heroEnterHouse = true;
        }
    }

    public void HeroEnterColliderPorch(GameObject col)
    {
        if (!inHouse)
        {
            inHouse = true;
            savedCameraPos = Camera.main.transform.position;
            savedCameraEulAngles = Camera.main.transform.localEulerAngles;
            ClickToMove.canUnselect = false;
            Camera3dPerson.canMoveCam = false;
            Camera3dPerson.cameraLooksAtPlayer = true;
            Camera.main.transform.position = houseCamera.transform.position;
            Camera.main.transform.rotation = houseCamera.transform.rotation;
            Camera.main.GetComponent<Camera3dPerson>().SwitchSelect();
            Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Add(col);
            GameObject.Find("CutScenesManager").SendMessage("WarriorsWaitOutsideHouse");
        }
        else
            Exit();
    }

    public void HeroExitColliderPorch(GameObject col)
    {
        if(!heroEnterHouse)
            Exit();
    }

    public void Exit()
    {
        heroEnterHouse = false;
        heroWantExit = true;
        inHouse = false;
        ClickToMove.canUnselect = true;
        Camera3dPerson.canMoveCam = true;
        Camera3dPerson.cameraLooksAtPlayer = false;
        if (!Act2.needToReturnCamera)
        {
            Camera.main.transform.position = savedCameraPos;
            Camera.main.transform.localEulerAngles = savedCameraEulAngles;
            GameObject.Find("CutScenesManager").SendMessage("HeroExitAbandonedHouse");
        }
    }

    void Update()
    {
        if (inHouse)
        {
            ClickToMove.canUnselect = false;
            Camera3dPerson.canMoveCam = false;
            Camera3dPerson.cameraLooksAtPlayer = true;
            Camera.main.transform.position = houseCamera.transform.position;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            heroEnterHouse = false;
        }
    }
}
