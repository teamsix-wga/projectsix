﻿using UnityEngine;
using System.Collections;

public class Act0ArrowScript : MonoBehaviour
{
    public bool arrowFly = false;
    GameObject hero;
    GameObject mainCam;

    void Start()
    {
        mainCam = GameObject.FindGameObjectWithTag("MainCamera");
        hero = GameObject.Find("HorseHero");
    }

	void Update () {
	    if(arrowFly)
            transform.LookAt(transform.position + GetComponent<Rigidbody>().velocity);

        if(Vector3.Distance(hero.transform.position, transform.position) <= 20)
            mainCam.SendMessage("ArrowNear");
	}
}
