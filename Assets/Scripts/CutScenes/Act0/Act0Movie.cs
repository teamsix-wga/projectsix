﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using FMOD.Studio;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Act0Movie : MonoBehaviour {
    GameObject[] warriors;
    private GameObject horse;
    private bool arrowFlying, playing = false;
    private GameObject heroCam;
    GameObject fadeScreen;
    List<GameObject> horsemans = new List<GameObject>();
    IEnumerator Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        fadeScreen = GameObject.Find("FadeScreen");
        warriors = GameObject.FindGameObjectsWithTag("Unit");
        horse = GameObject.Find("HorseHero");
        horse.GetComponent<Animator>().Play("Horse_Idle");
        horsemans = GameObject.FindGameObjectsWithTag("HorseMan").ToList();
        heroCam = GameObject.Find("HeroCamera");

        foreach (var unit in warriors)
        {
            unit.GetComponent<NavMeshAgent>().SetDestination(unit.transform.position + new Vector3(0, 0, 300));
            unit.GetComponent<Animation>().Play("SwordCombatReady");
            unit.GetComponent<Animation>()["SwordCombatReady"].time = Random.Range(0, unit.GetComponent<Animation>()["SwordCombatReady"].length);
            unit.GetComponent<Animation>().wrapMode = WrapMode.Loop;
            unit.GetComponent<NavMeshAgent>().Stop();
        }

        foreach (var man in horsemans)
        {
            man.GetComponent<Animator>().Play("Horse_Idle");
            var unit = man.transform.FindChild("Warrior").gameObject;
            man.GetComponent<NavMeshAgent>().SetDestination(unit.transform.position + new Vector3(0, 0, 300));
            unit.GetComponent<Animation>().Play("Horseman_Idle");
            unit.GetComponent<Animation>()["Horseman_Idle"].time = Random.Range(0, unit.GetComponent<Animation>()["Horseman_Idle"].length);
            unit.GetComponent<Animation>().wrapMode = WrapMode.Loop;
            man.GetComponent<NavMeshAgent>().Stop();
        }

        horse.GetComponent<NavMeshAgent>().speed = 7;
        var heroCenterPos = GameObject.Find("HeroComesHere").transform.position;
        horse.GetComponent<NavMeshAgent>().SetDestination(heroCenterPos);
        horse.GetComponent<Animator>().Play("Horse_Walk");

        var heroLookStraight = GameObject.Find("HeroLookStraight");

        while (Vector3.Distance(horse.GetComponent<NavMeshAgent>().destination, horse.transform.position) >
               horse.GetComponent<NavMeshAgent>().stoppingDistance)
        {
            heroCam.transform.rotation = Quaternion.Lerp(heroCam.transform.rotation, heroLookStraight.transform.rotation,
                0.01f);
            yield return new WaitForEndOfFrame();
        }

        heroCam.transform.rotation = Quaternion.Lerp(heroCam.transform.rotation, heroLookStraight.transform.rotation, 0.7f);

        horse.GetComponent<NavMeshAgent>().SetDestination(GameObject.Find("HeroLooksHere").transform.position);

        yield return new WaitForSeconds(0.5f);
        SetUnitsDest();
        SetHorseDest();
        yield return new WaitForSeconds(4.5f);
        StartCoroutine(SetArrowPos());
        var heroLookAtSky = GameObject.Find("HeroLookAtSky");
        yield return new WaitForSeconds(1.5f);
        while (heroCam.transform.rotation != heroLookAtSky.transform.rotation)
        {
            heroCam.transform.rotation = Quaternion.Lerp(heroCam.transform.rotation, heroLookAtSky.transform.rotation,
    0.009f);
            yield return null;
        }
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    void SetUnitsDest()
    {
        foreach (var unit in warriors)
        {
            unit.GetComponent<Animation>().CrossFade("GENRun");
            unit.GetComponent<Animation>()["GENRun"].time = Random.Range(0, unit.GetComponent<Animation>()["GENRun"].length);
            unit.GetComponent<NavMeshAgent>().Resume();
        }
    }

    void SetHorseDest()
    {
        
        horse.GetComponent<NavMeshAgent>().SetDestination(horse.transform.position + new Vector3(0, 0, 300));
        horse.GetComponent<Animator>().Play("Horse_Run");
        horse.GetComponent<NavMeshAgent>().speed = 22;
        foreach (var man in horsemans)
        {
            var unit = man.transform.FindChild("Warrior").gameObject;
            unit.GetComponent<Animation>().CrossFade("Horseman_Riding_Galloping_Horse");
            unit.GetComponent<Animation>()["Horseman_Riding_Galloping_Horse"].time = Random.Range(0, unit.GetComponent<Animation>()["Horseman_Riding_Galloping_Horse"].length);
            unit.GetComponent<Animation>().wrapMode = WrapMode.Loop;
            man.GetComponent<NavMeshAgent>().speed = 27;
            man.GetComponent<NavMeshAgent>().Resume();
            man.GetComponent<Animator>().Play("Horse_Run");
        }
    }

    IEnumerator SetArrowPos()
    {
        var arrows = GameObject.FindGameObjectsWithTag("Arrow");
        foreach (var arrow in arrows)
        {
            arrow.GetComponent<Rigidbody>().AddForce(Vector3.up * 1000 - Vector3.forward * 1000);
            arrow.GetComponent<Act0ArrowScript>().arrowFly = true;
            yield return new WaitForSeconds(Random.Range(0.01f, 0.02f));
        }
    }

    public void ArrowNear()
    {
        arrowFlying = true;
    }

    void Update()
    {
        if (arrowFlying)
        {
            horse.GetComponent<Animator>().Play("Horse_Run");
        }
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space) || (!playing && arrowFlying))
        {
            StartCoroutine(FadeScreenToBlack(5f));
        }
    }

    IEnumerator FadeScreenToBlack(float speed)
    {
        playing = true;
        fadeScreen.GetComponent<Image>().enabled = true;
        fadeScreen.GetComponent<CanvasGroup>().alpha = 0;

        EventDescription evt;
        FMODUnity.RuntimeManager.StudioSystem.getEvent(Camera.main.GetComponent<FMODUnity.StudioEventEmitter>().Event, out evt);
        EventInstance[] inst;
        evt.getInstanceList(out inst);

        while (fadeScreen.GetComponent<CanvasGroup>().alpha <= 0.99f)
        {
            inst[0].setVolume(1 - fadeScreen.GetComponent<CanvasGroup>().alpha);

            fadeScreen.GetComponent<CanvasGroup>().alpha += speed * Time.deltaTime;
            yield return null;
        }
        fadeScreen.GetComponent<CanvasGroup>().alpha = 1;
        SceneManager.LoadScene("Act1");
        yield return null;
    }
}
