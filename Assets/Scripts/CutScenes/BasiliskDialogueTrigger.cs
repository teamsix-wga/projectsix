﻿using UnityEngine;
using System.Collections;

public class BasiliskDialogueTrigger : MonoBehaviour {
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            GameObject.Find("CutScenesManager").SendMessage("CaveDialogueWithBasilisk");
            Destroy(gameObject);
        }
    }
}
