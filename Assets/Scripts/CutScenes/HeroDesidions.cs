﻿using UnityEngine;
using System.Collections;

public class HeroDesidions : MonoBehaviour
{
    public static bool lustPassed = false;
    public static bool pridePassed = false;
    public static bool killedWizard = false;

    public void LustPassed() // не поддался похоти
    {
        lustPassed = true;
        gameObject.SendMessage("LustEnd");
    }

    public void LustNotPassed() // поддался похоти
    {
        lustPassed = false;
        gameObject.SendMessage("LustEnd");
    }
}
