﻿using UnityEngine;
using System.Collections;

public class LostHouseTrigger : MonoBehaviour {
    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            transform.parent.SendMessage("HeroEnterColliderPorch", col.gameObject);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            transform.parent.SendMessage("HeroExitColliderPorch", col.gameObject);
        }
    }
}
