﻿using UnityEngine;
using System.Collections;

public class Act1Squad3Trigger2 : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.GetComponent<FriendUnit>() && col.gameObject.GetComponent<FriendUnit>().squadGO.name == "Squad3")
        {
            GetComponent<Collider>().enabled = false;
            StartCoroutine(col.gameObject.GetComponent<BubbleDialogue>().Say("Похоже, что это мирное поселение! Вернёмся в лагерь и доложим Господину.", 3.5f, "Dialog2", 2));
            Destroy(gameObject);
        }
    }
}
