﻿using UnityEngine;
using System.Collections;

public class LostWorkers : MonoBehaviour
{

    NavMeshAgent agent;
    Animation anim;

	void Awake()
	{
	    agent = GetComponent<NavMeshAgent>();
	    anim = GetComponent<Animation>();
	}
	
	void Update ()
	{
	    if (Vector3.Distance(agent.destination, transform.position) > agent.stoppingDistance)
	        anim.Play("Walk");
	    else
	        anim.Play("Idle");
	}
}
