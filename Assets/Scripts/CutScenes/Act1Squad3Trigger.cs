﻿using UnityEngine;
using System.Collections;

public class Act1Squad3Trigger : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.GetComponent<FriendUnit>() && col.gameObject.GetComponent<FriendUnit>().squadGO.name == "Squad3")
        {
            GetComponent<Collider>().enabled = false;
            StartCoroutine(SquadTalk(col.gameObject));
        }
    }

    IEnumerator SquadTalk(GameObject warrior)
    {
        StartCoroutine(warrior.GetComponent<BubbleDialogue>().Say("Посмотрите! Я вижу жилую деревню на горизонте!", 2.8f, "Dialog2", 0));
        yield return new WaitForSeconds(2.5f);
        GameObject warrior2 = warrior.GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().units[0];
        int i = 1;
        while (warrior == warrior2)
            warrior2 = warrior.GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().units[i++];
        StartCoroutine(warrior2.GetComponent<BubbleDialogue>().Say("Давайте подойдём ближе и узнаем побольше об этом поселении!", 3f, "Dialog2", 1));
        Destroy(gameObject);
    }
}
