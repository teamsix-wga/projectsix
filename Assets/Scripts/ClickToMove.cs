using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ClickToMove : MonoBehaviour
{
    public static bool attacking;
    public bool settingFlag;
    bool goingToEnemy;
    public NavMeshAgent agent;
    public Vector3 waitPosition;
    public GameObject pointer;
    public static bool canUnselect = true;
    GameObject uiCanBeDisabled;

    void Awake()
    {
        uiCanBeDisabled = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff").gameObject;
        pointer = Instantiate(Resources.Load("Pointer"), Vector3.zero, Quaternion.identity) as GameObject;
        pointer.SetActive(false);
        agent = GetComponent<NavMeshAgent>();
        agent.speed = GetComponent<Parameters>().speed;
        attacking = false;
        goingToEnemy = false;
        settingFlag = false;
        agent.destination = waitPosition = transform.position;
        Camera3dPerson.hero = gameObject;
        if (!Camera.main.GetComponent<Camera3dPerson>().UnitList.Exists(unit => (unit == gameObject)))
            Camera.main.GetComponent<Camera3dPerson>().UnitList.Add(gameObject);
    }

    void LocatePosition()
    {
        RaycastHit hit;
        // Пускаем луч
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // если попали куда-то
        if (Physics.Raycast(ray, out hit, 1000))
        {
            if (hit.collider.gameObject.CompareTag("Enemy") || hit.collider.gameObject.CompareTag("EnemyBuild"))
            {
                Fighter.Opponent = hit.collider.gameObject;
                StopCoroutine("waitWhileAttacking");
                if (!attacking)
                    agent.destination = waitPosition = Fighter.Opponent.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
                else
                    waitPosition = Fighter.Opponent.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
                goingToEnemy = true;
            }
            else
            {
                NavMeshPath path = new NavMeshPath();
                if (agent.CalculatePath(hit.point, path))
                {
                    // Присваиваем переменной место, куда попали лучом
                    if (!attacking)
                    {
                        Fighter.Opponent = null;
                        agent.destination = waitPosition = hit.point;
                    }
                    else
                    {
                        waitPosition = hit.point;
                        StartCoroutine("waitWhileAttacking");
                    }
                    goingToEnemy = false;
                }
            }
            pointer.transform.position = waitPosition+new Vector3(0,1f,0);
            pointer.SetActive(true);
        }
    }

    IEnumerator waitWhileAttacking()
    {
        while(attacking)
            yield return new WaitForEndOfFrame();
        Fighter.Opponent = null;
        goingToEnemy = false;
    }

    void MoveToPosition()
    {
        // если мы идём к врагу
        if (goingToEnemy && Fighter.Opponent)
            agent.destination = waitPosition = Fighter.Opponent.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
        
        if(!goingToEnemy)
            agent.destination = waitPosition;

        pointer.transform.position = waitPosition + new Vector3(0, 1f, 0);
    }

    void Update()
    {
        if (!attacking && (GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.name == "IDLE" || GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.name == "Spear_run"))
            if (Vector3.Distance(transform.position, agent.destination) <= agent.stoppingDistance + 0.3f)
            {
                pointer.SetActive(false);
                GetComponent<Animator>().Play("IDLE");
            }
            else if (!agent.pathPending && agent.pathStatus != NavMeshPathStatus.PathInvalid)
            {
                if (uiCanBeDisabled.activeInHierarchy && !pointer.activeInHierarchy && GetComponent<Parameters>().selected)
                    pointer.SetActive(true);
                GetComponent<Animator>().Play("Spear_run");
            }

        if (agent.isPathStale)
            agent.destination = waitPosition = transform.position;

        if(!attacking && Fighter.Opponent)
        {
            agent.destination = Fighter.Opponent.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
            goingToEnemy = true;
        }

        if (Input.GetMouseButton(1) && GetComponent<Parameters>().selected && !EventSystem.current.IsPointerOverGameObject() && !Input.GetKey(KeyCode.LeftAlt))
            LocatePosition();

        if (!attacking && !GetComponent<Parameters>().died) 
            MoveToPosition();

        if (!uiCanBeDisabled.activeInHierarchy && pointer.activeInHierarchy)
            pointer.SetActive(false);
    } 

    public void ObjectSelected()
    {
        pointer.SetActive(true);
    }

    public void ObjectUnselected()
    {
        pointer.SetActive(false);
    }
}
