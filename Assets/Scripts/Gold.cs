﻿using UnityEngine;
using System.Collections;

public class Gold : MonoBehaviour {
    public float currentGold;
    // сколько в секунду приходит золота
    public float goldPerSecond;
    // сколько дадут денег за убийство
    public float costOfDie;

    public void AddGold(float gold)
    {
        currentGold += gold;
    }
}
