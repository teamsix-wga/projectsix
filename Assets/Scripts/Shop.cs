﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class Shop : MonoBehaviour
{
    public GameObject inventorySlot;
    public GameObject inventoryItem;
    public static Shop openedShop;
    public float shopOpensRange;
    float destroyedTime;
    int slotsAmount;
    public static bool shopOpened;
    GameObject ui;
    public List<Item> items = new List<Item>();
    public List<Item> shopItems = new List<Item>();
    public List<GameObject> slots = new List<GameObject>();
    public List<GameObject> shopSlots = new List<GameObject>();
    GameObject textGold;
    GameObject hero;
    public List<Item> assortiment, uniqueAssortiment;

    void Start()
    {
        ui = GameObject.Find("ShopUI");
        slotsAmount = 21;
        items = ui.GetComponent<ShopInventory>().items;
        slots = ui.GetComponent<ShopInventory>().slots;
        shopItems = ui.GetComponent<ShopInventory>().shopItems;
        shopSlots = ui.GetComponent<ShopInventory>().shopSlots;
        ui.GetComponent<Canvas>().enabled = false;
    }

    void Awake()
    {
        hero = GameObject.FindGameObjectWithTag("Player");
        textGold = GameObject.Find("GoldText");
        shopOpened = false;
        uniqueAssortiment = new List<Item>();
        
        assortiment = GameObject.Find("Inventory").GetComponent<ItemDatabase>().FetchItemsForShop(Item.ItemQuality.Common);
    }

    void Update()
    {
        if (GetComponent<Parameters>().died && destroyedTime <= Time.time)
        {
            Destroy(gameObject);
            if (GetComponent<Construction>().buildEffect)
                Destroy(GetComponent<Construction>().buildEffect);
            Destroy(this);
        }

        if (!GetComponent<Parameters>().died)
        {
            // если нажата ЛКМ
            if (Input.GetMouseButtonDown(0) && GetComponent<Parameters>().selected)
            {
                // пускаем луч из мышки
                RaycastHit hit = Builder.CastFromCursor();
                // если не попали по зданию
                if (hit.collider != null && !hit.collider.CompareTag("FriendBuild") && !EventSystem.current.IsPointerOverGameObject())
                {
                    GetComponent<Parameters>().selected = false;
                }
            }

            if(Input.GetKey(KeyCode.Escape) && shopOpened)
            {
                CloseShop();
            }

            if(shopOpened)
                textGold.GetComponent<Text>().text = "<color=#FFA000>Золото: <b>"+hero.GetComponent<Gold>().currentGold+"</b></color>";
        }
    }

    public void OpenShop()
    {
        openedShop = this;
        Time.timeScale = 0;
        shopOpened = true;
        UpdateAssortiment();
        ui.GetComponent<Canvas>().enabled = true;
    }

    public void CloseShop()
    {
        openedShop = null;
        Time.timeScale = 1;
        shopOpened = false;
        ui.GetComponent<Canvas>().enabled = false;
    }

    public void UpdateAssortiment()
    {
        for (int i = 0; i < slots.Count; i++)
        {
            items[i] = new Item();
            if (slots[i].transform.childCount == 1)
            {
                Destroy(slots[i].transform.GetChild(0).gameObject);
            }
        }
        for (int i = 0; i < shopSlots.Count; i++)
        {
            shopItems[i] = new Item();
            if (shopSlots[i].transform.childCount == 1)
            {
                Destroy(shopSlots[i].transform.GetChild(0).gameObject);
            }
        }

        slotsAmount = 21 + hero.GetComponent<InventoryUnit>().slotsCount + 5;

        for (int i = 0; i < assortiment.Count; i++)
        {
            if (i >= slotsAmount)
                break;
            AddItemToShop(assortiment[i], i + hero.GetComponent<InventoryUnit>().slotsCount + 5, false);
        }
        for(int i = 0; i < uniqueAssortiment.Count; i++){
            if( i >= slotsAmount)
                break;
            AddItemToShop(uniqueAssortiment[i], i + hero.GetComponent<InventoryUnit>().slotsCount + 5 + assortiment.Count, false);
        }
        
        for (int i = 0; i < hero.GetComponent<InventoryUnit>().items.Count; i++)
        {
            AddItemToShop(hero.GetComponent<InventoryUnit>().items[i], hero.GetComponent<InventoryUnit>().items[i].slotID, true);
        }
    }

    public void AddItemToShop(Item itemToAdd, int slotID, bool inven)
    {
        itemToAdd.slotID = slotID;
        GameObject itemObj = Instantiate(inventoryItem);
        itemObj.GetComponent<ShopItem>().item = itemToAdd;
        itemObj.GetComponent<ShopItem>().slot = slotID;
        itemObj.GetComponent<ShopItem>().inventory = this;
        if (!inven)
        {
            itemObj.transform.SetParent(shopSlots[slotID - (hero.GetComponent<InventoryUnit>().slotsCount + 5) ].transform);
            shopItems[slotID] = itemToAdd;
            shopItems[slotID].slotID = slotID;
        }
        else
        {
            itemObj.transform.SetParent(slots[slotID].transform);
            items[slotID] = itemToAdd;
            items[slotID].slotID = slotID;
        }
        itemObj.GetComponent<Image>().sprite = itemToAdd.sprite;
        itemObj.name = itemToAdd.Name;
        if (itemToAdd.Stackable)
        {
            itemObj.GetComponent<ShopItem>().amount++;
            itemObj.GetComponent<ShopItem>().transform.GetChild(0).GetComponent<Text>().text = "1";
        }
        itemObj.transform.localPosition = Vector2.zero;
        itemObj.transform.localScale = Vector3.one;
    }

    void OnMouseOver()
    {
        if (!EventSystem.current.IsPointerOverGameObject() && !GetComponent<Parameters>().died && GetComponent<Construction>().builded)
        {
            if(Vector3.Distance(hero.transform.position, gameObject.GetComponent<Collider>().ClosestPointOnBounds(hero.transform.position)) <= shopOpensRange &&
                !BuildingMenu.opened && Input.GetMouseButton(1))
            {
                OpenShop();
            }
            if(Input.GetMouseButton(0))
                GetComponent<Parameters>().selected = true;
        }
    }

    public void GetHit(AttackStructure atStr)
    {
        float damage = atStr.damage;
        if (GetComponent<Parameters>().defence <= damage)
            GetComponent<Parameters>().health -= damage - GetComponent<Parameters>().defence;
        GetComponent<Parameters>().health = GetComponent<Parameters>().health;
        if (!GetComponent<Parameters>().died && GetComponent<Parameters>().health <= 0)
        {
            hero.GetComponent<Builder>().listBuild.Remove(gameObject);
            destroyedTime = Time.time;
            GetComponent<Parameters>().died = true;
            Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Remove(gameObject);
        }
    }
}
