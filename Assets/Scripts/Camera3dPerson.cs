﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class Camera3dPerson : MonoBehaviour {
    public List<GameObject> SelectedUnit;
	public List<GameObject> UnitList;
    public List<GameObject> lumberjacks;
    Vector2 startPos, endPos;
    bool drawing;
    Rect rect;
    bool selUnitInSquad;
    public int UnitInSquadNum;
    private float x = 0.0f;
    private float y = 0.0f;
    public float rotationDampening;
    public float xSpeed = 250.0f;
    public float ySpeed = 120.0f;
    public bool lumberFlagPlaced;
    public GameObject lumberFlag;
    public static GameObject hero, townhall;
    public bool heroDied = false;
    public float timeToFadeDie = 5;
    bool diedStop = false;
    GameObject escMenuGO;
    public static bool canDraw = true;
    public static List<GameObject> squadsGO;
    private GameObject lumberFlagGO;
    private GameObject squadCompositionGO;
    public static bool canMoveCam = true;
    GameObject unitPanelUI;
    GameObject inventoryOpenButton;
    public static bool cameraLooksAtPlayer = false, isometric = false;

    #region RPGCameraVariables
        public static bool RPGMode = false;
        bool RPGRotating = false;
        Vector3 caveCamSavedPos = Vector3.zero;
        bool RPGModeEnabledNow = true, RPGcameraMoved = false;
    #endregion

    void Awake()
    {
        inventoryOpenButton = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/SkillPanel/Inventory").gameObject;
        unitPanelUI = GameObject.Find("UnitPanelUI");
        squadCompositionGO = Resources.Load("SquadComposition") as GameObject;
        lumberFlagGO = Resources.Load("LumberFlag") as GameObject;
        escMenuGO = GameObject.Find("EscMenu");
        townhall = GameObject.FindGameObjectWithTag("TownHall");
        hero = GameObject.FindGameObjectWithTag("Player");
        StartCoroutine(RotateRPGCam());
        if (!EscMenu.loading)
        {
            rotationDampening = 3.0f;
            selUnitInSquad = lumberFlagPlaced = false;
            if (UnitList.Count == 0 && hero)
                UnitList = new List<GameObject>() { hero };
            else
            if (UnitList.Count == 0)
                UnitList = new List<GameObject>();
            
            UnitInSquadNum = 0;
            squadsGO = new List<GameObject>();
        }
    }

    bool CanMoveCamera()
    {
        return !escMenuGO.GetComponentsInChildren<Button>()[0].GetComponent<EscMenu>().menuActive &&
               !CommandLine.enableConsole && !EventSystem.current.IsPointerOverGameObject() && !InventoryUnit.inventoryActive && !Shop.shopOpened && 
               !Cutscene2Trigger.cutscenePlaying && !Act1Startup.ruinsPlaying &&
               !diedStop && !Act1Startup.animPlaying && !DialoguesTalker.talking && canMoveCam;
    }

    void LateUpdate()
    {
        if (heroDied && !diedStop)
        {
            Vector3 po = hero.transform.position;
            transform.position = new Vector3(po.x, transform.position.y+1, po.z);
            transform.localEulerAngles = new Vector3(90,0,0);
            GameObject.Find("DieDramatic").transform.GetChild(0).position = new Vector3(transform.position.x, transform.position.y-2, transform.position.z);
        }
        else
        if(CanMoveCamera())
            {
                // если колёсико мыши крутится назад
                if (Input.GetAxis("Mouse ScrollWheel") < 0 && !hero.GetComponent<Builder>().building &&
                    !Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), 5))
                {
                    transform.position += Vector3.up*2;
                }
                if (Input.GetAxis("Mouse ScrollWheel") > 0 && !hero.GetComponent<Builder>().building &&
                    !Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), 5))
                {
                    transform.position += Vector3.down*2;
                }

                if (((Input.mousePosition.y >= Screen.height - 10) && (Input.mousePosition.y <= Screen.height)) ||
                    Input.GetKey(KeyCode.W))
                {
                    float trY = transform.position.y;
                    float ox = transform.localEulerAngles.x;
                    transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y,
                        transform.localEulerAngles.z);
                    if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), 3))
                    {
                        transform.Translate(Vector3.forward);
                        transform.position = new Vector3(transform.position.x, trY, transform.position.z);
                    }
                    else if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), 3))
                        transform.Translate(Vector3.up);
                    transform.localEulerAngles = new Vector3(ox, transform.localEulerAngles.y,
                        transform.localEulerAngles.z);
                }
                if ((((Input.mousePosition.y >= 0) && (Input.mousePosition.y <= 10)) || (Input.GetKey(KeyCode.S))))
                {
                    float trY = transform.position.y;
                    float ox = transform.localEulerAngles.x;
                    transform.localEulerAngles = new Vector3(0, transform.localEulerAngles.y,
                        transform.localEulerAngles.z);
                    if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.back), 3))
                    {
                        transform.Translate(Vector3.back);
                        transform.position = new Vector3(transform.position.x, trY, transform.position.z);
                    }
                    else if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), 3))
                        transform.Translate(Vector3.up);

                    transform.localEulerAngles = new Vector3(ox, transform.localEulerAngles.y,
                        transform.localEulerAngles.z);
                }
                if ((((Input.mousePosition.x >= 0) && (Input.mousePosition.x <= 10)) || (Input.GetKey(KeyCode.A))))
                {
                    if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.left), 3))
                        transform.Translate(Vector3.left);
                    else if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), 3))
                        transform.Translate(Vector3.up);
                }
                if ((((Input.mousePosition.x >= Screen.width - 10) && (Input.mousePosition.x <= Screen.width)) ||
                     (Input.GetKey(KeyCode.D))))
                {
                    if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), 3))
                        transform.Translate(Vector3.right);
                    else if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), 3))
                        transform.Translate(Vector3.up);
                }

                if (Input.GetKey(KeyCode.Q))
                {
                    transform.Rotate(new Vector3(0f, transform.position.y, 0f), -2.5f);
                }
                if (Input.GetKey(KeyCode.E))
                {
                    transform.Rotate(new Vector3(0f, transform.position.y, 0f), 2.5f);
                }

                Vector3 angles = transform.eulerAngles;
                x = angles.y;
                y = angles.x;

                if (Input.GetMouseButton(1) && (!hero.GetComponent<Parameters>().selected || (hero.GetComponent<Parameters>().selected && Input.GetKey(KeyCode.LeftAlt))))
                {
                    x += Input.GetAxis("Mouse X")*xSpeed*0.02f;
                    y -= Input.GetAxis("Mouse Y")*ySpeed*0.02f;
                }

                y = ClampAngle(y, -1, 359);

                Quaternion rotation = Quaternion.Euler(y, x, 0);
                transform.rotation = rotation;
            }
        if(RPGMode)
        {
            if (Input.GetKey(KeyCode.Q))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, hero.transform.FindChild("RPGCameraLeft").rotation, 0.1f);
                RPGRotating = true;
            }
            if (Input.GetKey(KeyCode.E))
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, hero.transform.FindChild("RPGCameraRight").rotation, 0.1f);
                RPGRotating = true;

            }

            if (Input.GetKeyUp(KeyCode.E) || Input.GetKeyUp(KeyCode.Q))
                RPGRotating = false;


            if (RPGModeEnabledNow) // если только что включили режим RPG
            {
                transform.position = hero.transform.FindChild("CaveCamera").position; // переместили камеру
                caveCamSavedPos = hero.transform.FindChild("CaveCamera").position; // сохранили положение
                RPGModeEnabledNow = false; 
                RPGcameraMoved = false;
            }
            else
            {
                if (caveCamSavedPos != hero.transform.FindChild("CaveCamera").position)
                {
                    caveCamSavedPos = hero.transform.FindChild("CaveCamera").position;
                    RPGcameraMoved = false;
                }
            }
            
            RaycastHit hit;
            bool somethingWrong = false;
            Ray ray = new Ray(transform.position, (hero.transform.FindChild("HeroHead").position - transform.position).normalized);
            if (Physics.Raycast(ray, out hit, 100f) && !hit.collider.CompareTag("Player") && !hit.collider.CompareTag("Unit") && !hit.collider.CompareTag("Lumberjack") && !hit.collider.CompareTag("NPC"))
            {
                transform.position += (hero.transform.FindChild("HeroHead").position - transform.position).normalized*2;
                RPGcameraMoved = true;
                somethingWrong = true;
            }

            if (!RPGcameraMoved)
            {
                // возвращаем камеру к CaveCamera
                if (Vector3.Distance(transform.position, caveCamSavedPos) < 2f)
                    transform.position = caveCamSavedPos;
                transform.position += (caveCamSavedPos - transform.position).normalized;
            }

            if (transform.position != caveCamSavedPos && !somethingWrong)
            {
                if (
                    !Physics.Raycast(new Ray(transform.position, caveCamSavedPos), out hit, 2f) &&
                    !Physics.Raycast(new Ray(transform.position + (caveCamSavedPos - transform.position).normalized*2, hero.transform.position), out hit, 2f)
                   )
                    transform.position += (caveCamSavedPos - transform.position).normalized;
            }
        }
        else
            RPGModeEnabledNow = false;

        if (cameraLooksAtPlayer)
        {
            transform.LookAt(hero.transform);
        }

        if (isometric)
        {
            transform.position = hero.transform.position - transform.forward * 50;
        }
    }

    IEnumerator RotateRPGCam()
    {
        while (true) { 
            if (RPGMode && !RPGRotating)
                transform.rotation = Quaternion.Lerp(transform.rotation, hero.transform.FindChild("RPGCameraForward").rotation, 0.1f);
            yield return new WaitForEndOfFrame();
        }
    }

    public static float ClampAngle(float angle, float min, float max)
    {
       if (angle < -360)
            angle = 0;
        if (angle > 360)
            angle = 0;
        return Mathf.Clamp(angle, min, max);
    }

    bool CanDrawRect()
    {
        return (Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))&& !EventSystem.current.IsPointerOverGameObject() && !hero.GetComponent<Fighter>().aoe && !InventoryUnit.inventoryActive
               && !GetComponent<Squads>().setFlag && !GetComponent<Squads>().flagSetted && !hero.GetComponent<Builder>().building && !Act1Startup.ruinsPlaying && 
               Cursor.visible && Cursor.lockState == CursorLockMode.None && canDraw && 
               !hero.GetComponent<ClickToMove>().settingFlag && !Kazarma.flagSetted && !Shop.shopOpened && !CommandLine.enableConsole && !Act1Startup.animPlaying
               && !DialoguesTalker.talking && !Cutscene2Trigger.cutscenePlaying && ClickToMove.canUnselect;
    }

    void Update() {      
        if(Input.GetMouseButtonDown(0) && CanDrawRect())
        {
            if ((lumberFlag && !lumberFlag.GetComponent<LumberFlag>().settingSecond) || !lumberFlag)
            {
                RaycastHit hit;
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 1000) && !hit.collider.CompareTag("LumberFlag"))
                {
                    startPos = Input.mousePosition;
                    if (!Input.GetKey(KeyCode.LeftControl))
                    {
                        SwitchSelect();
                        drawing = true;
                    }
                }
            }
        }
        if(Input.GetMouseButtonUp(0))
        {
            drawing = false;
        }

        if (drawing && !Input.GetKey(KeyCode.LeftControl))
        {
            SwitchSelect();
            foreach (var unit in UnitList)
            {
                if (IsWithinSelectionBounds(unit) && unit.GetComponent<Parameters>())
                {
                    if (SelectedUnit.Count > 5)
                        break;
                    if(unit.GetComponent<Parameters>().health <= 0)
                        continue;
                    if (unit.CompareTag("Player"))
                    {
                        SelectedUnit.Add(unit);
                        unit.SendMessage("SelectUnit");
                    }
                    if (unit.CompareTag("Unit"))
                    {
                        if (SelectedUnit.Exists(un => un.CompareTag("Player")))
                            break;

                        if (!unit.activeInHierarchy)
                            continue;

                        // если юнит в отряде и юнит в отряде ещё не выделен
                        if (unit.GetComponent<FriendUnit>().inSquad && !selUnitInSquad)
                        {
                            SwitchSelect();
                            // выбран юнит в отряде
                            selUnitInSquad = true;
                            // запоминаем номер отряда
                            GetComponent<Squads>().selectedSquad =
                                UnitInSquadNum = unit.GetComponent<FriendUnit>().numSquad;
                            GetComponent<Squads>().selectedSquad--;
                            unit.GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().CheckSquadReadyToGo();
                            SelectedUnit.Add(unit);
                            unit.SendMessage("SelectUnit");
                        }
                        if (selUnitInSquad && UnitInSquadNum == unit.GetComponent<FriendUnit>().numSquad)
                        {
                            squadsGO.Find(sqd => sqd.name == "Squad" + UnitInSquadNum).GetComponent<Squad>().CheckSquadReadyToGo();
                            SelectedUnit.Add(unit);
                            unit.SendMessage("SelectUnit");
                        }

                        if (!selUnitInSquad)
                        {
                            SelectedUnit.Add(unit);
                            unit.SendMessage("SelectUnit");
                        }
                    }
                }
            }
            if(SelectedUnit.Count == 0)
                foreach (var lumberjack in lumberjacks)
                {
                    if (IsWithinSelectionBounds(lumberjack) && lumberjack.GetComponent<Parameters>())
                    {
                        if (SelectedUnit.Count > 5)
                            break;
                        SelectedUnit.Add(lumberjack);
                        lumberjack.SendMessage("SelectUnit");
                    }
                }
            if(SelectedUnit.Count == 0)
                if (IsWithinSelectionBounds(townhall))
                {
                    SelectedUnit.Add(townhall);
                    townhall.SendMessage("SelectUnit");
                }
            if(SelectedUnit.Count == 0)
                foreach (var build in hero.GetComponent<Builder>().listBuild)
                {
                    if (IsWithinSelectionBounds(build) && build.GetComponent<Parameters>())
                    {
                        SelectedUnit.Add(build);
                        build.SendMessage("SelectUnit");
                    }
                }
        }

        if (Input.GetMouseButtonUp(0) && GetComponent<Squads>().flagSetted)
            GetComponent<Squads>().flagSetted = false;

        if (Input.GetMouseButtonUp(0) && CanDrawRect())
        {
            endPos = Input.mousePosition;
            if (Vector2.Distance(startPos, endPos) < 3)
            {
                RaycastHit hit;
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 1000) && hit.collider.GetComponent<Parameters>())
                {
                    if (!hit.collider.gameObject.activeInHierarchy)
                        return;

                    if (!Input.GetKey(KeyCode.LeftControl))
                        SwitchSelect();
                    else
                    {
                        if (SelectedUnit.Count > 5)
                            return;

                        if (SelectedUnit.Count > 0)
                        {
                            if (hit.collider.CompareTag("Player"))
                                SwitchSelect();
                            else
                            if (hit.collider.CompareTag("Unit"))
                            {
                                if (SelectedUnit.Exists(un => !un.CompareTag("Unit")))
                                    SwitchSelect();

                                if (!hit.collider.GetComponent<FriendUnit>().inSquad && selUnitInSquad)
                                    SwitchSelect();

                                // если юнит в отряде и юнит в отряде ещё не выделен
                                if (hit.collider.GetComponent<FriendUnit>().inSquad && (!selUnitInSquad || (selUnitInSquad && UnitInSquadNum != hit.collider.GetComponent<FriendUnit>().numSquad)))
                                {
                                    SwitchSelect();
                                    // выбран юнит в отряде
                                    selUnitInSquad = true;
                                    // запоминаем номер отряда
                                    GetComponent<Squads>().selectedSquad =
                                        UnitInSquadNum = hit.collider.GetComponent<FriendUnit>().numSquad;
                                    GetComponent<Squads>().selectedSquad--;
                                    hit.collider.GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().CheckSquadReadyToGo();
                                }
                                if (hit.collider.GetComponent<Parameters>().health > 0 &&
                                    (!selUnitInSquad || (selUnitInSquad && UnitInSquadNum == hit.collider.GetComponent<FriendUnit>().numSquad)))
                                {
                                    if (hit.collider.GetComponent<FriendUnit>().inSquad)
                                        squadsGO.Find(sqd => sqd.name == "Squad" + UnitInSquadNum).GetComponent<Squad>().CheckSquadReadyToGo();
                                }
                            }
                            else if (hit.collider.CompareTag("Lumberjack"))
                            {
                                if (SelectedUnit.Exists(un => !un.CompareTag("Lumberjack")))
                                    SwitchSelect();
                            }
                            else
                                SwitchSelect();
                        }
                    }
                    SelectedUnit.Add(hit.collider.gameObject);
                    hit.collider.gameObject.SendMessage("SelectUnit");
                }
            }
        }
    }
    
    public bool IsWithinSelectionBounds(GameObject gameObject)
    {
        if (!drawing || gameObject == null)
            return false;

        var camera = Camera.main;
        var viewportBounds = Utils.GetViewportBounds(camera, startPos, Input.mousePosition);
        return viewportBounds.Contains(camera.WorldToViewportPoint(gameObject.transform.position));
    }

    void OnGUI()
    {
        if (drawing)
        {
            // Create a rect from both mouse positions
            var rect = Utils.GetScreenRect(startPos, Input.mousePosition);
            Utils.DrawScreenRect(rect, new Color(1f, 1f, 1f, 0.5f));
            Utils.DrawScreenRectBorder(rect, 2, new Color(1f, 1f, 1f));
        }
    }

    public void SetLumberFlag()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
        RaycastHit hit;
        Physics.Raycast(ray, out hit, 1000);
        lumberFlagPlaced = true;
        GameObject flag = Instantiate(lumberFlagGO, hit.point, Quaternion.identity) as GameObject;
        flag.name = "LumberFlag";
    }

    public void SwitchSelect()
    {
        unitPanelUI.SendMessage("NoneSelected");
        foreach (GameObject selectedUnit in SelectedUnit)
        {
            if (selectedUnit != null)
            {
                if (selectedUnit.CompareTag("Player") && !ClickToMove.canUnselect)
                    continue;
                if(selectedUnit.GetComponent<Parameters>())
                    selectedUnit.GetComponent<Parameters>().selected = false;
                selectedUnit.SendMessage("UnselectUnit");
            }
        }
        
        SelectedUnit = new List<GameObject>();
        selUnitInSquad = false;
        if (!ClickToMove.canUnselect)
        {
            SelectedUnit.Add(hero);
            hero.SendMessage("SelectUnit");
        }

        inventoryOpenButton.GetComponent<Button>().onClick.RemoveAllListeners();
        inventoryOpenButton.GetComponent<Button>().onClick.AddListener(hero.GetComponent<InventoryUnit>().OpenOrClose);
    }

    public void MakeNewSquadButton()
    {
        hero.GetComponent<Builder>().BuildSquadTent();
    }

    public void MakeNewSquad(GameObject camp, List<GameObject> newSquad)
    {
        GameObject squadGO = Instantiate(squadCompositionGO, camp.transform.position - new Vector3(0, 0, 8), Quaternion.identity) as GameObject;
        squadGO.name = "Squad" + (Squads.squads.Count+1);

        squadsGO.Add(squadGO);
        squadsGO.Sort((a, b) => int.Parse(a.name.Remove(0, 5)).CompareTo(int.Parse(b.name.Remove(0, 5))));
        List<GameObject> sqd = new List<GameObject>();

        for (int i = 0; i < newSquad.Count; i++)
        {
            newSquad[i].GetComponent<FriendUnit>().inSquad = true;
            newSquad[i].GetComponent<FriendUnit>().numSquad = Squads.squads.Count+1;
            newSquad[i].GetComponent<FriendUnit>().goingToReadyPos = true;
            newSquad[i].GetComponent<FriendUnit>().squadGO = squadGO;
            newSquad[i].GetComponent<NavMeshAgent>().destination = newSquad[i].GetComponent<FriendUnit>().cameFrom = camp.transform.GetChild(i).transform.position;
            sqd.Add(newSquad[i]);
        }
        Squads.squads.Add(sqd);
        Squads.squads.Sort((a, b) => a[0].GetComponent<FriendUnit>().numSquad.CompareTo(b[0].GetComponent<FriendUnit>().numSquad));
        selUnitInSquad = true;
        UnitInSquadNum = Squads.squads.Count+1;
        GetComponent<Squads>().selectedSquad = UnitInSquadNum - 1;
        Squads.squadTents[GetComponent<Squads>().selectedSquad] = camp;
        squadGO.GetComponent<Squad>().SetNumberOfSquad(Squads.squads.Count, sqd, camp);
        unitPanelUI.SendMessage("SelectedUnitInSquad", newSquad[0]);
    }

    public void MakeNewSquad(List<GameObject> units, GameObject squadGO)
    {
        List<GameObject> sqd = units;
        int numSqd = int.Parse(squadGO.name.Remove(0, 5));
        foreach (GameObject unit in sqd)
        {
            unit.GetComponent<FriendUnit>().inSquad = true;
            unit.GetComponent<FriendUnit>().numSquad = numSqd;
            unit.GetComponent<FriendUnit>().goingToReadyPos = false;
        }
        selUnitInSquad = true;
        UnitInSquadNum = numSqd;
        GetComponent<Squads>().selectedSquad = UnitInSquadNum - 1;
        squadsGO.Add(squadGO);
        squadsGO.Sort((a, b) => int.Parse(a.name.Remove(0, 5)).CompareTo(int.Parse(b.name.Remove(0, 5))));
    }

    public void HeroDied()
    {
        heroDied = true;
        transform.position = hero.transform.position;
        StartCoroutine(FadeDiePlane());
    }

    IEnumerator FadeDiePlane()
    {
        for(float i = 0; i < 1; i+=0.05f)
        {
            Color cl = GameObject.Find("DieDramatic").transform.GetChild(0).GetComponent<MeshRenderer>().material.color;
            GameObject.Find("DieDramatic").transform.GetChild(0).GetComponent<MeshRenderer>().material.color = new Color(cl.r, cl.g, cl.b, i);
            yield return new WaitForSeconds(0.2f);
        }
        Time.timeScale = 0;
        diedStop = true;
        GameObject.Find("DieMenu").GetComponent<Canvas>().enabled = true;
        yield return null;
    }
}
