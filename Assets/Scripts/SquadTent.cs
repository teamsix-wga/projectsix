﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class SquadTent : MonoBehaviour
{
    public List<GameObject> units = new List<GameObject>();
    bool squadMade = false;
    void Start()
    {
        units = Camera.main.GetComponent<Camera3dPerson>().SelectedUnit;
    }

    void Update()
    {
        if (GetComponent<Construction>().builded && !squadMade)
        {
            Camera.main.GetComponent<Camera3dPerson>().MakeNewSquad(gameObject, units);
            squadMade = true;
        }
    }

    void OnMouseDown()
    {
        Camera3dPerson.canDraw = false;
        Camera.main.GetComponent<Camera3dPerson>().SwitchSelect();
        foreach (var un in units)
        {
            Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Add(un);
            un.SendMessage("SelectUnit");
        }
    }

    void OnMouseUp()
    {
        Camera3dPerson.canDraw = true;
    }
}
