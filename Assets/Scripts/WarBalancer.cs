﻿using UnityEngine;
using System.Collections.Generic;

public class WarBalancer : MonoBehaviour {
    public static bool working = false;
    public List<Vector3> spawnPoints;
    public int killCount = 0;
    public List<GameObject> spawnedEnemies;
    public bool firstTriggered, secondTriggered, thirdTriggered, eventTriggered, win;
    public GameObject enemyGO;
    public bool canAttack = true;
    GameObject cutscenesManager;

    void Start()
    {
        cutscenesManager = GameObject.Find("CutScenesManager");
        //spawnedEnemies = new List<GameObject>();
        enemyGO = Resources.Load("Enemy_Gulia") as GameObject;
        if(working)
            while (spawnedEnemies.Count < Camera.main.GetComponent<Camera3dPerson>().UnitList.Count)
                SpawnEnemy();
    }

    void Update()
    {
        /*if (!win && Act2.lastBattle && spawnedEnemies.Count == 0)
        {
            win = true;
            cutscenesManager.SendMessage("HeroWinBattle");
        }*/

        if (Act2.lastBattle && !HeroDesidions.pridePassed) // демоницы
        {
            while (spawnedEnemies.Count < Camera.main.GetComponent<Camera3dPerson>().UnitList.Count * 2)
                SpawnEnemy();
        }
    }

    public void StartWork()
    {
        working = true;
        while (spawnedEnemies.Count < Camera.main.GetComponent<Camera3dPerson>().UnitList.Count)
            SpawnEnemy();
    }

    public void AnotherKill(GameObject whoDied)
    {
        killCount++;

        if (whoDied.name == "MainEnemy")
        {
            win = true;
            cutscenesManager.SendMessage("HeroWinBattle");
        }

        if (whoDied.CompareTag("Enemy"))
            spawnedEnemies.Remove(whoDied);

        if (!working)
            return;

        if (killCount >= 1 && !firstTriggered)
        {
            firstTriggered = true;
            cutscenesManager.SendMessage("Act2KilledOneUnit");
        }

        if (killCount >= 3 && !secondTriggered)
        {
            secondTriggered = true;
            cutscenesManager.SendMessage("Act2KilledThreeUnit");
        }

        if (killCount >= 6 && !thirdTriggered)
        {
            thirdTriggered = true;
            cutscenesManager.SendMessage("Act2KilledSixUnit");
        }

        if (!eventTriggered)
            while(spawnedEnemies.Count < Camera.main.GetComponent<Camera3dPerson>().UnitList.Count)
                SpawnEnemy();
    }

    void SpawnEnemy()
    {
        spawnedEnemies.Add(Instantiate(enemyGO, spawnPoints[Random.Range(0, spawnPoints.Count)], Quaternion.identity) as GameObject);
        var randPoint = Random.insideUnitSphere*10;
        randPoint.y = 0;
        spawnedEnemies[spawnedEnemies.Count - 1].GetComponent<Enemy>().standHere = 
            spawnedEnemies[spawnedEnemies.Count - 1].GetComponent<NavMeshAgent>().destination = GameObject.Find("HeroVillageCenter").transform.position + randPoint;
        spawnedEnemies[spawnedEnemies.Count - 1].GetComponent<Enemy>().canAttack = canAttack;
    }
}
