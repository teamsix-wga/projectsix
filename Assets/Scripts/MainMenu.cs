﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System.Xml;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    static AsyncOperation AOp;
    static int rounded;
    public static bool loaded;
    int pRounded = 0;
    bool load = false;
    string filepath;

    void Awake()
    {
        filepath = "Saves\\save.xml";
        GameObject.Find("Back").GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        GameObject.Find("SpravkaBack").GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        GameObject.Find("Panel").GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        GameObject.Find("Info").GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width - Screen.width/10, Screen.height - Screen.height/7);
    }

    public void PlayGame()
    {
        GameObject.Find("Buttons").GetComponent<Canvas>().enabled = false;
        GameObject.Find("LoadingScreen").GetComponent<Canvas>().enabled = true;
        GameObject loading = GameObject.Find("LoadingScreen").transform.GetChild(0).gameObject;
        loading.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        loading.GetComponent<RectTransform>().transform.position = Vector3.zero;

        StartCoroutine("StartGame");
    }

    public void PlayEndlessMode()
    {
        GameObject.Find("Buttons").GetComponent<Canvas>().enabled = false;
        GameObject.Find("LoadingScreen").GetComponent<Canvas>().enabled = true;
        GameObject loading = GameObject.Find("LoadingScreen").transform.GetChild(0).gameObject;
        loading.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        loading.GetComponent<RectTransform>().transform.position = Vector3.zero;

        StartCoroutine("StartEndless");
    }

    public void Exit()
    {
        Application.Quit();
    }
    
    public void Load()
    {
        if (File.Exists(filepath))
        {
            load = true;
            GameObject.Find("Buttons").GetComponent<Canvas>().enabled = false;
            GameObject.Find("LoadingScreen").GetComponent<Canvas>().enabled = true;
            GameObject loading = GameObject.Find("LoadScreenImage");
            loading.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
            loaded = true;
            GameObject.Find("ProgressBarLabelFollow").GetComponent<ProgressBar.ProgressBarBehaviour>().SetFillerSize(Screen.width);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filepath);
            var rootNode = xmlDoc.DocumentElement;
            StartCoroutine(LoadLevel(rootNode.Attributes[0].Value + "ForLoad"));
        }
        else
            PlayGame();
    }

    IEnumerator StartGame()
    {
        AOp = SceneManager.LoadSceneAsync("Act0");

        while (!AOp.isDone)
        {
            float p = AOp.progress * 100f;
            pRounded = Mathf.RoundToInt(p);
            GameObject.Find("ProgressBarLabelFollow").GetComponent<ProgressBar.ProgressBarBehaviour>().SetFillerSizeAsPercentage(pRounded);
            yield return new WaitForSeconds(0.001f);
        }
    }

    IEnumerator StartEndless()
    {
        AOp = SceneManager.LoadSceneAsync("EndlessMode");

        while (!AOp.isDone)
        {
            float p = AOp.progress * 100f;
            pRounded = Mathf.RoundToInt(p);
            GameObject.Find("ProgressBarLabelFollow").GetComponent<ProgressBar.ProgressBarBehaviour>().SetFillerSizeAsPercentage(pRounded);
            yield return new WaitForSeconds(0.001f);
        }
    }

    IEnumerator LoadLevel(string name)
    {
        AOp = SceneManager.LoadSceneAsync(name);

        while (!AOp.isDone)
        {
            float p = AOp.progress * 100f;
            pRounded = Mathf.RoundToInt(p);
            GameObject.Find("ProgressBarLabelFollow").GetComponent<ProgressBar.ProgressBarBehaviour>().SetFillerSizeAsPercentage(pRounded);
            yield return new WaitForSeconds(0.001f);
        }
    }

    public void SpravkaClicked()
    {
        GameObject.Find("Buttons").GetComponent<Canvas>().enabled = false;
        GameObject.Find("Spravka").GetComponent<Canvas>().enabled = true;
    }

    public void BackFromSpravka()
    {
        GameObject.Find("Buttons").GetComponent<Canvas>().enabled = true;
        GameObject.Find("Spravka").GetComponent<Canvas>().enabled = false;
    }
}
