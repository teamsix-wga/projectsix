﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;

public class Minimap : MonoBehaviour
{
    private static Camera cam;
    
	IEnumerator Start ()
	{
	    yield return StartCoroutine(SetMinimap());
	}

    public static IEnumerator SetMinimap()
    {
        cam = GameObject.Find("MiniMapCamera").GetComponent<Camera>();

        var minimapQuad = GameObject.Find("MinimapQuad");

        var savedMask = cam.cullingMask;
        var savedTargetTexture = cam.targetTexture;

        cam.cullingMask = (1 << LayerMask.NameToLayer("Default")) | (1 << LayerMask.NameToLayer("Environment"));


        RenderTexture rt = new RenderTexture(1500, 1500, 24);
        cam.targetTexture = rt;
        Texture2D screenShot = new Texture2D(1500, 1500, TextureFormat.RGB24, false);
        cam.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, 1500, 1500), 0, 0);
        cam.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        Destroy(rt);
        screenShot.Apply();

        minimapQuad.GetComponent<Renderer>().materials[0].mainTexture = screenShot;
        cam.cullingMask = savedMask;
        cam.targetTexture = savedTargetTexture;

        yield return null;
    }
}
