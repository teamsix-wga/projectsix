﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class ShopSlots : MonoBehaviour, IDropHandler
{
    public int id;
    private ShopInventory inventory;
    GameObject unit;

    void Start()
    {
        unit = GameObject.FindGameObjectWithTag("Player");
        inventory = GameObject.Find("ShopUI").GetComponent<ShopInventory>();
    }

    // когда опускаем вещь в слот
    public void OnDrop(PointerEventData eventData)
    {
        // получаем вещь, которую опускаем
        ShopItem droppedItem = eventData.pointerDrag.GetComponent<ShopItem>();

        if (droppedItem.slot > Camera3dPerson.hero.GetComponent<InventoryUnit>().slotsCount + 5 && id > Camera3dPerson.hero.GetComponent<InventoryUnit>().slotsCount + 5)
            return;

        // если мусорка
        if (gameObject.name == "Trash")
        {
            unit.GetComponent<InventoryUnit>().DropItem(droppedItem.item, unit);
            if (inventory.slots[droppedItem.slot].transform.parent.name != "Slot Panel")
            {
                UpdateParameters(droppedItem.item, -1);
            }
            // обнуляем старый слот
            inventory.items[droppedItem.slot] = new Item();
            Destroy(droppedItem.gameObject);
        }

        // если слот пустой
        if (inventory.items[id].ID == -1 && transform.parent.name == "Slot Panel")
        {
            if (inventory.slots[droppedItem.slot].transform.parent.name != "Slot Panel")
            {
                UpdateParameters(droppedItem.item, -1);
            }
            Item itm = unit.GetComponent<InventoryUnit>().items.Find(delegate (Item it) { return it.slotID == droppedItem.slot; });
            itm.slotID = id;
            // обнуляем старый слот
            inventory.items[droppedItem.slot] = new Item();
            // слот, в который сбросили кладём вещь
            inventory.items[id] = droppedItem.item;
            // присваиваем id слота вещи id нового слота
            droppedItem.slot = id;
        }
        // (слот не пуст) иначе, если слот не совпадает с тем, откуда мы взяли вещь 
        else if (droppedItem.slot != id && transform.parent.name == "Slot Panel")
        {
            Item itm = unit.GetComponent<InventoryUnit>().items.Find(delegate (Item it) { return it.slotID == droppedItem.slot; });
            itm.slotID = id;
            // получаем вещь, лежащую в слоте (назовем её "старой")
            Transform item = this.transform.GetChild(0);
            itm = unit.GetComponent<InventoryUnit>().items.Find(delegate (Item it) { return it.slotID == item.GetComponent<ShopItem>().slot; });
            itm.slotID = droppedItem.slot;
            // слоту старой вещи, присваиваем слот перетаскиваемой вещи
            item.GetComponent<ShopItem>().slot = droppedItem.slot;
            // старой вещи присваиваем родителя перетаскиваемой вещи
            item.transform.SetParent(inventory.slots[droppedItem.slot].transform);
            // старой вещи присваиваем положение слота перетаскиваемой вещи
            item.transform.position = inventory.slots[droppedItem.slot].transform.position;
            // id слота новой вещи присваиваем id нового слота
            droppedItem.slot = id;
            droppedItem.transform.SetParent(this.transform);
            droppedItem.transform.position = this.transform.position;

            inventory.items[droppedItem.slot] = item.GetComponent<ShopItem>().item;
            inventory.items[id] = droppedItem.item;
        }
        else
        if (inventory.items[id].ID == -1 && transform.name == droppedItem.item.type.ToString())
        {
            if (inventory.slots[droppedItem.slot].transform.parent.name != "Slot Panel")
            {
                UpdateParameters(droppedItem.item, -1);
            }
            Item itm = unit.GetComponent<InventoryUnit>().items.Find(delegate (Item it) { return it.slotID == droppedItem.slot; });
            itm.slotID = id;
            // обнуляем старый слот
            inventory.items[droppedItem.slot] = new Item();
            // слот, в который сбросили кладём вещь
            inventory.items[id] = droppedItem.item;
            // присваиваем id слота вещи id нового слота
            droppedItem.slot = id;
            UpdateParameters(droppedItem.item, 1);
        }
        else if (droppedItem.slot != id && transform.name == droppedItem.item.type.ToString())
        {
            Item itm = unit.GetComponent<InventoryUnit>().items.Find(delegate (Item it) { return it.slotID == droppedItem.slot; });
            itm.slotID = id;
            // получаем вещь, лежащую в слоте (назовем её "старой")
            Transform item = this.transform.GetChild(0);
            itm = unit.GetComponent<InventoryUnit>().items.Find(delegate (Item it) { return it.slotID == item.GetComponent<ShopItem>().slot; });
            itm.slotID = droppedItem.slot;
            UpdateParameters(item.GetComponent<ShopItem>().item, -1);
            // слоту старой вещи, присваиваем слот перетаскиваемой вещи
            item.GetComponent<ShopItem>().slot = droppedItem.slot;
            // старой вещи присваиваем родителя перетаскиваемой вещи
            item.transform.SetParent(inventory.slots[droppedItem.slot].transform);
            // старой вещи присваиваем положение слота перетаскиваемой вещи
            item.transform.position = inventory.slots[droppedItem.slot].transform.position;
            // id слота новой вещи присваиваем id нового слота
            droppedItem.slot = id;
            droppedItem.transform.SetParent(this.transform);
            droppedItem.transform.position = this.transform.position;

            inventory.items[droppedItem.slot] = item.GetComponent<ShopItem>().item;
            inventory.items[id] = droppedItem.item;
            UpdateParameters(droppedItem.item, 1);
        }
    }

    public void UpdateParameters(Item item, int index)
    {
        if (item.Damage != 0)
            unit.GetComponent<Parameters>().damage += item.Damage * index;
        if (item.Concentration != 0)
            unit.GetComponent<Parameters>().concentration += item.Concentration * index;
        if (item.Health != 0)
            unit.GetComponent<Parameters>().health += item.Health * index;
        if (item.Defence != 0)
            unit.GetComponent<Parameters>().defence += item.Defence * index;
        if (item.MovingSpeed != 0)
        {
            unit.GetComponent<Parameters>().speed += item.MovingSpeed * index;
            unit.GetComponent<Parameters>().agent.speed = unit.GetComponent<Parameters>().speed;
        }
    }

    public static void UpdateParameters(GameObject who, Item item, int index)
    {
        if (item.Damage != 0)
            who.GetComponent<Parameters>().damage += item.Damage * index;
        if (item.Concentration != 0)
            who.GetComponent<Parameters>().concentration += item.Concentration * index;
        if (item.Health != 0)
            who.GetComponent<Parameters>().health += item.Health * index;
        if (item.Defence != 0)
            who.GetComponent<Parameters>().defence += item.Defence * index;
        if (item.MovingSpeed != 0)
        {
            who.GetComponent<Parameters>().speed += item.MovingSpeed * index;
            who.GetComponent<Parameters>().agent.speed = who.GetComponent<Parameters>().speed;
        }
    }
}
