﻿using UnityEngine;
using System.Collections;

public class OpenDoors : MonoBehaviour {
    Collider lastEnter;
    Animation anim;
    bool animStopped, closing;
    
    void Awake()
    {
        closing = false;
        animStopped = false;
        anim = transform.parent.GetComponent<Animation>();
        anim.Play("Flag");
        foreach (AnimationState state in anim)
        {
            if(state.clip.name != "Flag")   
                state.speed = 6f;
        }
        anim["Open"].time = 0;
    }

    void Update()
    {
        
        if ((anim["Open"].time >= anim["Open"].length - 0.1f) && !animStopped && !closing)
        {
            anim["Open"].speed = 0;
            animStopped = true;
        }
        if(anim["Close"].time >= anim["Close"].length-0.1f)
        {
            closing = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (transform.parent.GetComponent<Construction>().builded && other.name != "Checker")
        {
            lastEnter = other;
            if (!animStopped && !anim.IsPlaying("Open"))
            {
                anim.CrossFade("Open");
                anim["Open"].speed = 6;
                anim.Blend("Flag");
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other == lastEnter && other.name != "Checker")
        {
            animStopped = false;
            anim["Close"].speed = 6;
            closing = true;
            anim.CrossFade("Close");
            anim.Blend("Flag");
        }
    }
}
