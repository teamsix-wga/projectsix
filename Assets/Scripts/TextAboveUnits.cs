﻿using UnityEngine;
using System.Collections;

public class TextAboveUnits : MonoBehaviour {
    
	void Start () {
        if(transform.parent.GetComponent<Parameters>() && transform.parent.GetComponent<Parameters>().name != "")
            GetComponent<TextMesh>().text = transform.parent.GetComponent<Parameters>().objectName;
        else
            GetComponent<TextMesh>().text = transform.parent.name;

        GetComponent<RectTransform>().localScale = new Vector3(-1* GetComponent<RectTransform>().localScale.x, GetComponent<RectTransform>().localScale.y, GetComponent<RectTransform>().localScale.z);
	}

    void Update() {
        if (transform.parent.GetComponent<Parameters>())
        {
            if (transform.parent.GetComponent<Parameters>().selected && !GetComponent<Renderer>().enabled && !transform.parent.CompareTag("NPC"))
            {
                bool canRender = false;
                foreach (Renderer ren in transform.parent.GetComponentsInChildren<Renderer>())
                {
                    if (transform == ren.transform)
                        continue;
                    if (ren.enabled == true)
                    {
                        canRender = true;
                        break;
                    }
                }
                GetComponent<Renderer>().enabled = canRender;
            }

            if (!transform.parent.GetComponent<Parameters>().selected && GetComponent<Renderer>().enabled && !transform.parent.CompareTag("NPC"))
            {
                GetComponent<Renderer>().enabled = false;
            }

            if (transform.parent.CompareTag("NPC"))
            {
                if (!transform.parent.GetComponent<RealFoWUnit>().CheckBlackPlace())
                    GetComponent<Renderer>().enabled = true;
                else
                    GetComponent<Renderer>().enabled = false;
            }
        }

        if (GetComponent<Renderer>().enabled)
            transform.LookAt(Camera.main.transform);
	}
} 
