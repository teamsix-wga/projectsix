﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class Squads : MonoBehaviour {
    public static List<List<GameObject>> squads;
    public bool setFlag;
    int flags;
    int clickedButton;
    public int selectedSquad;
    public bool flagSetted;
    public static GameObject[] visibleFlags;
    public static GameObject[] squadTents;
    float clickdelay = 0.8f;
    int playerButtonClicked = 0;
    float timePlayerButtonClicked = 0;
    GameObject hero;
    GameObject unitFlagGO;
    GameObject unitPanelUI;

    void Awake()
    {
        unitPanelUI = GameObject.Find("UnitPanelUI");
        squads = new List<List<GameObject>>();
        hero = GameObject.FindGameObjectWithTag("Player");
        unitFlagGO = Resources.Load("UnitFlag") as GameObject;
        visibleFlags = new GameObject[12];
        squadTents = new GameObject[12];
        for (int i = 0; i < 12; i++)
            visibleFlags[i] = null;

        for(int i = 0; i < squads.Count; i++)
            squads[i] = new List<GameObject>();

        flagSetted = false;
    }

    void Update()
    {
        if(Input.GetKey(KeyCode.Escape) && setFlag && flags != 0)
        {
            setFlag = false;
        }

        // если нажата ЛКМ и мы ставим флаг
        if (Input.GetMouseButton(0) && setFlag && !EventSystem.current.IsPointerOverGameObject() && flags != 0)
        {
            
            // пускаем луч из мышки
            RaycastHit hit = Builder.CastFromCursor();
            selectedSquad = Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0].GetComponent<FriendUnit>().numSquad - 1;
            List<GameObject> o = squads[selectedSquad];
            GameObject squadGO = GameObject.Find("Squad" + (selectedSquad + 1).ToString());
            if (hero.GetComponent<Gold>().currentGold < squadGO.GetComponent<Squad>().GetFlagCost((FlagType)flags))
            {
                setFlag = false;
                StartCoroutine(FadeText.FadeTxt("Недостаточно золота!", Color.red, 3));
                return;
            }

            NavMeshPath path;

            bool squadCanGo = true;
            foreach(var unit in o)
            {
                path = new NavMeshPath();
                if (!unit.GetComponent<FriendUnit>().agent.CalculatePath(hit.point, path) || path.status == NavMeshPathStatus.PathPartial)
                {
                    squadCanGo = false;
                    break;
                }
            }

            if (!squadCanGo)
            {
                NavMeshHit hitNavMesh;
                float distanceCounter = 1;
                NavMesh.SamplePosition(hit.point, out hitNavMesh, distanceCounter, NavMesh.AllAreas);

                while (!squadCanGo)
                {
                    path = new NavMeshPath();

                    squadCanGo = true;

                    GameObject whoCantGo = new GameObject();

                    foreach (var unit in o)
                    {
                        if (float.IsPositiveInfinity(hitNavMesh.position.x) || !unit.GetComponent<FriendUnit>().agent.CalculatePath(hitNavMesh.position, path) || path.status == NavMeshPathStatus.PathPartial)
                        {
                            squadCanGo = false;
                            whoCantGo = unit;
                            break;
                        }
                        path = new NavMeshPath();
                    }

                    if (!squadCanGo)
                        NavMesh.SamplePosition(hit.point + (whoCantGo.transform.position - hit.point).normalized * distanceCounter, out hitNavMesh, distanceCounter++, NavMesh.AllAreas);
                    else
                        hit.point = hitNavMesh.position;
                }
            }

            if (visibleFlags[selectedSquad])
                Destroy(visibleFlags[selectedSquad]);
            visibleFlags[selectedSquad] = Instantiate(unitFlagGO, hit.point, Quaternion.identity) as GameObject;
            visibleFlags[selectedSquad].name = "UnitFlag";
            visibleFlags[selectedSquad].GetComponent<FriendUnitFlag>().SetTypeFlag((FriendUnitFlag.TypeFlag)flags);
            squadGO.GetComponent<Squad>().type = (FlagType)flags;
            squadGO.GetComponent<Squad>().GoToFlag(hit.point, (FlagType)flags);
            // больше мы не ставим флаг
            setFlag = false;
            flagSetted = true;
            unitPanelUI.GetComponent<UnitPanelController>().SetSquadTaskUnavailable();
        }

        if (Input.GetKeyDown(KeyCode.F1) && Camera3dPerson.canMoveCam)
        {
            SelectPlayer();
        }
    }

    void SelectPlayer()
    {
        GetComponent<Camera3dPerson>().SwitchSelect();

        hero.GetComponent<Parameters>().SelectUnit();

        GetComponent<Camera3dPerson>().SelectedUnit.Add(hero);

        playerButtonClicked++;
        if (playerButtonClicked == 1) timePlayerButtonClicked = Time.time;

        if (playerButtonClicked > 1 && Time.time - timePlayerButtonClicked < clickdelay)
        {
            playerButtonClicked = 0;
            timePlayerButtonClicked = 0;
            
            Camera.main.transform.position = hero.transform.FindChild("HeroCamera").position;
            Camera.main.transform.LookAt(hero.transform.position);

            Ray ray = new Ray(transform.position, (hero.transform.position - transform.position).normalized);
            RaycastHit hit;
            while (Physics.Raycast(ray, out hit, 100f) && hit.collider.CompareTag("Untagged"))
            {
                transform.position += (hero.transform.position - transform.position).normalized;
                ray = new Ray(transform.position, (hero.transform.position - transform.position).normalized);
                Camera.main.transform.LookAt(hero.transform.position);
            }
        }
        else
            if (playerButtonClicked > 2 || Time.time - timePlayerButtonClicked > 1) playerButtonClicked = 0;
    }

    public void SquadAttackFlag()
    {
        setFlag = true;
        flags = 1;
    }

    public void SquadDefenceFlag()
    {
        setFlag = true;
        flags = 2;
    }

    public void SquadExploreFlag()
    {
        setFlag = true;
        flags = 3;
    }

    public void ButtonDelSquad()
    {
        DelSquad();
    }

    public void SquadCheck()
    {
        // по каждому отряду
        for (int i = 0; i < squads.Count; i++)
        {
            // счётчик убитых
            int cnt = 0;
            // по каждому юниту
            for (int j = 0; j < squads[i].Count; j++)
            {
                // если юнит убит
                if (squads[i][j].GetComponent<Parameters>().health == 0) 
                    // счетчик увеличиваем
                    cnt++;
            }
            // если убиты все в отряде
            if (cnt == squads[i].Count)
            {
                selectedSquad = i;
                DelSquad();
            }
        }
    }

    void DelSquad()
    {
        // очищаем список юнитов отряда
        for (int i = 0; i < squads[selectedSquad].Count; i++)
        {
            squads[selectedSquad][i].GetComponent<FriendUnit>().inSquad = false;
            squads[selectedSquad][i].GetComponent<FriendUnit>().controllingBySquad = false;
            squads[selectedSquad][i].GetComponent<FriendUnit>().numSquad = 0;

            var parentBuild = squads[selectedSquad][i].GetComponent<FriendUnit>().parentBuilding;
            if (parentBuild && (parentBuild.GetComponent<Kazarma>() || parentBuild.GetComponent<Church>()))
            {
                if(parentBuild.GetComponent<Kazarma>())
                    squads[selectedSquad][i].GetComponent<NavMeshAgent>().destination = squads[selectedSquad][i].GetComponent<FriendUnit>().cameFrom = parentBuild.GetComponent<Kazarma>().UnitFlag;
                else
                    squads[selectedSquad][i].GetComponent<NavMeshAgent>().destination = squads[selectedSquad][i].GetComponent<FriendUnit>().cameFrom = parentBuild.GetComponent<Church>().UnitFlag;
            }
            if((parentBuild && !parentBuild.GetComponent<Kazarma>() && !parentBuild.GetComponent<Church>()) || parentBuild)
            {
                if (squads[selectedSquad][i].GetComponent<FriendUnit>().type == UnitType.warrior || squads[selectedSquad][i].GetComponent<FriendUnit>().type == UnitType.scout)
                {
                    GameObject kazarma = hero.GetComponent<Builder>().listBuild.Find(bld => bld.GetComponent<Kazarma>());
                    if (kazarma)
                    {
                        squads[selectedSquad][i].GetComponent<FriendUnit>().parentBuilding = kazarma;
                        squads[selectedSquad][i].GetComponent<NavMeshAgent>().destination =
                            squads[selectedSquad][i].GetComponent<FriendUnit>().cameFrom = kazarma.GetComponent<Kazarma>().UnitFlag;
                    }
                }
                else if (squads[selectedSquad][i].GetComponent<FriendUnit>().type == UnitType.mage)
                {
                    GameObject church = hero.GetComponent<Builder>().listBuild.Find(bld => bld.GetComponent<Church>());
                    if (church)
                    {
                        squads[selectedSquad][i].GetComponent<FriendUnit>().parentBuilding = church;
                        squads[selectedSquad][i].GetComponent<NavMeshAgent>().destination =
                            squads[selectedSquad][i].GetComponent<FriendUnit>().cameFrom = church.GetComponent<Church>().UnitFlag;
                    }
                }
            }
        }
        squads[selectedSquad].Clear();
        for (int j = selectedSquad; j < squads.Count - 1; j++)
        {
            // нашему отряду присваиваем следующий
            squads[j] = squads[j + 1];
            if (visibleFlags[j])
                Destroy(visibleFlags[j]);
            visibleFlags[j] = visibleFlags[j + 1];
            squadTents[j] = squadTents[j+1];

            GameObject curSquadGO = GameObject.Find("Squad" + ((j + 1).ToString()));

            foreach (GameObject un in squads[j])
            {
                un.GetComponent<FriendUnit>().numSquad--;
                un.GetComponent<FriendUnit>().squadGO = curSquadGO;
            }
            
            if(curSquadGO.GetComponent<Squad>().squadCamp && curSquadGO.GetComponent<Squad>().squadCamp != curSquadGO)
                Destroy(curSquadGO.GetComponent<Squad>().squadCamp);
            if (curSquadGO.GetComponent<Squad>().squadCamp != curSquadGO)
                curSquadGO.GetComponent<Squad>().squadCamp = GameObject.Find("Squad" + ((j + 2).ToString())).GetComponent<Squad>().squadCamp;
            curSquadGO.GetComponent<Squad>().units = squads[j];

            var squadScript = curSquadGO.GetComponent<Squad>();
            squadScript.squadButton.GetComponent<SquadFoldoutButton>().NewSquad(squads[j]);

            squadScript.squadButton.SetActive(squadScript.squadActive);
        }

        GameObject sqdGO = GameObject.Find("Squad" + (squads.Count));

        if (sqdGO.GetComponent<Squad>().squadCamp)
            Destroy(sqdGO.GetComponent<Squad>().squadCamp);

        Camera3dPerson.squadsGO.Remove(sqdGO);
        Destroy(sqdGO);
              
        if (visibleFlags[squads.Count - 1])
            Destroy(visibleFlags[squads.Count - 1]);
        
        squads.RemoveAt(squads.Count-1);
        setFlag = false;

        GameObject.Find("UnitPanelUI").SendMessage("ResetSelectedUnits");

        var squadButtonUI = GameObject.Find("SquadButtonsUI");
        var unitButtonRes = Resources.Load("UI/UnitInSquadButton") as GameObject;

        for (int i = 0; i < squadButtonUI.transform.childCount; i++)
            Destroy(squadButtonUI.transform.GetChild(i).gameObject);

        foreach (var squad in squads)
        {
            var squadButton = Instantiate(Resources.Load("UI/SquadSprite")) as GameObject;
            squadButton.transform.SetParent(squadButtonUI.transform);
            squadButton.GetComponent<SquadFoldoutButton>().NewSquad(squad);
            if(!squad[0].activeInHierarchy)
                squadButton.GetComponent<Button>().interactable = false;
            squad[0].GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().squadButton = squadButton;
        }
    }
}
