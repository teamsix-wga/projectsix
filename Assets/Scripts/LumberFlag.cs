﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class LumberFlag : MonoBehaviour {
    public bool setted;
    float timer;
    public bool settingSecond;
    bool deleted;
    GameObject lumbPopUp, lumbEffect;
    float maxArea;
    public List<GameObject> workingLumberjacks;
    public float workingRange;
    bool closedPopUp;
    TextMesh countLumbers;
    public bool workDone;
    Projector lumbArea;
    GameObject woodEffectGO, lumberPopupGO;
    GameObject FoWProjector;
    GameObject unitPanelUI;

    void Awake () {
        lumberPopupGO = Resources.Load("LumberPopUp") as GameObject;
        woodEffectGO = Resources.Load("woodEffect") as GameObject;
        lumbArea = transform.Find("Projector").GetComponent<Projector>();
        FoWProjector = GameObject.FindGameObjectWithTag("FoWProjector");
        workDone = false;
        closedPopUp = false;
        workingLumberjacks = new List<GameObject>();
        workingRange = 0;
        maxArea = 70f;
        deleted = false;
        setted = false;
        settingSecond = false;
        unitPanelUI = GameObject.Find("UnitPanelUI");
        unitPanelUI.SendMessage("SetSquadTaskUnavailable");
    }
	
	void Update () {
        // если флаг поставлен, а эффекта сверху нет
        if (setted && !settingSecond && !lumbEffect)
        {
            Collider[] col = Physics.OverlapSphere(transform.position, workingRange);
            for (int i = 0; i < col.Length; i++)
            {
                if (col[i].CompareTag("Tree"))
                {
                    lumbEffect = Instantiate(woodEffectGO, new Vector3(transform.position.x, col[i].bounds.max.y + 6, transform.position.z), Quaternion.identity) as GameObject;
                    break;
                }
            }
        }
        if (settingSecond)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            // игнорируем 8-й слой
            int mask = ~(1 << 8);
            mask = ~(mask << 2);
            // пускаем луч из мышки
            RaycastHit hit;
            Physics.Raycast(ray, out hit, 1000, mask);
            if(Vector3.Distance(hit.point, transform.position) < maxArea)
                lumbArea.orthographicSize = Vector3.Distance(hit.point, transform.position);
            if (lumbArea.orthographicSize == 0)
                lumbArea.orthographicSize = 0.2f;
            if (Input.GetMouseButtonDown(0) && lumbArea.orthographicSize < maxArea)
            {
                settingSecond = false;
                workingRange = lumbArea.orthographicSize;
                Collider[] col = Physics.OverlapSphere(transform.position, workingRange);
                bool findedTree = false;
                for(int i = 0; i<col.Length; i++)
                {
                    if(col[i].CompareTag("Tree"))
                    {
                        findedTree = true;
                        lumbEffect = Instantiate(woodEffectGO, new Vector3(transform.position.x, col[i].bounds.max.y + woodEffectGO.transform.localScale.y, transform.position.z), Quaternion.identity) as GameObject;
                        break;
                    }
                }
                if (!findedTree)
                    DeleteFlag();
                else
                {
                    gameObject.AddComponent<RealFoWUnit>();
                    gameObject.GetComponent<RealFoWUnit>().radius = 35;
                }
            }
        }
        if (!setted) {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            // игнорируем 8-й слой
            int mask = ~(1 << 8);
            mask = ~(mask << 2);
            // пускаем луч из мышки
            RaycastHit hit;
            Physics.Raycast(ray, out hit, 1000, mask);
            transform.position = hit.point;
            
            if (Input.GetMouseButtonDown(0) &&
                !FoWProjector.GetComponent<RealFoW>().IsBlackPlace(new Vector2(gameObject.GetComponent<Collider>().bounds.min.x, gameObject.GetComponent<Collider>().bounds.min.z),
                                                              new Vector2(gameObject.GetComponent<Collider>().bounds.max.x, gameObject.GetComponent<Collider>().bounds.max.z)))
            {
                NavMeshPath path = new NavMeshPath();
                NavMesh.CalculatePath(Camera.main.GetComponent<Camera3dPerson>().lumberjacks[0].transform.position, hit.point, NavMesh.AllAreas, path);
                if (path.status == NavMeshPathStatus.PathComplete)
                {
                    lumbArea.enabled = true;
                    lumbArea.orthographicSize = 0.2f;
                    setted = true;
                    settingSecond = true;
                    Camera.main.GetComponent<Camera3dPerson>().lumberFlag = gameObject;
                }
            }
        }

        if (!deleted && !closedPopUp && setted && !settingSecond && Vector3.Distance(Camera.main.transform.position, transform.position) <= maxArea+30f)
        {
            if (!lumbPopUp)
            {
                lumbPopUp = Instantiate(lumberPopupGO, new Vector3(GetComponent<Collider>().bounds.center.x, GetComponent<Collider>().bounds.max.y + 30, GetComponent<Collider>().bounds.center.z), Quaternion.identity) as GameObject;
                Button[] btns = lumbPopUp.GetComponentsInChildren<Button>();
                btns[0].onClick.RemoveAllListeners();
                btns[0].onClick.AddListener(DeleteFlag);
                btns[1].onClick.RemoveAllListeners();
                btns[1].onClick.AddListener(AddLumberjacks);
                btns[2].onClick.RemoveAllListeners();
                btns[2].onClick.AddListener(DeleteLumberjack);
                btns[3].onClick.RemoveAllListeners();
                btns[3].onClick.AddListener(ClosePopUp);

                countLumbers = lumbPopUp.transform.FindChild("Working/Text").GetComponent<TextMesh>();
                countLumbers.text = "Лесорубов нет";
            }
            if(!lumbPopUp.activeInHierarchy)
                lumbPopUp.SetActive(true);
            lumbPopUp.transform.LookAt(Camera.main.transform);
            lumbPopUp.transform.localScale = new Vector3(-1, 1, 1);
        } else
        if(Vector3.Distance(Camera.main.transform.position, transform.position) > maxArea + 30f)
        {
            closedPopUp = false;
            if (lumbPopUp && lumbPopUp.activeInHierarchy)
                lumbPopUp.SetActive(false);
        }

        if ((Input.GetKey(KeyCode.Escape) || Input.GetMouseButton(1)) && (!setted || settingSecond))
        {
            DeleteFlag();
        }
    }

    public void DeleteFlag()
    {
        Camera.main.GetComponent<Camera3dPerson>().lumberFlagPlaced = false;
        if (lumbPopUp)
            Destroy(lumbPopUp);
        if (lumbEffect)
            Destroy(lumbEffect);
        setted = false;
        settingSecond = false;
        for(int i = workingLumberjacks.Count - 1; i >= 0 ; i--)
        {
            workingLumberjacks[i].GetComponent<Lumberjack>().GoBack();
            workingLumberjacks.RemoveAt(workingLumberjacks.Count - 1);
        }
        workingLumberjacks.Clear();
        unitPanelUI.SendMessage("SetSquadTaskAvailable");
        Destroy(gameObject);
        Destroy(this);
    }

    public void AddLumberjacks()
    {
        GameObject lumber = Camera.main.GetComponent<Camera3dPerson>().lumberjacks.Find((GameObject lumb) => (!lumb.GetComponent<Lumberjack>().working));

        if (lumber)
        {
            lumber.GetComponent<Lumberjack>().state = LumberState.Lumberjack;
            lumber.GetComponent<Lumberjack>().GoToWork(transform.position, gameObject);
            workingLumberjacks.Add(lumber);
            countLumbers.text = "Сейчас работа" + WordWorkEnding(workingLumberjacks.Count) + "\n" + workingLumberjacks.Count + " лесоруб" + WordCountEnding(workingLumberjacks.Count);
        }
    }

    public void DeleteLumberjack()
    {
        if (workingLumberjacks.Count > 0)
        {
            workingLumberjacks[workingLumberjacks.Count - 1].GetComponent<Lumberjack>().GoBack();
            workingLumberjacks.RemoveAt(workingLumberjacks.Count - 1);
            if (workingLumberjacks.Count > 0)
                countLumbers.text = "Сейчас работа" + WordWorkEnding(workingLumberjacks.Count) + "\n" + workingLumberjacks.Count + " лесоруб" + WordCountEnding(workingLumberjacks.Count);
            else
                countLumbers.text = "Лесорубов нет";
        }
        if(workingLumberjacks.Count == 0)
            DeleteFlag();
    }

    public void DeleteLumberjack(GameObject lumb)
    {
        if (workingLumberjacks.Count > 0)
        {
            lumb.GetComponent<Lumberjack>().GoBack();
            workingLumberjacks.Remove(lumb);
            if (lumbPopUp)
                if (workingLumberjacks.Count > 0)
                    countLumbers.text = "Сейчас работа" + WordWorkEnding(workingLumberjacks.Count) + "\n" + workingLumberjacks.Count + " лесоруб" + WordCountEnding(workingLumberjacks.Count);
                else
                    countLumbers.text = "Лесорубов нет";
        }
        if (workingLumberjacks.Count == 0)
            DeleteFlag();
    }

    string WordCountEnding(int count)
    {
        var num = 0;
        if (count > 0 && count < 10)
            num = count;
        else
            num = count % 10;

        if (num == 1 && count != 11)
            return "";
        if (count >= 2 && count <= 4)
            return "а";
        return "ов";
    }

    string WordWorkEnding(int count)
    {
        var num = 0;
        if (count > 0 && count < 10)
            num = count;
        else
            num = count % 10;

        if (num == 1 && count != 11)
            return "ет";
        else
            return "ют";
        return "";
    }

    public void ClosePopUp()
    {
        Destroy(lumbPopUp);
        closedPopUp = true;
    }

    void OnMouseDown()
    {
        closedPopUp = false;
    }
}
