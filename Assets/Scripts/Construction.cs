﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Construction : MonoBehaviour {
    public bool placed;
    public float timeNeedToBuild;
    [HideInInspector]
    public float placedTime;
    //флаг коллизии
    [HideInInspector]
    public bool col;
    // построено ли здание
    public bool builded;
    // таймер 
    [HideInInspector]
    public float timer;
    float buildingProgress;
    [HideInInspector]
    public GameObject buildEffect;
    GameObject hero;
    
    public float costInGold, costInWood, costInStone;

    [Tooltip("Где нам надо строить здание (по квесту)")]
    public Vector3 buildHere;

    [Tooltip("Надо ли строить здание в определённом месте (по квесту)")]
    public bool needToBuildInPlace = false;

    [Tooltip("Как далеко можем строить от флага (по квесту)")]
    public float buildRadius = 20;

    public bool canRecruit = true;
    public List<string> recruitersNames = new List<string>();
    public GameObject recruitmentMenu;
    
    private GameObject buildEffectGO;
    
    private List<Material> materials = new List<Material>(), refMaterials = new List<Material>();

    private GameObject referenceBuild;
     
    void Start()
    {
        if (canRecruit)
        {
            recruitmentMenu = transform.Find("Construction_Recruitment").gameObject;
            recruitmentMenu.GetComponent<Construction_Recruitment>().LoadRecruitButtons(recruitersNames);
            recruitmentMenu.transform.FindChild("Header/Construction_Name").GetComponent<TextMesh>().text = GetComponent<Parameters>().objectName;
            recruitmentMenu.SetActive(false);
        }

        foreach (Renderer r in gameObject.GetComponentsInChildren<Renderer>())
        {
            if (r.name == "TextAbove" || r.name == "VisibleSelection")
                continue;
            foreach (Material mat in r.materials)
            {
                materials.Add(mat);
            }
        }
        buildEffectGO = Resources.Load("buildEffect") as GameObject;
        hero = GameObject.FindGameObjectWithTag("Player");
        Quest quest = hero.GetComponent<QuestReceiver>().quests.FindAll((Quest q) => (q.questType == Quest.QuestType.BuildInPlace)).Find((Quest qu) => (qu.nameWho == gameObject.name));
        if (quest != null)
        {
            needToBuildInPlace = true;
            buildHere = quest.whereToGo[0];
        }
        foreach (NavMeshObstacle obs in GetComponentsInChildren<NavMeshObstacle>())
            obs.enabled = false;
    }

    void Update()
    {
        if (placed && !builded)
            BuildBuilding();
        // если прошло время в timer и была коллизия и здание не построено
        if ((timer -= Time.deltaTime) <= 0 && col && !placed)
        {
            // сбрасываем флаг коллизии
            col = false;
            // закрашиваем зелёным здание
            Parameters.MakeColor(gameObject, Color.green);
        }

        if (!placed && needToBuildInPlace && Vector3.Distance(gameObject.transform.position, buildHere) > buildRadius)
        {
            timer = 0.05f;
            Parameters.MakeColor(gameObject, Color.red);
            col = true;
        }
        if (!placed && needToBuildInPlace && Vector3.Distance(gameObject.transform.position, buildHere) <= buildRadius)
        {
            Parameters.MakeColor(gameObject, Color.green);
            col = false;
        }
        if (!placed && !CompareTag("TownHall") && Camera3dPerson.townhall && !needToBuildInPlace && Vector3.Distance(transform.position, Camera3dPerson.townhall.transform.position) > 300)
        {
            timer = 0.05f;
            Parameters.MakeColor(gameObject, Color.red);
            col = true;
        }

        if (!builded && !placed && Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (CompareTag("TownHall") || name == "Church")
                transform.Rotate(new Vector3(0, 0, transform.position.z), 5f);
            else
                transform.Rotate(new Vector3(0, transform.position.y, 0f), 5f);
        }

        if (!builded && !placed && Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (CompareTag("TownHall") || name == "Church")
                transform.Rotate(new Vector3(0, 0, transform.position.z), -5f);
            else
                transform.Rotate(new Vector3(0, transform.position.y, 0f), -5f);
        }

        if (!builded && !placed)
        {
            if (
                GameObject.FindWithTag("FoWProjector")
                    .GetComponent<RealFoW>()
                    .IsBlackPlace(
                        new Vector2(GetComponent<Collider>().bounds.min.x, GetComponent<Collider>().bounds.min.z),
                        new Vector2(GetComponent<Collider>().bounds.max.x, GetComponent<Collider>().bounds.max.z)))
            {
                timer = 0.05f;
                Parameters.MakeColor(gameObject, Color.red);
                col = true;
                return;
            }

            NavMeshPath path = new NavMeshPath();
            if (!hero.GetComponent<NavMeshAgent>().CalculatePath(new Vector3(transform.position.x, hero.transform.position.y, transform.position.z), path))
            {
                timer = 0.05f;
                Parameters.MakeColor(gameObject, Color.red);
                col = true;
                return;
            }

            path = new NavMeshPath();
            if (!hero.GetComponent<NavMeshAgent>().CalculatePath(new Vector3(GetComponent<Collider>().bounds.max.x, hero.transform.position.y, GetComponent<Collider>().bounds.max.z), path))
            {
                timer = 0.05f;
                Parameters.MakeColor(gameObject, Color.red);
                col = true;
                return;
            }

            path = new NavMeshPath();
            if (!hero.GetComponent<NavMeshAgent>().CalculatePath(new Vector3(GetComponent<Collider>().bounds.min.x, hero.transform.position.y, GetComponent<Collider>().bounds.min.z), path))
            {
                timer = 0.05f;
                Parameters.MakeColor(gameObject, Color.red);
                col = true;
                return;
            }
        }
    }

    void BuildBuilding()
    {
        if (placedTime <= 0)
        {
            builded = true;
            for (int i = 0; i < materials.Count; i++)
            {
                //materials[i].CopyPropertiesFromMaterial(refMaterials[i]);
                materials[i].SetInt("_Mode", 0);
                materials[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                materials[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                materials[i].SetInt("_ZWrite", 1);
                materials[i].DisableKeyword("_ALPHATEST_ON");
                materials[i].DisableKeyword("_ALPHABLEND_ON");
                materials[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");
                materials[i].renderQueue = -1;
                Color col = refMaterials[i].color;
                col.a = 1;
                materials[i].color = col;
            }
            Destroy(buildEffect);
            if(GetComponent<BuildBuild>())
                GetComponent<BuildBuild>().DeleteFlag();
            if(GetComponent<FMODUnity.StudioEventEmitter>())
                GetComponent<FMODUnity.StudioEventEmitter>().Stop();
            hero.SendMessage("CheckBuildedBuild", gameObject.name);

            if (GetComponent<TownHall>())
                foreach (GameObject lumber in Camera.main.GetComponent<Camera3dPerson>().lumberjacks)
                    lumber.GetComponent<Lumberjack>().SetTownhall(gameObject);

            if (GetComponent<Kazarma>())
            {
                foreach (GameObject unit in Camera.main.GetComponent<Camera3dPerson>().UnitList.FindAll((GameObject un)=>(un.GetComponent<FriendUnit>())))
                {
                    unit.GetComponent<FriendUnit>().parentBuilding = gameObject;
                    if (!unit.GetComponent<FriendUnit>().inSquad)
                        unit.GetComponent<NavMeshAgent>().destination = unit.GetComponent<FriendUnit>().cameFrom = GetComponent<Kazarma>().spawnPoint;
                }
            }
            if (GetComponent<Shop>())
            {
                foreach (var unit in Camera.main.GetComponent<Camera3dPerson>().UnitList)
                    unit.SendMessage("CheckShop");
            }
        }
        else
        {
            if (!buildEffect)
            {
                buildEffect = Instantiate(buildEffectGO, new Vector3(GetComponent<Collider>().bounds.center.x, GetComponent<Collider>().bounds.max.y + 15f, GetComponent<Collider>().bounds.center.z), Quaternion.identity) as GameObject;
                GetComponent<FMODUnity.StudioEventEmitter>().Play();
            }
            buildingProgress = 1 - placedTime / timeNeedToBuild;
            GetComponent<BuildBuild>().UpdateProgressBar(buildingProgress);
            for (int i = 0; i < materials.Count; i++)
            {
                Color col = refMaterials[i].color;
                col.a = buildingProgress;
                materials[i].color = col;
            }
        }
    }

    void OnTriggerStay(Collider collid)
    {
        if (!builded && !placed && collid.name != "Checker" && collid.tag != "SceneTrigger")
        {
            timer = 0.05f;
            Parameters.MakeColor(gameObject, Color.red);
            col = true;
        }
    }

    void OnCollisionStay(Collision collis)
    {
        if (!builded && !placed && collis.gameObject.name != "Checker")
        {
            timer = 0.05f;
            Parameters.MakeColor(gameObject, Color.red);
            col = true;
        }
    }

    void OnDestroy()
    {
        if(hero)
            hero.GetComponent<Builder>().listBuild.Remove(gameObject);
    }

    public void ObjectSelected()
    {
        if(canRecruit && builded) 
            recruitmentMenu.SetActive(true);
    }

    public void ObjectUnselected()
    {
        if (canRecruit && builded)
            recruitmentMenu.SetActive(false);
    }

    public void Placed(GameObject refer)
    {
        referenceBuild = refer;
        foreach (Renderer r in refer.GetComponentsInChildren<Renderer>())
        {
            if (r.name == "TextAbove" || r.name == "VisibleSelection")
                continue;
            foreach (Material mat in r.sharedMaterials)
            {
                refMaterials.Add(mat);
            }
        }
        for (int i = 0; i < materials.Count; i++)
        {
            Color col = refMaterials[i].color;
            col.a = 0;
            materials[i].color = col;
        }
    }
}
