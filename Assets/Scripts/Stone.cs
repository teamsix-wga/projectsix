﻿using UnityEngine;
using System.Collections;

public class Stone : MonoBehaviour {
    void Start () {
        Quarry.stones.Add(gameObject);
	}

    public void LumberStart()
    {

    }

    public void GetHit(float damage, GameObject lumber)
    {
        if (lumber.GetComponent<Lumberjack>().resourcesLimit < damage)
        {
            lumber.GetComponent<Lumberjack>().carryStone += lumber.GetComponent<Lumberjack>().resourcesLimit;
            lumber.GetComponent<Lumberjack>().resourcesLimit = 0;
        }
        else
        {
            lumber.GetComponent<Lumberjack>().resourcesLimit -= damage;
            lumber.GetComponent<Lumberjack>().carryStone += damage;
        }
    }
}
