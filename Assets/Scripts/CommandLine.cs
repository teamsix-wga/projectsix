﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CommandLine : MonoBehaviour
{

    public GUISkin consoleSkin; //Our skin
    public GUIStyle consoleStyle; //Our style
    public static bool enableConsole = false; //If false, console is unaviable
    private bool enableHelp = false; //If false, help is unaviable
    private Rect consoleWindow; //Main console window
    private Rect helpWindow; //Help console window

    private ArrayList messages = new ArrayList(); //Array where we store our messages
    private ArrayList commands = new ArrayList(); //Array of system commands
    private ArrayList time = new ArrayList(); //Array of current date

    private string[] split; //We use this variable to cut strings
    private string messageToSend = ""; //Textfield variable
    private string currentTime = ""; //Data variable
    public int maxMessages = 4; //How much messages can save history?
    private Vector2 scrollPosition; //Scrolling
    private Vector2 helpScrollPosition; //Scrolling

    private int scrollingCounter = 0; //Helpful counter, for scrolling system

    UIController uiController;

    //At start we must add system console commands
    private void Awake()
    {
        uiController = GameObject.Find("UI").GetComponent<UIController>();
        commands.Add("Gold"); //0
        commands.Add("Stone"); //1
        commands.Add("Wood"); //2
        commands.Add("Health"); //3
        commands.Add("Help"); //4
        commands.Add("Unfog"); // 5
        commands.Add("SpawnItem"); // 6
        commands.Add("TimeScale"); // 7
        commands.Add("CameraMove"); // 8
        commands.Add("GiveQuest"); // 9
        commands.Add("RPG"); // 10
        commands.Add("AddBuild"); // 11
        commands.Add("Dialogue"); // 12
        commands.Add("Send"); // 13
    }


    //Draw our console
    private void OnGUI()
    {
        GUI.skin = consoleSkin;
        if (enableConsole)
        {
            consoleWindow = new Rect(0, 0, Screen.width, Screen.height / 3);
            consoleWindow = GUI.Window(0, consoleWindow, consoleControls, "Console");
        }
        if (enableHelp)
        {
            helpWindow = consoleWindow = new Rect(0, Screen.height / 3, Screen.width, Screen.height / 3);
            helpWindow = GUI.Window(1, helpWindow, helpControls, "Help system");
        }
    }

    //Main console window
    private void consoleControls(int windowID)
    {
        scrollPosition = GUILayout.BeginScrollView(scrollPosition);
        for (int i = 0; i < messages.Count; i++)
        {
            GUILayout.Label("" + time[i] + messages[i], consoleStyle);
        }
        GUILayout.EndScrollView();
        messageToSend = GUILayout.TextField(messageToSend);
    }

    private void helpControls(int windowID)
    {
        GUILayout.Label("To close this window use ESC key");
        GUILayout.Label("Console commands :");
        helpScrollPosition = GUILayout.BeginScrollView(helpScrollPosition);
        GUILayout.Label("Gold X , где X - желаемое количество золота в казне героя", consoleStyle);
        GUILayout.Label("Gold Selected X , где X - желаемое количество золота у каждого выделенного юнита (если у него может быть золото)", consoleStyle);
        GUILayout.Label("Gold All X , где X - желаемое количество золота у каждого дружественного юнита (если у него может быть золото)", consoleStyle);
        GUILayout.Label("Stone X , где X - желаемое количество камня", consoleStyle);
        GUILayout.Label("Wood X , где X - желаемое количество дерева", consoleStyle);
        GUILayout.Label("Health X , где X - желаемое количество здоровья героя", consoleStyle);
        GUILayout.Label("Health Selected X , где X - желаемое количество здоровья у каждого выделенного юнита (если у него может быть здоровье)", consoleStyle);
        GUILayout.Label("Health All X , где X - желаемое количество здоровья у каждого дружественного юнита (если у него может быть здоровье)", consoleStyle);
        GUILayout.Label("Unfog - убирает туман войны", consoleStyle);
        GUILayout.Label("SpawnItem [ID] - создаёт предмет по его ID", consoleStyle);
        GUILayout.Label("TimeScale X, где X - модификатор времени", consoleStyle);
        GUILayout.Label("CameraMove - отключает или включает возможность двигать камеру", consoleStyle);
        GUILayout.Label("GiveQuest X - выдаёт квест по ID", consoleStyle);
        GUILayout.Label("RPG - отключает или включает RPG-like камеру", consoleStyle);
        GUILayout.Label("AddBuild - добавляет здание", consoleStyle);
        GUILayout.Label("Dialogue - запускает диалог", consoleStyle);
        GUILayout.EndScrollView();
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            enableHelp = false;
        }
    }

    //Add our messages in our array
    private void sendMessages()
    {
        Input.eatKeyPressOnTextFieldFocus = false;
        if ((messageToSend.Length != 0) && (enableConsole) && (Input.GetKeyDown(KeyCode.Return)))
        {
            currentTime = DateTime.Now.ToString();
            messages.Add(messageToSend);
            time.Add(currentTime + ", Console message : ");
            consoleCommands();
            messageToSend = "";
        }
        if (messages.Count > maxMessages)
        {
            messages.RemoveAt(0);
            time.RemoveAt(0);
        }
    }

    //Here we adding our console commands
    private void consoleCommands()
    {
        split = messageToSend.Split(new Char[] { ' ', ',', '.', ':', '\t' });

        // Добавить золото 
        if (split[0] == commands[0] as string)
        {
            //в казну игрока
            if (split.Length == 2)
                AddGold(int.Parse(split[1]));
            // выделенным юнитам
            if ((split.Length == 3) && (split[1] == "Selected"))
                AddGoldSelected(int.Parse(split[2]));
            // всем юнитам
            if ((split.Length == 3) && (split[1] == "All"))
                AddGoldAll(int.Parse(split[2]));
        }

        // Добавить камень
        if ((split[0] == commands[1] as string) && (split.Length == 2))
            AddStone(int.Parse(split[1]));
        // Добавить дерево
        if ((split[0] == commands[2] as string) && (split.Length == 2))
            AddWood(int.Parse(split[1]));

        // добавить здоровье
        if (split[0] == commands[3] as string)
        {
            // игроку
            if(split.Length == 2)
                AddHealth(int.Parse(split[1]));
            // выделенным юнитам
            if ((split.Length == 3) && (split[1] == "Selected"))
                AddHealthSelected(int.Parse(split[2]));
            // всем юнитам
            if ((split.Length == 3) && (split[1] == "All"))
                AddHealthAll(int.Parse(split[2]));
        }

        if ((split[0] == commands[4] as string) && (split.Length == 1))
            enableHelp = true;

        if ((split[0] == commands[5] as string) && (split.Length == 1))
            GameObject.FindWithTag("FoWProjector").GetComponent<RealFoW>().Unfog();

        if ((split[0] == commands[6] as string))
        {
            Item spawnedItem = GameObject.Find("Inventory").GetComponent<ItemDatabase>().FetchItemByID(int.Parse(split[1]));
            Vector3 whereToSpawn = Camera.main.transform.position;

            GameObject it = Instantiate(spawnedItem.model, whereToSpawn, Quaternion.identity) as GameObject;
            it.name = spawnedItem.model.name;
            it.tag = "LyingItem";
            it.layer = 8;
            it.AddComponent<Rigidbody>();
            it.AddComponent<DroppedItem>();
            it.SendMessage("SetItem", spawnedItem);
            it.GetComponent<SphereCollider>().enabled = true;
            it.GetComponent<BoxCollider>().enabled = true;
        }

        if ((split[0] == commands[7] as string) && (split.Length == 2))
        {
            Time.timeScale = int.Parse(split[1]);
        }

        if ((split[0] == commands[8] as string))
        {
            Camera3dPerson.canMoveCam = !Camera3dPerson.canMoveCam;
        }
        if ((split[0] == commands[9] as string) && (split.Length == 2))
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<QuestGiver>().AcceptQuest(int.Parse(split[1]));
        }
        if ((split[0] == commands[10] as string))
        {
            Camera3dPerson.RPGMode = !Camera3dPerson.RPGMode;
        }
        if ((split[0] == commands[11] as string) && (split.Length == 2))
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Builder>().buildingUI.GetComponent<BuildingMenu>().AddBuildToList(new List<string>() { split[1] });
        }
        if ((split[0] == commands[12] as string) && (split.Length == 2))
        {
            var hero = GameObject.FindGameObjectWithTag("Player");
            hero.GetComponent<DialoguesTalker>().dialID = int.Parse(split[1]);
            hero.GetComponent<DialoguesTalker>().ReloadDialogue();
            hero.GetComponent<DialoguesTalker>().OpenDialogue();
        }
        if ((split[0] == commands[13] as string) && (split.Length == 3))
        {
            GameObject.Find(split[1]).SendMessage(split[2]);
        }
    }

    //We can scroll now
    private void scrollingMessages()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (scrollingCounter == messages.Count)
            {
                scrollingCounter = 0;
            }
            scrollingCounter++;
            messageToSend = messages[messages.Count - scrollingCounter] as string;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (scrollingCounter <= 1)
            {
                scrollingCounter = messages.Count + 1;
            }
            scrollingCounter--;
            messageToSend = messages[messages.Count - scrollingCounter] as string;
        }
    }

    ///***///***Console commands***///***///

    void AddGold(int amount)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Gold>().currentGold = amount;
    }

    void AddGoldSelected(int amount)
    {
        foreach (GameObject unit in Camera.main.GetComponent<Camera3dPerson>().SelectedUnit)
        {
            if (unit.GetComponent<Gold>())
                unit.GetComponent<Gold>().currentGold = amount;
        }
    }

    void AddGoldAll(int amount)
    {
        foreach (GameObject unit in Camera.main.GetComponent<Camera3dPerson>().UnitList)
        {
            if (unit.GetComponent<Gold>())
                unit.GetComponent<Gold>().currentGold = amount;
        }
    }

    void AddStone(int amount)
    {
        Camera.main.GetComponent<GameResources>().stone = amount;
    }

    void AddWood(int amount)
    {
        Camera.main.GetComponent<GameResources>().wood = amount;
    }

    void AddHealth(int amount)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Parameters>().health = amount;
    }

    void AddHealthSelected(int amount)
    {
        foreach (GameObject unit in Camera.main.GetComponent<Camera3dPerson>().SelectedUnit)
        {
            if (unit.GetComponent<Parameters>())
                unit.GetComponent<Parameters>().health = amount;
        }
    }

    void AddHealthAll(int amount)
    {
        foreach (GameObject unit in Camera.main.GetComponent<Camera3dPerson>().UnitList)
        {
            if (unit.GetComponent<Parameters>())
                unit.GetComponent<Parameters>().health = amount;
        }
    }

    ///***///***END Console commands***///***///
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote) && (!enableConsole))
        {
            enableConsole = true;
            messageToSend = ""; //When console is enable, clear previous "non-sendable" message
        }

        else if (Input.GetKeyDown(KeyCode.BackQuote) && (enableConsole))
        {
            enableConsole = false;
            scrollingCounter = 0; //When we closing console, we bring back our default value of scrollingCounter
            enableHelp = false;
        }
        if (enableConsole)
        {
            sendMessages();
            scrollingMessages();
        }
    }
}
