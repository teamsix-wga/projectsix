﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class UnitPanelController : MonoBehaviour
{

    GameObject squadNamePanel, unitListPanel, worksPanel, squadOpsPanel, groupPanel; // панели
    GameObject unitButton, workerButton; // кнопки в панель отряда
    GameObject squadAttackButton, squadDefenceButton, squadExploreButton, lumberingButton; // кнопки заданий
    GameObject lumberButton; 
    GameObject selectedSquadGO;
    GameObject hero;

	void Start ()
	{
	    hero = GameObject.FindWithTag("Player");

        squadNamePanel = gameObject.transform.FindChild("GroupPanel/SquadName").gameObject;
        unitListPanel = gameObject.transform.FindChild("GroupPanel/UnitList").gameObject;
        worksPanel = gameObject.transform.FindChild("GroupPanel/Works").gameObject;
        squadOpsPanel = gameObject.transform.FindChild("GroupPanel/SquadOps").gameObject;
        groupPanel = gameObject.transform.FindChild("GroupPanel").gameObject;

        unitButton = Resources.Load("UI/UnitInSquadButton") as GameObject;

        squadAttackButton = Resources.Load("UI/SquadAttackButton") as GameObject;
        squadDefenceButton = Resources.Load("UI/SquadDefenceButton") as GameObject;
        squadExploreButton = Resources.Load("UI/SquadScoutButton") as GameObject;

        lumberingButton = Resources.Load("UI/LumberingFlag") as GameObject;
	    workerButton = Resources.Load("UI/WorkerButton") as GameObject;

        lumberButton = Instantiate(lumberingButton);
        lumberButton.transform.SetParent(worksPanel.transform.FindChild("List"));
        lumberButton.GetComponent<Button>().onClick.RemoveAllListeners();
        lumberButton.GetComponent<Button>().onClick.AddListener(Camera.main.GetComponent<Camera3dPerson>().SetLumberFlag);
        lumberButton.SetActive(false);

        NoneSelected();
	}

    public void NoneSelected() // никто не выделен или выделен герой
    {
        if (!groupPanel.activeInHierarchy)
            return;
        groupPanel.SetActive(false);
        selectedSquadGO = null;
        for (int i = 0; i < unitListPanel.transform.FindChild("List").childCount; i++)
            Destroy(unitListPanel.transform.FindChild("List").GetChild(i).gameObject);

        for (int i = 0; i < worksPanel.transform.FindChild("List").childCount; i++)
            if(lumberButton != worksPanel.transform.FindChild("List").GetChild(i).gameObject)
                Destroy(worksPanel.transform.FindChild("List").GetChild(i).gameObject);
            else
                lumberButton.SetActive(false);
        squadNamePanel.transform.FindChild("InputField").GetComponent<InputField>().text = "";
        squadNamePanel.transform.FindChild("InputField").GetComponent<InputField>().onValueChanged.RemoveAllListeners();
    }

    public void ResetSelectedUnits()
    {
        var selectedUnits = Camera.main.GetComponent<Camera3dPerson>().SelectedUnit;
        NoneSelected();
        if (selectedUnits.Count > 0)
        {
            if (selectedUnits.Exists(unit => unit == null))
                return;
            if (selectedUnits[0].CompareTag("Unit") && selectedUnits[0].GetComponent<FriendUnit>().inSquad)
            {
                SelectedUnitInSquad(selectedUnits[0]);
            }
            if (selectedUnits[0].CompareTag("Unit") && !selectedUnits[0].GetComponent<FriendUnit>().inSquad)
            {
                foreach (var unit in selectedUnits)
                {
                    SelectedUnitNotInSquad(unit);
                }
            }
        }
    }

    public void SelectedUnitInSquad(GameObject unit) // выделен(ы) юнит(ы) в отряде
    {
        if(selectedSquadGO && unit.GetComponent<FriendUnit>().squadGO == selectedSquadGO)
            return;

        NoneSelected();

        var input = squadNamePanel.transform.FindChild("InputField").GetComponent<InputField>();
        input.interactable = true;
        input.onValueChanged.RemoveAllListeners();
        input.onValueChanged.AddListener(WriteSquadNameChanges);

        selectedSquadGO = unit.GetComponent<FriendUnit>().squadGO;

        input.text = selectedSquadGO.GetComponent<Squad>().squadName;

        foreach (var un in unit.GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().units)
        {
            var button = Instantiate(unitButton);
            button.transform.SetParent(unitListPanel.transform.FindChild("List"));
            button.GetComponent<UnitInSquadButton>().unit = un;
            button.GetComponent<UnitInSquadButton>().units = unit.GetComponent<FriendUnit>().squadGO.GetComponent<Squad>().units;
            button.GetComponent<UnitInSquadButton>().inSquad = true;
        }

        var squadAttack = Instantiate(squadAttackButton);
        squadAttack.transform.SetParent(worksPanel.transform.FindChild("List"));
        squadAttack.GetComponent<Button>().onClick.RemoveAllListeners();
        squadAttack.GetComponent<Button>().onClick.AddListener(Camera.main.GetComponent<Squads>().SquadAttackFlag);
        var squadDefence = Instantiate(squadDefenceButton);
        squadDefence.transform.SetParent(worksPanel.transform.FindChild("List"));
        squadDefence.GetComponent<Button>().onClick.RemoveAllListeners();
        squadDefence.GetComponent<Button>().onClick.AddListener(Camera.main.GetComponent<Squads>().SquadDefenceFlag);
        var squadExplore = Instantiate(squadExploreButton);
        squadExplore.transform.SetParent(worksPanel.transform.FindChild("List"));
        squadExplore.GetComponent<Button>().onClick.RemoveAllListeners();
        squadExplore.GetComponent<Button>().onClick.AddListener(Camera.main.GetComponent<Squads>().SquadExploreFlag);

        var squadOpsButton = squadOpsPanel.transform.FindChild("List/Button");
        squadOpsButton.GetComponent<Button>().transform.FindChild("Text").GetComponent<Text>().text = "Распустить отряд";
        squadOpsButton.GetComponent<Button>().onClick.RemoveAllListeners();
        squadOpsButton.GetComponent<Button>().onClick.AddListener(Camera.main.GetComponent<Squads>().ButtonDelSquad);
        squadOpsButton.GetComponent<Button>().interactable = hero.GetComponent<Builder>().listBuild.Exists(bld => bld.GetComponent<Kazarma>() || bld.GetComponent<Church>());

        selectedSquadGO.GetComponent<Squad>().CheckSquadReadyToGo();

        groupPanel.SetActive(true);
    }

    public void SetSquadTaskUnavailable()
    {
        if(Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Exists(un => un.CompareTag("Player")))
            return;
        if (Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Exists(un => un.CompareTag("Unit")))
        {
            for (int i = 0; i < worksPanel.transform.FindChild("List").childCount; i++)
                worksPanel.transform.FindChild("List").GetChild(i).GetComponent<Button>().interactable = false;
            squadOpsPanel.transform.FindChild("List/Button").GetComponent<Button>().interactable = false;
        }

        if (Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Exists(un => un.CompareTag("Lumberjack")))
        {
            lumberButton.GetComponent<Button>().interactable = false;
        }
    }

    public void SetSquadTaskAvailable()
    {
        if (Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Exists(un => un.CompareTag("Player")))
            return;
        if (Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Exists(un => un.CompareTag("Unit")))
        {
            for (int i = 0; i < worksPanel.transform.FindChild("List").childCount; i++)
                worksPanel.transform.FindChild("List").GetChild(i).GetComponent<Button>().interactable = true;
            squadOpsPanel.transform.FindChild("List/Button").GetComponent<Button>().interactable = hero.GetComponent<Builder>().listBuild.Exists(bld => bld.GetComponent<Kazarma>());
        }
        if (Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Exists(un => un.CompareTag("Lumberjack")))
        {
            lumberButton.GetComponent<Button>().interactable = true;
        }
    }

    public void SelectedUnitNotInSquad(GameObject unit) // выделен(ы) юнит(ы) в отряде
    {
        squadNamePanel.transform.FindChild("InputField").GetComponent<InputField>().interactable = false;
        squadNamePanel.transform.FindChild("InputField").GetComponent<InputField>().text = "";
        
        var button = Instantiate(unitButton);
        button.transform.SetParent(unitListPanel.transform.FindChild("List"));
        button.GetComponent<UnitInSquadButton>().unit = unit;
        button.GetComponent<UnitInSquadButton>().inSquad = false;

        var squadOpsButton = squadOpsPanel.transform.FindChild("List/Button");
        squadOpsButton.GetComponent<Button>().transform.FindChild("Text").GetComponent<Text>().text = "Создать отряд";
        squadOpsButton.GetComponent<Button>().onClick.RemoveAllListeners();
        squadOpsButton.GetComponent<Button>().onClick.AddListener(Camera.main.GetComponent<Camera3dPerson>().MakeNewSquadButton);

        if (Squads.squads.Count >= 12)
            squadOpsButton.GetComponent<Button>().interactable = false;
        else
            squadOpsButton.GetComponent<Button>().interactable = true;

        groupPanel.SetActive(true);
    }

    public void SelectedUnitsNotInSquad(List<GameObject> units) // выделен(ы) юнит(ы) в отряде
    {
        NoneSelected();

        squadNamePanel.transform.FindChild("InputField").GetComponent<InputField>().interactable = false;
        squadNamePanel.transform.FindChild("InputField").GetComponent<InputField>().text = "";

        foreach (var unit in units)
        {
            var button = Instantiate(unitButton);
            button.transform.SetParent(unitListPanel.transform.FindChild("List"));
            button.GetComponent<UnitInSquadButton>().unit = unit;
            button.GetComponent<UnitInSquadButton>().inSquad = false;
        }

        var squadOpsButton = squadOpsPanel.transform.FindChild("List/Button");
        squadOpsButton.GetComponent<Button>().transform.FindChild("Text").GetComponent<Text>().text = "Создать отряд";
        squadOpsButton.GetComponent<Button>().onClick.RemoveAllListeners();
        squadOpsButton.GetComponent<Button>().onClick.AddListener(Camera.main.GetComponent<Camera3dPerson>().MakeNewSquadButton);

        if (Squads.squads.Count >= 12)
            squadOpsButton.GetComponent<Button>().interactable = false;
        else
            squadOpsButton.GetComponent<Button>().interactable = true;

        groupPanel.SetActive(true);
    }

    public void SelectedWorker(GameObject unit) // выделен(ы) юнит(ы) в отряде
    {
        squadNamePanel.transform.FindChild("InputField").GetComponent<InputField>().interactable = false;
        squadNamePanel.transform.FindChild("InputField").GetComponent<InputField>().text = "";

        var button = Instantiate(workerButton);
        button.transform.SetParent(unitListPanel.transform.FindChild("List"));

        lumberButton.SetActive(true);
        lumberButton.GetComponent<Button>().interactable = true;

        var squadOpsButton = squadOpsPanel.transform.FindChild("List/Button");
        squadOpsButton.GetComponent<Button>().transform.FindChild("Text").GetComponent<Text>().text = "Создать отряд";
        squadOpsButton.GetComponent<Button>().onClick.RemoveAllListeners();
        squadOpsButton.GetComponent<Button>().onClick.AddListener(Camera.main.GetComponent<Camera3dPerson>().MakeNewSquadButton);
        squadOpsButton.GetComponent<Button>().interactable = false;

        groupPanel.SetActive(true);
    }

    void WriteSquadNameChanges(string newSquadName)
    {
        if(selectedSquadGO)
            selectedSquadGO.GetComponent<Squad>().squadName = newSquadName;
    }

}
