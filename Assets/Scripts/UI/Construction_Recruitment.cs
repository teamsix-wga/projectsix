﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class Construction_Recruitment : MonoBehaviour
{
    void Update()
    {
        if (gameObject.activeInHierarchy)
            transform.LookAt(Camera.main.transform);
    }

    public void LoadRecruitButtons(List<string> names)
    {
        foreach (var name in names)
        {
            var button = Instantiate(Resources.Load("UI/"+name)) as GameObject;
            var scale = button.transform.localScale;
            button.transform.parent = transform.FindChild("RecruitZone");
            button.transform.localPosition = Vector3.zero;
            button.transform.localScale = scale;
            var who = name.Remove(0, name.IndexOf('_') + 1);
            UnitType type = UnitType.warrior;
            switch (who)
            {
                case "Warrior":
                    type = UnitType.warrior;
                    break;
                case "Mage":
                    type = UnitType.mage;
                    break;
                case "Scout":
                    type = UnitType.scout;
                    break;
                case "Worker":
                    type = UnitType.worker;
                    break;
            }
            button.GetComponent<RecruitmentButton>().SetupButton(transform.parent.gameObject, "Make"+who, type);
        }
    }

    void OnMouseOver()
    {
        Camera3dPerson.canDraw = false;
    }
    void OnMouseExit()
    {
        Camera3dPerson.canDraw = true;
    }
}
