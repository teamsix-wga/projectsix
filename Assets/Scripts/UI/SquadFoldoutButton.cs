﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class SquadFoldoutButton : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    GameObject buffer;
    bool pointerInsideButton;
    bool foldoutOpened;
    List<GameObject> squad; 

    void Start()
    {
        buffer = transform.FindChild("Buffer").gameObject;
        Close();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !pointerInsideButton && foldoutOpened)
            Close();
    }
	
    void Open()
    {
        if(!GetComponent<Button>().interactable || !ClickToMove.canUnselect)
            return;
        buffer.SetActive(true);
        foldoutOpened = true;
        Camera.main.GetComponent<Camera3dPerson>().SwitchSelect();
        foreach (var un in squad)
        {
            Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Add(un);
            un.SendMessage("SelectUnit");
        }
    }

    void Close()
    {
        buffer.SetActive(false);
        foldoutOpened = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (foldoutOpened)
            Close();
        else
            Open();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        pointerInsideButton = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        pointerInsideButton = false;
    }

    public void NewSquad(List<GameObject> sqd)
    {
        if (sqd.Count == 0)
            Destroy(gameObject);

        squad = new List<GameObject>();

        for (int i = 0; i < transform.Find("Buffer").childCount; i++)
        {
            Destroy(transform.Find("Buffer").GetChild(i).gameObject);
        }

        var unitButtonRes = Resources.Load("UI/UnitInSquadButton") as GameObject;
        foreach (var unit in sqd)
        {
            if (!unit)
                continue;
            var unitButton = Instantiate(unitButtonRes);
            unitButton.transform.SetParent(transform.FindChild("Buffer"));
            unitButton.GetComponent<UnitInSquadButton>().unit = unit;
            unitButton.GetComponent<UnitInSquadButton>().units = sqd;

            squad.Add(unit);
        }
        GetComponent<Button>().interactable = squad[0].activeInHierarchy;
    }
}
