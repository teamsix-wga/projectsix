﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RecruitmentTooltip : MonoBehaviour {
    private string data;
    private GameObject tooltip;
    bool act = false;

    void Start()
    {
        tooltip = GameObject.Find("Tooltip").transform.FindChild("TooltipBar").gameObject;
        tooltip.SetActive(false);
    }

    void Update()
    {
        if (act)
        {
            tooltip.transform.position = Input.mousePosition + new Vector3(tooltip.GetComponent<RectTransform>().sizeDelta.x/2, tooltip.GetComponent<RectTransform>().sizeDelta.y / 2, 0);
            if(tooltip.transform.position.x < 0)
                tooltip.transform.position += new Vector3(tooltip.transform.position.x*2, 0,0);
            float byY = tooltip.GetComponent<RectTransform>().position.y - tooltip.GetComponent<RectTransform>().rect.height;
            if (byY < 0)
            {
                tooltip.transform.position = new Vector3(tooltip.transform.position.x, tooltip.transform.position.y - byY, tooltip.transform.position.z);
            }
            float byX = tooltip.GetComponent<RectTransform>().position.x + tooltip.GetComponent<RectTransform>().rect.width;
            if (byX > Screen.width)
            {
                tooltip.transform.position = new Vector3(Screen.width - tooltip.GetComponent<RectTransform>().rect.width, tooltip.transform.position.y, tooltip.transform.position.z);
            }
        }
    }

    public void Activate(string info)
    {
        if (tooltip.activeInHierarchy)
            return;
        data = info;
        ConstructDataString();
        tooltip.SetActive(true);
        act = true;
    }

    public void Deactivate()
    {
        tooltip.SetActive(false);
        act = false;
    }

    public void ConstructDataString()
    {
        tooltip.transform.GetChild(0).GetComponent<Text>().text = data;
    }
}
