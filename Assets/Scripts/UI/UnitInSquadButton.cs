﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class UnitInSquadButton : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject unit;
    public List<GameObject> units; 
    int count = 0;
    float pressedTime;
    Camera3dPerson selectScript;
    public bool inSquad = true;
    GameObject objInfoTooltip;

	void Start ()
	{
        objInfoTooltip = GameObject.Find("Tooltip");
	    selectScript = Camera.main.GetComponent<Camera3dPerson>();
	}

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!ClickToMove.canUnselect)
            return;
        selectScript.SwitchSelect();
        pressedTime = Time.realtimeSinceStartup;
        count++;
        if (count == 2 && Time.realtimeSinceStartup <= pressedTime + 0.7f && inSquad)
        {
            foreach (var un in units)
            {
                selectScript.SelectedUnit.Add(un);
                un.SendMessage("SelectUnit");
            }
            count = 0;
        }
        else
        {
            selectScript.SelectedUnit.Add(unit);
            unit.SendMessage("SelectUnit");

            if (Time.realtimeSinceStartup > pressedTime + 0.7f)
                count = 0;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        objInfoTooltip.GetComponent<ObjectInfoTooltip>().Activate(unit);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        objInfoTooltip.GetComponent<ObjectInfoTooltip>().Deactivate();
    }
}
