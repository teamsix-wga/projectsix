﻿using UnityEngine;
using System.Collections;

public class RecruitmentButton : MonoBehaviour
{
    public GameObject recruitConstruction;
    public string message;
    UnitType unitType;
    GameObject tooltip;
    string infoForTooltip;

    void Start()
    {
        tooltip = GameObject.Find("Tooltip");
    }

    public void SetupButton(GameObject build, string msg, UnitType type)
    {
        if (type == UnitType.warrior)
            infoForTooltip = "Воины - солдаты, сражающиеся в ближнем бою.";
        if (type == UnitType.scout)
            infoForTooltip = "Разведчики.";
        if (type == UnitType.mage)
            infoForTooltip = "Маги. Пыщь-пыщь.";
        if (type == UnitType.worker)
            infoForTooltip = "Рабочие. Незаменимые помощники в хозяйстве.";

        recruitConstruction = build;
        message = msg;
        unitType = type;
    }

    void OnMouseDown()
    {
        if (recruitConstruction.transform.FindChild("Construction_Recruitment/ProductionQueue").childCount >= 6)
        {
            StartCoroutine(FadeText.FadeTxt("Слишком много юнитов в очереди!", Color.red, 3));
            return;
        }
        recruitConstruction.SendMessage("MakeUnit", unitType);
    }

    void OnMouseEnter()
    {
        tooltip.GetComponent<RecruitmentTooltip>().Activate(infoForTooltip);
    }

    void OnMouseOver()
    {
        Camera3dPerson.canDraw = false;
    }

    void OnMouseExit()
    {
        Camera3dPerson.canDraw = true;
        tooltip.GetComponent<RecruitmentTooltip>().Deactivate();
    }
}
