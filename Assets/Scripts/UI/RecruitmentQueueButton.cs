﻿using UnityEngine;
using System.Collections;

public class RecruitmentQueueButton : MonoBehaviour
{
    public Coroutine unitCoroutine = null;
    public GameObject savedWorker;
    public float returnMoneyIfCancel;

    void OnMouseDown()
    {
        if (unitCoroutine != null)
        {
            StopCoroutine(unitCoroutine);
            savedWorker.SetActive(true);
            GameObject.FindGameObjectWithTag("Player").GetComponent<Gold>().currentGold += returnMoneyIfCancel;
        }
        Camera.main.GetComponent<Camera3dPerson>().lumberjacks.Add(savedWorker);
        savedWorker.GetComponent<Lumberjack>().GoBack();
        
        Destroy(gameObject);
    }

    void OnMouseOver()
    {
        Camera3dPerson.canDraw = false;
    }

    void OnMouseExit()
    {
        Camera3dPerson.canDraw = true;
    }
}
