﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class UnitPanelWorks : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    GameObject tooltip;
    public string info = "";

    void Start()
    {
        tooltip = GameObject.Find("Tooltip").gameObject;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        tooltip.GetComponent<RecruitmentTooltip>().Activate(info);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tooltip.GetComponent<RecruitmentTooltip>().Deactivate();
    }
}
