﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class InventorySlot : MonoBehaviour, IDropHandler {
    public int id;
    private Inventory inventory;
    GameObject unit;
    UIController uiController;
    
    void Start()
    {
        uiController = GameObject.Find("UI").GetComponent<UIController>();
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
     }

    // когда опускаем вещь в слот
    public void OnDrop(PointerEventData eventData)
    {
        // получаем вещь, которую опускаем
        ItemData droppedItem = eventData.pointerDrag.GetComponent<ItemData>();

        // если мусорка
        if(gameObject.name == "Trash")
        {
            Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0].GetComponent<InventoryUnit>().DropItem(droppedItem.item, Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0]);
            if (inventory.slots[droppedItem.slot].transform.parent.name != "Slot Panel")
            {
                UpdateParameters(droppedItem.item, -1);
            }
            // обнуляем старый слот
            inventory.items[droppedItem.slot] = new Item();
            Destroy(droppedItem.gameObject);
        }

        // если слот пустой
        if (inventory.items[id].ID == -1 && transform.parent.name == "Slot Panel")
        {
            if(inventory.slots[droppedItem.slot].transform.parent.name != "Slot Panel")
            {
                UpdateParameters(droppedItem.item, -1);
            }
            unit = Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0]; 
            Item itm = unit.GetComponent<InventoryUnit>().items.Find(it => it.slotID == droppedItem.slot);
            itm.slotID = id;
            // обнуляем старый слот
            inventory.items[droppedItem.slot] = new Item();
            // слот, в который сбросили кладём вещь
            inventory.items[id] = droppedItem.item;
            // присваиваем id слота вещи id нового слота
            droppedItem.slot = id;
        }
        // (слот не пуст) если слот не совпадает с тем, откуда мы взяли вещь 
        else if(droppedItem.slot != id && transform.parent.name == "Slot Panel")
        {
            unit = Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0];
            Item itm = unit.GetComponent<InventoryUnit>().items.Find(it => it.slotID == droppedItem.slot);
            itm.slotID = id;
            // получаем вещь, лежащую в слоте (назовем её "старой")
            Transform item = this.transform.GetChild(0);
            itm = unit.GetComponent<InventoryUnit>().items.Find(it => it.slotID == item.GetComponent<ItemData>().slot);
            itm.slotID = droppedItem.slot;
            // слоту старой вещи, присваиваем слот перетаскиваемой вещи
            item.GetComponent<ItemData>().slot = droppedItem.slot;
            // старой вещи присваиваем родителя перетаскиваемой вещи
            item.transform.SetParent(inventory.slots[droppedItem.slot].transform);
            // старой вещи присваиваем положение слота перетаскиваемой вещи
            item.transform.position = inventory.slots[droppedItem.slot].transform.position;
            // id слота новой вещи присваиваем id нового слота
            droppedItem.slot = id;
            droppedItem.transform.SetParent(this.transform);
            droppedItem.transform.position = this.transform.position;

            inventory.items[droppedItem.slot] = item.GetComponent<ItemData>().item;
            inventory.items[id] = droppedItem.item;
        } else 
        if(inventory.items[id].ID == -1 && transform.name == droppedItem.item.type.ToString())
        {
            if (inventory.slots[droppedItem.slot].transform.parent.name != "Slot Panel")
            {
                UpdateParameters(droppedItem.item, -1);
            }
            unit = Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0];
            Item itm = unit.GetComponent<InventoryUnit>().items.Find(it => it.slotID == droppedItem.slot);
            itm.slotID = id;
            // обнуляем старый слот
            inventory.items[droppedItem.slot] = new Item();
            // слот, в который сбросили кладём вещь
            inventory.items[id] = droppedItem.item;
            // присваиваем id слота вещи id нового слота
            droppedItem.slot = id;
            UpdateParameters(droppedItem.item, 1);
        }
        else if (droppedItem.slot != id && transform.name == droppedItem.item.type.ToString())
        {
            unit = Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0];
            Item itm = unit.GetComponent<InventoryUnit>().items.Find(it => it.slotID == droppedItem.slot);
            itm.slotID = id;
            // получаем вещь, лежащую в слоте (назовем её "старой")
            Transform item = this.transform.GetChild(0);
            itm = unit.GetComponent<InventoryUnit>().items.Find(it => it.slotID == item.GetComponent<ItemData>().slot);
            itm.slotID = droppedItem.slot;
            UpdateParameters(item.GetComponent<ItemData>().item, -1);
            // слоту старой вещи, присваиваем слот перетаскиваемой вещи
            item.GetComponent<ItemData>().slot = droppedItem.slot;
            // старой вещи присваиваем родителя перетаскиваемой вещи
            item.transform.SetParent(inventory.slots[droppedItem.slot].transform);
            // старой вещи присваиваем положение слота перетаскиваемой вещи
            item.transform.position = inventory.slots[droppedItem.slot].transform.position;
            // id слота новой вещи присваиваем id нового слота
            droppedItem.slot = id;
            droppedItem.transform.SetParent(this.transform);
            droppedItem.transform.position = this.transform.position;

            inventory.items[droppedItem.slot] = item.GetComponent<ItemData>().item;
            inventory.items[id] = droppedItem.item;
            UpdateParameters(droppedItem.item, 1);
        }
    }

    public void UpdateParameters(Item item, int index)
    {
        unit = Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0];
        
        Transform hand = null, armor = null, bootsL = null, bootsR = null, secondHand = null, handL = null, handR = null;
        
        if(unit.GetComponent<Enemy>()){
            hand = unit.GetComponent<Enemy>().hand;
            handL = unit.GetComponent<Enemy>().handL;
            handR = unit.GetComponent<Enemy>().handR;
            armor = unit.GetComponent<Enemy>().armor;
            bootsL = unit.GetComponent<Enemy>().bootsL;
            bootsR = unit.GetComponent<Enemy>().bootsR;
            secondHand = unit.GetComponent<Enemy>().secondHand;
        }else
        if(unit.GetComponent<Fighter>()){
            hand = unit.GetComponent<Fighter>().hand;
            handL = unit.GetComponent<Fighter>().handL;
            handR = unit.GetComponent<Fighter>().handR;
            armor = unit.GetComponent<Fighter>().armor;
            bootsR = unit.GetComponent<Fighter>().bootsR;
            bootsL = unit.GetComponent<Fighter>().bootsL;
            secondHand = unit.GetComponent<Fighter>().secondHand;
        } else 
        if(unit.GetComponent<FriendUnit>()){
            hand = unit.GetComponent<FriendUnit>().hand;
            handL = unit.GetComponent<FriendUnit>().handL;
            handR = unit.GetComponent<FriendUnit>().handR;
            armor = unit.GetComponent<FriendUnit>().armor;
            bootsL = unit.GetComponent<FriendUnit>().bootsL;
            bootsR = unit.GetComponent<FriendUnit>().bootsR;
            secondHand = unit.GetComponent<FriendUnit>().secondHand;
        }else
        if (unit.GetComponent<Lumberjack>())
            hand = unit.GetComponent<Lumberjack>().hand;

        
        if (item.Damage != 0)
            unit.GetComponent<Parameters>().damage += item.Damage * index;
        if (item.Concentration != 0)
            unit.GetComponent<Parameters>().concentration += item.Concentration * index;
        if (item.Health != 0)
        {
            unit.GetComponent<Parameters>().health += item.Health * index;
            unit.GetComponent<Parameters>().maxHealth += item.Health * index;
        }
        if (item.Defence != 0)
            unit.GetComponent<Parameters>().defence += item.Defence * index;
        if (item.MovingSpeed != 0)
        {
            unit.GetComponent<Parameters>().speed += item.MovingSpeed * index;
            unit.GetComponent<Parameters>().agent.speed = unit.GetComponent<Parameters>().speed;
        }

        switch (unit.tag)
        {
            case "Player":
                if (index == 1)
                    unit.GetComponent<Fighter>().skills.AddRange(item.skills);
                else
                    foreach (var skill in item.skills)
                        unit.GetComponent<Fighter>().skills.Remove(skill);
                unit.GetComponent<Fighter>().ResetSkills();
                break;
            case "Unit":
                if (index == 1)
                    unit.GetComponent<FriendUnit>().skills.AddRange(item.skills);
                else
                    foreach (var skill in item.skills)
                        unit.GetComponent<FriendUnit>().skills.Remove(skill);
                unit.GetComponent<FriendUnit>().ResetSkills();
                break;
            case "Enemy":
                if (index == 1)
                    unit.GetComponent<Enemy>().skills.AddRange(item.skills);
                else
                    foreach (var skill in item.skills)
                        unit.GetComponent<Enemy>().skills.Remove(skill);
                unit.GetComponent<Enemy>().ResetSkills();
                break;
        }

        if (index == -1 && unit.GetComponent<InventoryUnit>()){
            switch (item.type)
            {
                case Item.ItemType.Weapon:
                    Destroy(unit.GetComponent<InventoryUnit>().swordGO);
                    break;
                case Item.ItemType.WorkersToolAxe:
                    Destroy(unit.GetComponent<InventoryUnit>().swordGO);
                    break;
                case Item.ItemType.WorkersToolHammer:
                    Destroy(unit.GetComponent<InventoryUnit>().swordGO);
                    break;
                case Item.ItemType.WorkersToolPickaxe:
                    Destroy(unit.GetComponent<InventoryUnit>().swordGO);
                    break;
                case Item.ItemType.WorkersToolScythe:
                    Destroy(unit.GetComponent<InventoryUnit>().swordGO);
                    break;
                case Item.ItemType.Armor:
                    Destroy(unit.GetComponent<InventoryUnit>().armorGO);
                    break;
                case Item.ItemType.Boots:
                    Destroy(unit.GetComponent<InventoryUnit>().bootsLGO);
                    Destroy(unit.GetComponent<InventoryUnit>().bootsRGO);
                    break;
                case Item.ItemType.SecondWeapon:
                    Destroy(unit.GetComponent<InventoryUnit>().shieldGO);
                    break;
                case Item.ItemType.Gloves:
                    Destroy(unit.GetComponent<InventoryUnit>().glovesRGO);
                    Destroy(unit.GetComponent<InventoryUnit>().glovesLGO);
                    break;
            }
        }
        if(index == 1)
            GameObject.Find("Inventory").GetComponent<Inventory>().EquipModel(item, unit, hand, handL, handR, armor, bootsL, bootsR, secondHand, true);
        
        SetTextParameters();
    }

    public static void UpdateParameters(GameObject who, Item item, int index, bool visible)
    {
        Transform hand = null, armor = null, bootsR = null, bootsL = null, secondHand = null, handL = null, handR = null;

        if (who.GetComponent<Enemy>())
        {
            hand = who.GetComponent<Enemy>().hand;
            armor = who.GetComponent<Enemy>().armor;
            bootsL = who.GetComponent<Enemy>().bootsL;
            bootsR = who.GetComponent<Enemy>().bootsR;
            secondHand = who.GetComponent<Enemy>().secondHand;
        }
        else
        if (who.GetComponent<Fighter>())
        {
            hand = who.GetComponent<Fighter>().hand;
            armor = who.GetComponent<Fighter>().armor;
            bootsL = who.GetComponent<Fighter>().bootsL;
            bootsR = who.GetComponent<Fighter>().bootsR;
            secondHand = who.GetComponent<Fighter>().secondHand;
        }
        else
        if (who.GetComponent<FriendUnit>())
        {
            hand = who.GetComponent<FriendUnit>().hand;
            armor = who.GetComponent<FriendUnit>().armor;
            bootsL = who.GetComponent<FriendUnit>().bootsL;
            bootsR = who.GetComponent<FriendUnit>().bootsR;
            secondHand = who.GetComponent<FriendUnit>().secondHand;
        }
        else
        if (who.GetComponent<Lumberjack>())
            hand = who.GetComponent<Lumberjack>().hand;

        switch (who.tag)
        {
            case "Player":
                if (index == 1)
                    who.GetComponent<Fighter>().skills.AddRange(item.skills);
                else
                    foreach (var skill in item.skills)
                        who.GetComponent<Fighter>().skills.Remove(skill);
                who.GetComponent<Fighter>().ResetSkills();
                break;
            case "Unit":
                if (index == 1)
                    who.GetComponent<FriendUnit>().skills.AddRange(item.skills);
                else
                    foreach (var skill in item.skills)
                        who.GetComponent<FriendUnit>().skills.Remove(skill);
                who.GetComponent<FriendUnit>().ResetSkills();
                break;
            case "Enemy":
                if (index == 1)
                    who.GetComponent<Enemy>().skills.AddRange(item.skills);
                else
                    foreach (var skill in item.skills)
                        who.GetComponent<Enemy>().skills.Remove(skill);
                who.GetComponent<Enemy>().ResetSkills();
                break;
        }

        if (item.Damage != 0)
            who.GetComponent<Parameters>().damage += item.Damage * index;
        if (item.Concentration != 0)
            who.GetComponent<Parameters>().concentration += item.Concentration * index;
        if (item.Health != 0)
        {
            who.GetComponent<Parameters>().health += item.Health * index;
            who.GetComponent<Parameters>().maxHealth += item.Health * index;
        }
        if (item.Defence != 0)
            who.GetComponent<Parameters>().defence += item.Defence * index;
        if (item.MovingSpeed != 0)
        {
            who.GetComponent<Parameters>().speed += item.MovingSpeed * index;
            who.GetComponent<NavMeshAgent>().speed = who.GetComponent<Parameters>().speed;
        }
        if(index == -1 && who.GetComponent<InventoryUnit>()){
            switch (item.type)
            {
                case Item.ItemType.Weapon:
                    Destroy(who.GetComponent<InventoryUnit>().swordGO);
                    break;
                case Item.ItemType.Armor:
                    Destroy(who.GetComponent<InventoryUnit>().armorGO);
                    break;
                case Item.ItemType.Boots:
                    Destroy(who.GetComponent<InventoryUnit>().bootsLGO);
                    Destroy(who.GetComponent<InventoryUnit>().bootsRGO);
                    break;
                case Item.ItemType.SecondWeapon:
                    Destroy(who.GetComponent<InventoryUnit>().shieldGO);
                    break;
                case Item.ItemType.Gloves:
                    Destroy(who.GetComponent<InventoryUnit>().glovesRGO);
                    Destroy(who.GetComponent<InventoryUnit>().glovesLGO);
                    break;
            }
        }
        if(index == 1)
            GameObject.Find("Inventory").GetComponent<Inventory>().EquipModel(item, who, hand, handL, handR, armor, bootsL, bootsR, secondHand, visible);
    }

    public void SetTextParameters()
    {
        unit = Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0];
        Text ui = GameObject.Find("InventoryUI").transform.FindChild("Inventory Panel").transform.FindChild("Parameters").GetComponent<Text>();
        ui.text = "<b>Параметры</b>\n\n";
        ui.text += "Скорость перемещения: <b>"+ unit.GetComponent<Parameters>().speed.ToString()+"</b>\n";
        ui.text += "Здоровье: <b>"+ unit.GetComponent<Parameters>().health.ToString() + "</b>\n";
        ui.text += "Урон: <b>" + Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0].GetComponent<Parameters>().damage.ToString() + "</b>\n";
        ui.text += "Защита: <b>" + Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0].GetComponent<Parameters>().defence.ToString() + "</b>\n";
        ui.text += "Концентрация: <b>" + Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0].GetComponent<Parameters>().concentration.ToString() + "</b>\n";
    }
}
