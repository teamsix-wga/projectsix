﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FriendUnitFlag : MonoBehaviour {
    public enum TypeFlag
    {
        Attack = 1,
        Defence = 2,
        Scout = 3
    }

    public TypeFlag type;

	void Start () {
        SetAreaOfFlag(25f);
    }
	
	void Update () {
	
	}

    public void SetTypeFlag(TypeFlag type)
    {
        this.type = type;
    }

    public void SetAreaOfFlag(float range)
    {
        //transform.FindChild("Checker").GetComponent<SphereCollider>().radius = range;
    }

}
