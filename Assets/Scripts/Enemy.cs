﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Assertions.Comparers;

public class Enemy : MonoBehaviour
{
    public AnimationClip run, stop, dying;
	// дальность "видения"
    public float range;
	// выделенный враг
    public GameObject Opponent;
	// сагрились ли
	bool agro;
	// получили ли урон
	bool hitted;
	// здание, где "родились"
	public GameObject parentBuilding;
	// компонента навигации
	NavMeshAgent agent;
	// наносим ли удар?
    bool gethit;
	// кто нас ударил?
	GameObject whohitted;
    public List<Item> haveItems;
    
    public Transform hand, handL, handR;
    public Transform secondHand;
    public Transform bootsL, bootsR;
    public Transform armor;

    public List<Skill> skills = new List<Skill>();
    public List<float> cooldownTimer;
    int usedSkill = 0;
    public Vector3 standHere;
    GameObject skillFuncManager;

    private bool _canAttack;
    public bool canAttack
    {
        get { return _canAttack; }
        set
        {
            _canAttack = value;
            OnCanAttackChange();
        }
    }
    
    GameObject warBalancer;

    void Start ()
    {
        StartCoroutine(LoadItems());
        canAttack = true;
        warBalancer = GameObject.Find("WarBalancer");
        skillFuncManager = GameObject.Find("SkillsManager");
        transform.FindChild("Checker").GetComponent<SphereCollider>().radius = range / 5;
        if (gameObject.name.Contains("Clone"))
            gameObject.name = gameObject.name.Remove(gameObject.name.IndexOf("(Clone)"), "(Clone)".Length);
        agent = GetComponent<NavMeshAgent>();
		agent.speed = GetComponent<Parameters>().speed;

        if(!GetComponent<Parameters>().visible)
            foreach (Renderer ren in gameObject.GetComponentsInChildren<Renderer>())
                ren.enabled = false;

        skills = GameObject.Find("SkillsManager").GetComponent<SkillsLoader>().LoadSkills(gameObject.name);
        ResetSkills();

        standHere = transform.position;    
    }

    IEnumerator LoadItems()
    {
        yield return new WaitForSeconds(0.2f);
        if (haveItems.Count != 0)
        {
            var itemDB = GameObject.Find("Inventory");
            for (int i = 0; i < haveItems.Count; i++)
            {
                var it = itemDB.GetComponent<ItemDatabase>().database.Find(itm => itm.ID == haveItems[i].ID);
                haveItems[i] = Item.CopyItem(it);
                InventorySlot.UpdateParameters(gameObject, haveItems[i], 1, GetComponent<Parameters>().visible);
            }
        }
    }

    public void ResetSkills()
    {
        cooldownTimer = new List<float>();
        for (int i = 0; i < skills.Count; i++)
            cooldownTimer.Add(0);
    }

    void LateUpdate()
    {
        if(haveItems.Count == 0){
            GenerateItem();
            if(haveItems[0].ID != -1)
                InventorySlot.UpdateParameters(gameObject, haveItems[0], 1, GetComponent<Parameters>().visible);
        }
    }

    void Update ()
    {
        if ((!canAttack && Opponent) || (Act2.endlessBattle && Opponent && Opponent.CompareTag("Player") && name != "Koldyn"))
            Opponent = null;
        if (Vector3.Distance(transform.position, agent.destination) > agent.stoppingDistance + 0.5f && !GetComponent<Parameters>().died && !gethit && !GetComponent<Parameters>().knockedBack)
            if (GetComponent<Animation>())
                GetComponent<Animation>().CrossFade(run.name);
            else
                GetComponent<Animator>().Play(run.name);

        if (!Opponent && Vector3.Distance(agent.destination, transform.position) <= agent.stoppingDistance + 0.5f && !gethit && !GetComponent<Parameters>().knockedBack)
            if (GetComponent<Animation>())
                GetComponent<Animation>().CrossFade(stop.name);
            else
                GetComponent<Animator>().Play(stop.name);

        for (int i = 0; i < skills.Count; i++)
            if (cooldownTimer[i] > 0)
                cooldownTimer[i] -= Time.deltaTime;
            else
                cooldownTimer[i] = 0;

        FindBestSkill();

        if (Opponent && ((Opponent.gameObject.CompareTag("Player") || Opponent.gameObject.CompareTag("Unit")) && Opponent.gameObject.GetComponent<Parameters>().died))
            Opponent = null;

        if (!GetComponent<Parameters>().died) {
            if (!_canAttack)
                return;
            // если нас атаковали, а мы его не выделили 
            if (!Opponent && hitted) {
				// агримся
				agro = true;
				hitted = false;
                Opponent = whohitted;
				// бежим до того, кто ударил
				RunToPlayer ();
			} else
	            if(!gethit) 
					RunToPlayer();
        }
    }

    void FindBestSkill()
    {
        if(gethit)
            return;
        usedSkill = cooldownTimer.FindIndex(coold => Mathf.Approximately(coold, 0));
        if (usedSkill == -1)
            usedSkill = 0;
        for (int i = 0; i < skills.Count; i++)
            if (Mathf.Approximately(cooldownTimer[i], 0) && skills[usedSkill].priority < skills[i].priority)
                usedSkill = i;
    }

    // Функция бега до объекта
    void RunToPlayer() {
        if ((agro && Opponent && Vector3.Distance(transform.position, standHere) > 60 && Vector3.Distance(transform.position, Opponent.transform.position) > 30) || !_canAttack)
        {
            Opponent = null;
            agent.destination = standHere;
            agro = false;
        }

        if (!_canAttack)
            return;

        if(!Opponent)
            Opponent = CheckOpponents();
        
        if (skills[usedSkill].needOpponent && Opponent && !gethit)
        {
            agent.destination = Opponent.GetComponent<Collider>().ClosestPointOnBounds(transform.position);

            // если дистанция до врага позволяет атаковать и враг выделен
            if (Vector3.Distance(agent.destination, GetComponent<Collider>().ClosestPointOnBounds(Opponent.transform.position))
                <= skills[usedSkill].distance + agent.stoppingDistance)
            {
                StopCoroutine("WaitCast");
                // атакуем
                gethit = false;
                StartCoroutine("WaitCast");
            }
        }
        else if (!skills[usedSkill].needOpponent && Opponent && !gethit) // если для скилла не нужен враг, но мы в бою
        {
            StopCoroutine("WaitCast");
            // атакуем
            gethit = false;
            StartCoroutine("WaitCast");
        }
        else
        if(!GetComponent<NPC>() || (GetComponent<NPC>() && GetComponent<NPC>().isRunning) || GetComponent<Parameters>().knockedBack)
        {
            if (agent.destination != standHere)
                agent.destination = standHere;
        }
    }

    public IEnumerator WaitCast()
    {
        if (!_canAttack)
            yield break;
        if (cooldownTimer[usedSkill] > 0)
            goto end;

        if (skills[usedSkill].needOpponent)
            if (!Opponent || Opponent.GetComponent<Parameters>().invulnerability)
                goto end;

        gethit = true;
        agent.destination = transform.position;

        if (skills[usedSkill].clipName != "")
            if (GetComponent<Animation>())
                GetComponent<Animation>().CrossFade(skills[usedSkill].clipName);
            else
                GetComponent<Animator>().Play(skills[usedSkill].clipName);

        yield return new WaitForSeconds(skills[usedSkill].castLength);
        
        if (GetComponent<Animation>())
            GetComponent<Animation>().CrossFade(stop.name);
        else
            GetComponent<Animator>().Play(stop.name);

        Attack();
        end:
        yield return null;
    }

    void Attack()
    {
        if (!_canAttack)
        {
            gethit = false;
            return;
        }
        if (skills[usedSkill].needOpponent && Opponent && !Opponent.GetComponent<Parameters>().died && !Opponent.GetComponent<Parameters>().invulnerability &&
            Vector3.Distance(GetComponent<Collider>().ClosestPointOnBounds(Opponent.transform.position), Opponent.GetComponent<Collider>().ClosestPointOnBounds(transform.position)) <=
                agent.stoppingDistance + skills[usedSkill].distance)
        {
            transform.LookAt(Opponent.transform.position);
            // наносим врагу урон
            skillFuncManager.SendMessage(skills[usedSkill].skillFunc, new whoUseSpell() { who = gameObject, skill = skills[usedSkill], Opponent = Opponent });
            cooldownTimer[usedSkill] = skills[usedSkill].cooldown - GetComponent<Parameters>().concentration;
            if (Opponent.GetComponent<Parameters>().health <= 0)
            {
                Opponent = CheckOpponents();
                if (GetComponent<Animation>())
                    GetComponent<Animation>().CrossFade(stop.name);
                else
                    GetComponent<Animator>().Play(stop.name);
            }
            FindBestSkill();
        }
        else if (!skills[usedSkill].needOpponent)
        {
            skillFuncManager.SendMessage(skills[usedSkill].skillFunc, new whoUseSpell() { who = gameObject, skill = skills[usedSkill], Opponent = Opponent });
            cooldownTimer[usedSkill] = skills[usedSkill].cooldown - GetComponent<Parameters>().concentration;
            FindBestSkill();
        }
        gethit = false;
    }

    // функция получения урона
    public void GetHit(AttackStructure atStr)
    {
        GetComponent<Parameters>().knockedBack = false;

        float damage = atStr.damage;
        GameObject assaulter = atStr.whoHit;
        // уменьшаем здоровье
        if (GetComponent<Parameters>().defence <= damage)
            GetComponent<Parameters>().health -= damage - GetComponent<Parameters>().defence;
        if (GetComponent<Parameters>().selected)
        {
        }
        hitted = true;
        // запоминаем, кто ударил
        whohitted = assaulter;
        // если здоровье меньше 0 и мы не мертвы
        if (GetComponent<Parameters>().health <= 0 && !GetComponent<Parameters>().died)
        {
            GetComponent<NavMeshAgent>().enabled = false;
            if (whohitted.CompareTag("Player"))
                Fighter.Opponent = null;
            GameObject.FindGameObjectWithTag("Player").SendMessage("CheckKilledEnemy", gameObject.name);
            // умираем
            StartCoroutine(Die());
        }
    }

    GameObject CheckOpponents()
    {
        if (!_canAttack)
            return null;
        GameObject enemy = null;
        Collider[] hitColliders = Physics.OverlapSphere(transform.FindChild("Checker").position, range*2);
        // для каждого найденного объекта
        foreach (Collider hit in hitColliders) {
            if (hit && hit.GetComponent<Parameters>() && hit.GetComponent<Parameters>().health < 0)
                continue;
            if ((hit.CompareTag("Player") && !Act2.endlessBattle) || hit.CompareTag("Unit"))
            {
                return hit.gameObject;
            }
            else
            if (hit.CompareTag("FriendBuild"))
            {
                if (hit.GetComponent<Construction>().builded && !hit.GetComponent<Parameters>().died)
                {
                    enemy = hit.gameObject;
                }
            }
            else
                if (hit.CompareTag("Lumberjack"))
                    enemy = hit.gameObject;
        }
        return enemy;
    }

    // функция умирания
    IEnumerator Die() {
        if (warBalancer)
            warBalancer.SendMessage("AnotherKill", gameObject);
        if (Act2.lastBattle && name == "MainEnemy")
        {
            GetComponent<Parameters>().health = 1;
            GetComponent<Parameters>().invulnerability = true;
            yield break;
        }
        if (parentBuilding)
            parentBuilding.GetComponent<EnemySpawn>().spawnedEnemies.Remove(gameObject);
        GetComponent<Parameters>().died = true;
        // играем анимацию смерти  
        if (GetComponent<Animation>())
            GetComponent<Animation>().CrossFade(dying.name);
        else
            GetComponent<Animator>().Play(dying.name);
        DropItems();
        yield return new WaitForSeconds(dying.length);
        Destroy(gameObject);
    }

    void DropItems()
    {
        GameObject hero = GameObject.FindGameObjectWithTag("Player");
        foreach (Item itm in haveItems)
        {
            hero.GetComponent<InventoryUnit>().DropItem(itm, gameObject);
        }
    }

    void GenerateItem()
    {
        Item.ItemQuality qual = Item.ItemQuality.None;
        Item.ItemType tp = Item.ItemType.None;
        // [0;25] - броня; [26;40] - ботинки; [41;55] - перчатки; [56;65] - щит; [66;95] - оружие; >95 трофеи
        int type = Random.Range(0, 101);
        if (type >= 0 && type <= 25)
            tp = Item.ItemType.Armor;
        else
        if (type > 25 && type <= 40)
            tp = Item.ItemType.Boots;
        else
        if (type > 40 && type <= 55)
            tp = Item.ItemType.Gloves;
        else
        if (type > 55 && type <= 65)
            tp = Item.ItemType.SecondWeapon;
        else
        if (type > 65 /*&& type <= 95*/)
            tp = Item.ItemType.Weapon;

        if(tp == Item.ItemType.SecondWeapon)
        {
            qual = Item.ItemQuality.Common;
        } else {
            // [0;50] - сломанное ; [51;70] - повреждённое ; [71;85] - обычное ; [86;95] - хорошее ; [96;100] - отличное
            int quality = Random.Range(0, 101);
            if (quality <= 50)
                qual = Item.ItemQuality.Broken;
            else
            if (quality > 50 && quality <= 70)
                qual = Item.ItemQuality.Damaged;
            else
            if (quality > 70 && quality <= 85)
                qual = Item.ItemQuality.Common;
            else
            if (quality > 85 && quality <= 95)
                qual = Item.ItemQuality.Good;
            else
            if (quality > 95)
                qual = Item.ItemQuality.Rare;
        }

        List<Item> refItems = GameObject.Find("Inventory").GetComponent<ItemDatabase>().FetchItemsForGenerator(qual, tp);
        if (refItems.Count > 0)
        {
            Item refItem = refItems[Random.Range(0, refItems.Count)];
            haveItems.Add(Item.CopyItem(refItem));
        }
    }

    public void SetParentBuilding(GameObject bld) {
        parentBuilding = bld;
    }
    
    public void EnemyDetected(GameObject who)
    {
        if (!enabled || !_canAttack || (Act2.endlessBattle && who.CompareTag("Player")))
            return;
        if(!Opponent || isEnemyImportant(who))
            Opponent = who;
    }

    bool isEnemyImportant(GameObject enemy)
    {
        if (!Opponent)
            return true;

        if (Opponent.GetComponent<Parameters>().invulnerability && !enemy.GetComponent<Parameters>().invulnerability)
            return true;

        if (!Opponent.GetComponent<Parameters>().invulnerability && enemy.GetComponent<Parameters>().invulnerability)
            return false;

        if (enemy.CompareTag("Player") && Act2.endlessBattle)
            return false;

        if (Opponent.CompareTag("Player"))
            return false;

        if (Opponent.CompareTag("Lumberjack") && !enemy.CompareTag("Lumberjack"))
            return true;

        if (Opponent.CompareTag("FriendBuild") && !Opponent.GetComponent<DefenceTower>() && (enemy.CompareTag("Unit") || enemy.CompareTag("Player")))
            return true;

        return false;
    }

    public void SetVisible(bool vis)
    {
        GetComponent<Parameters>().visible = vis;
        foreach (Renderer ren in gameObject.GetComponentsInChildren<Renderer>())
        {
            ren.enabled = vis;
        }
        if (GetComponent<Parameters>().visibleSelection)
            GetComponent<Parameters>().visibleSelection.GetComponent<Renderer>().enabled = vis;
    }

    void OnCanAttackChange()
    {
        if (_canAttack)
        {
            Opponent = CheckOpponents();
        }
        else
        {
            Opponent = null;
        }
    }
}