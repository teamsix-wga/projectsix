﻿using UnityEngine;
using System.Collections;

public class StartScreen : MonoBehaviour {
    
    MovieTexture movie;
    bool canBeLoaded;
    AsyncOperation AOp;

    void Start () {
        GameObject movieTexture = GameObject.Find("Movie");
        Renderer r = movieTexture.GetComponent<Renderer>();
        movie = (MovieTexture)r.material.mainTexture;
        movie.Play();
        StartCoroutine(LoadMenu());
    }
	
	void Update () {
        if (!movie.isPlaying || Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space))
        {
            if (canBeLoaded)
                AOp.allowSceneActivation = true;
        }
	}

    IEnumerator LoadMenu()
    {
        AOp = Application.LoadLevelAsync("MainMenu");
        AOp.allowSceneActivation = false;
        while (!AOp.isDone && AOp.progress != 0.9f)
            yield return new WaitForEndOfFrame();

        canBeLoaded = true;
    }
}
