﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Parameters : MonoBehaviour {
    public float speed = 15;
    public bool died;
    private float _health;
    public float health
    {
        get { return _health;}
        set
        {
            _health = value;
            OnHealthChange();
        }
    }

    public float maxHealth;
    public float damage;
    public int defence;
    public float concentration;
    public NavMeshAgent agent;
    public bool selected;
    public bool visible = false;
    public string objectName = "";
    private static ObjectInfoTooltip objectInfoTooltip;
    public bool needHighlight = true;
    public bool invulnerability = false;
    
    GameObject healthUI;
    public GameObject visibleSelection;
    public float outlineWidth = 0;
    bool mouseOver = false;
    public bool knockedBack = false;

    void Awake()
    {
        healthUI = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/MinimapUI/Minimap_Border/HPBar").gameObject;
        
        health = maxHealth;
        agent = GetComponent<NavMeshAgent>();
        selected = false;

        if(!objectInfoTooltip)
            objectInfoTooltip = GameObject.Find("Tooltip").GetComponent<ObjectInfoTooltip>();

        if (transform.FindChild("VisibleSelection"))
        {
            visibleSelection = transform.FindChild("VisibleSelection").gameObject;
            var mats = visibleSelection.GetComponent<Renderer>().materials;
            foreach (var material in mats)
            {
                material.shader = Shader.Find("Outlined/Silhouette Only");
                material.SetColor("_OutlineColor", !CompareTag("Enemy") ? Color.green : Color.red);

                if (outlineWidth > 0)
                    material.SetFloat("_Outline", outlineWidth);
            }

            visibleSelection.SetActive(false);
        }
    }

    void Update()
    {
        if (Act2.endlessBattle && health <= maxHealth && name != "Koldyn" && name != "Hero")
            health = maxHealth;
    }

    void OnMouseEnter() {
        if((GetComponent<Construction>() && !GetComponent<Construction>().builded) || Act1Startup.animPlaying || DialoguesTalker.talking || !needHighlight || !visible || EventSystem.current.IsPointerOverGameObject())
            return;
        if(visibleSelection)
            visibleSelection.SetActive(true);
        objectInfoTooltip.Activate(gameObject);
        mouseOver = true;
    }
    
    void OnMouseExit() {
        if ((GetComponent<Construction>() && !GetComponent<Construction>().builded) || !needHighlight || !visible)
            return;
        if(!selected && visibleSelection)
            visibleSelection.SetActive(false);
        objectInfoTooltip.Deactivate();
        mouseOver = false;
    }

    void OnHealthChange()
    {
        if (CompareTag("Player"))
        {
            healthUI.GetComponent<Image>().fillAmount = GetComponent<Parameters>().health / GetComponent<Parameters>().maxHealth;
        }
    }

    public void SelectUnit()
    {
        selected = true;
        if (visibleSelection)
            visibleSelection.SetActive(true);
        gameObject.BroadcastMessage("ObjectSelected");
    }

    public void ObjectSelected() { /*заглушка*/ }
    public void ObjectUnselected() {/*заглушка*/}

    public void UnselectUnit()
    {
        selected = false;
        if (visibleSelection && !mouseOver)
            visibleSelection.SetActive(false);
        gameObject.BroadcastMessage("ObjectUnselected");
    }

    public static void MakeColor(GameObject who, Color col)
    {
        foreach (Renderer r in who.GetComponentsInChildren<Renderer>())
        {
            if (r.name == "TextAbove" || r.name == "VisibleSelection")
                continue;
            foreach (Material mat in r.materials)
                mat.color = col;
        }
    }
}

public class AttackStructure
{
    public float damage;
    public GameObject whoHit;
}
