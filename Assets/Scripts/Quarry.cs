﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Quarry : MonoBehaviour {
    public static List<GameObject> stones;
    GameObject quarryPopUp, quarryEffect;
    bool builded,closedPopUp;
    float maxArea = 30f;
    List<GameObject> workingStoners;
    TextMesh countStoners;
    GameObject mainCam;
    GameObject quarryPopupGO;
    GameObject FoW;

	void Start () {
        FoW = GameObject.FindWithTag("FoWProjector");
        quarryPopupGO = Resources.Load("QuarryPopUp") as GameObject;
        stones = new List<GameObject>();
        workingStoners = new List<GameObject>();
        quarryEffect = Instantiate(Resources.Load("QuarryEffect"), transform.position + Vector3.up*10, Quaternion.identity) as GameObject;
        quarryEffect.SetActive(false);
        mainCam = Camera.main.gameObject;
    }

	void Update () {
        if(!FoW.GetComponent<RealFoW>()
            .IsBlackPlace(new Vector2(GetComponent<Collider>().bounds.min.x, GetComponent<Collider>().bounds.min.z),
                        new Vector2(GetComponent<Collider>().bounds.max.x, GetComponent<Collider>().bounds.max.z)))
	    {
	        if (!quarryEffect.activeInHierarchy)
	        {
	            quarryEffect.SetActive(true);
	        }

	        if (!closedPopUp && Vector3.Distance(mainCam.transform.position, transform.position) <= maxArea + 30f)
	        {
	            if (!quarryPopUp)
	            {
	                quarryPopUp = Instantiate(quarryPopupGO, transform.position, Quaternion.identity) as GameObject;
	                Button[] btns = quarryPopUp.GetComponentsInChildren<Button>();
	                btns[0].onClick.RemoveAllListeners();
	                btns[0].onClick.AddListener(AddLumberjacks);
	                btns[1].onClick.RemoveAllListeners();
	                btns[1].onClick.AddListener(DeleteLumberjack);
	                countStoners = quarryPopUp.transform.FindChild("Working/Text").GetComponent<TextMesh>();
	                countStoners.text = "Рабочих нет";
	            }
                if(!quarryPopUp.activeInHierarchy)
                    quarryPopUp.SetActive(true);
	            quarryPopUp.transform.LookAt(mainCam.transform);
	            quarryPopUp.transform.localScale = new Vector3(-1, 1, 1);
	        }
	        else if (Vector3.Distance(mainCam.transform.position, transform.position) > maxArea + 30f)
	        {
	            closedPopUp = false;
	            if (quarryPopUp && quarryPopUp.activeInHierarchy)
	                quarryPopUp.SetActive(false);
	        }
	    }
	}
    
    public void AddLumberjacks()
    {
        GameObject lumber = Camera.main.GetComponent<Camera3dPerson>().lumberjacks.Find(lumb => (!lumb.GetComponent<Lumberjack>().working));

        if (lumber)
        {
            int randomStone = Random.Range(0, stones.Count - 1);
            lumber.GetComponent<Lumberjack>().state = LumberState.Stoner;
            lumber.GetComponent<Lumberjack>().GoToWork(stones[randomStone].GetComponent<Collider>().ClosestPointOnBounds(lumber.transform.position), stones[randomStone]);
            workingStoners.Add(lumber);
            SetCountWorkers();
        }
    }

    public void DeleteLumberjack()
    {
        if (workingStoners.Count > 0)
        {
            if(workingStoners[workingStoners.Count - 1].GetComponent<Lumberjack>().resourcesLimit != 0)
                workingStoners[workingStoners.Count - 1].GetComponent<Lumberjack>().resourcesLimit = 0;
            workingStoners[workingStoners.Count - 1].SetActive(true);
            workingStoners[workingStoners.Count - 1].GetComponent<Lumberjack>().GoBack();
            workingStoners.RemoveAt(workingStoners.Count - 1);
            SetCountWorkers();
        }
    }

    public void DeleteLumberjack(GameObject lumb)
    {
        if (workingStoners.Count > 0)
        {
            lumb.GetComponent<Lumberjack>().GoBack();
            workingStoners.Remove(lumb);
            if (quarryPopUp)
                SetCountWorkers();
        }
    }

    void SetCountWorkers()
    {
        if (workingStoners.Count > 0)
            countStoners.text = "Сейчас работа" + WordWorkEnding(workingStoners.Count) + "\n" + workingStoners.Count + " рабоч" + WordCountEnding(workingStoners.Count);
        else
            countStoners.text = "Рабочих нет";
    }

    string WordCountEnding(int count)
    {
        var num = 0;
        if (count > 0 && count < 10)
            num = count;
        else
            num = count % 10;

        if (num == 1 && count != 11)
            return "ий";
        return "их";
    }

    string WordWorkEnding(int count)
    {
        var num = 0;
        if (count > 0 && count < 10)
            num = count;
        else
            num = count % 10;

        if (num == 1 && count != 11)
            return "ет";
        return "ют";
    }
}
