﻿using UnityEngine;
using System.Collections.Generic;

public class UnitsSphere : MonoBehaviour {

    public float radius;
    List<GameObject> enemiesHere = new List<GameObject>(); 

	void Start () {
        GetComponent<SphereCollider>().radius = radius;
        GetComponent<SphereCollider>().isTrigger = true;
	}

    void OnTriggerEnter(Collider col)
    {
        // если на вошедшем в триггер GO нет такого же скрипта
        if (col.CompareTag("Enemy") || col.CompareTag("EnemyBuild"))
        {
            if (transform.parent.gameObject.CompareTag("Unit") && col.gameObject.GetComponent<Parameters>().health > 0)
            {
                enemiesHere.Add(col.gameObject);
                transform.parent.gameObject.SendMessage("EnemyDetected", col.gameObject);
            }
        }
        if (col.CompareTag("LyingItem") && transform.parent.gameObject.CompareTag("Unit") && col.GetComponent<DroppedItem>().item.type <= (Item.ItemType)11 && col.GetComponent<DroppedItem>().item.type > (Item.ItemType)(-1))
            transform.parent.gameObject.SendMessage("CheckItems");
    }

    void Update()
    {
        if (enemiesHere.Contains(null))
            enemiesHere.RemoveAll(en => en == null);
        if (enemiesHere.Count > 0 && transform.parent.gameObject.CompareTag("Unit") &&
            !transform.parent.gameObject.GetComponent<FriendUnit>().Opponent)
        {
            transform.parent.gameObject.SendMessage("EnemyDetected", enemiesHere[0]);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("Enemy") || col.CompareTag("EnemyBuild"))
        {
            if (transform.parent.gameObject.CompareTag("Unit"))
            {
                enemiesHere.Remove(col.gameObject);
                transform.parent.gameObject.SendMessage("EnemyOut");
            }
        }
        if (col.CompareTag("LyingItem") && transform.parent.gameObject.CompareTag("Unit") && col.GetComponent<DroppedItem>().item.type <= (Item.ItemType)11 && col.GetComponent<DroppedItem>().item.type > (Item.ItemType)(-1))
            transform.parent.gameObject.SendMessage("CheckItems");
    }
}
