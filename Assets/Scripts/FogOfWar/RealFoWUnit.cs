﻿using UnityEngine;
using System.Collections;

public class RealFoWUnit : MonoBehaviour {

    public int radius = 20;
    public bool friend = true;
    GameObject realFoW;

    bool rendering = false;

	void Start () {
        realFoW = GameObject.FindGameObjectWithTag("FoWProjector");
        if (friend)
        {
            realFoW.GetComponent<RealFoW>().FoWUnits.Add(gameObject, transform.position);
            if(GetComponent<Parameters>())
                GetComponent<Parameters>().visible = true;
            realFoW.GetComponent<RealFoW>().UnfogPlace(gameObject, radius);
        }
    }

    void Update()
    {
        if (!friend)
        {
            if (!GetComponent<Parameters>())
                return;
            GetComponent<Parameters>().visible = realFoW.GetComponent<RealFoW>().CanRenderObject(GetComponent<Collider>().bounds.min, GetComponent<Collider>().bounds.max);
            if(GetComponent<Parameters>().visible && !rendering)
            {
                rendering = true;
                gameObject.SendMessage("SetVisible", true);
            }
            if (!GetComponent<Parameters>().visible && rendering)
            {
                rendering = false;
                gameObject.SendMessage("SetVisible", false);
            }
        }
    }

    public bool CheckBlackPlace()
    {
        return
            realFoW.GetComponent<RealFoW>()
                .IsBlackPlace(
                    new Vector2(GetComponent<Collider>().bounds.min.x, GetComponent<Collider>().bounds.min.z),
                    new Vector2(GetComponent<Collider>().bounds.max.x, GetComponent<Collider>().bounds.max.z));
    }

    void OnDestroy()
    {
        if(realFoW) 
            realFoW.GetComponent<RealFoW>().FoWUnits.Remove(gameObject);
    }
}
