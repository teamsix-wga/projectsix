﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class RealFoW : MonoBehaviour {
    Projector projectorFoW;
    public Material mat;
    Texture2D texture;
    public Dictionary<GameObject, Vector3> FoWUnits = new Dictionary<GameObject, Vector3>();
    Color whiteColor;
    public Color fogBlackColor;
    bool needToUpdateTexture = true; // true, если необходимо обновить текстуру тумана

    void Start () {
        whiteColor = Color.white;
        InitProjector();
        GenerateTexture(); // генерируем чёрную текстуру
        LightTexture(true);
        StartCoroutine(UpdateTex());
        StartCoroutine(ApplyFoWTexture());
    }

    void InitProjector()
    {
        GameObject terra = GameObject.Find("Terrain");

        projectorFoW = GetComponent<Projector>();
        projectorFoW.orthographic = true;
        projectorFoW.orthographicSize = terra.GetComponent<Terrain>().GetComponent<Collider>().bounds.extents.x;
        projectorFoW.transform.position = new Vector3(terra.GetComponent<Terrain>().GetComponent<Collider>().bounds.center.x, transform.position.y, terra.GetComponent<Terrain>().GetComponent<Collider>().bounds.center.z);
        projectorFoW.farClipPlane = terra.GetComponent<Terrain>().GetComponent<Collider>().bounds.extents.x;
        projectorFoW.nearClipPlane = -terra.GetComponent<Terrain>().GetComponent<Collider>().bounds.extents.x;
    }
	
	void GenerateTexture()
    {
        if (!projectorFoW)
            InitProjector();

        if (!texture)
        {
            texture = new Texture2D(Mathf.RoundToInt(projectorFoW.orthographicSize) * 2, Mathf.RoundToInt(projectorFoW.orthographicSize) * 2);
            for (int i = 0; i < texture.height; i++)
            {
                for (int j = 0; j < texture.width; j++)
                {
                    texture.SetPixel(i, j, fogBlackColor);
                }
            }
            texture.Apply();
        }

        texture.wrapMode = TextureWrapMode.Clamp;        

        mat.SetTexture("_ShadowTex", texture);

        projectorFoW.material = mat;
    }

    IEnumerator UpdateTex()
    {
        while (true)
        {
            LightTexture(false);
            yield return new WaitForEndOfFrame();
        }
    }

    IEnumerator ApplyFoWTexture()
    {
        while (true)
        {
            if (needToUpdateTexture)
            {
                texture.Apply();
                needToUpdateTexture = false;
            }
            yield return new WaitForSeconds(0.7f);
        }
    }

    void LightTexture(bool firstTime)
    {
        List<GameObject> keys = new List<GameObject>(FoWUnits.Keys);
        // растуманиваем
        foreach (GameObject hero in keys)
        {
            if (!hero || !hero.GetComponent<RealFoWUnit>().friend)
                continue;

            if (hero.transform.position != FoWUnits[hero] || firstTime)
            {
                FoWUnits[hero] = hero.transform.position;
                int radius = hero.GetComponent<RealFoWUnit>().radius;
                int x = Mathf.RoundToInt(hero.transform.position.x);
                int y = Mathf.RoundToInt(hero.transform.position.z);

                for (int i = x - radius; i < x + radius; i++)
                {
                    for (int j = y - radius; j < y + radius; j++)
                    {
                        if ((i - x) * (i - x) + (j - y) * (j - y) <= radius * radius)
                        {
                            texture.SetPixel(i, j, whiteColor);
                        }
                    }
                }
                needToUpdateTexture = true;
            }
        }
    }

    /// <summary>
    /// Функция проверки того, может ли объект рендериться.
    /// </summary>
    /// <param name="colliderFrom"> минимальная координата объекта </param>
    /// <param name="colliderTo"> максимальная координата объекта </param>
    /// <returns></returns>
    public bool CanRenderObject(Vector3 colliderFrom, Vector3 colliderTo)
    {
        int xFrom = Mathf.RoundToInt(colliderFrom.x);
        int zFrom = Mathf.RoundToInt(colliderFrom.z);
        int xTo = Mathf.RoundToInt(colliderTo.x);
        int zTo = Mathf.RoundToInt(colliderTo.z);

        for (int i = xFrom; i < xTo; i++)
        {
            for (int j = zFrom; j < zTo; j++)
            {
                if (texture.GetPixel(i, j) == whiteColor)
                    return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Функция проверки, "касается" ли туман данного места
    /// </summary>
    /// <param name="from"> начальные координаты </param>
    /// <param name="to"> конечные координаты </param>
    /// <returns></returns>
    public bool IsBlackPlace(Vector2 from, Vector2 to)
    {
        int xFrom = Mathf.RoundToInt(from.x);
        int zFrom = Mathf.RoundToInt(from.y);
        int xTo = Mathf.RoundToInt(to.x);
        int zTo = Mathf.RoundToInt(to.y);

        for (int i = xFrom; i < xTo; i++)
        {
            for (int j = zFrom; j < zTo; j++)
            {
                if (texture.GetPixel(i, j) != whiteColor)
                    return true;
            }
        }
        return false;
    }

    /// <summary>
    /// Функция сохранения текстуры тумана на диск
    /// </summary>
    public void SaveTexture()
    {
        byte[] bytes = texture.EncodeToPNG();
        File.WriteAllBytes(Application.dataPath + "/../Saves/FoW.png", bytes);
    }

    /// <summary>
    /// Функция загрузки текстуры тумана 
    /// </summary>
    public void LoadTexture()
    {
        GenerateTexture();
        texture.LoadImage(File.ReadAllBytes(Application.dataPath + "/../Saves/FoW.png"));
    }

    /// <summary>
    /// Функция, открывающая всю территорию, скрытую туманом
    /// </summary>
    public void Unfog()
    {
        for (int i = 0; i < texture.height; i++)
        {
            for (int j = 0; j < texture.width; j++)
            {
                texture.SetPixel(i, j, whiteColor);
            }
        }
        texture.Apply();
        StopCoroutine(UpdateTex());
        StopCoroutine(ApplyFoWTexture());
    }

    /// <summary>
    /// Функция, закрывающая всю территорию
    /// </summary>
    public void MakeFog()
    {
        for (int i = 0; i < texture.height; i++)
        {
            for (int j = 0; j < texture.width; j++)
            {
                texture.SetPixel(i, j, fogBlackColor);
            }
        }
        List<GameObject> keys = new List<GameObject>(FoWUnits.Keys);
        foreach (GameObject hero in keys)
        {
            if (!hero || !hero.GetComponent<RealFoWUnit>().friend)
                continue;

            FoWUnits[hero] = hero.transform.position;
            int radius = hero.GetComponent<RealFoWUnit>().radius;
            int x = Mathf.RoundToInt(hero.transform.position.x);
            int y = Mathf.RoundToInt(hero.transform.position.z);

            for (int i = x - radius; i < x + radius; i++)
            {
                for (int j = y - radius; j < y + radius; j++)
                {
                    if ((i - x) * (i - x) + (j - y) * (j - y) <= radius * radius)
                    {
                        texture.SetPixel(i, j, whiteColor);
                    }
                }
            }
            needToUpdateTexture = true;
        }
        texture.Apply();
    }

    /// <summary>
    /// Открывает область с определённым центром и радиусом
    /// </summary>
    /// <param name="center"> Центр </param>
    /// <param name="rad"> Радиус </param>
    public void UnfogPlace(Vector3 center, int rad)
    {
        int radius = rad;
        int x = Mathf.RoundToInt(center.x);
        int y = Mathf.RoundToInt(center.z);

        for (int i = x - radius; i < x + radius; i++)
        {
            for (int j = y - radius; j < y + radius; j++)
            {
                if ((i - x) * (i - x) + (j - y) * (j - y) <= radius * radius)
                {
                    texture.SetPixel(i, j, whiteColor);
                }
            }
        }
        texture.Apply();
    }

    /// <summary>
    /// Открывает область вокруг GameObject
    /// </summary>
    /// <param name="go"> Объект </param>
    /// <param name="rad"> Радиус </param>
    public void UnfogPlace(GameObject go, int rad)
    {
        if (!texture)
            return;
        
        int radius = rad;
        int x = Mathf.RoundToInt(go.transform.position.x);
        int y = Mathf.RoundToInt(go.transform.position.z);

        for (int i = x - radius; i < x + radius; i++)
        {
            for (int j = y - radius; j < y + radius; j++)
            {
                if ((i - x) * (i - x) + (j - y) * (j - y) <= radius * radius)
                {
                    texture.SetPixel(i, j, whiteColor);
                }
            }
        }
        texture.Apply();
    }
}
