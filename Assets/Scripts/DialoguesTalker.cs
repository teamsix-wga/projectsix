﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using System.Linq;
using FMOD.Studio;
using FMODUnity;

public class DialoguesTalker : MonoBehaviour
{
    List<int> questCanGive = new List<int>(); // id квестов, которые он может дать

    public int dialID = -1;
    public Dialogue dial;
    public Phrase rootPhrase;
    GameObject dialogueUIGO;
    GameObject hero;
    public static bool talking = false;
    public bool canOpenDialogueByClick = true; // можно ли начать диалог, нажав правой кнопкой мыши
    private GameObject questAndDialogueManager;
    private GameObject questButtonGO;
    private float savedTimeScale = 1;
    public bool selectOnClick = true;
    GameObject uiCanBeTurnedOff;

    void Start () {
        uiCanBeTurnedOff = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff").gameObject;
        questAndDialogueManager = GameObject.Find("QuestAndDialogueManager");
        questButtonGO = Resources.Load("QuestButton") as GameObject;
        hero = GameObject.FindGameObjectWithTag("Player");
        dialogueUIGO = GameObject.Find("UI").transform.FindChild("Dialogue").gameObject;
        dialogueUIGO.SetActive(false);
        dial = new Dialogue(-1, new Phrase(), "");
        StartCoroutine(Wait());
	}

    IEnumerator Wait()
    {
        if(Time.realtimeSinceStartup < 1f)
            yield return new WaitForSeconds(1);
        if (dialID != -1)
            dial = questAndDialogueManager.GetComponent<DialoguesManager>().dialoguesDB.Find(di => di.dialID == dialID);
        FindQuests(dial.rootPhrase); // ищем все квесты, которые можем выдать
        GetComponent<QuestGiver>().SetupQuests(questCanGive);
        yield return null;
    }

    public void ReloadDialogue()
    {
        if (dialID != -1)
        {
            if(questAndDialogueManager)
                dial = questAndDialogueManager.GetComponent<DialoguesManager>().dialoguesDB.Find(di => di.dialID == dialID);
            else
                dial = GameObject.Find("QuestAndDialogueManager").GetComponent<DialoguesManager>().dialoguesDB.Find(di => di.dialID == dialID);
            FindQuests(dial.rootPhrase); // ищем все квесты, которые можем выдать
            GetComponent<QuestGiver>().SetupQuests(questCanGive);
        }
    }

    void FindQuests(Phrase phrase)
    {
        questCanGive.Clear();
        if (phrase.questID != -1 && !questCanGive.Exists(id => id == phrase.questID))
        {
            questCanGive.Add(phrase.questID);
        }

        if (phrase.answers.Count > 0)
            foreach(Phrase answer in phrase.answers)
                FindQuests(answer);
    }
	
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0) && GetComponent<Parameters>() && selectOnClick)
                GetComponent<Parameters>().selected = true;
        if (Input.GetMouseButtonDown(1) && !EventSystem.current.IsPointerOverGameObject() && Vector3.Distance(hero.transform.position, transform.position) < 30f && dialID != -1 && canOpenDialogueByClick)
            OpenDialogue();
    }

    public void OpenDialogue()
    {
        if (hero.GetComponent<BubbleDialogue>().bubbleActive)
            return;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        uiCanBeTurnedOff.SetActive(false);
        Camera3dPerson.canMoveCam = false;
        if (hero.GetComponent<Builder>().building)
        {
            hero.GetComponent<Builder>().building = false;
            Destroy(hero.GetComponent<Builder>().bld);
        }
        hero.SendMessage("CheckTalk", gameObject.name);

        if (GetComponent<QuestGiver>().IsQuestsNeededToReward())
        {
            Quest quest = GetComponent<QuestGiver>().questsToReward.Find(q => (!q.rewardGived && q.whoGiveQuest == gameObject));
            if (quest != null)
            {
                dialID = quest.doneDialogueID;
                GetComponent<QuestGiver>().GiveReward();
                ReloadDialogue();
            }
        }
        else
        if(!gameObject.CompareTag("Player"))
        {
            Quest quest = hero.GetComponent<QuestReceiver>().quests.Find(q => (!q.questDone && q.whoGiveQuest == gameObject));
            if (quest != null)
            {
                dialID = quest.waitDialogueID;
                ReloadDialogue();
            }
        }
        talking = true;
        dialogueUIGO.SetActive(true);
        rootPhrase = dial.rootPhrase; // назначаем рут-фразу
        dialogueUIGO.transform.FindChild("DialogueImage/Text").GetComponent<Text>().text = rootPhrase.phrase; // пишем фразу вверху окошка
        dialogueUIGO.transform.FindChild("DialogueImage").FindChild("Buttons").GetComponent<RectTransform>().transform.position = new Vector3(150, 30, 0); // размещаем кнопки
        savedTimeScale = Time.timeScale;
        Time.timeScale = 0; // останавливаем время
        Camera.main.transform.FindChild("DialoguesFMOD").GetComponent<StudioEventEmitter>().Stop();
        if (dial.eventName != "")
        {
            Camera.main.transform.FindChild("DialoguesFMOD").GetComponent<StudioEventEmitter>().Event = "event:/" + dial.eventName;
            Camera.main.transform.FindChild("DialoguesFMOD").GetComponent<StudioEventEmitter>().Lookup();
        }
        NextPhrase(rootPhrase, rootPhrase.parameterVal);
    }

    void NextPhrase(Phrase newPhrase, int fmodParam)
    {
        if (dial.eventName != "")
        {
            Camera.main.transform.FindChild("DialoguesFMOD").GetComponent<StudioEventEmitter>().Stop();
            Camera.main.transform.FindChild("DialoguesFMOD").GetComponent<StudioEventEmitter>().Play();
            Camera.main.transform.FindChild("DialoguesFMOD").GetComponent<StudioEventEmitter>().SetParameter("Parameter 1", fmodParam);
        }
        if (newPhrase.action == Dialogue.Action.Exit)
        {
            dialogueUIGO.SetActive(false);
            if (savedTimeScale > 0)
                Time.timeScale = savedTimeScale;
            else
                Time.timeScale = 1;
            talking = false;
            Camera3dPerson.canMoveCam = true;
            GameObject.Find("CutScenesManager").SendMessage("DialogueClosed");
            uiCanBeTurnedOff.SetActive(true);
        }
        if (newPhrase.action == Dialogue.Action.ExitAndSendMessage)
        {
            dialogueUIGO.SetActive(false);
            if (savedTimeScale > 0)
                Time.timeScale = savedTimeScale;
            else
                Time.timeScale = 1;
            talking = false;
            Camera3dPerson.canMoveCam = true;
            GameObject.Find("CutScenesManager").SendMessage("DialogueClosed");
            uiCanBeTurnedOff.SetActive(true);
            GameObject.Find(newPhrase.whoWillSendMessage).SendMessage(newPhrase.whatMessageWeWillSend);
        }
        if (newPhrase.action == Dialogue.Action.TakeQuest)
        {
            GetComponent<QuestGiver>().AcceptQuest(newPhrase.questID);
            var quest = hero.GetComponent<QuestReceiver>().quests.Find(q => q.ID == newPhrase.questID);
            if (!quest.whoGiveQuest.CompareTag("Player"))
            {
                quest.whoGiveQuest.GetComponent<DialoguesTalker>().dialID = quest.waitDialogueID;
                quest.whoGiveQuest.GetComponent<DialoguesTalker>().ReloadDialogue();
            }
            dialogueUIGO.SetActive(false);
            if (savedTimeScale > 0)
                Time.timeScale = savedTimeScale;
            else
                Time.timeScale = 1;
            talking = false;
            Camera3dPerson.canMoveCam = true;
            GameObject.Find("CutScenesManager").SendMessage("DialogueClosed");
            uiCanBeTurnedOff.SetActive(true);
        }
        if (newPhrase.action == Dialogue.Action.ToRoot)
            newPhrase = rootPhrase;

        dialogueUIGO.transform.FindChild("DialogueImage/Text").GetComponent<Text>().text = newPhrase.phrase;
        for(int i = 0; i < dialogueUIGO.transform.FindChild("DialogueImage").FindChild("Buttons").childCount; i++)
            Destroy(dialogueUIGO.transform.FindChild("DialogueImage").FindChild("Buttons").GetChild(i).gameObject);

        float theBiggestPhraseLength = 50; // для изменения размера кнопки относительно текста в ней
        int biggestCountOfTransfers = 1;
        // создаём кнопки ответов
        foreach (Phrase phrases in newPhrase.answers)
        {
            GameObject button = Instantiate(questButtonGO) as GameObject;
            button.transform.SetParent(dialogueUIGO.transform.FindChild("DialogueImage").FindChild("Buttons").transform);
            button.transform.localScale = Vector3.one;
            button.transform.GetChild(0).GetComponent<Text>().text = phrases.phrase;
            
            if (button.transform.GetChild(0).GetComponent<Text>().preferredWidth > theBiggestPhraseLength)
                theBiggestPhraseLength = button.transform.GetChild(0).GetComponent<Text>().preferredWidth;
            
            if (Screen.width / dialogueUIGO.GetComponent<CanvasScaler>().referenceResolution.x * theBiggestPhraseLength > Screen.width)
            {
                string[] splitted = button.transform.GetChild(0).GetComponent<Text>().text.Split(' '); // разбили строку 

                button.transform.GetChild(0).GetComponent<Text>().text = "";
                int transfers = 1;
                foreach (var splitStr in splitted)
                {
                    var savedText = button.transform.GetChild(0).GetComponent<Text>().text;
                    button.transform.GetChild(0).GetComponent<Text>().text += splitStr + " ";

                    if (Screen.width / dialogueUIGO.GetComponent<CanvasScaler>().referenceResolution.x * button.transform.GetChild(0).GetComponent<Text>().preferredWidth > Screen.width)
                    {
                        button.transform.GetChild(0).GetComponent<Text>().text = savedText + "\n" + splitStr + " ";
                        transfers++;
                        theBiggestPhraseLength = button.transform.GetChild(0).GetComponent<Text>().preferredWidth;
                    }
                }
                if (transfers > biggestCountOfTransfers)
                    biggestCountOfTransfers = transfers;
            }

            Phrase newPhr = phrases;

            if (newPhr.answers.Count > 0)
            {
                button.GetComponent<Button>().onClick.AddListener(() => NextPhrase(newPhr.answers[0], newPhr.parameterVal));
            }
            else
            {
                // если при нажатии на кнопку будет вызываться квест и он ещё не выдан или это не квест
                if ((newPhr.action == Dialogue.Action.TakeQuest && !GetComponent<QuestGiver>().IsQuestGiven(newPhr.questID)) || newPhr.action != Dialogue.Action.TakeQuest)
                {
                    button.GetComponent<Button>().onClick.AddListener(() => NextPhrase(newPhr, newPhr.parameterVal));
                }else 
                if(newPhr.action == Dialogue.Action.TakeQuest && GetComponent<QuestGiver>().IsQuestGiven(newPhr.questID))
                {
                    List<Phrase> phr = new List<Phrase>();
                    Phrase.FindParent(newPhrase, dial.rootPhrase, phr);
                    Phrase parentPhrase = phr.Find(p => p.phrase != "" && p.phrase != newPhr.phrase && p.phrase != newPhrase.phrase);
                    
                    List<Phrase> canWeGivePhrases = new List<Phrase>();
                    GetComponent<QuestGiver>().CanWeGiveMoreQuests(parentPhrase, canWeGivePhrases);
                    if (canWeGivePhrases.Count > 0)
                    {// значит, мы можем выдать квест
                        NextPhrase(canWeGivePhrases[0], canWeGivePhrases[0].parameterVal);
                        break;
                    }
                    foreach (Phrase ph in parentPhrase.answers)
                    {
                        if (ph.action == Dialogue.Action.TakeQuest && !GetComponent<QuestGiver>().IsQuestGiven(ph.questID))
                        {
                            NextPhrase(ph, ph.parameterVal);
                            break;
                        }
                    }

                    NextPhrase(parentPhrase.answers[parentPhrase.answers.Count - 1], parentPhrase.parameterVal);
                    break;
                }
            }
        }
        dialogueUIGO.transform.FindChild("DialogueImage").FindChild("Buttons").GetComponent<GridLayoutGroup>().cellSize = new Vector2(theBiggestPhraseLength + 30, 15 + 10 * biggestCountOfTransfers);
    }
}
