﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class TownHall : MonoBehaviour {
    GameObject hero;

    public Vector3 spawnPoint;
    GameObject visibleFlag;
    public int workerCostInGold = 100;
    public float workerTimeToCreate = 2;
    private GameObject campfireGO, workerGO;
    bool alert = false;

    void Start()
    {
        Camera3dPerson.townhall = gameObject;
        hero = GameObject.FindGameObjectWithTag("Player");
        hero.GetComponent<Builder>().buildingUI.GetComponent<BuildingMenu>().RemoveBuildFromList(new List<string>() { "TownHall" });
        if (GetComponent<Construction>().canRecruit)
        {
            workerGO = Resources.Load("Lumberjack") as GameObject;
            campfireGO = Resources.Load("Campfire") as GameObject;
            spawnPoint = transform.Find("SpawnPoint").position;
        }
    }

    void Update()
    {
        if (alert)
        {
            foreach (var lumberjack in Camera.main.GetComponent<Camera3dPerson>().lumberjacks)
            {
                if (lumberjack.activeInHierarchy && Vector3.Distance(lumberjack.GetComponent<NavMeshAgent>().destination, lumberjack.transform.position) <=
                        lumberjack.GetComponent<NavMeshAgent>().stoppingDistance + 0.4f)
                {
                    lumberjack.SetActive(false);
                    lumberjack.GetComponent<Lumberjack>().working = false;
                }
            }
        }
    }

    public void MakeUnit(UnitType type)
    {
        if (type == UnitType.worker)
        {
            if (hero.GetComponent<Gold>().currentGold >= workerCostInGold)
            {
                    var unitButton = Instantiate(Resources.Load("UI/Townhall_Worker_Queue")) as GameObject;
                    var butScale = unitButton.transform.localScale;

                    unitButton.transform.parent =
                        GetComponent<Construction>().recruitmentMenu.transform.FindChild("ProductionQueue");
                    unitButton.transform.localScale = butScale;
                    unitButton.transform.localPosition = Vector3.zero;
                    unitButton.transform.localEulerAngles = Vector3.zero;
                    unitButton.transform.FindChild("ProgressBar/CountDown").GetComponent<TextMesh>().text =
                        workerTimeToCreate.ToString();
                    unitButton.GetComponent<SpriteRenderer>().color = Color.black;

                hero.GetComponent<Gold>().currentGold -= workerCostInGold;
                unitButton.GetComponent<RecruitmentQueueButton>().returnMoneyIfCancel = workerCostInGold;
                unitButton.GetComponent<RecruitmentQueueButton>().unitCoroutine = StartCoroutine(waitMakeWarrior(unitButton));
            }
            else
                StartCoroutine(FadeText.FadeTxt("Нужно больше золота!", Color.red, 3));
        }
    }

    IEnumerator waitMakeWarrior(GameObject button)
    {
        float time = workerTimeToCreate;
        float oneTimeMeasure = workerTimeToCreate / 100;
        float oneColorMeasure = 1f / 100;
        while (time > 0)
        {
            button.transform.FindChild("ProgressBar/CountDown").GetComponent<TextMesh>().text = time.ToString("#");
            button.GetComponent<SpriteRenderer>().color = Color.Lerp(button.GetComponent<SpriteRenderer>().color, Color.white, oneColorMeasure);
            time -= oneTimeMeasure;
            yield return new WaitForSeconds(oneTimeMeasure);
        }

        var unit = Instantiate(workerGO, spawnPoint, Quaternion.identity) as GameObject;
        unit.name = workerGO.name;
        unit.GetComponent<Lumberjack>().SetTownhall(gameObject);
        hero.SendMessage("CheckCreatedUnit", unit.name);
        Destroy(button);
        yield return null;
    }

    public void ObjectSelected()
    {
        if (GetComponent<Construction>().canRecruit && !visibleFlag && GetComponent<Construction>().builded)
        {
            visibleFlag = Instantiate(campfireGO, transform.FindChild("Place").position, Quaternion.identity) as GameObject;
            visibleFlag.transform.parent = null;
            visibleFlag.transform.localEulerAngles = new Vector3(-90f, -180f, 0f);
        }
    }

    public void AttackAlarm()
    {
        alert = true;
        foreach (var lumberjack in Camera.main.GetComponent<Camera3dPerson>().lumberjacks)
        {
            lumberjack.GetComponent<Lumberjack>().GoBack();
            lumberjack.GetComponent<Lumberjack>().working = true;
        }
    }

    public void StopAttackAlarm()
    {
        alert = false;
        foreach (var lumberjack in Camera.main.GetComponent<Camera3dPerson>().lumberjacks)
        {
            lumberjack.SetActive(true);
        }
    }

    public void ObjectUnselected()
    {
        Destroy(visibleFlag);
    }

    public void OnDestroy()
    {
        hero.GetComponent<Builder>().buildingUI.GetComponent<BuildingMenu>().AddBuildToList(new List<string>() { "TownHall" });
    }
}
