﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class EndlessModeController : MonoBehaviour {

	void Start () {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Builder>().buildingUI.GetComponent<BuildingMenu>().AddBuildToList(new List<string> { "Kazarma", "Church", "Shop", "DefenceTower" });

        var builds = GameObject.FindGameObjectsWithTag("EnemyBuild").ToList();
	    var spotNum = RandVal(3, 6, 1); // количество спотов

	    for (int i = 0; i < builds.Count - spotNum; i++)
	    {
	        var buildToDelete = builds[Random.Range(0, builds.Count)];
	        builds.Remove(buildToDelete);
            Destroy(buildToDelete);
	    }
	}

    int RandVal(int min, int max, int step)
    {
        return Random.Range(0, (max - min) / step + 1) * step + min;
    }
}
