﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Xml.Serialization;

public class DialoguesManager : MonoBehaviour {
    // путь к файлу
    string filepath;
    // база данных диалогов
    public List<Dialogue> dialoguesDB;

    void Start()
    {
        filepath = Application.dataPath + "\\StreamingAssets\\DialoguesDB\\Dialogues.xml";
        dialoguesDB = new List<Dialogue>();
        LoadDialoguesDB();
    }

    /// <summary>
    /// Функция загрузки базы данных диалогов
    /// </summary>
    void LoadDialoguesDB()
    {
        XmlDocument xmlDoc = new XmlDocument();
        XmlNode rootNode = null;

        // загружаем файл или выходим
        if (File.Exists(filepath))
        {
            xmlDoc.Load(filepath);
            rootNode = xmlDoc.DocumentElement;
        }
        else
            return;

        // получили все диалоги из XML файла
        XmlNodeList dialData = rootNode.ChildNodes;

        // по каждому диалогу в файле
        for (int i = 0; i < dialData.Count; i++)
        {
            int dialID = 0;
            string eventName = "";
            dialID = int.Parse(dialData[i].Attributes["ID"].Value); // получаем ID диалога
            if(dialData[i].Attributes["eventName"] != null)
                eventName = dialData[i].Attributes["eventName"].Value;
            XmlNodeList nodes = dialData[i].ChildNodes; // получаем все дочерние элементы
            Phrase rootPhrase = new Phrase(); // начальная фраза
            rootPhrase.type = Dialogue.Type.NPCSay;
            if (nodes[0].Attributes.Count == 1) // если эта фраза не выдаёт квест
                rootPhrase.phrase = nodes[0].Attributes[0].Value;
            else
            { // если фраза выдаёт квест
                rootPhrase.phrase = nodes[0].Attributes[0].Value;
                rootPhrase.questID = int.Parse(nodes[0].Attributes[1].Value);
            }

            LoadDBNodes(rootPhrase, nodes);

            dialoguesDB.Add(new Dialogue(dialID, rootPhrase.answers[0], eventName));
        }
    }

    void LoadDBNodes(Phrase phrase, XmlNodeList nodes)
    {
        foreach(XmlNode node in nodes)
        {
            // разбираем ноду
            Phrase newPhrase = new Phrase();
            if (node.Name == "NPCSay")
            {
                newPhrase.type = Dialogue.Type.NPCSay;
                newPhrase.phrase = node.Attributes[0].Value;
                if (node.Attributes["Parameter"] != null)
                    newPhrase.parameterVal = int.Parse(node.Attributes["Parameter"].Value);
            }
            else
            if (node.Name == "answer")
            {
                newPhrase.type = Dialogue.Type.answer;
                newPhrase.action = (Dialogue.Action)int.Parse(node.Attributes["action"].Value);
                newPhrase.phrase = node.Attributes["phrase"].Value;
                if(node.Attributes["QuestID"] != null)
                    newPhrase.questID = int.Parse(node.Attributes["QuestID"].Value);
                if (node.Attributes["whoMessage"] != null)
                    newPhrase.whoWillSendMessage = node.Attributes["whoMessage"].Value;
                if (node.Attributes["whatMessage"] != null)
                    newPhrase.whatMessageWeWillSend = node.Attributes["whatMessage"].Value;
                if (node.Attributes["Parameter"] != null)
                    newPhrase.parameterVal = int.Parse(node.Attributes["Parameter"].Value);
            }

            if (node.HasChildNodes) // если есть дочерние элементы
            {
                LoadDBNodes(newPhrase, node.ChildNodes);
            }
            phrase.answers.Add(newPhrase);
        }
    }
}

[System.Serializable]
public class Dialogue
{
    // действия при выборе ответа
    public enum Action
    {
        None = 0,
        Exit = 1,
        ToRoot = 2,
        TakeQuest = 3,
        Talk = 4,
        ExitAndSendMessage = 5
    }

    // тип фразы
    public enum Type
    {
        NPCSay = 0,
        answer = 1,
        None = 2
    }

    public int dialID; // идентефикатор диалога
    public Phrase rootPhrase; // начальная фраза
    public string eventName;

    public Dialogue(int ID, Phrase root, string evt)
    {
        this.dialID = ID;
        this.rootPhrase = root;
        eventName = evt;
    }

}

[System.Serializable]
public class Phrase
{
    public string phrase; // фраза
    public Dialogue.Action action; // действие при выборе фразы
    public Dialogue.Type type; // кто говорит (мы или нпс)
    public int questID; // идентефикатор квеста
    public List<Phrase> answers; // список вариантов ответа
    public int parameterVal;
    public string whoWillSendMessage;
    public string whatMessageWeWillSend;

    public Phrase(string phrase, Dialogue.Action action, Dialogue.Type type, int questID, string whoMessage, string whatMessage, int par)
    {
        this.phrase = phrase;
        this.action = action;
        this.type = type;
        this.questID = questID;
        whoWillSendMessage = whoMessage;
        whatMessageWeWillSend = whatMessage;
        parameterVal = par;
    }

    public Phrase()
    {
        phrase = "";
        action = Dialogue.Action.None;
        type = Dialogue.Type.None;
        questID = -1;
        answers = new List<Phrase>();
        whoWillSendMessage = "";
        whatMessageWeWillSend = "";
        parameterVal = 0;
    }

    public static Phrase FindParent(Phrase phrase, Phrase root, List<Phrase> phr)
    {
        if (root.phrase == phrase.phrase)
        {
            phr.Add(root);
        }
        else
            foreach (Phrase ph in root.answers)
            {
                if (ph.phrase == phrase.phrase)
                {
                    phr.Add(root);
                }
                FindParent(phrase, ph, phr);
            }
        return new Phrase();
    }
}
