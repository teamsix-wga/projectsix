﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using FMODUnity;

public class BubbleDialogue : MonoBehaviour {

    GameObject bubbleGO; // загружается из ресурсов
    GameObject bubble;
    public bool bubbleActive;

	void Start () {
        if(!bubbleGO)
            bubbleGO = Resources.Load("BubblePhrase") as GameObject;
	}

    void Update()
    {
        if (bubble && bubbleActive)
        {
            bubble.transform.GetChild(0).GetComponent<RectTransform>().LookAt(Camera.main.transform);
        }
    }

    /// <summary>
    /// Функция вывода сообщения в бабл
    /// </summary>
	public IEnumerator Say(string phrase, float sec, string eventName, int paramVal)
    {
        if (!bubble)
        {
            if(bubbleGO)
                bubble = Instantiate(bubbleGO, new Vector3(transform.position.x, GetComponent<Collider>().bounds.max.y + 2, transform.position.z), Quaternion.identity) as GameObject;
            else
                bubble = Instantiate(Resources.Load("BubblePhrase"), new Vector3(transform.position.x, GetComponent<Collider>().bounds.max.y + 2, transform.position.z), Quaternion.identity) as GameObject;
            bubble.transform.SetParent(gameObject.transform);
        }

        if (bubbleActive)
            StopCoroutine("BubbleHider");

        // в строку влезает 25 символов, потому:
        string[] splitted = phrase.Split(' '); // разбили строку 

        string complete = "";
        int transfers = 1;
        foreach (var splitStr in splitted)
        {
            if ((complete + splitStr+" ").Length > 25 * transfers)
            {
                complete += "\n";
                transfers++;
            }
            complete += splitStr+" ";
        }

        bubble.transform.FindChild("PhraseImage/LayoutGroup").GetComponent<Text>().text = complete;
        if(bubble.transform.FindChild("PhraseImage/LayoutGroup").localScale.x > 0)
            bubble.transform.FindChild("PhraseImage/LayoutGroup").localScale = new Vector3(-bubble.transform.FindChild("PhraseImage/LayoutGroup").localScale.x,
                bubble.transform.FindChild("PhraseImage/LayoutGroup").localScale.y, bubble.transform.FindChild("PhraseImage/LayoutGroup").localScale.z);

        bubble.transform.GetChild(0).GetComponent<RectTransform>().LookAt(Camera.main.transform);

        bubble.SetActive(true);
        bubbleActive = true;

        if (eventName != "")
        {
            bubble.GetComponent<StudioEventEmitter>().Stop();
            bubble.GetComponent<StudioEventEmitter>().Event = "event:/" + eventName;
            bubble.GetComponent<StudioEventEmitter>().Lookup();
            bubble.GetComponent<StudioEventEmitter>().Play();
            bubble.GetComponent<StudioEventEmitter>().SetParameter("Parameter 1", paramVal);
        }

        yield return StartCoroutine(BubbleHider(sec));
    }

    IEnumerator BubbleHider(float sec)
    {
        yield return new WaitForSeconds(sec);
        bubble.GetComponent<StudioEventEmitter>().Stop();
        bubble.SetActive(false);
        bubbleActive = false;
    }
}
