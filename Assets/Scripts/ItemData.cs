﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class ItemData : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler,IPointerDownHandler {
    public Item item;
    public int amount;
    public int slot;
    private Inventory inventory;

    private InventoryTooltip tooltip;  
    private Vector2 offset;

    void Start()
    {
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
        tooltip = inventory.GetComponent<InventoryTooltip>();
    }
        
    public void OnBeginDrag(PointerEventData eventData)
    {
        if(item != null)
        {
            offset = eventData.position - new Vector2(this.transform.position.x, this.transform.position.y);
            this.transform.SetParent(this.transform.parent.parent);
            this.transform.position = eventData.position - offset;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (item != null)
        {
            this.transform.position = eventData.position - offset;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.transform.SetParent(inventory.slots[slot].transform);
        this.transform.position = inventory.slots[slot].transform.position;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        tooltip.Activate(item);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tooltip.Deactivate();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            GameObject unit = Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0];
            // если вещь лежит в инвентаре
            if(slot < unit.GetComponent<InventoryUnit>().slotsCount)
            {
                int slt = 0;
                switch (item.type)
                {
                    case Item.ItemType.Weapon:
                        slt = unit.GetComponent<InventoryUnit>().slotsCount + 3;
                        break;
                    case Item.ItemType.WorkersToolAxe:
                        slt = unit.GetComponent<InventoryUnit>().slotsCount + 3;
                        break;
                    case Item.ItemType.WorkersToolHammer:
                        slt = unit.GetComponent<InventoryUnit>().slotsCount + 3;
                        break;
                    case Item.ItemType.WorkersToolPickaxe:
                        slt = unit.GetComponent<InventoryUnit>().slotsCount + 3;
                        break;
                    case Item.ItemType.WorkersToolScythe:
                        slt = unit.GetComponent<InventoryUnit>().slotsCount + 3;
                        break;
                    case Item.ItemType.Armor:
                        slt = unit.GetComponent<InventoryUnit>().slotsCount + 1;
                        break;
                    case Item.ItemType.Boots:
                        slt = unit.GetComponent<InventoryUnit>().slotsCount + 2;
                        break;
                    case Item.ItemType.SecondWeapon:
                        slt = unit.GetComponent<InventoryUnit>().slotsCount + 4;
                        break;
                    case Item.ItemType.Gloves:
                        slt = unit.GetComponent<InventoryUnit>().slotsCount;
                        break;
                }
                
                // получили вещь в слоте
                Item itm = unit.GetComponent<InventoryUnit>().items.Find(it => it.slotID == slt);                    
                if(itm != null){
                    GameObject newItem = inventory.slots[slot].transform.GetChild(0).gameObject;
                    inventory.slots[slot].GetComponent<InventorySlot>().UpdateParameters(itm, -1);                   
                    // слоту старой вещи присваиваем слот новой
                    inventory.slots[slt].transform.GetChild(0).GetComponent<ItemData>().slot = slot;
                    // изменяем родителя старой вещи
                    inventory.slots[slt].transform.GetChild(0).transform.position = inventory.slots[slot].transform.position;
                    inventory.slots[slt].transform.GetChild(0).SetParent(inventory.slots[slot].transform);
                    
                    inventory.items[slot] = itm;
                    itm.slotID = slot;
                    item.slotID = slt;
                    slot = slt;
                    newItem.transform.SetParent(inventory.slots[slt].transform);
                    newItem.transform.position = inventory.slots[slt].transform.position;
                    inventory.items[slt] = item;
                    inventory.slots[slot].GetComponent<InventorySlot>().UpdateParameters(item, 1);
                }else {
                    GameObject newItem = inventory.slots[slot].transform.GetChild(0).gameObject;
                    inventory.items[slot] = new Item();
                    slot = item.slotID = slt;
                    newItem.transform.SetParent(inventory.slots[slt].transform);
                    newItem.transform.position = inventory.slots[slt].transform.position;
                    inventory.items[slt] = item;
                    inventory.slots[slot].GetComponent<InventorySlot>().UpdateParameters(item, 1);
                }
            }else
            // если вещь надета
            {
                int slt = 0;
                for (int i = 0; i < inventory.slots.Count; i++){
                    if(inventory.slots[i].transform.childCount == 0){
                        slt = i;
                        break;
                    }
                }                
                // получили вещь в слоте
                GameObject newItem = inventory.slots[slot].transform.GetChild(0).gameObject;
                inventory.items[slot] = new Item();
                slot = item.slotID = slt;
                newItem.transform.SetParent(inventory.slots[slt].transform);
                newItem.transform.position = inventory.slots[slt].transform.position;
                inventory.items[slt] = item;
                inventory.slots[slot].GetComponent<InventorySlot>().UpdateParameters(item, -1);
            }
        }
    }
}
