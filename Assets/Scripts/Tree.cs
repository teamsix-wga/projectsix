﻿using UnityEngine;
using System.Collections;

public class Tree : MonoBehaviour {
    public bool busy;
    public float health;

    void Awake () {
        busy = false;
	}
	
    public void LumberStart()
    {
        busy = true;
    }

    public void GetHit(float damage, GameObject lumber)
    {
        if (lumber.GetComponent<Lumberjack>().resourcesLimit < damage)
        {
            lumber.GetComponent<Lumberjack>().carryWood += lumber.GetComponent<Lumberjack>().resourcesLimit;
            lumber.GetComponent<Lumberjack>().resourcesLimit = 0;
        } else
        {
            lumber.GetComponent<Lumberjack>().resourcesLimit -= damage;
            lumber.GetComponent<Lumberjack>().carryWood += damage;
        }

        health -= damage;

        if(health <= 0)
        {
            lumber.GetComponent<Lumberjack>().tree = null;
            Destroy(gameObject);
        }
    }
}
