﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BuildingMenu : MonoBehaviour {
    RectTransform rect;
    float openPos;
    float closedPos;
    public static bool opened;
    public int speed = 12;
    private int curWidth, curHeight;
    private GameObject hero;
    List<string> listOfBuildings = new List<string>();
    private GameObject buildButtonGO;

    void Start ()
    {
        StartListCanBuild(new List<string> { "TownHall", "Kazarma", "Church", "Shop", "DefenceTower" });
        hero = GameObject.FindGameObjectWithTag("Player");
        curHeight = Screen.height;
        curWidth = Screen.width;
        opened = false;
        rect = GetComponent<RectTransform>();
        closedPos = curWidth + GetComponent<RectTransform>().rect.width / 2;
        openPos = curWidth - GetComponent<RectTransform>().rect.width / 2;
        rect.position = new Vector3(closedPos, rect.position.y);
	}

    void Update()
    {
        if (curHeight != Screen.height || curWidth != Screen.width)
        {
            curHeight = Screen.height;
            curWidth = Screen.width;
            closedPos = curWidth + GetComponent<RectTransform>().rect.width / 2;
            openPos = curWidth - GetComponent<RectTransform>().rect.width / 2;
        }

        if (Input.GetKey(KeyCode.Escape))
            CloseMenu();

        if (opened && rect.position.x > openPos)
        {
            rect.position = new Vector3(rect.position.x - speed, rect.position.y);
            if(rect.position.x < openPos)
                rect.position = new Vector3(openPos, rect.position.y);
        }
        else
        if(!opened && rect.position.x < closedPos)
        {
            rect.position = new Vector3(rect.position.x + speed, rect.position.y);
            if (rect.position.x > closedPos)
                rect.position = new Vector3(closedPos, rect.position.y);
        }
    }

    public void OpenClose()
    {
        if(!InventoryUnit.inventoryActive && !Shop.shopOpened && !CommandLine.enableConsole && Camera3dPerson.canMoveCam)
            if (opened)
                CloseMenu();
            else
                OpenMenu();
    }
	
	public void OpenMenu()
    {
        openPos = Screen.width - GetComponent<RectTransform>().rect.width / 2;
	    transform.FindChild("Scroll View/Scrollbar Vertical").GetComponent<Scrollbar>().value = 1;
        opened = true;
    }

    public void CloseMenu()
    {
        closedPos = Screen.width + GetComponent<RectTransform>().rect.width / 2;
        opened = false;
    }

    public void StartListCanBuild(List<string> canBuild)
    {
        GameObject buttons = transform.FindChild("Scroll View/Viewport/Buttons").gameObject;

        for (int i = 0; i < buttons.transform.childCount; i++)
            Destroy(buttons.transform.GetChild(i).gameObject);

        foreach (var build in canBuild)
        {
            if(!buildButtonGO)
                buildButtonGO = Resources.Load("BuildButton") as GameObject;
            GameObject button = Instantiate(buildButtonGO) as GameObject;
            button.GetComponent<Button>().onClick.RemoveAllListeners();
            switch (build)
            {
                case "TownHall":
                    button.GetComponent<Button>().onClick.AddListener(delegate { hero.GetComponent<Builder>().BuildTH(); });
                    button.GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text =
                        "Главное здание\nСтоимость постройки:\n<b>1500 золота\n250 дерева\n150 камня</b>";
                    break;
                case "Kazarma":
                    button.GetComponent<Button>().onClick.AddListener(delegate { hero.GetComponent<Builder>().BuildKazarma(); });
                    button.GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text =
                        "Казарма\nСтоимость постройки:\n<b>500 золота\n200 дерева\n200 камня</b>";
                    break;
                case "Shop":
                    button.GetComponent<Button>().onClick.AddListener(delegate { hero.GetComponent<Builder>().BuildShop(); });
                    button.GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text =
                        "Магазин\nСтоимость постройки:\n<b>750 золота\n300 дерева\n100 камня</b>";
                    break;
                case "DefenceTower":
                    button.GetComponent<Button>().onClick.AddListener(delegate { hero.GetComponent<Builder>().BuildTower(); });
                    button.GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text =
                        "Защитная башня\nСтоимость постройки:\n<b>200 золота\n100 дерева\n50 камня</b>";
                    break;
                case "Church":
                    button.GetComponent<Button>().onClick.AddListener(delegate { hero.GetComponent<Builder>().BuildChurch(); });
                    button.GetComponent<Button>().transform.GetChild(0).GetComponent<Text>().text =
                        "Церковь\nСтоимость постройки:\n<b>1000 золота\n100 дерева\n300 камня</b>";
                    break;
            }
            button.transform.SetParent(buttons.transform);
            button.transform.localScale = Vector3.one;
        }
        listOfBuildings = canBuild;
    }

    void UpdateListCanBuild(List<string> canBuild)
    {
        GameObject buttons = transform.FindChild("Scroll View/Viewport/Buttons").gameObject;
        buttons.transform.GetChild(0).gameObject.SetActive(canBuild.Exists(str => str == "TownHall"));
        buttons.transform.GetChild(1).GetComponent<Button>().interactable = canBuild.Exists(str => str == "Kazarma");
        buttons.transform.GetChild(2).GetComponent<Button>().interactable = canBuild.Exists(str => str == "Church");
        buttons.transform.GetChild(3).GetComponent<Button>().interactable = canBuild.Exists(str => str == "Shop");
        buttons.transform.GetChild(4).GetComponent<Button>().interactable = canBuild.Exists(str => str == "DefenceTower");
    }

    public void RemoveBuildFromList(List<string> blds)
    {
        foreach (var bld in blds)
        {
            listOfBuildings.Remove(bld);
        }
        UpdateListCanBuild(listOfBuildings);
    }

    public void AddBuildToList(List<string> blds)
    {
        foreach (var bld in blds)
        {
            if(!listOfBuildings.Exists(str => str == bld))
                listOfBuildings.Add(bld);
        }
        UpdateListCanBuild(listOfBuildings);
    }
}
