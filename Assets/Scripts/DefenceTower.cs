﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class DefenceTower : MonoBehaviour
{
    float health;
    bool died;
    float destroyedTime;
    UIController uiController;
    public float attackRange;
    GameObject opponent;
    public float skillCooldown;
    float damage;
    int defence;
    public float cooldownTimer;

    private GameObject arrowGO;

    void Awake()
    {
        arrowGO = Resources.Load("Arrow") as GameObject;
        uiController = GameObject.Find("UI").GetComponent<UIController>();
        health = GetComponent<Parameters>().health;
        died = GetComponent<Parameters>().died;
        defence = GetComponent<Parameters>().defence;
        damage = GetComponent<Parameters>().damage;
    }

    void Update()
    {
        if (cooldownTimer > 0)
            cooldownTimer -= Time.deltaTime;
        else
            cooldownTimer = 0;

        if (died && destroyedTime <= Time.time)
        {
            Destroy(gameObject);
            Destroy(this);
        }
        if (opponent && opponent.GetComponent<Parameters>().died)
            opponent = null;

        if (!died)
        {
            // если нажата ЛКМ
            if (Input.GetMouseButtonDown(0))
            {
                // пускаем луч из мышки
                RaycastHit hit = Builder.CastFromCursor();
                // если не попали по зданию
                if (hit.collider && !hit.collider.CompareTag("FriendBuild") && !EventSystem.current.IsPointerOverGameObject())
                {
                    GetComponent<Parameters>().selected = false;
                }
            }

            if(GetComponent<Construction>().builded && GetComponent<Construction>().placed)
            {
                FindAndAttack();
            }
        }
    }

    void OnMouseDown()
    {
        if (!died && GetComponent<Construction>().builded)
        {
            GetComponent<Parameters>().selected = true;
        }
    }

    public void GetHit(AttackStructure atStr)
    {
        float damage = atStr.damage;
        if (defence <= damage)
            health -= damage - defence;
        if (GetComponent<Parameters>().selected)
        {
        }
        GetComponent<Parameters>().health = health;
        if (!died && health <= 0)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Builder>().listBuild.Remove(gameObject);
            destroyedTime = Time.time;
            died = true;
            GetComponent<Parameters>().died = died;
            Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Remove(gameObject);
        }
    }

    public void FindAndAttack()
    {
        // получаем список всех объектов в радиусе range
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, attackRange);
        // если у нас нет выделенного врага
        if (!opponent)
            // для каждого найденного объекта
            foreach (Collider hit in hitColliders)
            {
                // если его тэг нам подходит
                if ((hit.CompareTag("Enemy") || hit.CompareTag("EnemyBuild")) &&  !hit.gameObject.GetComponent<Parameters>().died)
                {
                    // выделяем врага
                    opponent = hit.gameObject;
                    // выходим из цикла
                    break;
                }
            }

        if(opponent != null && Vector3.Distance(transform.GetComponent<Collider>().ClosestPointOnBounds(opponent.transform.position), opponent.GetComponent<Collider>().ClosestPointOnBounds(transform.position)) <= attackRange && cooldownTimer == 0)
        {
            cooldownTimer = skillCooldown;
            GameObject grenade = Instantiate(arrowGO, new Vector3(transform.position.x, gameObject.GetComponent<Collider>().bounds.max.y, transform.position.z), Quaternion.identity) as GameObject;
            grenade.GetComponent<TowerGrenade>().SetOpponent(opponent, damage, gameObject);
        }
    }
}
