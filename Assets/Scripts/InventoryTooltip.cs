﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InventoryTooltip : MonoBehaviour {
    private Item item;
    private string data;
    private GameObject tooltip;
    bool act = false;

    void Start()
    {
        tooltip = GameObject.Find("Tooltip").transform.FindChild("TooltipBar").gameObject;
        tooltip.SetActive(false);
    }

    void Update()
    {
        if (act)
        {
            tooltip.transform.position = Input.mousePosition + new Vector3(tooltip.GetComponent<RectTransform>().sizeDelta.x / 2, tooltip.GetComponent<RectTransform>().sizeDelta.y / 2);
            float byY = tooltip.GetComponent<RectTransform>().position.y - tooltip.GetComponent<RectTransform>().rect.height;
            if (byY < 0)
            {
                tooltip.transform.position = new Vector3(tooltip.transform.position.x, tooltip.transform.position.y - byY, tooltip.transform.position.z);
            }
            float byX = tooltip.GetComponent<RectTransform>().position.x + tooltip.GetComponent<RectTransform>().rect.width;
            if (byX > Screen.width)
            {
                tooltip.transform.position = new Vector3(Screen.width - tooltip.GetComponent<RectTransform>().rect.width, tooltip.transform.position.y, tooltip.transform.position.z);
            }
        }
    }

    public void Activate(Item item)
    {
        if (tooltip.activeInHierarchy)
            return;
        this.item = item;
        ConstructDataString();
        tooltip.SetActive(true);
        act = true;
    }

    public void Deactivate()
    {
        tooltip.SetActive(false);
        act = false;
    }

    public void ConstructDataString()
    {
        switch (item.quality)
        {
            case Item.ItemQuality.Broken:
                data = "<color=#FF0000><b>" + item.Name + "</b></color>\n\n";
                break;
            case Item.ItemQuality.Damaged:
                data = "<color=#FFA000><b>" + item.Name + "</b></color>\n\n";
                break;
            case Item.ItemQuality.Common:
                data = "<color=#FFFFFF><b>" + item.Name + "</b></color>\n\n";
                break;
            case Item.ItemQuality.Good:
                data = "<color=#00FF00><b>" + item.Name + "</b></color>\n\n";
                break;
            case Item.ItemQuality.Rare:
                data = "<color=#8B00FF><b>" + item.Name + "</b></color>\n\n";
                break;
        }

        data += item.Description+"\n\n";
        if (item.Damage != 0)
            data += "Урон: <b>"+item.Damage.ToString() +"</b>\n";
        if (item.Concentration != 0)
            data += "Концентрация: <b>" + item.Concentration.ToString() + "</b>\n";
        if (item.Health != 0)
            data += "Здоровье: <b>" + item.Health.ToString() + "</b>\n";
        if (item.Defence != 0)
            data += "Защита: <b>" + item.Defence.ToString() + "</b>\n";
        if (item.MovingSpeed != 0)
            data += "Скорость перемещения: <b>" + item.MovingSpeed.ToString() + "</b>\n";
        data += "\n";
        data += "Уровень: <b>" + item.ItemLevel.ToString() + "</b>" + "  Тип: <b>" + item.type + "</b>";
        if (InventoryUnit.inventoryActive)
            data += "\n" + "Стоимость продажи в лавку: <color=#FFA000><b>" + item.sellCost+"</b></color>";
        else
        if (Shop.shopOpened)
        {
            if (item.slotID >= Camera3dPerson.hero.GetComponent<InventoryUnit>().slotsCount + 5)
                data += "\n" + "Стоимость покупки: <color=#FFA000><b>" + item.buyCost + "</b></color>";
            else
                data += "\n" + "Стоимость продажи в лавку: <color=#FFA000><b>" + item.sellCost + "</b></color>";
        }
        tooltip.transform.GetChild(0).GetComponent<Text>().text = data;
    }
}
