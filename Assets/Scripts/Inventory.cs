﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class Inventory : MonoBehaviour {
    GameObject inventoryPanel;
    GameObject slotPanel;
    public GameObject inventorySlot;
    public GameObject inventoryItem;
    int busySlot;
    int slotsAmount = 0;
    public List<Item> items = new List<Item>();
    public List<GameObject> slots = new List<GameObject>();

    GameObject ui;
    
    void Start()
    {
        busySlot = 0;
        ui = GameObject.Find("InventoryUI");
        ui.GetComponent<Canvas>().enabled = false;
        inventoryPanel = ui.transform.FindChild("Inventory Panel").gameObject;
        slotPanel = inventoryPanel.transform.FindChild("Slot Panel").gameObject;
    }
    
    public void AddItem(Item itm)
    {
        Item itemToAdd = itm;
        if (itemToAdd.Stackable && IsItemInInventory(itemToAdd))
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].ID == itm.ID)
                {
                    ItemData data = slots[i].transform.GetChild(0).GetComponent<ItemData>();
                    data.amount++;
                    data.transform.GetChild(0).GetComponent<Text>().text = data.amount.ToString();
                    break;
                }
            }
        }else
        // если заполненных слотов столько же, сколько их всего
        if (busySlot == slotsAmount)
            // выйти из функции
            return;
        else
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (items[i].ID == -1)
                {
                    busySlot++;
                    items[i] = itemToAdd;
                    GameObject itemObj = Instantiate(inventoryItem);
                    itemObj.transform.localScale = Vector3.one;
                    itemObj.GetComponent<ItemData>().item = itemToAdd;
                    itemObj.GetComponent<ItemData>().slot = i;
                    itemObj.transform.SetParent(slots[i].transform);
                    itemObj.GetComponent<Image>().sprite = itemToAdd.sprite;
                    itemObj.name = itemToAdd.Name;
                    if (itemToAdd.Stackable)
                    {
                        itemObj.GetComponent<ItemData>().amount++;
                        itemObj.GetComponent<ItemData>().transform.GetChild(0).GetComponent<Text>().text = "1";
                    }
                    itemObj.transform.localPosition = Vector2.zero;
                    break;
                }
            }
        }
    }
    
    public void AddItemToSlot(Item itm, int slotID)
    {
        Item itemToAdd = itm;

        busySlot++;
        items[slotID] = itemToAdd;
        GameObject itemObj = Instantiate(inventoryItem);
        itemObj.GetComponent<ItemData>().item = itemToAdd;
        itemObj.GetComponent<ItemData>().slot = slotID;
        itemObj.transform.SetParent(slots[slotID].transform);
        itemObj.transform.localScale = Vector3.one;
        itemObj.GetComponent<Image>().sprite = itemToAdd.sprite;
        itemObj.name = itemToAdd.Name;
        if (itemToAdd.Stackable)
        {
            itemObj.GetComponent<ItemData>().amount++;
            itemObj.GetComponent<ItemData>().transform.GetChild(0).GetComponent<Text>().text = "1";
        }
        itemObj.transform.localPosition = Vector2.zero;
    }

    public void EquipModel(Item haveItem, GameObject who, Transform hand, Transform handL, Transform handR, Transform armor, Transform bootsL, Transform bootsR, Transform secondHand, bool visible){
        if (haveItem.ID == -1)
            return;
        GameObject han = Instantiate(haveItem.model, who.transform.position, Quaternion.identity) as GameObject;
        GameObject second = null;

        if (haveItem.type == Item.ItemType.Boots || haveItem.type == Item.ItemType.Gloves)
            second = Instantiate(haveItem.model, who.transform.position, Quaternion.identity) as GameObject;

        switch (haveItem.type)
        {
            case Item.ItemType.Weapon:
                han.transform.SetParent(hand);
                break;
            case Item.ItemType.Armor:
                han.transform.SetParent(armor);
                break;
            case Item.ItemType.Boots:
                han.transform.SetParent(bootsR);
                second.transform.SetParent(bootsL);
                break;
            case Item.ItemType.SecondWeapon:
                han.transform.SetParent(secondHand);
                break;
            case Item.ItemType.Gloves:
                han.transform.SetParent(handR);
                second.transform.SetParent(handL);
                break;
            case (Item.ItemType)12:
                han.transform.SetParent(hand);
                break;
            case (Item.ItemType)13:
                han.transform.SetParent(hand);
                break;
            case (Item.ItemType)14:
                han.transform.SetParent(hand);
                break;
            case (Item.ItemType)15:
                han.transform.SetParent(hand);
                break;
        }

        han.transform.localEulerAngles = Vector3.zero;
        han.transform.localPosition = Vector3.zero;
        if (second)
        {
            second.transform.localEulerAngles = Vector3.zero;
            second.transform.localPosition = Vector3.zero;
        }

        if (who.GetComponent<InventoryUnit>()){
            switch (haveItem.type)
            {
                case Item.ItemType.Weapon:
                    who.GetComponent<InventoryUnit>().swordGO = han;
                    break;
                case Item.ItemType.Armor:
                    who.GetComponent<InventoryUnit>().armorGO = han;
                    break;
                case Item.ItemType.Boots:
                    who.GetComponent<InventoryUnit>().bootsRGO = han;
                    who.GetComponent<InventoryUnit>().bootsLGO = second;
                    break;
                case Item.ItemType.SecondWeapon:
                    who.GetComponent<InventoryUnit>().shieldGO = han;
                    break;
                case Item.ItemType.Gloves:
                    who.GetComponent<InventoryUnit>().glovesLGO = second;
                    who.GetComponent<InventoryUnit>().glovesRGO = han;
                    break;
            }
        }

        if(!visible)
            foreach (Renderer ren in han.GetComponentsInChildren<Renderer>())
            {
                ren.enabled = false;
            }
    }

    public void OpenInventory(List<Item> itms, int slotsCount)
    {
        busySlot = 0;
        for (int i = 0; i < slots.Count; i++)
        {
            if (slots[i].transform.childCount == 1)
            {
                Destroy(slots[i].transform.GetChild(0).gameObject);
            }
        }
        for (int i = 0; i < slotsAmount; i++)
        {
            Destroy(slots[i]);
            items[i] = new Item();
        }
        items = new List<Item>();
        slots = new List<GameObject>();
        slotsAmount = slotsCount;

        for (int i = 0; i < slotsAmount; i++)
        {
            items.Add(new Item());
            GameObject instSlot = Instantiate(inventorySlot);
            slots.Add(instSlot);
            slots[i].GetComponent<InventorySlot>().id = i;
            slots[i].transform.SetParent(slotPanel.transform);
            instSlot.transform.localScale = Vector3.one;
        }
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("Gloves").gameObject);
        slots[slots.Count - 1].GetComponent<InventorySlot>().id = slotsAmount;
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("Armor").gameObject);
        slots[slots.Count - 1].GetComponent<InventorySlot>().id = slotsAmount + 1;
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("Boots").gameObject);
        slots[slots.Count - 1].GetComponent<InventorySlot>().id = slotsAmount + 2;
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("Weapon").gameObject);
        slots[slots.Count - 1].GetComponent<InventorySlot>().id = slotsAmount + 3;
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("SecondWeapon").gameObject);
        slots[slots.Count - 1].GetComponent<InventorySlot>().id = slotsAmount + 4;
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("Trash").gameObject);
        slots[slots.Count - 1].GetComponent<InventorySlot>().id = slotsAmount + 5;
        for (int i = 0; i < itms.Count; i++)
        {
            if (itms[i].slotID == -1)
                AddItem(itms[i]);
            else
                AddItemToSlot(itms[i], itms[i].slotID);
        }
    }

    public void CloseInventory()
    {

    }

    bool IsItemInInventory(Item item)
    {
        for(int i = 0; i <items.Count; i++)
            if (items[i].ID == item.ID)
                return true;
        return false;
    }   
}
