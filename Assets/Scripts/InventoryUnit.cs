﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InventoryUnit : MonoBehaviour {
    public static bool inventoryActive;
    public List<Item> items = new List<Item>();
    public GameObject swordGO = null, shieldGO = null, armorGO = null, glovesRGO = null, glovesLGO = null, bootsRGO = null, bootsLGO = null;
    [Tooltip("Количество слотов в инвентаре объекта")]
    public int slotsCount = 6;

    void Awake()
    {
        inventoryActive = false;
    }

    void Update()
    {
        if (!Shop.shopOpened && Input.GetKeyDown(KeyCode.I) && !BuildingMenu.opened && !CommandLine.enableConsole)
        {
            if (Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Count == 1 && Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0] == gameObject)
            {
                if (!inventoryActive)
                    Open();
                else
                    Close();
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape) && inventoryActive)
            Close();
    }

    public void Close()
    {
        inventoryActive = false;
        GameObject.Find("Inventory").GetComponent<Inventory>().CloseInventory();
        GameObject.Find("InventoryUI").GetComponent<Canvas>().enabled = false;
    }

    public void OpenOrClose()
    {
        if (!inventoryActive)
            Open();
        else
            Close();
    }

    public void Open()
    {
        inventoryActive = true;
        GameObject.Find("Inventory").GetComponent<Inventory>().OpenInventory(items, slotsCount);
        SetTextParameters();
        GameObject.Find("InventoryUI").GetComponent<Canvas>().enabled = true;
    }

    public void DropAllItems()
    {
        foreach (Item it in items)
            DropItem(it, gameObject);
    }

    public void DropItem(Item itm, GameObject who)
    {
        if(who == gameObject)
            items.Remove(itm);
        GameObject it = Instantiate(itm.model, who.transform.position + new Vector3(0, who.GetComponent<Collider>().bounds.extents.y, 0), Quaternion.identity) as GameObject;
        it.name = itm.model.name;
        it.tag = "LyingItem";
        it.layer = 8;
        it.AddComponent<Rigidbody>();
        it.AddComponent<DroppedItem>();
        it.SendMessage("SetItem", itm);
        it.GetComponent<Rigidbody>().velocity = (transform.up + new Vector3(Random.Range(0, 0.3f),0, Random.Range(0, 0.3f)) * 10);
        it.GetComponent<SphereCollider>().enabled = true;
        it.GetComponent<BoxCollider>().enabled = true;
    }

    public void SetTextParameters()
    {
        Text ui = GameObject.Find("InventoryUI").transform.FindChild("Inventory Panel").transform.FindChild("Parameters").GetComponent<Text>();
        ui.text = "<b>Параметры</b>\n\n";
        ui.text += "Скорость перемещения: <b>" + GetComponent<Parameters>().speed.ToString() + "</b>\n";
        ui.text += "Здоровье: <b>" + GetComponent<Parameters>().health.ToString() + "</b>\n";
        ui.text += "Урон: <b>" + GetComponent<Parameters>().damage.ToString() + "</b>\n";
        ui.text += "Защита: <b>" + GetComponent<Parameters>().defence.ToString() + "</b>\n";
        ui.text += "Концентрация: <b>" + GetComponent<Parameters>().concentration.ToString() + "</b>\n";
    }

    public int FindFreeSlot()
    {
        for(int i = 0; i < slotsCount; i++)
        {
            if (items.Exists((Item it) => (it.slotID == i)))
                continue;
            else
                return i;
        }
        return -1;
    }
}
