﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions.Comparers;
using UnityEngine.UI;

public enum UnitType
{
    warrior = 0,
    mage = 1,
    scout = 2,
    worker = 3
}

public class FriendUnit : MonoBehaviour {
	// дальность "видения"
	public float range;
	// анимации
	public AnimationClip run, stop, dying;
	// выделенный враг
	public GameObject Opponent;
	// здание, где "родились"
	public GameObject parentBuilding;
	// компонента навигации
	public NavMeshAgent agent;
    // переменная для запоминания, откуда пришли
    public Vector3 cameFrom;
    // состоим ли в отряде
    public bool inSquad = false;
    // номер отряда. 0 - не состоит.
    public int numSquad = 0;
    GameObject targetShop;
    public bool goingToReadyPos = false;
    bool goShopping = false;
    bool goToHeal = false;
    bool healing = false;
    public int healSpeed = 2;
    public bool squadCreated = false;
    public int taskCost = 50;
    bool checkShop = true;
    public Transform hand, handL, handR, secondHand, bootsL, bootsR, armor;
    
    public bool controllingBySquad; // управляет ли нами сейчас отряд

    Vector3 healingPoint;
    public bool needToStopHealing = false;

    public GameObject squadGO;
    public GameObject goingToThisItem;

    private bool _canAttack;

    public bool canAttack
    {
        get { return _canAttack; }
        set
        {
            _canAttack = value;
            OnCanAttackChange();
        }
    }

    private GameObject hero;
    GameObject inventoryOpenButton;
    GameObject skillFuncManager;
    public List<Skill> skills = new List<Skill>();
    public List<float> cooldownTimer;
    bool attacking = false;
    int usedSkill = 0;
    GameObject unitPanelUI;
    GameObject warBalancer;
    public UnitType type;

    void Awake ()
    {
        canAttack = true;
        warBalancer = GameObject.Find("WarBalancer");
        inventoryOpenButton = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/SkillPanel/Inventory").gameObject;
        unitPanelUI = GameObject.Find("UnitPanelUI");
        skillFuncManager = GameObject.Find("SkillsManager");
        hero = GameObject.FindGameObjectWithTag("Player");
        if (CompareTag("Unit") && !Camera.main.GetComponent<Camera3dPerson>().UnitList.Exists(un=>(un == gameObject)))
            Camera.main.GetComponent<Camera3dPerson>().UnitList.Add(gameObject);
        agent = GetComponent<NavMeshAgent>();
		agent.speed = GetComponent<Parameters>().speed;
        agent.stoppingDistance = 2f;
        cameFrom = transform.position;
        if(gameObject.name.Contains("Clone"))
            gameObject.name = gameObject.name.Remove(gameObject.name.IndexOf("(Clone)"), "(Clone)".Length);
        skills = GameObject.Find("SkillsManager").GetComponent<SkillsLoader>().LoadSkills(gameObject.name);
        ResetSkills();
    }

    public void ResetSkills()
    {
        cooldownTimer = new List<float>();
        for (int i = 0; i < skills.Count; i++)
            cooldownTimer.Add(0);
    }

    void Update()
    {
        if (GetComponent<Parameters>().died)
            return;

        if (Vector3.Distance(transform.position, agent.destination) > agent.stoppingDistance && !attacking)
        {
            if(GetComponent<Animation>())
                GetComponent<Animation>().CrossFade(run.name);
            else
                GetComponent<Animator>().Play(run.name);
        }
        else
        if(Vector3.Distance(transform.position, agent.destination) <= agent.stoppingDistance && !attacking)
        {
            if(GetComponent<Animation>())
                GetComponent<Animation>().CrossFade(stop.name);
            else
                GetComponent<Animator>().Play(stop.name);
        }

        for (int i = 0; i < skills.Count; i++)
            if (cooldownTimer[i] > 0)
                cooldownTimer[i] -= Time.deltaTime;
            else
                cooldownTimer[i] = 0;

        FindBestSkill();

        if (parentBuilding == null && inSquad) // если у нас нет казармы, пытаемся её найти
        {
            parentBuilding = squadGO.GetComponent<Squad>().squadCamp;
            healingPoint = cameFrom;
        }else
        if(parentBuilding == null)
        {
            parentBuilding = GameObject.FindGameObjectWithTag("Player").GetComponent<Builder>().listBuild.Find(
                build => build.GetComponent<Kazarma>());
            if(parentBuilding)
                healingPoint = parentBuilding.GetComponent<Kazarma>().spawnPoint;
        }

        if(inSquad && squadGO.GetComponent<Squad>().isComingBack && Vector3.Distance(transform.position, cameFrom) <= agent.stoppingDistance)
            squadGO.GetComponent<Squad>().CheckSquadReadyToGo(); // проверяем готовность отряда

        if (goingToThisItem && !Opponent)
            TryPickupItem();

        if (!controllingBySquad && !Opponent &&(!GetComponent<NPC>() || (GetComponent<NPC>() && !GetComponent<NPC>().isRunning))) // если нами не управляет отряд
        {
            // если мы идём к позиции готовности и уже пришли
            if (goingToReadyPos && Vector3.Distance(transform.position, cameFrom) <= agent.stoppingDistance)
            {
                goingToReadyPos = false; // не идём к позиции
                if (inSquad && !squadGO.GetComponent<Squad>().isSquadReady && !attacking)
                    squadGO.GetComponent<Squad>().CheckSquadReadyToGo(); // проверяем готовность отряда
            }
            if (goingToReadyPos && Vector3.Distance(transform.position, cameFrom) > agent.stoppingDistance && Vector3.Distance(agent.destination, cameFrom) > agent.stoppingDistance)
                agent.destination = cameFrom;

            if (!goingToReadyPos)
                HealAndShopping();

            if (goShopping && !goingToReadyPos)
            {
                GoShopping();
            }

            if (goToHeal && !healing && parentBuilding != null && Vector3.Distance(transform.position, healingPoint) <= agent.stoppingDistance)
            {
                healing = true;
                needToStopHealing = false;
                StartCoroutine(Healing());
                goToHeal = false;
            }
        }

        if (Opponent && _canAttack)
        {
            if (inSquad && !squadGO.GetComponent<Squad>().isAttacking)
                squadGO.SendMessage("AttackEnemy", Opponent);
            TryToAttack();
        }
    }

    void TryToAttack()
    {
        if (!Opponent || attacking || !_canAttack)
            return;
        if (skills[usedSkill].needOpponent)
        {
            agent.destination = Opponent.transform.position;

            // если дистанция до врага позволяет атаковать и враг выделен
            if (Vector3.Distance(agent.destination, GetComponent<Collider>().ClosestPointOnBounds(Opponent.transform.position))
                <= skills[usedSkill].distance + agent.stoppingDistance)
            {
                StopCoroutine("WaitCast");
                StartCoroutine("WaitCast");
            }
        }
        else if (!skills[usedSkill].needOpponent) // если для скилла не нужен враг, но мы в бою
        {
            StopCoroutine("WaitCast");
            StartCoroutine("WaitCast");
        }
    }

    public IEnumerator WaitCast()
    {
        if (!_canAttack)
            yield break;
        if (!Mathf.Approximately(cooldownTimer[usedSkill], 0) || cooldownTimer[usedSkill] > 0)
            goto end;

        if (skills[usedSkill].needOpponent)
            if (!Opponent || Opponent.GetComponent<Parameters>().invulnerability)
                goto end;

        attacking = true;
        agent.destination = transform.position;

        if(skills[usedSkill].clipName != "")
            if(GetComponent<Animation>())
                GetComponent<Animation>().Play(skills[usedSkill].clipName);
            else
                GetComponent<Animator>().Play(skills[usedSkill].clipName);

            yield return new WaitForSeconds(skills[usedSkill].castLength);

        if(GetComponent<Animation>())
            GetComponent<Animation>().CrossFade(stop.name);
        else
            GetComponent<Animator>().Play(stop.name);

        Attack();
        end:
        yield return null;
    }

    void FindBestSkill()
    {
        if(attacking)
            return;
        usedSkill = cooldownTimer.FindIndex(coold => Mathf.Approximately(coold, 0));
        if (usedSkill == -1)
            usedSkill = 0;
        for (int i = 0; i < skills.Count; i++)
            if (FloatComparer.AreEqual(0, cooldownTimer[i], 0.01f) && skills[usedSkill].priority < skills[i].priority)
                usedSkill = i;
    }

    IEnumerator Healing()
    {
        while (GetComponent<Parameters>().health < GetComponent<Parameters>().maxHealth && !needToStopHealing)
        {
            GetComponent<Parameters>().health += healSpeed;
            if (GetComponent<Parameters>().health >= GetComponent<Parameters>().maxHealth)
            {
                healing = false;
                GetComponent<Parameters>().health = GetComponent<Parameters>().maxHealth;
                if (GetComponent<Parameters>().selected)
                {
                }
                HealAndShopping();
            }
            if (GetComponent<Parameters>().selected)
            {
            }
            yield return new WaitForSeconds(1); // ждём 1 секунду
        }
    }
	
    void EnemyDetected(GameObject opp)
    {
        if (!_canAttack || !enabled)
            return;
        if (!Opponent)
            if(inSquad)
                squadGO.SendMessage("AttackEnemy", opp);
            else 
            if (!inSquad)
                Opponent = opp;
    }

    void EnemyOut()
    {
        if (Opponent && Opponent.GetComponent<Parameters>().health > 0)
            return;
        if (!_canAttack)
            return;
        var maybeEnemyHere = CheckOpponents();
        if (maybeEnemyHere && maybeEnemyHere.GetComponent<Parameters>().health > 0)
            if(inSquad && !squadGO.GetComponent<Squad>().isAttacking)
                squadGO.SendMessage("AttackEnemy", maybeEnemyHere);
            else 
            if (!inSquad)
                Opponent = maybeEnemyHere;

        if(!maybeEnemyHere)
        {
            if (inSquad)
                squadGO.SendMessage("StopAttackingEnemy");
            else
                Opponent = null;
            CheckItems();
        }
    }

    /// <summary>
    /// Проверка, пришёл ли юнит на место
    /// </summary>
    /// <returns></returns>
    public bool isReady()
    {
        if (Vector3.Distance(transform.position, agent.destination) <= agent.stoppingDistance && !attacking)
        {
            if (GetComponent<NPC>())
            {
                if (GetComponent<NPC>().isRunning)
                    return false;
                else
                    return true;
            }
            else
                return true;
        }
        else
            return false;
    }

    void SetOpponent(GameObject enemy)
    {
        Opponent = enemy;
    }
    void SetNullOpponent()
    {
        Opponent = null;
    }
	
    void HealAndShopping()
    {
        // если здоровье неполное
        if(GetComponent<Parameters>().health < GetComponent<Parameters>().maxHealth && parentBuilding != null)
        {
            agent.destination = healingPoint;
            goToHeal = true;
        } else
        // если здоровье полное
        {
            if (!checkShop)
                return;
            checkShop = false;
            if (!goShopping && !goingToReadyPos)
            {
                int cnt = 0;
                if (GetComponent<InventoryUnit>().items.Count > 0)
                    for (int i = 0; i < GetComponent<InventoryUnit>().slotsCount; i++)
                    {
                        if (GetComponent<InventoryUnit>().items.Exists(it => it.slotID == i))
                            cnt++;
                    }
                // нашли все магазины
                List<GameObject> shops = new List<GameObject>();
                if(hero.GetComponent<Builder>().listBuild.Count > 0)
                    shops = hero.GetComponent<Builder>().listBuild.FindAll(
                        bld => (bld.GetComponent<Shop>() && bld.GetComponent<Construction>().builded));
                // если есть хоть 1 магазин
                if (shops.Count > 0)
                {
                    // ищем ближайший магазин
                    targetShop = shops[0];
                    for (int i = 0; i < shops.Count; i++)
                    {
                        if (Vector3.Distance(shops[i].transform.position, transform.position) < Vector3.Distance(targetShop.transform.position, transform.position))
                            targetShop = shops[i];
                    }
                }
                else
                if (transform.position != cameFrom || !goingToReadyPos)
                {
                    if ((GetComponent<NPC>() && !GetComponent<NPC>().isRunning) || !GetComponent<NPC>())
                    {
                        goingToReadyPos = true;
                        agent.destination = cameFrom;
                    }
                }
                // если есть вещи на продажу
                if ((cnt > 0 && shops.Count > 0) || (shops.Count > 0 && (targetShop.GetComponent<Shop>().assortiment.Exists(
                    it => it.buyCost <= GetComponent<Gold>().currentGold) || 
                                                    targetShop.GetComponent<Shop>().uniqueAssortiment.Exists(
                                                        it => it.buyCost <= GetComponent<Gold>().currentGold))))
                {
                    bool needToShop = false;
                    if(cnt > 0)
                        needToShop = true;
                    else {
                        // ассортимент уникальных вещей, что мы можем купить
                        List<Item> unAssort = targetShop.GetComponent<Shop>().uniqueAssortiment.FindAll(
                            it => it.buyCost <= GetComponent<Gold>().currentGold);
                        foreach(Item unIt in unAssort){
                            // если есть вещь в слоте
                            Item itInSlot = GetComponent<InventoryUnit>().items.Find(it => it.slotID == (int) unIt.type);
                            if(itInSlot != null){
                                //проверяем, если вещь лучше
                                if((int)unIt.quality > (int)itInSlot.quality)
                                {
                                    needToShop = true;
                                    break;
                                }
                            } else {
                                needToShop = true;
                                break;
                            }
                        }
                        List<Item> assort = targetShop.GetComponent<Shop>().assortiment.FindAll(
                            it => it.buyCost <= GetComponent<Gold>().currentGold);
                        foreach(Item commonIt in assort){
                            // если есть вещь в слоте
                            Item itInSlot = GetComponent<InventoryUnit>().items.Find(
                                it => it.slotID == (int) commonIt.type);
                            if(itInSlot != null){
                                //проверяем, если вещь лучше
                                if((int)commonIt.quality > (int)itInSlot.quality)
                                    needToShop = true;
                            } else {
                                needToShop = true;
                                break;
                            }
                        }

                    }
                    if(needToShop){
                        agent.destination = targetShop.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
                        goShopping = true;
                    }
                }
                else {
                    if (transform.position != cameFrom || !goingToReadyPos)
                    {
                        if ((GetComponent<NPC>() && !GetComponent<NPC>().isRunning) || !GetComponent<NPC>())
                        {
                            goingToReadyPos = true;
                            agent.destination = cameFrom;
                        }
                    }
                }
            }
        }
    }

    void GoShopping()
    {
        if (Vector3.Distance(transform.position, targetShop.GetComponent<Collider>().ClosestPointOnBounds(transform.position)) <= agent.stoppingDistance + 0.5f)
        {
            SellAndBuy();
        }
        else{
            agent.destination = targetShop.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
        }
    }

    void SellAndBuy()
    {
        targetShop.GetComponent<Shop>().UpdateAssortiment(); 
        // Продали шмотки
        for(int i = 0; i < GetComponent<InventoryUnit>().items.Count; i++)
        {
            if(GetComponent<InventoryUnit>().items[i].slotID >= 0 && GetComponent<InventoryUnit>().items[i].slotID < GetComponent<InventoryUnit>().slotsCount)
            {
                GetComponent<Gold>().currentGold += GetComponent<InventoryUnit>().items[i].sellCost;
                int itmQual = (int)GetComponent<InventoryUnit>().items[i].quality;

                if(itmQual > 2)
                    targetShop.GetComponent<Shop>().uniqueAssortiment.Add(GetComponent<InventoryUnit>().items[i]);
                GetComponent<InventoryUnit>().items.RemoveAt(i);
            }
        }

        // Покупаем
        for(int i = GetComponent<InventoryUnit>().slotsCount; i < GetComponent<InventoryUnit>().slotsCount+5; i++)
        {
            Item item = GetComponent<InventoryUnit>().items.Find(it => it.slotID == i);
            if(item != null)
            {
                int itmQual = (int)item.quality;

                // если качество одетой вещи хуже, чем в магазине
                if(itmQual < 2)
                {
                    Item shopItem = targetShop.GetComponent<Shop>().uniqueAssortiment.Find(it => it.type == item.type);
                    bool unique = false;
                    if(shopItem == null)
                        // ищем такого же типа вещь в магазине
                        shopItem = targetShop.GetComponent<Shop>().assortiment.Find(it => it.type == item.type);
                    else
                        unique = true;
                    // если стоимость вещи в магазине меньше, чем золота у юнита + стоимость вещи в слоте
                    if(shopItem.buyCost <= GetComponent<Gold>().currentGold + item.sellCost)
                    {
                        // продали старую вещь
                        GetComponent<Gold>().currentGold += item.sellCost;
                        GetComponent<InventoryUnit>().items.Remove(item);

                        Item itemToAdd = Item.CopyItem(shopItem);
                        itemToAdd.slotID = item.slotID;
                        GetComponent<Gold>().currentGold -= shopItem.buyCost;
                        if(unique)
                            targetShop.GetComponent<Shop>().uniqueAssortiment.Remove(shopItem);
                        GetComponent<InventoryUnit>().items.Add(itemToAdd);
                    }
                }
            } else
            {
                Item.ItemType imtType = (Item.ItemType)i;

                Item shopItem = targetShop.GetComponent<Shop>().uniqueAssortiment.Find(
                    it => (it.type == imtType && it.buyCost <= GetComponent<Gold>().currentGold));
                bool unique = false;
                if(shopItem == null)
                    // ищем такого же типа вещь в магазине
                    shopItem = targetShop.GetComponent<Shop>().assortiment.Find(
                        it => (it.type == imtType && it.buyCost <= GetComponent<Gold>().currentGold));
                else
                    unique = true;
                if (shopItem != null && shopItem.buyCost <= GetComponent<Gold>().currentGold)
                {
                    Item itemToAdd = Item.CopyItem(shopItem);
                    itemToAdd.slotID = i;
                    GetComponent<Gold>().currentGold -= shopItem.buyCost;
                    if(unique)
                        targetShop.GetComponent<Shop>().uniqueAssortiment.Remove(shopItem);
                    GetComponent<InventoryUnit>().items.Add(itemToAdd);
                }
            }
        }
        
        goShopping = false;
        if ((GetComponent<NPC>() && !GetComponent<NPC>().isRunning) || !GetComponent<NPC>())
        {
            goingToReadyPos = true;
            agent.destination = cameFrom;
        }
    }

	// функция получения урона
	public void GetHit(AttackStructure atStr) {
        float damage = atStr.damage;
        GameObject assaulter = atStr.whoHit;

        if (!Opponent && !inSquad)
            Opponent = assaulter;
        if(!Opponent && inSquad)
            squadGO.SendMessage("AttackEnemy", assaulter);

        if (damage >= GetComponent<Parameters>().defence)
            // уменьшаем здоровье
            GetComponent<Parameters>().health -= damage - GetComponent<Parameters>().defence;
	    // если здоровье меньше 0 и мы не мертвы
		if(GetComponent<Parameters>().health <= 0 && !GetComponent<Parameters>().died) {
            // умираем
            Die();
		}
	}
	
	// функция атаки
	void Attack()
	{
	    if (!_canAttack)
	    {
            attacking = false;
            return;
	    }

	    if (skills[usedSkill].needOpponent && Opponent && !Opponent.GetComponent<Parameters>().invulnerability && !Opponent.GetComponent<Parameters>().died &&
	        Vector3.Distance(GetComponent<Collider>().ClosestPointOnBounds(Opponent.transform.position), Opponent.transform.position) <= agent.stoppingDistance + skills[usedSkill].distance)
	    {
            transform.LookAt(Opponent.transform.position);
            // наносим врагу урон
            skillFuncManager.SendMessage(skills[usedSkill].skillFunc, new whoUseSpell() { who = gameObject, skill = skills[usedSkill], Opponent = Opponent });
            cooldownTimer[usedSkill] = skills[usedSkill].cooldown - GetComponent<Parameters>().concentration;
            if (Opponent.GetComponent<Parameters>().health <= 0)
            {
                if (inSquad)
                    squadGO.GetComponent<Squad>().StopAttackingEnemy();
                EnemyOut();
                if (GetComponent<Animation>())
                    GetComponent<Animation>().CrossFade(stop.name);
                else
                    GetComponent<Animator>().Play(stop.name);
            }
            FindBestSkill();
        }
        else if (!skills[usedSkill].needOpponent)
        {
            skillFuncManager.SendMessage(skills[usedSkill].skillFunc, new whoUseSpell() { who = gameObject, skill = skills[usedSkill], Opponent = Opponent });
            cooldownTimer[usedSkill] = skills[usedSkill].cooldown - GetComponent<Parameters>().concentration;
            FindBestSkill();
        }
        attacking = false;
    }
	
	// функция умирания
	void Die() {
        if (warBalancer)
            warBalancer.SendMessage("AnotherKill", gameObject);
        if (inSquad)
        {
            Squads.squads[numSquad - 1].Remove(gameObject);
            squadGO.GetComponent<Squad>().units.Remove(gameObject);
            squadGO.GetComponent<Squad>().UnitDie();
        }
        GetComponent<Parameters>().health = 0;
        inSquad = false;
        if (GetComponent<Parameters>().selected)
        {
            Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Remove(gameObject);
            GetComponent<Parameters>().selected = false;
        }
        numSquad = 0;
        Camera.main.GetComponent<Squads>().SquadCheck();
        // мы мертвы
        GetComponent<Parameters>().died = true;
        Camera.main.GetComponent<Camera3dPerson>().UnitList.Remove(gameObject);
        // играем анимацию смерти
        if (GetComponent<Animation>())
            GetComponent<Animation>().CrossFade(dying.name);
        else
            GetComponent<Animator>().Play(dying.name);
        // игнорим коллизии со всем
		gameObject.GetComponent<Collider> ().enabled = false;
        // выключаем навигацию
		gameObject.GetComponent<NavMeshAgent> ().enabled = false;
	    StartCoroutine(DestroyAfterDie());
        GetComponent<InventoryUnit>().DropAllItems();
    }

    IEnumerator DestroyAfterDie()
    {
        yield return new WaitForSeconds(dying.length);
        Destroy(gameObject);
        Destroy(this);
    }

    void CheckItems()
    {
        if (GetComponent<Parameters>().died)
            return;
        if (Opponent != null)
            return;
        foreach (Collider col in Physics.OverlapSphere(transform.position, range))
        {
            if(col.CompareTag("LyingItem") && !col.GetComponent<DroppedItem>().someoneGoingHere)
            {
                goingToThisItem = col.gameObject;
                col.GetComponent<DroppedItem>().someoneGoingHere = true;
                TryPickupItem();
            }
        }
    }

    GameObject CheckOpponents()
    {
        GameObject enemy = null;
        Collider[] hitColliders = Physics.OverlapSphere(transform.FindChild("Checker").position, range * 2);
        // для каждого найденного объекта
        foreach (Collider hit in hitColliders)
        {
            if (hit && hit.GetComponent<Parameters>() && hit.GetComponent<Parameters>().health < 0)
                continue;
            if (hit.CompareTag("Enemy"))
            {
                if (hit.GetComponent<Parameters>().health > 0)
                    return hit.gameObject;
            }
            else
            if (hit.CompareTag("EnemyBuild"))
            {
                if (hit.GetComponent<Construction>().builded && !hit.GetComponent<Parameters>().died)
                {
                    enemy = hit.gameObject;
                }
            }
        }
        return enemy;
    }

    bool TryPickupItem()
    {
        if (GetComponent<Parameters>().died)
            return false;
        if (Vector3.Distance(transform.position, goingToThisItem.transform.position) <= goingToThisItem.GetComponent<DroppedItem>().pickupRange)
        {
            Destroy(goingToThisItem);
            PickupItem(goingToThisItem.GetComponent<DroppedItem>().item);
            goingToThisItem = null;
            return true;
        }
        agent.destination = goingToThisItem.transform.position;
        return false;
    }

    void PickupItem(Item item)
    {
        int type = (int)item.type; // переводим тип вещи в число

        Item better = GetComponent<InventoryUnit>().items.Find(it => it.slotID == type); // ищем надетую на нас вещь
        if (better != null) // если нашли
        {
            if ((int)item.quality > (int)better.quality) // если новая вещь лучше
            {
                InventorySlot.UpdateParameters(gameObject, better, -1, true); // обновляем параметры
                item.slotID = type; // надеваем новую вещь
                GetComponent<InventoryUnit>().items.Add(item);
                InventorySlot.UpdateParameters(gameObject, item, 1, true); // обновляем параметры

                // пробуем найти свободный слот
                int cnt = 0;
                for (int i = 0; i < GetComponent<InventoryUnit>().slotsCount; i++)
                {
                    if (GetComponent<InventoryUnit>().items.Exists(it => it.slotID == i))
                        cnt++;
                    else { better.slotID = cnt; break; }
                }

                if (cnt == GetComponent<InventoryUnit>().slotsCount) // если все слоты заняты
                { // пытаемся найти вещь, которая дешевле этой вещи
                    List<Item> itemCosts = GetComponent<InventoryUnit>().items.FindAll(
                        it => (it.sellCost < better.sellCost && it.slotID < GetComponent<InventoryUnit>().slotsCount)); // получаем список всех вещей, которые дешевле нашей
                    if (itemCosts.Count == 0)
                    { // если все вещи дороже этой
                        GetComponent<InventoryUnit>().DropItem(better, gameObject);
                        GetComponent<InventoryUnit>().items.Remove(better);
                        return;
                    }

                    itemCosts.Sort((a, b) => a.sellCost.CompareTo(b.sellCost)); // сортируем по убыванию цены (0-й элемент - самая дешёвая вещь)
                    better.slotID = itemCosts[0].slotID; // кладём на её место новую вещь
                    GetComponent<InventoryUnit>().DropItem(itemCosts[0], gameObject);
                    GetComponent<InventoryUnit>().items.Remove(itemCosts[0]); // старую выбрасываем
                }
            }
        }
        else
        { // если не нашли
            item.slotID = type; // надеваем вещь  
            InventorySlot.UpdateParameters(gameObject, item, 1, true); // обновляем параметры
            GetComponent<InventoryUnit>().items.Add(item);
        }
        if (GetComponent<Parameters>().selected && InventoryUnit.inventoryActive)
            GetComponent<InventoryUnit>().Open();
    }

    public void CheckShop()
    {
        if(enabled) 
            checkShop = true;
    }

	public void SetParentBuilding(GameObject bld) {
		parentBuilding = bld;
	}

    public void ObjectSelected()
    {
        if (enabled)
        {
            if (name != "Angel")
            {
                if (inSquad)
                {
                    unitPanelUI.SendMessage("SelectedUnitInSquad", gameObject);
                    squadGO.GetComponent<Squad>().CheckSquadReadyToGo();
                }
                else
                {
                    unitPanelUI.SendMessage("SelectedUnitNotInSquad", gameObject);
                }
            }
            if (Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Count == 1 &&
                Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0] == gameObject)
            {
                inventoryOpenButton.GetComponent<Button>().onClick.RemoveAllListeners();
                inventoryOpenButton.GetComponent<Button>()
                    .onClick.AddListener(GetComponent<InventoryUnit>().OpenOrClose);
            }
            else
            {
                inventoryOpenButton.GetComponent<Button>().onClick.RemoveAllListeners();
                inventoryOpenButton.GetComponent<Button>()
                    .onClick.AddListener(hero.GetComponent<InventoryUnit>().OpenOrClose);
            }
        }
    }
    public void ObjectUnselected() {/*заглушка*/}

    void OnCanAttackChange()
    {
        if (_canAttack)
        {
            EnemyOut();
        }
        else
        {
            Opponent = null;
        }
    }
}
