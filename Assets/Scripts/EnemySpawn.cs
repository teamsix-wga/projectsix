﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawn : MonoBehaviour {
    public List<GameObject> spawningUnit = new List<GameObject>(); // кого мы создаём
    public List<GameObject> spawnedEnemies = new List<GameObject>(); // созданные юниты
    
    public int timeNeedToSpawn; // время, через которое будет появляться новый враг у точки. [10; 60], шаг 5
    public int timeToAttack; // время, через которое все свободные враги идут атаковать. [30; 120], шаг 10
    public int guards; // количество врагов, охраняющих спот. [1; 5]. Шаг 1
    public int minEnemiesToAttack; // минимальное количество врагов для атаки. [1; 10]. Шаг 1

    GameObject spawnPoint, unitFlag;

    IEnumerator Start ()
    {
        timeNeedToSpawn = RandVal(10, 60, 5);
        timeToAttack = RandVal(30, 120, 10);
        guards = RandVal(1, 5, 1);
        minEnemiesToAttack = RandVal(1, 10, 1);

        spawnPoint = transform.FindChild("spawnPoint").gameObject;
        unitFlag = transform.FindChild("UnitFlag").gameObject;

        yield return new WaitForSeconds(30);

        StartCoroutine(SpawnEnemies());
        StartCoroutine(SendEnemiesToFight());
    }

    int RandVal(int min, int max, int step)
    {
        return Random.Range(0, (max - min) / step + 1) * step + min;
    }

    void Update()
    {
        if (spawnedEnemies.Contains(null))
            spawnedEnemies.RemoveAll(go => go == null);
    }

    IEnumerator SpawnEnemies()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeNeedToSpawn);
            var enemy = Instantiate(spawningUnit[Random.Range(0, spawningUnit.Count)], spawnPoint.transform.position, Quaternion.identity) as GameObject;
            enemy.GetComponent<Enemy>().standHere = enemy.GetComponent<NavMeshAgent>().destination =
                Act1Startup.FindNearestFreePos(enemy, unitFlag.transform.position + Random.insideUnitSphere * 5, 5);
            spawnedEnemies.Add(enemy);
        }
    }

    IEnumerator SendEnemiesToFight()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeToAttack);
            if (spawnedEnemies.Count - guards < minEnemiesToAttack)
                continue;
            var whoRemove = new List<GameObject>();
            for (int i = 0; i < spawnedEnemies.Count - guards; i++)
            {
                spawnedEnemies[i].GetComponent<Enemy>().standHere =
                    spawnedEnemies[i].GetComponent<NavMeshAgent>().destination = 
                        Act1Startup.FindNearestFreePos(spawnedEnemies[i], GameObject.FindGameObjectWithTag("TownHall").transform.position + Random.insideUnitSphere * 15, 15);
                whoRemove.Add(spawnedEnemies[i]);
            }
            foreach (var rem in whoRemove)
                spawnedEnemies.Remove(rem);
        }
    }



    public void GetHit(AttackStructure at)
    {
        if (GetComponent<Parameters>().defence <= at.damage)
            GetComponent<Parameters>().health -= at.damage - GetComponent<Parameters>().defence;
        if (GetComponent<Parameters>().health <= 0)
        {
            Destroy(gameObject);
        }
    }
	
}
