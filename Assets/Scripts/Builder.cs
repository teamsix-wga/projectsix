﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
public class Builder : MonoBehaviour {
    public List<GameObject> listBuild;
    public bool building;
    public GameObject bld;
    public GameObject buildingUI;
    GameObject kazarmaToLoad, shopToLoad, townhallToLoad, defTowerToLoad, churchToLoad;
    GameObject FoWProjector;

    void Awake ()
    {
        buildingUI = GameObject.Find("BuildBuildingUI/Image").gameObject;
        kazarmaToLoad = Resources.Load("Kazarma") as GameObject;
        shopToLoad = Resources.Load("Shop") as GameObject;
        townhallToLoad = Resources.Load("TownHall") as GameObject;
        defTowerToLoad = Resources.Load("DefenceTower") as GameObject;
        churchToLoad = Resources.Load("Church") as GameObject;
        FoWProjector = GameObject.FindGameObjectWithTag("FoWProjector");
        building = false;
        
        //buildingUI.GetComponent<BuildingMenu>().UpdateListCanBuild(new List<string> {"TownHall", "Kazarma", "Church", "Shop", "DefenceTower"});

        if (listBuild.Count == 0)
            listBuild = new List<GameObject>();
        else
            listBuild.RemoveAll((build) => (build == null));
    }
    
    void Update () { 
        // если нажат Esc и мы строим
        if((Input.GetKey(KeyCode.Escape) || Input.GetMouseButtonDown(1)) && building) {
            // сбрасываем флаг
            building = false;
            // уничтожаем здание
            Destroy(bld);
            bld = null;
        }
        // если что-то строим
        if(building) {
            NavMeshHit nav;
            // игнорируем 8-й слой
            int mask = ~(1 << 8);
            // игнорим 2-й слой
            mask = ~(mask << 2);
            // пускаем луч из мышки
            RaycastHit hit = CastFromCursorMask(mask);
            NavMesh.Raycast(hit.point, hit.point, out nav, NavMesh.AllAreas);
            if (hit.collider.gameObject.layer == 4 || nav.mask == 0)
            {
                Parameters.MakeColor(bld, Color.red);
                bld.GetComponent<Construction>().timer = 0.05f;
                bld.GetComponent<Construction>().col = true;
            }
            
            bld.transform.position = new Vector3(hit.point.x, hit.point.y + 0.2f, hit.point.z);
            if (hit.point.y > bld.GetComponent<Collider>().bounds.min.y)
                bld.transform.position = new Vector3(hit.point.x, hit.point.y + (hit.point.y - bld.GetComponent<Collider>().bounds.min.y + 0.5f)+0.2f , hit.point.z);
            
        }
        // если нажали ЛКМ и строим и нет коллизии
        if(Input.GetMouseButton(0) && building && !bld.GetComponent<Construction>().col && GetComponent<Gold>().currentGold >= bld.GetComponent<Construction>().costInGold &&
            Camera.main.GetComponent<GameResources>().wood >= bld.GetComponent<Construction>().costInWood && Camera.main.GetComponent<GameResources>().stone >= bld.GetComponent<Construction>().costInStone) {
            // уже ничего не строим
            building = false;

            bld.GetComponent<Construction>().placed = true;
            bld.GetComponent<Construction>().placedTime = bld.GetComponent<Construction>().timeNeedToBuild;
            foreach(NavMeshObstacle obs in bld.GetComponentsInChildren<NavMeshObstacle>())
                obs.enabled = true;

            // пускаем луч из мышки
            int mask = ~(1 << 8);
            mask = ~(mask << 2);
            RaycastHit hit = CastFromCursorMask(mask);
            GetComponent<Gold>().currentGold -= bld.GetComponent<Construction>().costInGold;
            Camera.main.GetComponent<GameResources>().stone -= bld.GetComponent<Construction>().costInStone;
            Camera.main.GetComponent<GameResources>().wood -= bld.GetComponent<Construction>().costInWood;
            bld.GetComponent<Construction>().Placed(Resources.Load(bld.name) as GameObject);
            bld.transform.position = new Vector3(hit.point.x, hit.point.y, hit.point.z);
            if (hit.point.y > bld.GetComponent<Collider>().bounds.min.y)
                bld.transform.position = new Vector3(hit.point.x, hit.point.y + (hit.point.y - bld.GetComponent<Collider>().bounds.min.y), hit.point.z);

            bld.AddComponent<RealFoWUnit>();

            gameObject.SendMessage("CheckPlacedBuild", bld.name);

            foreach (Renderer r in bld.GetComponentsInChildren<Renderer>())
                foreach (Material material in r.materials)
                {
                    material.SetFloat("_Mode", 3);
                    material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                    material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    material.SetInt("_ZWrite", 0);
                    material.DisableKeyword("_ALPHATEST_ON");
                    material.DisableKeyword("_ALPHABLEND_ON");
                    material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
                    material.renderQueue = 3000;
                }

            if (bld.GetComponent<Kazarma>())
            {
                bld.GetComponent<Kazarma>().UnitFlag = bld.transform.FindChild("UnitFlag").position;
                bld.GetComponent<Kazarma>().spawnPoint = new Vector3(bld.GetComponent<Collider>().bounds.center.x, bld.GetComponent<Collider>().bounds.min.y, bld.GetComponent<Collider>().bounds.center.z);
                bld.GetComponent<RealFoWUnit>().radius = 80;
            }
            else if (bld.GetComponent<Church>())
            {
                bld.GetComponent<Church>().UnitFlag = bld.transform.FindChild("UnitFlag").position;
                bld.GetComponent<Church>().spawnPoint = bld.transform.FindChild("SpawnPoint").position;
                bld.GetComponent<RealFoWUnit>().radius = 80;
            }
            else
            if (bld.GetComponent<TownHall>())
            {
                bld.GetComponent<RealFoWUnit>().radius = 100;
            }
            else
            if (bld.GetComponent<DefenceTower>())
                bld.GetComponent<RealFoWUnit>().radius = 60;
            listBuild.Add(bld);
        }
	}

    // функция пускания луча из мышки
    public static RaycastHit CastFromCursor() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Physics.Raycast(ray, out hit, 1000);
        return hit;
    }

    public static RaycastHit CastFromCursorMask(int mask)
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); ;
        RaycastHit hit;
        Physics.Raycast(ray, out hit, 1000, mask);
        return hit;
    }

    public void BuildKazarma() {
        if (building == false && GetComponent<Gold>().currentGold >= kazarmaToLoad.GetComponent<Construction>().costInGold && Camera.main.GetComponent<GameResources>().stone >= kazarmaToLoad.GetComponent<Construction>().costInStone
            && Camera.main.GetComponent<GameResources>().wood >= kazarmaToLoad.GetComponent<Construction>().costInWood)
        {
            buildingUI.GetComponent<BuildingMenu>().CloseMenu();
            building = true;
            // пускаем луч из мышки
            RaycastHit hit = CastFromCursor();
            // клонируем префаб здания
            bld = Instantiate(kazarmaToLoad, hit.point, Quaternion.identity) as GameObject;
            bld.name = kazarmaToLoad.name;
            bld.transform.position += new Vector3(0, 1, 0);
            Parameters.MakeColor(bld, Color.green);
        }
        else
        if (building == false && GetComponent<Gold>().currentGold < kazarmaToLoad.GetComponent<Construction>().costInGold)
            StartCoroutine(FadeText.FadeTxt("Нужно больше золота!", Color.red, 3));
        else
        if (building == false && Camera.main.GetComponent<GameResources>().wood < kazarmaToLoad.GetComponent<Construction>().costInWood)
            StartCoroutine(FadeText.FadeTxt("Нужно больше дерева!", Color.red, 3));
        else
        if (building == false && Camera.main.GetComponent<GameResources>().stone < kazarmaToLoad.GetComponent<Construction>().costInStone)
            StartCoroutine(FadeText.FadeTxt("Нужно больше камня!", Color.red, 3));
    }

    public void BuildTower()
    {
        if (building == false && GetComponent<Gold>().currentGold >= defTowerToLoad.GetComponent<Construction>().costInGold && Camera.main.GetComponent<GameResources>().wood >= defTowerToLoad.GetComponent<Construction>().costInWood 
            && Camera.main.GetComponent<GameResources>().stone >= defTowerToLoad.GetComponent<Construction>().costInStone)
        {
            buildingUI.GetComponent<BuildingMenu>().CloseMenu();
            building = true;
            // пускаем луч из мышки
            RaycastHit hit = CastFromCursor();
            // клонируем префаб здания
            bld = Instantiate(defTowerToLoad, hit.point, Quaternion.identity) as GameObject;
            bld.name = "DefenceTower";
            bld.transform.position += new Vector3(0, 1, 0);
            Parameters.MakeColor(bld, Color.green);
        }
        else
        if (building == false && GetComponent<Gold>().currentGold < defTowerToLoad.GetComponent<Construction>().costInGold)
            StartCoroutine(FadeText.FadeTxt("Нужно больше золота!", Color.red, 3));
        else
        if (building == false && Camera.main.GetComponent<GameResources>().wood < defTowerToLoad.GetComponent<Construction>().costInWood)
            StartCoroutine(FadeText.FadeTxt("Нужно больше дерева!", Color.red, 3));
        else
        if (building == false && Camera.main.GetComponent<GameResources>().stone < defTowerToLoad.GetComponent<Construction>().costInStone)
            StartCoroutine(FadeText.FadeTxt("Нужно больше камня!", Color.red, 3));
    }

    public void BuildShop ()
    {
        if (building == false && GetComponent<Gold>().currentGold >= shopToLoad.GetComponent<Construction>().costInGold && Camera.main.GetComponent<GameResources>().wood >= shopToLoad.GetComponent<Construction>().costInWood
            && Camera.main.GetComponent<GameResources>().stone >= shopToLoad.GetComponent<Construction>().costInStone)
        {
            buildingUI.GetComponent<BuildingMenu>().CloseMenu();
            building = true;
            // пускаем луч из мышки
            RaycastHit hit = CastFromCursor();
            // клонируем префаб здания
            bld = Instantiate(shopToLoad, hit.point, Quaternion.identity) as GameObject;
            bld.name = "Shop";
            bld.transform.position += new Vector3(0, 1 + bld.GetComponent<Collider>().bounds.extents.y, 0);
            Parameters.MakeColor(bld, Color.green);
        }
        else
        if (building == false && GetComponent<Gold>().currentGold < shopToLoad.GetComponent<Construction>().costInGold)
            StartCoroutine(FadeText.FadeTxt("Нужно больше золота!", Color.red, 3));
        else
        if (building == false && Camera.main.GetComponent<GameResources>().wood < shopToLoad.GetComponent<Construction>().costInWood)
            StartCoroutine(FadeText.FadeTxt("Нужно больше дерева!", Color.red, 3));
        else
        if (building == false && Camera.main.GetComponent<GameResources>().stone < shopToLoad.GetComponent<Construction>().costInStone)
            StartCoroutine(FadeText.FadeTxt("Нужно больше камня!", Color.red, 3));
    }
    
    public void BuildSquadTent(){
        if(!building) {
            building = true;
            // пускаем луч из мышки
            RaycastHit hit = CastFromCursor();
            // клонируем префаб здания
            bld = Instantiate(Resources.Load("SquadTent"), hit.point, Quaternion.identity) as GameObject;
            bld.name = "SquadTent";
            bld.transform.position += new Vector3(0, 1, 0);
            Parameters.MakeColor(bld, Color.green);
        }
    }

    public void BuildTH()
    {
        if (building == false && GetComponent<Gold>().currentGold >= townhallToLoad.GetComponent<Construction>().costInGold && Camera.main.GetComponent<GameResources>().stone >= townhallToLoad.GetComponent<Construction>().costInStone &&
            Camera.main.GetComponent<GameResources>().wood >= townhallToLoad.GetComponent<Construction>().costInWood)
        {
            buildingUI.GetComponent<BuildingMenu>().CloseMenu();
            building = true;
            // пускаем луч из мышки
            RaycastHit hit = CastFromCursor();
            // клонируем префаб здания
            bld = Instantiate(townhallToLoad, new Vector3(hit.point.x, hit.point.y, hit.point.z), Quaternion.identity) as GameObject;
            bld.name = "TownHall";
            bld.transform.position += new Vector3(0, 1 + bld.GetComponent<Collider>().bounds.extents.y, 0);
            bld.transform.localEulerAngles = new Vector3(-90, 0, 0);
            Parameters.MakeColor(bld, Color.green);
        }
        else
        if (building == false && GetComponent<Gold>().currentGold < townhallToLoad.GetComponent<Construction>().costInGold)
            StartCoroutine(FadeText.FadeTxt("Нужно больше золота!", Color.red, 3));
        else
        if (building == false && Camera.main.GetComponent<GameResources>().wood < townhallToLoad.GetComponent<Construction>().costInWood)
            StartCoroutine(FadeText.FadeTxt("Нужно больше дерева!", Color.red, 3));
        else
        if (building == false && Camera.main.GetComponent<GameResources>().stone < townhallToLoad.GetComponent<Construction>().costInStone)
            StartCoroutine(FadeText.FadeTxt("Нужно больше камня!", Color.red, 3));
    }

    public void BuildChurch()
    {
        if (building == false && GetComponent<Gold>().currentGold >= churchToLoad.GetComponent<Construction>().costInGold && Camera.main.GetComponent<GameResources>().wood >= churchToLoad.GetComponent<Construction>().costInWood
            && Camera.main.GetComponent<GameResources>().stone >= churchToLoad.GetComponent<Construction>().costInStone)
        {
            buildingUI.GetComponent<BuildingMenu>().CloseMenu();
            building = true;
            // пускаем луч из мышки
            RaycastHit hit = CastFromCursor();
            // клонируем префаб здания
            bld = Instantiate(churchToLoad, new Vector3(hit.point.x, hit.point.y, hit.point.z), Quaternion.identity) as GameObject;
            bld.name = "Church";
            bld.transform.position += new Vector3(0, 1 + bld.GetComponent<Collider>().bounds.extents.y, 0);
            bld.transform.localEulerAngles = new Vector3(-90, 0, 0);
            Parameters.MakeColor(bld, Color.green);
        }
        else
        if (building == false && GetComponent<Gold>().currentGold < churchToLoad.GetComponent<Construction>().costInGold)
            StartCoroutine(FadeText.FadeTxt("Нужно больше золота!", Color.red, 3));
        else
        if (building == false && Camera.main.GetComponent<GameResources>().wood < churchToLoad.GetComponent<Construction>().costInWood)
            StartCoroutine(FadeText.FadeTxt("Нужно больше дерева!", Color.red, 3));
        else
        if (building == false && Camera.main.GetComponent<GameResources>().stone < churchToLoad.GetComponent<Construction>().costInStone)
            StartCoroutine(FadeText.FadeTxt("Нужно больше камня!", Color.red, 3));
    }
}
