﻿using UnityEngine;
using System.Collections;
using System.IO;

public class TerrainExport : MonoBehaviour
{
    public Terrain terra;
    public Camera main;

    public int resWidth = 4000;
    public int resHeight = 4000;

    // Use this for initialization
    void Start ()
	{
	    main = Camera.main;

	    main.orthographic = true;
        main.orthographicSize = terra.GetComponent<Terrain>().GetComponent<Collider>().bounds.extents.x;
        main.transform.position = new Vector3(terra.GetComponent<Terrain>().GetComponent<Collider>().bounds.center.x, transform.position.y, terra.GetComponent<Terrain>().GetComponent<Collider>().bounds.center.z);
        main.farClipPlane = terra.GetComponent<Terrain>().GetComponent<Collider>().bounds.extents.x + 300;
        main.nearClipPlane = -terra.GetComponent<Terrain>().GetComponent<Collider>().bounds.max.x;

	    SaveTexture();
	}

    void SaveTexture()
    {
        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        main.targetTexture = rt;
        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        main.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        main.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        Destroy(rt);
        byte[] bytes = screenShot.EncodeToPNG();
        string filename = Application.dataPath + "/../Saves/terrain.png";
        System.IO.File.WriteAllBytes(filename, bytes);
            
        

        //Application.CaptureScreenshot(Application.dataPath + "/../Saves/terrain.png");
    }

}
