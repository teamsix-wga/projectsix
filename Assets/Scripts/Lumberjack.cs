﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Lumberjack : MonoBehaviour
{
    // компонента навигации
    [HideInInspector]
    public NavMeshAgent agent;
    // куда идём после того, как "Родились"
    public Vector3 bornFlag;
    public float cooldownTimer;
    public bool comingBack;
    public AnimationClip run, stop, lumbering;
    public GameObject tree;
    public float cooldown;
    public GameObject workFlag;
    public bool working;
    public Vector3 workHere;
    public Vector3 cameFrom;
    public LumberState state;
    GameObject townHall;
    public float resourcesLimit = 30, resourcesMaxLimit = 30, carryWood, carryStone, carryGold;
    public float buildBuildHealth = 0.1f;
    public GameObject stone;
    public Transform hand;
    GameObject hero;
    GameObject unitPanelUI;
    bool goingToItem = false;
    GameObject goingToThisItem = null;
    GameObject inventoryOpenButton;
    public GameObject beWarriorQueueButton;

    void Awake()
    {
        hero = GameObject.FindWithTag("Player");
        inventoryOpenButton = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/SkillPanel/Inventory").gameObject;
        unitPanelUI = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/UnitPanelUI").gameObject;
        Camera.main.GetComponent<Camera3dPerson>().lumberjacks.Add(gameObject);

        carryGold = carryStone = carryWood = 0;
        resourcesMaxLimit = 30;
        resourcesLimit = resourcesMaxLimit;
        
        cooldown = 2f;
        working = false;
        agent = GetComponent<NavMeshAgent>();
        agent.speed = GetComponent<Parameters>().speed;
        cooldownTimer = 0f;
        cameFrom = transform.position;
    }

    void Update()
    {
        if(Vector3.Distance(transform.position, agent.destination) >= agent.stoppingDistance)
            GetComponent<Animation>().CrossFade(run.name);
        else
            if(!GetComponent<Animation>().IsPlaying(lumbering.name))
                GetComponent<Animation>().CrossFade(stop.name);

        // кулдаун удара топора/кирки
        if (cooldownTimer > 0)
            cooldownTimer -= Time.deltaTime;
        else
            cooldownTimer = 0;
        
        if(comingBack && Vector3.Distance(transform.position, cameFrom) >= agent.stoppingDistance + 1f)
            agent.destination = cameFrom;

        if (resourcesLimit <= 0 && townHall)
            CarryResources();
            
        if(resourcesLimit <= 0 && townHall && Vector3.Distance(transform.position, cameFrom) <= agent.stoppingDistance + 0.3f) {
                if(carryStone > 0)
                    Camera.main.GetComponent<GameResources>().AddStone(carryStone);
                if(carryWood > 0)
                    Camera.main.GetComponent<GameResources>().AddWood(carryWood);
                if(carryGold > 0)
                    GameObject.FindWithTag("Player").GetComponent<Gold>().currentGold += carryGold;
                    
                carryStone = carryWood = carryGold = 0;
                resourcesLimit = resourcesMaxLimit;
                agent.destination = workHere;
        }
       
        if(!goingToItem && working)
        {
            if (state == LumberState.Lumberjack && resourcesLimit > 0)
                Lumbering();
                
            if(state == LumberState.Miner && resourcesLimit > 0)
                Mining();
                
            if(state == LumberState.Builder)
                Building();

            if ((state == LumberState.Warrior || state == LumberState.Scout) && Vector3.Distance(transform.position, workHere) <= agent.stoppingDistance + 0.3f)
                workFlag.GetComponent<Kazarma>().MakeWarrior(gameObject, beWarriorQueueButton);

            if (state == LumberState.Mage && Vector3.Distance(transform.position, workHere) <= agent.stoppingDistance + 0.3f)
                workFlag.GetComponent<Church>().MakeWarrior(gameObject, beWarriorQueueButton);

            if (state == LumberState.Stoner && resourcesLimit > 0)
                Stoner();
        }

        if (goingToItem)
        {
            if (Vector3.Distance(transform.position, goingToThisItem.transform.position) <= agent.stoppingDistance)
                PickupItem();
            else
                agent.SetDestination(goingToThisItem.transform.position);
        }
    }
    
    void Lumbering()
    {
        // если у нас нет выделенного врага
        if (!tree)
        {
            // получаем список всех объектов в радиусе range
            Collider[] hitColliders = Physics.OverlapSphere(workFlag.transform.position, workFlag.GetComponent<LumberFlag>().workingRange);
            // для каждого найденного объекта
            foreach (Collider hit in hitColliders)
            {
                // если его тэг нам подходит и оно не занято
                if (hit.CompareTag("Tree") && !hit.GetComponent<Tree>().busy)
                {
                    hit.GetComponent<Tree>().LumberStart();
                    // выделяем врага
                    tree = hit.gameObject;
                    workHere = tree.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
                    agent.destination = workHere;
                    // выходим из цикла
                    break;
                }
            }
        }
        if (!tree)
        {
            workFlag.GetComponent<LumberFlag>().workDone = true;
            workFlag.GetComponent<LumberFlag>().DeleteLumberjack(gameObject);
        }else
        // если дистанция до врага позволяет атаковать и враг выделен
        if (tree && cooldownTimer <= 0 && Vector3.Distance(transform.position, workHere) <= agent.stoppingDistance + 0.1f)
        {
            GetComponent<Animation>().CrossFade(lumbering.name);

            Item instrument = GetComponent<InventoryUnit>().items.Find(it => (it.slotID == GetComponent<InventoryUnit>().slotsCount + 3));
            if (instrument == null)
            {
                tree.GetComponent<Tree>().GetHit(GetComponent<Parameters>().damage, gameObject);
                cooldownTimer = cooldown;
            }
            else
            {
                tree.GetComponent<Tree>().GetHit(GetComponent<Parameters>().damage + instrument.Damage, gameObject);
                cooldownTimer = cooldown - instrument.Concentration;
            }

            if (resourcesLimit <= 0)
                CarryResources();
        }else
        // если не наносим урон и нашли кого-то
        if (tree && cooldownTimer == 0)
            transform.LookAt(agent.destination);
    }
    
    void Mining(){
        if(Vector3.Distance(workHere, transform.position) <= agent.stoppingDistance + 0.3f && resourcesLimit > 0){
            workFlag.GetComponent<Mine>().StartMine(gameObject);
        }
    }
    
    void Building(){
        /*if (agent.destination != workHere)
            agent.destination = workHere;
*/
        if(cooldownTimer == 0 && Vector3.Distance(transform.position, workHere) <= agent.stoppingDistance+0.1f){
            transform.LookAt(workFlag.transform.position);
            GetComponent<Animation>().CrossFade(lumbering.name);

            Item instrument = GetComponent<InventoryUnit>().items.Find(it => (it.slotID == GetComponent<InventoryUnit>().slotsCount + 3));
            if (instrument == null)
            {
                workFlag.GetComponent<Construction>().placedTime -= buildBuildHealth;
                cooldownTimer = cooldown;
            }
            else
            {
                workFlag.GetComponent<Construction>().placedTime -= (buildBuildHealth + instrument.Damage);
                cooldownTimer = cooldown - instrument.Concentration;
            }
        }
    }
    
    void Stoner(){
        if(cooldownTimer == 0 && Vector3.Distance(transform.position, agent.destination) <= agent.stoppingDistance+0.2f){
            GetComponent<Animation>().CrossFade(lumbering.name);
            
            Item instrument = GetComponent<InventoryUnit>().items.Find(it => (it.slotID == GetComponent<InventoryUnit>().slotsCount + 3));
            if (instrument == null)
            {
                workFlag.GetComponent<Stone>().GetHit(GetComponent<Parameters>().damage, gameObject);
                cooldownTimer = cooldown;
            }
            else
            {
                workFlag.GetComponent<Stone>().GetHit(GetComponent<Parameters>().damage + instrument.Damage, gameObject);
                cooldownTimer = cooldown - instrument.Concentration;
            }

            if (resourcesLimit <= 0)
                CarryResources();
        }
    }
    
    public void CarryResources(){
        if (agent.destination != cameFrom)
            agent.destination = cameFrom;
    }

    public void GoToWork(Vector3 workingPlace, GameObject flag)
    {
        working = true;
        comingBack = false;
        agent.destination = workHere = workingPlace;
        workFlag = flag;

        // 1 - лесоруб; 2 - шахтёр; 3 - строитель; 4 - становится воином; 5 - каменщик
        switch (state)
        {
            case LumberState.Lumberjack:
                Item axe = GetComponent<InventoryUnit>().items.Find(it=>(it.type == Item.ItemType.WorkersToolAxe));
                if(axe != null)
                {
                    axe.slotID = GetComponent<InventoryUnit>().slotsCount + 3;
                    InventorySlot.UpdateParameters(gameObject, axe, 1, true);
                }
                break;
            case LumberState.Miner:
                Item pickaxe = GetComponent<InventoryUnit>().items.Find(it => (it.type == Item.ItemType.WorkersToolPickaxe));
                if (pickaxe != null)
                {
                    pickaxe.slotID = GetComponent<InventoryUnit>().slotsCount + 3;
                    InventorySlot.UpdateParameters(gameObject, pickaxe, 1, true);
                }
                break;
            case LumberState.Builder:
                Item hammer = GetComponent<InventoryUnit>().items.Find(it => (it.type == Item.ItemType.WorkersToolHammer));
                if (hammer != null)
                {
                    hammer.slotID = GetComponent<InventoryUnit>().slotsCount + 3;
                    InventorySlot.UpdateParameters(gameObject, hammer, 1, true);
                }
                break;
            case LumberState.Stoner:
                Item pick = GetComponent<InventoryUnit>().items.Find(it => (it.type == Item.ItemType.WorkersToolPickaxe));
                if (pick != null)
                {
                    pick.slotID = GetComponent<InventoryUnit>().slotsCount + 3;
                    InventorySlot.UpdateParameters(gameObject, pick, 1, true);
                }
                break;
        }

    }

    public void GoBack()
    {
        working = false;
        agent.destination = cameFrom;
        comingBack = true;
        workFlag = null;
        state = LumberState.None;
        Item workItem = GetComponent<InventoryUnit>().items.Find(it => (it.slotID == GetComponent<InventoryUnit>().slotsCount + 3));
        if (workItem != null)
        {
            workItem.slotID = GetComponent<InventoryUnit>().FindFreeSlot();
            InventorySlot.UpdateParameters(gameObject, workItem, -1, true);
        }
    }

    void CheckItems(List<GameObject> itms)
    {
        if (goingToItem)
            return;

        List<GameObject> needItems = itms.FindAll(itm => (!itm.GetComponent<DroppedItem>().someoneGoingHere)); // нашли все вещи, за которыми никто не идёт

        InventoryUnit invent = GetComponent<InventoryUnit>();
        
        foreach(GameObject needItem in needItems) // по каждой лежащей вещи
        {
            Item itemInInv = invent.items.Find(itemInInvent => (itemInInvent.type == needItem.GetComponent<DroppedItem>().item.type)); // попытались найти такую же вещь
            if ((itemInInv == null) || (needItem.GetComponent<DroppedItem>().item.quality > itemInInv.quality))
            {
                goingToItem = true;
                goingToThisItem = needItem;
                GetComponent<NavMeshAgent>().SetDestination(goingToThisItem.transform.position);
                needItem.GetComponent<DroppedItem>().someoneGoingHere = true;
                break;
            }
        }
    }

    void PickupItem()
    {
        goingToItem = false;
        InventoryUnit invent = GetComponent<InventoryUnit>();

        Item itemInInv = invent.items.Find(itemInInvent => (itemInInvent.type == goingToThisItem.GetComponent<DroppedItem>().item.type));
        Item newItem = goingToThisItem.GetComponent<DroppedItem>().item;
        if (itemInInv == null)
        {
            if (working) // если мы работаем уже, надо надеть предмет
            {
                newItem.slotID = invent.slotsCount + 3;
                InventorySlot.UpdateParameters(gameObject, newItem, 1, true);
            }
            else
            {
                int slot = invent.FindFreeSlot();
                if (slot != -1)
                    newItem.slotID = slot;
                GoBack();
            }
            invent.items.Add(newItem);
        } else
        {
            if (newItem.quality > itemInInv.quality)
            {
                newItem.slotID = itemInInv.slotID;
                if(newItem.slotID == invent.slotsCount + 3)
                {
                    InventorySlot.UpdateParameters(gameObject, itemInInv, -1, true);
                    InventorySlot.UpdateParameters(gameObject, newItem, 1, true);
                }

                invent.DropItem(itemInInv, gameObject);

                if (!working) { 
                    GoBack();
                }

                invent.items.Add(newItem);
            }
        }

        Destroy(goingToThisItem);
    }

    // функция получения урона
    public void GetHit(AttackStructure atStr)
    {
        float damage = atStr.damage;
        if(damage >= GetComponent<Parameters>().defence)
            // уменьшаем здоровье
            GetComponent<Parameters>().health -= damage - GetComponent<Parameters>().defence;
        if (GetComponent<Parameters>().selected)
        {
        }
        // если здоровье меньше 0 и мы не мертвы
        if (GetComponent<Parameters>().health <= 0)
        {
            GetComponent<Parameters>().health = 0;
            Camera.main.GetComponent<Camera3dPerson>().lumberjacks.Remove(gameObject);
            // умираем
            Die();
        }
    }

    // функция умирания
    void Die()
    {
        if(working)
            switch (state)
            {
                case LumberState.Lumberjack:
                    workFlag.GetComponent<LumberFlag>().DeleteLumberjack(gameObject);
                    break;
                case LumberState.Miner:
                    workFlag.GetComponent<Mine>().DeleteLumberjack(gameObject);
                    break;
                case LumberState.Builder:
                    workFlag.GetComponent<BuildBuild>().DeleteLumberjack(gameObject);
                    break;
            }
        Destroy(gameObject);
    }

    public void SetTownhall(GameObject TH){
        townHall = TH;
        GameObject place = TH.transform.FindChild("Place").gameObject;
        cameFrom = new Vector3(Random.Range(place.GetComponent<Collider>().bounds.min.x, place.GetComponent<Collider>().bounds.max.x), 
                                                     place.GetComponent<Collider>().bounds.min.y,
                                                     Random.Range(place.GetComponent<Collider>().bounds.min.z, place.GetComponent<Collider>().bounds.max.z));
        agent.destination = cameFrom;
    }

    public void ObjectSelected()
    {
        unitPanelUI.SendMessage("SelectedWorker", gameObject);

        if (Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Count == 1 &&
            Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0] == gameObject)
        {
            inventoryOpenButton.GetComponent<Button>().onClick.RemoveAllListeners();
            inventoryOpenButton.GetComponent<Button>().onClick.AddListener(GetComponent<InventoryUnit>().OpenOrClose);
        }
        else
        {
            inventoryOpenButton.GetComponent<Button>().onClick.RemoveAllListeners();
            inventoryOpenButton.GetComponent<Button>().onClick.AddListener(hero.GetComponent<InventoryUnit>().OpenOrClose);
        }
    }
}

public enum LumberState
{
    None = 0,
    Lumberjack = 1, // дерево
    Miner = 2, // золото
    Builder = 3, // здания
    Warrior = 4, // воин
    Stoner = 5, // камень
    Scout = 6,
    Mage = 7
}
