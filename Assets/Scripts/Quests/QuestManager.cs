﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;

public class QuestManager : MonoBehaviour
{
    // путь к файлу
    string filepath;
    // база данных квестов
    public List<Quest> questDatabase;

    void Start()
    {
        filepath = Application.dataPath + "\\StreamingAssets\\QuestDB\\Quests.xml";
        questDatabase = new List<Quest>();
        LoadQuestDB();
    }

    /// <summary>
    /// Функция загрузки базы данных квестов
    /// </summary>
    void LoadQuestDB()
    {
        XmlDocument xmlDoc = new XmlDocument();
        XmlNode rootNode = null;

        // загружаем файл или выходим
        if (File.Exists(filepath))
        {
            xmlDoc.Load(filepath);
            rootNode = xmlDoc.DocumentElement;
        }
        else
            return;

        // получили все квесты из XML файла
        XmlNodeList questsData = rootNode.ChildNodes;


        // по каждому квесту в файле
        for (int i = 0; i < questsData.Count; i++)
        {
            int ID = 0, dialID = -1, doneID = -1, waitID = -1;
            string Name = "", Description = "";
            float woodReward = 0, stoneReward = 0, goldReward = 0;
            bool av = false, done = false, rewAtGiver = true, selfDial = false, broadcast = false; ;
            Quest.QuestType tp = Quest.QuestType.None;
            float x = 0, y = 0, z = 0;
            int amount = 0;
            string name = "", whoBroadcast = "", whatBroadcast = "";
            int id = 0;
            float rad = 20;

            ID = int.Parse(questsData[i].Attributes["ID"].Value);
            if(questsData[i].Attributes["DoneDialogueID"] != null)
                doneID = int.Parse(questsData[i].Attributes["DoneDialogueID"].Value);
            if (questsData[i].Attributes["WaitDialogueID"] != null)
                waitID = int.Parse(questsData[i].Attributes["WaitDialogueID"].Value);
            av = bool.Parse(questsData[i].Attributes["Avaivable"].Value);
            rewAtGiver = bool.Parse(questsData[i].Attributes["RewardAtGiver"].Value);
            selfDial = bool.Parse(questsData[i].Attributes["StartSelfDialogue"].Value);
            dialID = int.Parse(questsData[i].Attributes["DialogueID"].Value);
            done = bool.Parse(questsData[i].Attributes["questDone"].Value);
            broadcast = bool.Parse(questsData[i].Attributes["Broadcast"].Value);

            Quest.WhoWillCome whoWillCome = Quest.WhoWillCome.none;
            bool needToRun = false;
            Quest.RunTo whoWeRun = Quest.RunTo.none;
            List<Vector3> places = new List<Vector3>();

            XmlNodeList nodes = questsData[i].ChildNodes;
            for(int j = 0; j < nodes.Count; j++)
            {
                switch (nodes[j].Name)
                {
                    case "Name":
                        Name = nodes[j].Attributes[0].Value;
                        break;

                    case "Description":
                        Description = nodes[j].Attributes[0].Value;
                        break;

                    case "Reward":
                        XmlNodeList rewards = nodes[j].ChildNodes;
                        for (int k = 0; k < rewards.Count; k++)
                        {
                            switch (rewards[k].Name)
                            {
                                case "Wood":
                                    woodReward = int.Parse(rewards[k].Attributes[0].Value);
                                    break;
                                case "Stone":
                                    stoneReward = int.Parse(rewards[k].Attributes[0].Value);
                                    break;
                                case "Gold":
                                    goldReward = int.Parse(rewards[k].Attributes[0].Value);
                                    break;
                            }
                        }
                        break;

                    case "QuestType":
                        XmlAttributeCollection typeCollection = nodes[j].Attributes;
                        for(int k = 0; k < typeCollection.Count; k++)
                        {
                            switch (typeCollection[k].Name)
                            {
                                case "type": // тип
                                    tp = (Quest.QuestType)int.Parse(typeCollection[k].Value);
                                    break;
                                case "amount": // количество
                                    amount = int.Parse(typeCollection[k].Value);
                                    break;
                                case "ItemID": // id квестового предмета
                                    id = int.Parse(typeCollection[k].Value);
                                    break;
                                case "name": // название
                                    name = typeCollection[k].Value;
                                    break;
                                case "whoWillCome": 
                                    whoWillCome = (Quest.WhoWillCome)int.Parse(typeCollection[k].Value);
                                    break;
                                case "radius":
                                    rad = float.Parse(typeCollection[k].Value);
                                    break;
                                case "willRunTo":
                                    whoWeRun = (Quest.RunTo)int.Parse(typeCollection[k].Value);
                                    break;
                                case "needToRun":
                                    needToRun = bool.Parse(typeCollection[k].Value);
                                    break;
                            }
                        }
                        break;

                    case "Place":
                        XmlAttributeCollection place = nodes[j].Attributes;
                        for (int k = 0; k < place.Count; k++)
                        {
                            switch (place[k].Name)
                            {
                                case "pos_x":
                                    x = float.Parse(place[k].Value);
                                    break;
                                case "pos_y":
                                    y = float.Parse(place[k].Value);
                                    break;
                                case "pos_z":
                                    z = float.Parse(place[k].Value);
                                    break;
                            }
                        }
                        places.Add(new Vector3(x, y, z));
                        break;

                    case "Broadcast":
                        XmlAttributeCollection broadcastCollection = nodes[j].Attributes;
                        for (int k = 0; k < broadcastCollection.Count; k++)
                        {
                            switch (broadcastCollection[k].Name)
                            {
                                case "who":
                                    whoBroadcast = broadcastCollection[k].Value;
                                    break;
                                case "what":
                                    whatBroadcast = broadcastCollection[k].Value;
                                    break;
                            }
                        }
                        break;
                }
            }
            // дополняем базу данных
            questDatabase.Add(new Quest(ID, dialID, doneID, waitID, Name, Description, woodReward, stoneReward, goldReward, av, tp, amount, 
                                        id, name, places, done, whoWillCome, needToRun, whoWeRun, rewAtGiver, selfDial, broadcast, whoBroadcast, whatBroadcast, rad));
        }
    }
}
