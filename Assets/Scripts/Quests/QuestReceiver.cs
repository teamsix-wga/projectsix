﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class QuestReceiver : MonoBehaviour {
    public List<Quest> quests;
    public List<Quest> doneQuests;

    public List<string> namesToKill, namesToBuild, namesToCreate, namesToPlace, namesToTalk;
    public List<int> idItemsToHave;

    GameObject questList;
    public bool questListOpened;

    GameObject questButtonGO;

    void Start () {
        questButtonGO = Resources.Load("QuestButton") as GameObject;
        questListOpened = false;
        questList = GameObject.Find("HaveQuestList");
        questList.SetActive(questListOpened);
        if(quests.Count == 0)
            quests = new List<Quest>();
        if(doneQuests.Count == 0)
            doneQuests = new List<Quest>();
        if(namesToKill.Count == 0)
            namesToKill = new List<string>();
        if(namesToBuild.Count == 0)
            namesToBuild = new List<string>();
        if (namesToCreate.Count == 0)
            namesToCreate = new List<string>();
        if (namesToPlace.Count == 0)
            namesToPlace = new List<string>();
        if (namesToTalk.Count == 0)
            namesToTalk = new List<string>();
        if (idItemsToHave.Count == 0)
            idItemsToHave = new List<int>();
    }

    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.L)) || (Input.GetKeyDown(KeyCode.Escape) && questListOpened) && Camera3dPerson.canMoveCam)
        {
            questListOpened = !questListOpened;
            if (questListOpened)
                OpenList();
            else
                CloseList();
            questList.SetActive(questListOpened);
        }
    }

    /// <summary>
    /// Функция открытия списка квестов
    /// </summary>
    public void OpenList()
    {
        GameObject text = new GameObject();
        text.transform.SetParent(questList.transform.FindChild("QuestList").transform);
        text.AddComponent<Text>();
        text.GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 14);
        text.GetComponent<Text>().text = "\n<color=#000000>Доступные квесты:</color>";
        foreach (Quest quest in quests)
        {
            GameObject button = Instantiate(questButtonGO);
            button.transform.FindChild("Text").GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
            button.transform.SetParent(questList.transform.FindChild("QuestList").transform);
            button.transform.GetChild(0).GetComponent<Text>().text = quest.Name;
            Quest q = quest;
            button.GetComponent<Button>().onClick.AddListener(() => OpenQuest(q));
        }

        GameObject doneText = new GameObject();
        doneText.transform.SetParent(questList.transform.FindChild("QuestList").transform);
        doneText.AddComponent<Text>();
        doneText.GetComponent<Text>().font = Font.CreateDynamicFontFromOSFont("Arial", 14);
        doneText.GetComponent<Text>().text = "\n<color=#000000>Выполненные квесты:</color>";
        foreach (Quest quest in doneQuests)
        {
            if (quest.rewardGived)
                continue;
            GameObject button = Instantiate(questButtonGO);
            button.transform.FindChild("Text").GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
            button.transform.SetParent(questList.transform.FindChild("QuestList").transform);
            button.transform.GetChild(0).GetComponent<Text>().text = quest.Name;
            Quest q = quest;
            button.GetComponent<Button>().onClick.AddListener(() => OpenQuest(q));
        }
    }

    /// <summary>
    /// Функция, вызывающаяся при нажатии на кнопку квеста
    /// </summary>
    void OpenQuest(Quest quest)
    {
        questList.transform.FindChild("QuestDescription").gameObject.SetActive(true);
        GameObject qDescr = questList.transform.FindChild("QuestDescription").gameObject;
        qDescr.transform.FindChild("Name").GetComponent<Text>().text = quest.Name;
        qDescr.transform.FindChild("Description").FindChild("Viewport").FindChild("Content").GetComponent<Text>().text = quest.Description;
        for(int i = 0; i < qDescr.transform.FindChild("Where").transform.childCount; i++)
        {
            Destroy(qDescr.transform.FindChild("Where").transform.GetChild(i).gameObject);
        }

        string task = "Задание:\n";
        qDescr.transform.FindChild("Task").GetComponent<Text>().text = "";
        switch (quest.questType)
        {
            case (Quest.QuestType.Build):
                task += "Полностью построить " + quest.amount + " " + quest.nameWho;
                break;
            case (Quest.QuestType.Create):
                task += "Нанять " + quest.amount + " " + quest.nameWho;
                break;
            case (Quest.QuestType.Explore):
                task += "Разведать территорию: ";
                for(int i = 0; i < quest.whereToGo.Count; i++)
                {
                    GameObject button = Instantiate(questButtonGO);
                    button.transform.FindChild("Text").GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
                    button.transform.SetParent(qDescr.transform.FindChild("Where").transform);
                    button.transform.GetChild(0).GetComponent<Text>().text = (i+1).ToString();
                    Vector3 plc = quest.whereToGo[i];
                    button.GetComponent<Button>().onClick.AddListener(() => CameraGoesToPlace(plc));
                }
                break;
            case Quest.QuestType.SendSquad:
                task += "Отправить отряд: ";
                for (int i = 0; i < quest.whereToGo.Count; i++)
                {
                    GameObject button = Instantiate(questButtonGO);
                    button.transform.FindChild("Text").GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
                    button.transform.SetParent(qDescr.transform.FindChild("Where").transform);
                    button.transform.GetChild(0).GetComponent<Text>().text = (i + 1).ToString();
                    Vector3 plc = quest.whereToGo[i];
                    button.GetComponent<Button>().onClick.AddListener(() => CameraGoesToPlace(plc));
                }
                break;
            case (Quest.QuestType.FindItem):
                task += "Принести " + quest.amount + " " + GameObject.Find("Inventory").GetComponent<ItemDatabase>().database.Find(it => it.ID == quest.idQuestItem).Name;
                break;
            case (Quest.QuestType.Kill):
                task += "Уничтожить " + quest.amount + " " + quest.nameWho;
                break;
            case Quest.QuestType.BuildInPlace:
                task += "Поставить " + quest.amount + " " + quest.nameWho + " здесь: ";
                for (int i = 0; i < quest.whereToGo.Count; i++)
                {
                    GameObject button = Instantiate(questButtonGO);
                    button.transform.FindChild("Text").GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
                    button.transform.SetParent(qDescr.transform.FindChild("Where").transform);
                    button.transform.GetChild(0).GetComponent<Text>().text = (i + 1).ToString();
                    Vector3 plc = quest.whereToGo[i];
                    button.GetComponent<Button>().onClick.AddListener(() => CameraGoesToPlace(plc));
                }
                break;
            case Quest.QuestType.PlaceBuild:
                break;
            case Quest.QuestType.TalkTo:
                task += "Поговорить с " + quest.nameWho;
                break;
        }
        qDescr.transform.FindChild("Task").GetComponent<Text>().text = task;
        qDescr.transform.FindChild("Reward").GetComponent<Text>().text = "";
        string reward = "";
        if (quest.goldReward > 0)
            reward += "<color=#FFF300FF>Золото: " + quest.goldReward.ToString() + "</color>\n";
        if (quest.woodReward > 0)
            reward += "<color=#8A480DFF>Дерево: " + quest.woodReward.ToString() + "</color>\n";
        if (quest.stoneReward > 0)
            reward += "<color=#858177FF>Камень: " + quest.stoneReward.ToString() + "</color>\n";

        qDescr.transform.FindChild("Reward").GetComponent<Text>().text = reward;
    }

    public void CameraGoesToPlace(Vector3 place)
    {
        Camera.main.transform.position = place+new Vector3(25,40,20);
        Camera.main.transform.LookAt(place);
    }

    public void CloseList()
    {
        for (int i = 0; i < questList.transform.FindChild("QuestList").transform.childCount; i++)
            Destroy(questList.transform.FindChild("QuestList").transform.GetChild(i).gameObject);
        questList.transform.FindChild("QuestDescription").gameObject.SetActive(false);
        questList.SetActive(false);
        questListOpened = false;
    }

    public void CloseDescriptionQuest()
    {
        questList.transform.FindChild("QuestDescription").gameObject.SetActive(false);
    }

    void CheckQuestDone(Quest quest)
    {
        if (quest.amount <= 0 && !quest.questDone)
        {
            StartCoroutine(FadeText.FadeTxt("Задание выполнено!", Color.green, 2));
            quest.questDone = true;

            switch (quest.questType)
            {
                case Quest.QuestType.Kill:
                    foreach (Quest questCheck in quests)
                        if (questCheck.ID != quest.ID && questCheck.nameWho == quest.nameWho)
                            break;
                        else
                            namesToKill.Remove(quest.nameWho);
                    break;

                case Quest.QuestType.Build:
                    foreach (Quest questCheck in quests)
                        if (questCheck.ID != quest.ID && questCheck.nameWho == quest.nameWho)
                            break;
                        else
                            namesToBuild.Remove(quest.nameWho);
                    break;

                case Quest.QuestType.BuildInPlace:
                    foreach (Quest questCheck in quests)
                        if (questCheck.ID != quest.ID && questCheck.nameWho == quest.nameWho)
                            break;
                        else
                            namesToBuild.Remove(quest.nameWho);
                    break;

                case Quest.QuestType.PlaceBuild:
                    foreach (Quest questCheck in quests)
                        if (questCheck.ID != quest.ID && questCheck.nameWho == quest.nameWho)
                            break;
                        else
                            namesToPlace.Remove(quest.nameWho);
                    break;

                case Quest.QuestType.Create:
                    foreach (Quest questCheck in quests)
                        if (questCheck.ID != quest.ID && questCheck.nameWho == quest.nameWho)
                            break;
                        else
                            namesToCreate.Remove(quest.nameWho);
                    break;

                case Quest.QuestType.TalkTo:
                    foreach (Quest questCheck in quests)
                        if (questCheck.ID != quest.ID && questCheck.nameWho == quest.nameWho)
                            break;
                        else
                            namesToTalk.Remove(quest.nameWho);
                    break;
                case Quest.QuestType.FindItem:
                    GetComponent<InventoryUnit>().items.Remove(GetComponent<InventoryUnit>().items.Find(it => it.ID == quest.idQuestItem));
                    break;
            }

            if (quest.Broadcast)
            {
                GameObject.Find(quest.BroadcastWho).BroadcastMessage(quest.BroadcastWhat);
            }

            if (quest.RewardAtGiver)
            {
                quests.Remove(quest);
                doneQuests.Add(quest);
                if(quest.whoGiveQuest.GetComponent<QuestGiver>())
                    quest.whoGiveQuest.GetComponent<QuestGiver>().GiveReward(quest.ID);
            }
            else
            {
                quest.rewardGived = false;
                quest.whoGiveQuest.GetComponent<QuestGiver>().questsToReward.Add(quest);
                quests.Remove(quest);
                doneQuests.Add(quest);
            }

            if (quest.StartSelfDialogue)
            {
                quest.whoGiveQuest.GetComponent<QuestGiver>().GiveReward(quest.ID);

                if (quest.doneDialogueID == -1)
                    return;

                GetComponent<DialoguesTalker>().dialID = quest.doneDialogueID;
                GetComponent<DialoguesTalker>().ReloadDialogue();
                GetComponent<DialoguesTalker>().OpenDialogue();
            }
        }
    }

    /// <summary>
    /// Функция проверки убитого моба
    /// </summary>
    public void CheckKilledEnemy(string name)
    {
        // по каждому имени моба, которого надо убить
        foreach (string n in namesToKill)
        {
            // если в этом имени есть такое же имя, как и у убитого
            if (n.Contains(name))
            {
                // находим квесты, который связан с этим именем
                List<Quest> relatedQuests = quests.FindAll(q => q.nameWho == name);
                // по каждому найденному квесту
                foreach (Quest quest in relatedQuests)
                {
                    // уменьшаем требуемое количество мобов
                    quest.amount--;
                    // если мы убили всех нужных мобов и квест ещё не выполнен
                    CheckQuestDone(quest);
                }
                //выходим
                break;
            }
        }
    }

    public void CheckBuildedBuild(string name)
    {
        // по каждому имени моба, которого надо убить
        foreach (string n in namesToBuild)
        {
            // если в этом имени есть такое же имя, как и у убитого
            if (n.Contains(name))
            {
                // находим квесты, который связан с этим именем
                List<Quest> relatedQuests = quests.FindAll(q => q.nameWho == name);
                // по каждому найденному квесту
                foreach (Quest quest in relatedQuests)
                {
                    // уменьшаем требуемое количество мобов
                    quest.amount--;
                    CheckQuestDone(quest);
                }
                //выходим
                break;
            }
        }
    }

    public void CheckPlacedBuild(string name)
    {
        // по каждому имени моба, которого надо убить
        foreach (string n in namesToPlace)
        {
            // если в этом имени есть такое же имя, как и у убитого
            if (n.Contains(name))
            {
                // находим квесты, который связан с этим именем
                List<Quest> relatedQuests = quests.FindAll(q => q.nameWho == name);
                // по каждому найденному квесту
                foreach (Quest quest in relatedQuests)
                {
                    // уменьшаем требуемое количество мобов
                    quest.amount--;
                    CheckQuestDone(quest);
                }
                //выходим
                break;
            }
        }
    }

    public void CheckCreatedUnit(string name)
    {
        // по каждому имени моба, которого надо убить
        foreach (string n in namesToCreate)
        {
            // если в этом имени есть такое же имя, как и у убитого
            if (n.Contains(name))
            {
                // находим квесты, который связан с этим именем
                List<Quest> relatedQuests = quests.FindAll(q => q.nameWho == name);
                // по каждому найденному квесту
                foreach (Quest quest in relatedQuests)
                {
                    // уменьшаем требуемое количество мобов
                    quest.amount--;
                    // если мы убили всех нужных мобов и квест ещё не выполнен
                    CheckQuestDone(quest);
                }
                //выходим
                break;
            }
        }
    }

    public void CheckTalk(string name)
    {
        // по каждому имени моба, которого надо убить
        foreach (string n in namesToTalk)
        {
            // если в этом имени есть такое же имя, как и у убитого
            if (n.Contains(name))
            {
                var talkTo = GameObject.Find(name);
                if (talkTo)
                {
                    var questIdent = talkTo.transform.FindChild("QuestIdent");
                    if (!questIdent)
                        questIdent = talkTo.transform.FindChild("QuestIdent(Clone)");
                    if (questIdent)
                        Destroy(questIdent.gameObject);
                }
                // находим квесты, который связан с этим именем
                List<Quest> relatedQuests = quests.FindAll(q => q.nameWho == name);
                // по каждому найденному квесту
                foreach (Quest quest in relatedQuests)
                {
                    // уменьшаем требуемое количество мобов
                    quest.amount--;
                    // если мы убили всех нужных мобов и квест ещё не выполнен
                    CheckQuestDone(quest);
                }
                //выходим
                break;
            }
        }
    }

    public void CheckBringedItem(int id)
    {
        Quest relatedQuest = quests.Find(q => q.ID == id);
        if (relatedQuest != null)
        {
            relatedQuest.amount--;
            CheckQuestDone(relatedQuest);
        }
    }

    public void CheckCreatedSquad(int unitCount)
    {
        Quest relatedQuest = quests.Find(q => q.questType == Quest.QuestType.MakeNewSquad);
        if(relatedQuest != null && relatedQuest.amount == unitCount)
        {
            relatedQuest.amount = 0;
            CheckQuestDone(relatedQuest);
        }
    }

    public void CheckPlacedSquadFlag(Quest que)
    {
        Quest quest = quests.Find(q => q.ID == que.ID);
        if (quest != null)
        {
            quest.amount--;
            CheckQuestDone(quest);
        }
    }

    public void CheckExploreQuest(Quest que)
    {
        Quest quest = quests.Find(q => q.ID == que.ID);
        if (quest != null)
        {
            quest.amount--;
            CheckQuestDone(quest);
        }
    }

    public void ReopenList()
    {
        CloseList();
        OpenList();
        questList.SetActive(true);
    }

    public void OpenOrCloseList()
    {
        questListOpened = !questListOpened;
        if (questListOpened)
            OpenList();
        else
            CloseList();
        questList.SetActive(questListOpened);
    }
}
