﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Quest {

    public enum QuestType
    {
        Explore = 0, // разведка
        Kill = 1, // убийство
        Create = 2, // создание 
        Build = 3,  // поставить строиться здание в любом месте
        FindItem = 4, // принести вещь в определённое место
        Text = 5, // текстовый
        None = 6, // нет
        BuildInPlace = 7, // поставить строиться в определённом месте
        PlaceBuild = 8,
        SendSquad = 9, // отправить отряд (т.е., просто поставить флаг)
        MakeNewSquad = 10,
        TalkTo = 11
    }

    public enum WhoWillCome
    {
        hero = 0,
        squad = 1,
        allabove = 2,
        none = 3
    }

    public enum RunTo
    {
        hero = 0,
        squad = 1,
        none = 2
    }
    
    public int ID; // идентефикатор квеста
    public int dialogueID; // идентефикатор диалога, связанного с этим квестом
    public int doneDialogueID;
    public int waitDialogueID;
    public string Name; // название квеста
    public string Description; // описание квеста
    public float woodReward, stoneReward, goldReward; // награда
    public bool Avaivable; // можно ли взять квест?
    public bool RewardAtGiver; // даётся ли награда у того, кто выдал квест (если нет, то награда выдаётся по завершению квеста)
    public QuestType questType; // тип квеста
    public bool StartSelfDialogue; // начать ли диалог с самим собой после выполнения квеста

    public bool Broadcast; // нужно ли отправлять сообщение после завершения квеста
    public string BroadcastWho; // кому нужно отправить сообщение
    public string BroadcastWhat; // что будем отправлять

    public int amount; // сколько надо (убить/построить/создать)
    public int idQuestItem; // идентефикатор квестового предмета
    public string nameWho; // название того, кого надо (убить/построить/создать)
    public List<Vector3> whereToGo; // места, которое надо исследовать
    public float unfogRadius; // как далеко мы будем видеть вокруг "кристалла"
    public WhoWillCome whoWillCome; // кто туда должен придти
    public bool needToRun; // надо ли нам за кем-то бежать
    public RunTo whoWeWillRunTo; // за кем надо бежать

    public GameObject whoGiveQuest; // кто выдал квест
    public bool questDone; // можно ли сдать квест?
    public bool rewardGived; // была ли выдана награда?

    public Quest()
    {
        ID = doneDialogueID = waitDialogueID = -1;
        Name = Description = "";
        woodReward = stoneReward = goldReward = 0;
        Avaivable = false;
        questType = QuestType.None;
        questDone = false;
        rewardGived = false;
        RewardAtGiver = true;
        StartSelfDialogue = false;
        whoWillCome = WhoWillCome.none;
        BroadcastWhat = "";
        BroadcastWho = "";
        unfogRadius = 20;
    }

    public Quest(int ID, int dialID, int doneID, int waitID, string Name, string Description, float wood, float stone, float gold, bool av, QuestType type, int amount, int idItem, string who, List<Vector3> where, bool done, WhoWillCome whoCome, bool runTo, RunTo whoWeRunTo, bool rewardAtGiver, bool startSelfDialogue, bool broadcast, string broadWho, string broadWhat, float rad)
    {
        this.ID = ID;
        this.dialogueID = dialID;
        doneDialogueID = doneID;
        waitDialogueID = waitID;
        this.Name = Name;
        this.Description = Description;
        this.woodReward = wood;
        this.stoneReward = stone;
        this.goldReward = gold;
        this.Avaivable = av;
        questType = type;
        this.amount = amount;
        idQuestItem = idItem;
        nameWho = who;
        whereToGo = where;
        questDone = done;
        rewardGived = false;
        whoWillCome = whoCome;
        needToRun = runTo;
        whoWeWillRunTo = whoWeRunTo;
        RewardAtGiver = rewardAtGiver;
        StartSelfDialogue = startSelfDialogue;
        Broadcast = broadcast;
        unfogRadius = rad;
        if (broadcast)
        {
            BroadcastWho = broadWho;
            BroadcastWhat = broadWhat;
        }
        else
        {
            BroadcastWhat = "";
            BroadcastWho = "";
        }
    }
}
