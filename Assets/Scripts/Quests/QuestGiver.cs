﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class QuestGiver : MonoBehaviour {
    public List<Quest> questsToGive; // квесты, которые он может дать
    public List<Quest> questsToReward; // квесты, которые выполнены и он может дать награду
    public float rangeOpenQuestList = 50f; // дальность открытия меню
    public bool questListOpened = false; // открыто ли меню
    GameObject questIdent;
    private GameObject questExploreGameObject, questIdentGameObject;
    GameObject questManager;

    GameObject hero;
    void Start()
    {
        questIdentGameObject = Resources.Load("QuestIdent") as GameObject;
        questExploreGameObject = Resources.Load("QuestExplore") as GameObject;
        hero = GameObject.FindGameObjectWithTag("Player");
        questManager = GameObject.Find("QuestAndDialogueManager");
    }

    void Update()
    {
        if (questsToReward.Count > 0 && !questIdent && !gameObject.CompareTag("Player"))
        {
            questIdent = Instantiate(questIdentGameObject, transform.position + new Vector3(0, 5, 0), Quaternion.identity) as GameObject;
            questIdent.transform.SetParent(transform);
            questIdent.transform.localEulerAngles = new Vector3(90, 0, 0);
            questIdent.GetComponent<VolumetricLines.VolumetricLineBehavior>().LineColor = Color.green;
        } else
        if (questsToGive.Count > 0 && !questIdent && !gameObject.CompareTag("Player"))
        {
            questIdent = Instantiate(questIdentGameObject, transform.position + new Vector3(0,5,0), Quaternion.identity) as GameObject;
            questIdent.transform.SetParent(transform);
            questIdent.transform.localEulerAngles = new Vector3(90,0,0);
            questIdent.GetComponent<VolumetricLines.VolumetricLineBehavior>().LineColor = Color.yellow;
        }
        else
        if(questsToGive.Count == 0 && questsToReward.Count == 0 && questIdent)
        {
            Destroy(questIdent);
        }
    }

    public void SetupQuests(List<int> questCanGive)
    {
        questsToGive.Clear();
        questsToReward.Clear();
        foreach (int idQuest in questCanGive)
        {
            Quest quest = questManager.GetComponent<QuestManager>().questDatabase.Find(q => idQuest == q.ID);
            if (!quest.questDone)
                questsToGive.Add(quest);
            else
            {
                questsToReward.Add(quest);
                hero.GetComponent<QuestReceiver>().doneQuests.Add(quest);
            }
        }
    }

    /// <summary>
    /// Функция выдачи награды
    /// </summary>
    public void GiveReward(int questID)
    {
        Quest que = hero.GetComponent<QuestReceiver>().doneQuests.Find(q => q.ID == questID);

        if (que == null)
            return;

        if (!que.rewardGived)
        {
            if (que.woodReward > 0)
                Camera.main.GetComponent<GameResources>().wood += que.woodReward;
            if (que.stoneReward > 0)
                Camera.main.GetComponent<GameResources>().stone += que.stoneReward;
            if (que.goldReward > 0)
                hero.GetComponent<Gold>().currentGold += que.goldReward;
        }
        que.rewardGived = true;

        if(que.needToRun)
            GetComponent<NPC>().SetTarget(null, false);

        if (questsToGive.Count == 0)
        {
            GetComponent<DialoguesTalker>().dialID = 1;
            GetComponent<DialoguesTalker>().ReloadDialogue();
        }
    }

    public void GiveReward()
    {
        foreach (Quest que in questsToReward)
        {
            if (!hero.GetComponent<QuestReceiver>().doneQuests.Find(q => q.ID == que.ID).rewardGived)
            {
                if (que.woodReward > 0)
                    Camera.main.GetComponent<GameResources>().wood += que.woodReward;
                if (que.stoneReward > 0)
                    Camera.main.GetComponent<GameResources>().stone += que.stoneReward;
                if (que.goldReward > 0)
                    hero.GetComponent<Gold>().currentGold += que.goldReward;
                hero.GetComponent<QuestReceiver>().doneQuests.Find(q => q.ID == que.ID).rewardGived = true;
            }
        }
        GetComponent<NPC>().SetTarget(null, false);

        if (questsToGive.Count == 0)
        {
            GetComponent<DialoguesTalker>().dialID = 1;
            GetComponent<DialoguesTalker>().ReloadDialogue();
        }
    }

    public bool IsQuestsNeededToReward()
    {
        foreach(Quest qu in questsToReward)
            if(!qu.rewardGived)
                return true;
        return false;
    }

    public bool IsQuestGiven(int questID)
    {
        return !questsToGive.Exists(qu => questID == qu.ID);
    }

    public void CanWeGiveMoreQuests(Phrase phrases, List<Phrase> canWeGivePhrases)
    {
        foreach(Phrase phrase in phrases.answers)
        {
            if(phrase.action == Dialogue.Action.TakeQuest && !IsQuestGiven(phrase.questID))
            {
                canWeGivePhrases.Add(phrases);
            }
            CanWeGiveMoreQuests(phrase, canWeGivePhrases);
        }
    }

    public void AcceptQuest(int questID)
    {
        Quest que = questManager.GetComponent<QuestManager>().questDatabase.Find(q => q.ID == questID);
        questsToGive.Remove(que);
        que.whoGiveQuest = gameObject;
        hero.GetComponent<QuestReceiver>().quests.Add(que);

        if (que.questType == Quest.QuestType.Kill && !hero.GetComponent<QuestReceiver>().namesToKill.Exists(name => name == que.nameWho))
            hero.GetComponent<QuestReceiver>().namesToKill.Add(que.nameWho);

        if ((que.questType == Quest.QuestType.Build || que.questType == Quest.QuestType.BuildInPlace) && !hero.GetComponent<QuestReceiver>().namesToBuild.Exists(name => name == que.nameWho))
            hero.GetComponent<QuestReceiver>().namesToBuild.Add(que.nameWho);

        if(que.questType == Quest.QuestType.PlaceBuild && !hero.GetComponent<QuestReceiver>().namesToPlace.Exists(name => name == que.nameWho))
            hero.GetComponent<QuestReceiver>().namesToPlace.Add(que.nameWho);

        if (que.questType == Quest.QuestType.Create && !hero.GetComponent<QuestReceiver>().namesToCreate.Exists(name => name == que.nameWho))
            hero.GetComponent<QuestReceiver>().namesToCreate.Add(que.nameWho);

        if (que.questType == Quest.QuestType.TalkTo &&
            !hero.GetComponent<QuestReceiver>().namesToTalk.Exists(name => name == que.nameWho))
        {
            hero.GetComponent<QuestReceiver>().namesToTalk.Add(que.nameWho);

            var who = GameObject.Find(que.nameWho);
            if (who)
            {
                var ident = Instantiate(Resources.Load("QuestIdent")) as GameObject;
                ident.name = "QuestIdent";
                ident.transform.parent = GameObject.Find(que.nameWho).transform;
                var maxBound = ident.transform.parent.GetComponent<Collider>().bounds.max;
                var centerBound = ident.transform.parent.GetComponent<Collider>().bounds.center;
                ident.transform.position = new Vector3(centerBound.x, maxBound.y + 6, centerBound.z);
            }
        }

        if (que.questType == Quest.QuestType.Explore)
        {
            foreach (Vector3 place in que.whereToGo)
            {
                GameObject flg = Instantiate(questExploreGameObject, place, Quaternion.identity) as GameObject;
                flg.transform.localEulerAngles = new Vector3(-90,0,0);
                flg.name = "QuestExplore";
                flg.GetComponent<RealFoWUnit>().radius = Mathf.FloorToInt(que.unfogRadius);
                if (que.whoWillCome == Quest.WhoWillCome.hero)
                { // если должен придти герой
                    flg.transform.GetChild(0).GetComponent<QuestWhereToGoCheck>().whoWillComeHere.Add(GameObject.FindGameObjectWithTag("Player").GetComponent<IDGetSet>().creatureID);
                    flg.transform.GetChild(0).GetComponent<QuestWhereToGoCheck>().quest = que;
                }
                if (que.whoWillCome == Quest.WhoWillCome.squad)
                { // если должен придти какой-нибудь отряд
                    flg.transform.GetChild(0).GetComponent<QuestWhereToGoCheck>().quest = que;
                    GameObject[] squadGO = GameObject.FindGameObjectsWithTag("SquadGO");
                    for (int i = 0; i < squadGO.Length; i++)
                    {
                        foreach (GameObject unit in squadGO[i].GetComponent<Squad>().units)
                            flg.transform.GetChild(0).GetComponent<QuestWhereToGoCheck>().whoWillComeHere.Add(unit.GetComponent<IDGetSet>().creatureID);
                    }
                }
                if (que.whoWillCome == Quest.WhoWillCome.allabove)
                {
                    flg.transform.GetChild(0).GetComponent<QuestWhereToGoCheck>().quest = que;
                    flg.transform.GetChild(0).GetComponent<QuestWhereToGoCheck>().whoWillComeHere.Add(GameObject.FindGameObjectWithTag("Player").GetComponent<IDGetSet>().creatureID);
                    for (int i = 0; i < Squads.squads.Count; i++)
                    {
                        GameObject squadGO = GameObject.Find("Squad" + i.ToString());
                        if (!squadGO)
                            continue;
                        foreach (GameObject unit in squadGO.GetComponent<Squad>().units)
                            flg.transform.GetChild(0).GetComponent<QuestWhereToGoCheck>().whoWillComeHere.Add(unit.GetComponent<IDGetSet>().creatureID);
                    }
                }
            }
        }    
        if(que.questType == Quest.QuestType.BuildInPlace || que.questType == Quest.QuestType.SendSquad || que.questType == Quest.QuestType.FindItem)
        {
            foreach (Vector3 place in que.whereToGo)
            {
                GameObject flg = Instantiate(questExploreGameObject, place, new Quaternion(0, 0, 0, 0)) as GameObject;
                flg.name = "QuestExplore";
                flg.GetComponent<RealFoWUnit>().radius = Mathf.FloorToInt(que.unfogRadius);
                flg.transform.localEulerAngles = new Vector3(-90, 0, 0);
                flg.transform.GetChild(0).GetComponent<QuestWhereToGoCheck>().quest = que;
            }
        }
        if (que.needToRun)
        {
            if (que.whoWeWillRunTo == Quest.RunTo.hero)
                GetComponent<NPC>().SetTarget(hero, true);
        }
        if (hero.GetComponent<QuestReceiver>().questListOpened)
            hero.GetComponent<QuestReceiver>().ReopenList();
        StartCoroutine(FadeText.FadeTxt("Получено новое задание.", Color.green, 2));
    }
}
