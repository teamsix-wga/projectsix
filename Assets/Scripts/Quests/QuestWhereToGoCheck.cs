﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuestWhereToGoCheck : MonoBehaviour {
    public List<int> whoWillComeHere = new List<int>();
    public List<GameObject> GOinTrigger = new List<GameObject>();
    public Quest quest;

    void OnTriggerEnter(Collider other)
    {
        if(quest.questType == Quest.QuestType.Explore)
            if(whoWillComeHere.Exists(
                who =>
                    (other.gameObject.GetComponent<IDGetSet>() &&
                     who == other.gameObject.GetComponent<IDGetSet>().creatureID)))
            {
                GameObject.FindGameObjectWithTag("Player").SendMessage("CheckExploreQuest", quest);
                Destroy(transform.parent.gameObject);
            }
        if (quest.questType == Quest.QuestType.SendSquad && other.name == "UnitFlag")
        {
            GameObject.FindGameObjectWithTag("Player").SendMessage("CheckPlacedSquadFlag", quest);
            Destroy(transform.parent.gameObject);
        }

        if (quest.questType == Quest.QuestType.FindItem && other.CompareTag("Player") && other.GetComponent<InventoryUnit>().items.Exists(itm => itm.ID == quest.idQuestItem))
        {
            GameObject.FindGameObjectWithTag("Player").SendMessage("CheckBringedItem", quest.ID);
            Destroy(transform.parent.gameObject);
        }

        GOinTrigger.Add(other.gameObject);
    }

    void OnTriggerExit(Collider other)
    {
        GOinTrigger.Remove(other.gameObject);
        GOinTrigger.RemoveAll(go => go == null);
    }

    void Update()
    {
        if(quest.questType == Quest.QuestType.BuildInPlace && GOinTrigger.Count > 0)
        {
            if (GOinTrigger.Exists(go => go == null))
                GOinTrigger.RemoveAll(go => go == null);
            
            GameObject neededBuild = GOinTrigger.Find(go => go.name == quest.nameWho);
            if (neededBuild && neededBuild.GetComponent<Construction>().placed)
            {
                GameObject.FindGameObjectWithTag("Player").SendMessage("CheckBuildedBuild", neededBuild.name);
                Destroy(transform.parent.gameObject);
            }
        }

        if (quest.questType == Quest.QuestType.FindItem && GOinTrigger.Count > 0)
        {
            var maybePlayerHere = GOinTrigger.Find(go => go.CompareTag("Player"));
            if (maybePlayerHere && maybePlayerHere.GetComponent<InventoryUnit>().items.Exists(itm => itm.ID == quest.idQuestItem))
            {
                GameObject.FindGameObjectWithTag("Player").SendMessage("CheckBringedItem", quest.ID);
                Destroy(transform.parent.gameObject);
            }
        }
    }
}
