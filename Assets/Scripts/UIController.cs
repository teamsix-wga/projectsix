﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIController : MonoBehaviour {
    public GameObject goldUI;
    public Text infoUI;

    void Awake ()
    {
        goldUI = GameObject.Find("GoldUI");
        infoUI = GameObject.Find("InfoUI").transform.GetChild(0).GetComponent<Text>();
        infoUI.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        infoUI.GetComponent<RectTransform>().localPosition = new Vector2(0, -1 * Screen.height / 5);
        infoUI.GetComponent<Text>().text = "";
    }

    void Update()
    {
        if (Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Count > 0)
        {
            GameObject selUnit = Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0];
            if (Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Count == 1 && selUnit && !selUnit.CompareTag("TownHall") && !selUnit.CompareTag("FriendBuild"))
            {
                if (Camera3dPerson.hero != null && selUnit.CompareTag("Player"))
                    goldUI.GetComponent<Text>().text = "<color=#FFF300FF>Золото игрока: " + Mathf.Round(Camera3dPerson.hero.GetComponent<Gold>().currentGold).ToString() + "</color>\n" +
                "<color=#8A480DFF>Дерево: " + Camera.main.GetComponent<GameResources>().wood + "</color>\n" + "<color=#858177FF>Камень: " + Camera.main.GetComponent<GameResources>().stone + "</color>";
                else
                if (selUnit.CompareTag("Unit"))
                    goldUI.GetComponent<Text>().text = "<color=#FFF300FF>Золото юнита: " + Mathf.Round(selUnit.GetComponent<Gold>().currentGold).ToString() + "</color>";
            }
            else
            {
                if (Camera3dPerson.hero != null)
                    goldUI.GetComponent<Text>().text = "<color=#FFF300FF>Золото игрока: " + Mathf.Round(Camera3dPerson.hero.GetComponent<Gold>().currentGold).ToString() + "</color>\n" +
    "<color=#8A480DFF>Дерево: " + Camera.main.GetComponent<GameResources>().wood + "</color>\n" + "<color=#858177FF>Камень: " + Camera.main.GetComponent<GameResources>().stone + "</color>";

            }
        }
        else
        {
            goldUI.GetComponent<Text>().text = "<color=#FFF300FF>Золото игрока: " + Mathf.Round(Camera3dPerson.hero.GetComponent<Gold>().currentGold).ToString() + "</color>\n" +
    "<color=#8A480DFF>Дерево: " + Camera.main.GetComponent<GameResources>().wood + "</color>\n" + "<color=#858177FF>Камень: " + Camera.main.GetComponent<GameResources>().stone + "</color>";
        }
    }

    public void CloseInventoryFromButton()
    {
        if(Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Count > 0)
            Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0].GetComponent<InventoryUnit>().Close();
        else
            GameObject.FindGameObjectWithTag("Player").GetComponent<InventoryUnit>().Close();
    }

    public void CloseShopFromButton()
    {
        Shop.openedShop.CloseShop();
    }
}
