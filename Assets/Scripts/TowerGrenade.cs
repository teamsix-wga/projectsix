﻿using UnityEngine;
using System.Collections;

public class TowerGrenade : MonoBehaviour {

    public GameObject opponent;
    public float speed;
    float damage;
    GameObject parent;

    void Start()
    {
        GetComponent<Collider>().isTrigger = true;
    }

    void Update () {
        if (opponent)
        {
            if(Vector3.Distance(opponent.GetComponent<Collider>().bounds.center, transform.position) <= 1f)
            {
                opponent.SendMessage("GetHit", new AttackStructure() { damage = this.damage, whoHit = parent });
                transform.parent = opponent.transform;
                transform.position = opponent.GetComponent<Collider>().bounds.center;
                opponent = null;
                StartCoroutine(WaitBeforeDestroy());
                return;
            }
            transform.LookAt(opponent.GetComponent<Collider>().bounds.center);
            transform.position += transform.forward * Time.deltaTime * speed;
            transform.localScale = new Vector3(-1,-1,-1);
        }
	}

    IEnumerator WaitBeforeDestroy()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }

    public void SetOpponent(GameObject target, float towerDamage, GameObject tower)
    {
        opponent = target;
        damage = towerDamage;
        parent = tower;
    }
}
