﻿using UnityEngine;
using System.Collections;

public class GameResources : MonoBehaviour {
    public float wood;
    public float stone;
    
    public void AddWood(float amount){
        wood += amount;
    }

    public void AddStone(float amount)
    {
        stone += amount;
    }
}
