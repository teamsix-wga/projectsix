﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Reflection;
using System;
using UnityEngine.SceneManagement;

public class EscMenu : MonoBehaviour {
    public bool menuActive;
    GameObject ui, unitUI, playerUI;
    bool uUI, pUI;
    string filepath;
    static AsyncOperation AOp;
    static int rounded;
    int pRounded = 0;
    bool load = false;
    public static bool loading = false;

    void Awake()
    {
        filepath = "Saves\\save.xml";
        menuActive = false;
        ui = GameObject.Find("EscMenu");
        unitUI = GameObject.Find("UnitUI");
        playerUI = GameObject.Find("SkillPanel");
        uUI = pUI = false;
    }

    void Update()
    {
        if (load)
        {
            RectTransform rectLoad = GameObject.Find("LoadBar").GetComponent<RectTransform>();
            rectLoad.sizeDelta = new Vector2((Screen.width / 10) * pRounded, rectLoad.transform.parent.GetChild(1).GetComponent<RectTransform>().rect.height/2);
            rectLoad.anchoredPosition = new Vector2(rectLoad.sizeDelta.x / 2, rectLoad.transform.parent.GetChild(1).GetComponent<RectTransform>().position.y);
        }

        if (Input.GetKeyDown(KeyCode.Escape) && !GameObject.FindGameObjectWithTag("Player").GetComponent<Fighter>().aoe && Camera3dPerson.canMoveCam && !BuildingMenu.opened && !CommandLine.enableConsole &&
            !GameObject.FindGameObjectWithTag("Player").GetComponent<Builder>().building && !Camera.main.GetComponent<Squads>().setFlag && !InventoryUnit.inventoryActive && !Shop.shopOpened && Camera3dPerson.canMoveCam)
        {
            menuActive = !menuActive;
            ui.GetComponent<Canvas>().enabled = menuActive;
        }
    }

    public void Restart()
    {
        MainMenu.loaded = false;
        RectTransform rectLoad = GameObject.Find("LoadBar").GetComponent<RectTransform>();
        load = true;
        rectLoad.sizeDelta = new Vector2(0, Screen.height / 20);
        rectLoad.anchoredPosition = rectLoad.sizeDelta;
        GameObject.Find("LoadingScreen").GetComponent<Canvas>().enabled = true;
        GameObject loading = GameObject.Find("LoadScreenImage");
        loading.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        StartCoroutine("RestartLvl");
    }

    public void Exit()
    {
        Application.Quit();
    }

    #region Save
    public void SaveInventory(XmlDocument xmlDoc, XmlElement elem, string name, List<Item> itemsToSave)
    {
        XmlElement node = xmlDoc.CreateElement(name);
        foreach (Item it in itemsToSave)
        {
            if (it.ID == -1)
                continue;
            Item itmToSave = Item.CopyItem(it);
            itmToSave.slotID = it.slotID;
            XmlElement itemXML = xmlDoc.CreateElement("Item");
            XmlAttribute item;
            foreach (FieldInfo a in itmToSave.GetType().GetFields())
            {
                if (a.Name == "sprite" || a.Name == "model")
                    continue;
                item = xmlDoc.CreateAttribute(a.Name);
                item.Value = a.GetValue(itmToSave).ToString();
                itemXML.SetAttributeNode(item);
            }
            node.AppendChild(itemXML);
        }
        elem.AppendChild(node);
    }

    public void Save()
    {
        // если директория не существует
        if (!Directory.Exists("Saves"))
            // создать директорию
            Directory.CreateDirectory("Saves");
        // создаём новый XML документ
        XmlDocument xmlDoc = new XmlDocument();
        // корневой элемент
        XmlNode rootNode = null;
        // создать новый корневой элемент "Level" 
        rootNode = xmlDoc.CreateElement("Level");
        XmlAttribute attr = xmlDoc.CreateAttribute("Name");
        attr.Value = Application.loadedLevelName;
        rootNode.Attributes.Append(attr);
        // добавить элемент
        xmlDoc.AppendChild(rootNode);
        
        SaveHero(GameObject.FindGameObjectWithTag("Player"), rootNode, xmlDoc);
        SaveCamera(Camera.main.gameObject, rootNode, xmlDoc);

        foreach (FriendUnit unit in Resources.FindObjectsOfTypeAll<FriendUnit>())
            SaveUnit(unit.gameObject, rootNode, xmlDoc);

        foreach (GameObject lumber in GameObject.FindGameObjectsWithTag("Lumberjack"))
            SaveLumberjack(lumber, rootNode, xmlDoc);

        foreach (GameObject npc in GameObject.FindGameObjectsWithTag("NPC"))
            SaveNPC(npc, rootNode, xmlDoc);

        foreach (GameObject build in GameObject.FindGameObjectsWithTag("FriendBuild"))
            SaveBuild(build, rootNode, xmlDoc);
        if(GameObject.FindGameObjectWithTag("TownHall"))
            SaveBuild(GameObject.FindGameObjectWithTag("TownHall"), rootNode, xmlDoc);

        foreach (GameObject build in GameObject.FindGameObjectsWithTag("EnemyBuild"))
            SaveEnemyBuild(build, rootNode, xmlDoc);

        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
            SaveEnemy(enemy, rootNode, xmlDoc);

        foreach (GameObject tent in GameObject.FindGameObjectsWithTag("SquadTent"))
            SaveTent(tent, rootNode, xmlDoc);

        foreach (GameObject squadGO in GameObject.FindGameObjectsWithTag("SquadGO"))
            SaveSquadGO(squadGO, rootNode, xmlDoc);

        foreach (GameObject mine in GameObject.FindGameObjectsWithTag("Mine"))
            SaveMine(mine, rootNode, xmlDoc);

        foreach (GameObject tree in GameObject.FindGameObjectsWithTag("Tree"))
            SaveTree(tree, rootNode, xmlDoc);
        foreach (GameObject stone in GameObject.FindGameObjectsWithTag("Stone"))
            SaveStone(stone, rootNode, xmlDoc);
        foreach (GameObject quarry in GameObject.FindGameObjectsWithTag("Quarry"))
            SaveQuarry(quarry, rootNode, xmlDoc);

        foreach (GameObject questExp in GameObject.FindGameObjectsWithTag("QuestExplore"))
            SaveQuestExplore(questExp, rootNode, xmlDoc);

        foreach (GameObject itemModel in GameObject.FindGameObjectsWithTag("LyingItem"))
            SaveLyingItem(itemModel, rootNode, xmlDoc);

        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().SaveTexture();

        xmlDoc.Save(filepath);
    }

    void SaveInfo(object objectToSave, string name, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement objXML = xmlDoc.CreateElement(name);
        foreach (FieldInfo a in objectToSave.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
        {
            if (a.FieldType == typeof(Text) || a.FieldType == typeof(NavMeshAgent) || a.FieldType == typeof(AnimationClip) || a.FieldType == typeof(Sprite) || a.FieldType == typeof(UnityEngine.Object) ||
                a.FieldType == typeof(Transform) || a.FieldType == typeof(UIController) || a.FieldType == typeof(Projector) || a.FieldType == typeof(List<Skill>)) 
                continue;

            if (a.FieldType == typeof(GameObject))
            {
                if ((a.GetValue(objectToSave) as GameObject) == null || ((a.GetValue(objectToSave) as GameObject) && !(a.GetValue(objectToSave) as GameObject).GetComponent<IDGetSet>()))
                    continue;
                XmlAttribute attr = xmlDoc.CreateAttribute(a.Name);
                attr.Value = (a.GetValue(objectToSave) as GameObject).GetComponent<IDGetSet>().objectID.ToString();
                objXML.SetAttributeNode(attr);
            } else
            if (a.FieldType == typeof(List<int>))
                SaveList<int>(a.GetValue(objectToSave), a.Name, objXML, xmlDoc);
            else
            if (a.FieldType == typeof(List<float>))
                SaveList<float>(a.GetValue(objectToSave), a.Name, objXML, xmlDoc);
            else
            if(a.FieldType == typeof(float[]))
                SaveArray<float>(a.GetValue(objectToSave), a.Name, objXML, xmlDoc);
            else
            if (a.FieldType == typeof(List<bool>))
                SaveList<bool>(a.GetValue(objectToSave), a.Name, objXML, xmlDoc);
            else
            if (a.FieldType == typeof(List<string>))
                SaveList<string>(a.GetValue(objectToSave), a.Name, objXML, xmlDoc);
            else
            if (a.FieldType == typeof(List<GameObject>))
            {
                XmlElement elt = xmlDoc.CreateElement(a.Name);
                for (int i = 0; i < (a.GetValue(objectToSave) as List<GameObject>).Count; i++) {
                    if ((a.GetValue(objectToSave) as List<GameObject>)[i] == null ||
                        ((a.GetValue(objectToSave) as List<GameObject>)[i] && !(a.GetValue(objectToSave) as List<GameObject>)[i].GetComponent<IDGetSet>()))
                        continue;
                    XmlElement val = xmlDoc.CreateElement(a.Name);
                    val.SetAttribute(a.Name, (a.GetValue(objectToSave) as List<GameObject>)[i].GetComponent<IDGetSet>().objectID.ToString());
                    elt.AppendChild(val);
                }
                objXML.AppendChild(elt);
            } else
            if (a.FieldType == typeof(List<Quest>))
                SaveQuestList(a.GetValue(objectToSave), a.Name, objXML, xmlDoc);
            else
            if (a.FieldType == typeof(Quest))
            {
                SaveQuest(a.GetValue(objectToSave), a.Name, objXML, xmlDoc);
            }
            else
            if (a.FieldType == typeof(Item))
                //SaveItem(xmlDoc, objXML, a.Name, (Item)a.GetValue(objectToSave));
                SaveInventory(xmlDoc, objXML, a.Name, new List<Item>() { (Item)a.GetValue(objectToSave) });
            else
            if (a.FieldType == typeof(List<Item>))
                SaveInventory(xmlDoc, objXML, a.Name, a.GetValue(objectToSave) as List<Item>);
            else
            {
                XmlAttribute attr = xmlDoc.CreateAttribute(a.Name);
                attr.Value = a.GetValue(objectToSave).ToString();
                objXML.SetAttributeNode(attr);
            }
        }
        rootNode.AppendChild(objXML);
    }

    void SaveList<Tp>(object objectToSave, string name, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement elt = xmlDoc.CreateElement(name);
        for (int i = 0; i < (objectToSave as List<Tp>).Count; i++)
        {
            XmlElement val = xmlDoc.CreateElement(name);
            val.SetAttribute(name, (objectToSave as List<Tp>)[i].ToString());
            elt.AppendChild(val);
        }
        rootNode.AppendChild(elt);
    }

    void SaveArray<Tp>(object objectToSave, string name, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement elt = xmlDoc.CreateElement(name);
        for (int i = 0; i < (objectToSave as Tp[]).Length; i++)
        {
            XmlElement val = xmlDoc.CreateElement(name);
            val.SetAttribute(name, (objectToSave as Tp[])[i].ToString());
            elt.AppendChild(val);
        }
        rootNode.AppendChild(elt);
    }


    void SaveQuestList(object objectToSave, string name, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement elt = xmlDoc.CreateElement(name);
        for (int i = 0; i < (objectToSave as List<Quest>).Count; i++)
            SaveQuest((objectToSave as List<Quest>)[i], name, elt, xmlDoc);

        rootNode.AppendChild(elt);
    }

    void SaveQuest(object objectToSave, string name, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement val = xmlDoc.CreateElement(name);
        Quest quest = objectToSave as Quest;
        foreach (FieldInfo a in quest.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
        {
            if (a.FieldType == typeof(GameObject))
            {
                if ((a.GetValue(quest) as GameObject) == null || ((a.GetValue(quest) as GameObject) && !(a.GetValue(quest) as GameObject).GetComponent<IDGetSet>()))
                    continue;
                val.SetAttribute(a.Name, (a.GetValue(quest) as GameObject).GetComponent<IDGetSet>().objectID.ToString());
            }
            else
            if (a.FieldType == typeof(List<Vector3>))
            {
                foreach (Vector3 vect in (a.GetValue(quest) as List<Vector3>))
                {
                    XmlElement ve = xmlDoc.CreateElement(a.Name);
                    ve.SetAttribute(a.Name, vect.ToString());
                    val.AppendChild(ve);
                }
            }
            else
            if (a.FieldType == typeof(Quest.QuestType))
                val.SetAttribute(a.Name, ((int)((Quest.QuestType)a.GetValue(quest))).ToString());
            else
            if(a.FieldType == typeof(Quest.RunTo))
                val.SetAttribute(a.Name, ((int)((Quest.RunTo)a.GetValue(quest))).ToString());
            else
            if (a.FieldType == typeof(Quest.WhoWillCome))
                val.SetAttribute(a.Name, ((int)((Quest.WhoWillCome)a.GetValue(quest))).ToString());
            else
                val.SetAttribute(a.Name, a.GetValue(quest).ToString());
        }
        rootNode.AppendChild(val);
    }

    void SaveHero(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<NavMeshAgent>().destination, "destination", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Parameters>(), "Parameters", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Builder>(), "Builder", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Fighter>(), "Fighter", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<ClickToMove>(), "ClickToMove", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Gold>(), "Gold", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<QuestReceiver>(), "QuestReceiver", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<InventoryUnit>(), "InventoryUnit", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }

    void SaveCamera(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement("Camera");
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Camera3dPerson>(), "Camera3dPerson", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Squads>(), "Squads", heroXML, xmlDoc);
        XmlElement flags = xmlDoc.CreateElement("UnitFlags");
        SaveInfo(hero.GetComponent<GameResources>(), "GameResources", heroXML, xmlDoc);
        foreach (GameObject flag in Squads.visibleFlags)
            SaveUnitFlag(flag, flags, xmlDoc);
        heroXML.AppendChild(flags);
        rootNode.AppendChild(heroXML);
    }

    void SaveUnit(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<NavMeshAgent>().destination, "destination", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Parameters>(), "Parameters", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<FriendUnit>(), "FriendUnit", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Gold>(), "Gold", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<InventoryUnit>(), "InventoryUnit", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }

    void SaveLumberjack(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<NavMeshAgent>().destination, "destination", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Parameters>(), "Parameters", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Lumberjack>(), "Lumberjack", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }

    void SaveNPC(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        //Debug.Log(AssetDatabase.GetAssetPath(PrefabUtility.GetPrefabParent(hero)));
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<NavMeshAgent>().destination, "destination", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Parameters>(), "Parameters", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<QuestGiver>(), "QuestGiver", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<NPC>(), "NPC", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<DialoguesTalker>(), "DialoguesTalker", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }

    void SaveBuild(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Parameters>(), "Parameters", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Construction>(), "Construction", heroXML, xmlDoc);
        if(hero.GetComponent<BuildBuild>())
            SaveInfo(hero.GetComponent<BuildBuild>(), "BuildBuild", heroXML, xmlDoc);

        if (hero.GetComponent<Kazarma>())
            SaveInfo(hero.GetComponent<Kazarma>(), "Kazarma", heroXML, xmlDoc);
        if (hero.GetComponent<DefenceTower>())
            SaveInfo(hero.GetComponent<DefenceTower>(), "DefenceTower", heroXML, xmlDoc);
        if (hero.GetComponent<Shop>())
            SaveInfo(hero.GetComponent<Shop>(), "Shop", heroXML, xmlDoc);
        if (hero.GetComponent<TownHall>())
            SaveInfo(hero.GetComponent<TownHall>(), "TownHall", heroXML, xmlDoc);

        rootNode.AppendChild(heroXML);
    }

    void SaveEnemyBuild(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Parameters>(), "Parameters", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<EnemySpawn>(), "EnemySpawn", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }

    void SaveEnemy(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Parameters>(), "Parameters", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Enemy>(), "Enemy", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }

    void SaveTent(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<SquadTent>(), "SquadTent", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Construction>(), "Construction", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }

    void SaveSquadGO(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Squad>(), "Squad", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }
    void SaveMine(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Mine>(), "Mine", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }
    void SaveTree(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Tree>(), "Tree", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }
    void SaveQuarry(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Quarry>(), "Quarry", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }

    void SaveStone(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<Stone>(), "Stone", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }

    void SaveQuestExplore(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.transform.GetChild(0).GetComponent<QuestWhereToGoCheck>(), "QuestWhereToGoCheck", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }

    void SaveUnitFlag(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    {
        if(hero == null)
        {
            XmlElement flagXml = xmlDoc.CreateElement("UnitFlag");
            rootNode.AppendChild(flagXml);
            return;
        }
        XmlElement heroXML = xmlDoc.CreateElement(hero.name);
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.transform.GetComponent<FriendUnitFlag>(), "FriendUnitFlag", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }

    void SaveLyingItem(GameObject hero, XmlNode rootNode, XmlDocument xmlDoc)
    { 
        XmlElement heroXML = xmlDoc.CreateElement("LyingItem");
        SaveInfo(hero.transform.position, "position", heroXML, xmlDoc);
        SaveInfo(hero.transform.localEulerAngles, "rotation", heroXML, xmlDoc);
        SaveInfo(hero.transform.GetComponent<DroppedItem>(), "DroppedItem", heroXML, xmlDoc);
        SaveInfo(hero.GetComponent<IDGetSet>(), "IDGetSet", heroXML, xmlDoc);
        rootNode.AppendChild(heroXML);
    }
    #endregion


    void OnLevelWasLoaded(int level)
    {
        Time.timeScale = 1;
        if (level == 4 && MainMenu.loaded)
        {
            MainMenu.loaded = false;
            Load();
        }
    }

    public void Ld()
    {
        if (File.Exists(filepath))
        {
            MainMenu.loaded = true;
            RectTransform rectLoad = GameObject.Find("LoadBar").GetComponent<RectTransform>();
            load = true;
            rectLoad.sizeDelta = new Vector2(0, Screen.height / 20);
            rectLoad.anchoredPosition = rectLoad.sizeDelta;
            GameObject.Find("LoadingScreen").GetComponent<Canvas>().enabled = true;
            GameObject loading = GameObject.Find("LoadScreenImage");
            loading.GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filepath);
            var rootNode = xmlDoc.DocumentElement;
            StartCoroutine(LoadLevel(rootNode.Attributes[0].Value+"ForLoad"));
        }
    }

    IEnumerator LoadLevel(string name)
    {
        AOp = SceneManager.LoadSceneAsync(name, LoadSceneMode.Single);

        while (!AOp.isDone)
        {
            float p = AOp.progress * 100f;
            pRounded = Mathf.RoundToInt(p);
            yield return null;
        }
        yield return AOp;
    }

    IEnumerator RestartLvl()
    {
        AOp = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
        AOp.allowSceneActivation = false;

        while (!AOp.isDone && Mathf.Approximately(0.9f, AOp.progress))
        {
            float p = AOp.progress * 100f;
            pRounded = Mathf.RoundToInt(p);
            yield return null;
        }
        AOp.allowSceneActivation = true;
        yield return AOp;
    }

    public void LoadItem(XmlNode inven, GameObject saveTo)
    {
        Item itemToAdd = new Item();
        for (int j = 0; j < inven.ChildNodes.Count; j++)
        {
            XmlAttributeCollection itm = inven.ChildNodes[j].Attributes;
            for (int k = 0; k < itm.Count; k++)
            {
                FieldInfo[] flds = itemToAdd.GetType().GetFields();

                List<FieldInfo> fldslist = new List<FieldInfo>(flds);
                FieldInfo fi = fldslist.Find(fiel => fiel.Name == itm[k].Name);

                if (fi.FieldType == typeof(int))
                    fi.SetValue(itemToAdd, int.Parse(itm[k].Value));
                else
                if (fi.FieldType == typeof(float))
                    fi.SetValue(itemToAdd, float.Parse(itm[k].Value));
                else
                if (fi.FieldType == typeof(bool))
                    fi.SetValue(itemToAdd, bool.Parse(itm[k].Value));
                else
                    fi.SetValue(itemToAdd, itm[k].Value);
            }

            Item it = Item.CopyItem(itemToAdd); 
            it.slotID = itemToAdd.slotID;
            if (saveTo.CompareTag("Unit") || saveTo.CompareTag("Player"))
            {
                saveTo.GetComponent<InventoryUnit>().items.Add(it);
                InventorySlot.UpdateParameters(saveTo, saveTo.GetComponent<InventoryUnit>().items[saveTo.GetComponent<InventoryUnit>().items.Count - 1], -1, true);
                InventorySlot.UpdateParameters(saveTo, saveTo.GetComponent<InventoryUnit>().items[saveTo.GetComponent<InventoryUnit>().items.Count - 1], 1, true);
            } else
            if(saveTo.CompareTag("FriendBuild")) {
                saveTo.GetComponent<Shop>().shopItems.Add(it);
            }
            if(saveTo.CompareTag("Enemy"))
            {
                if (saveTo.GetComponent<Enemy>().haveItems.Count != 0)
                    InventorySlot.UpdateParameters(saveTo, saveTo.GetComponent<Enemy>().haveItems[0], -1, false);
                saveTo.GetComponent<Enemy>().haveItems.Add(it);
                InventorySlot.UpdateParameters(saveTo, saveTo.GetComponent<Enemy>().haveItems[saveTo.GetComponent<Enemy>().haveItems.Count - 1], -1, false);
                InventorySlot.UpdateParameters(saveTo, saveTo.GetComponent<Enemy>().haveItems[saveTo.GetComponent<Enemy>().haveItems.Count - 1], 1, false);
            }
        }
    }

    public void Load()
    {
        loading = true;
        XmlDocument xmlDoc = new XmlDocument();
        XmlNode rootNode = null;

        if (File.Exists(filepath))
        {
            xmlDoc.Load(filepath);
            rootNode = xmlDoc.DocumentElement;
        }
        else
            return;

        XmlNodeList leveldata = rootNode.ChildNodes;
        Dictionary<XmlNode, GameObject> dic = new Dictionary<XmlNode, GameObject>();
        Camera.main.GetComponent<Camera3dPerson>().UnitList = new List<GameObject>();
        Camera.main.GetComponent<Camera3dPerson>().lumberjacks = new List<GameObject>();
        for (int i = 0; i < leveldata.Count; i++)
        {
            if (leveldata[i].Name == "LyingItem")
            {
                GameObject loadGO = Instantiate(Resources.Load("ItemModels/"+leveldata[i]["DroppedItem"].ChildNodes[0].Attributes["modelName"].Value), new Vector3(float.Parse(leveldata[i]["position"].Attributes["x"].Value), float.Parse(leveldata[i]["position"].Attributes["y"].Value), float.Parse(leveldata[i]["position"].Attributes["z"].Value)), Quaternion.identity) as GameObject;
                loadGO.name = leveldata[i]["DroppedItem"].ChildNodes[0].Attributes["modelName"].Value;
                loadGO.transform.localEulerAngles = new Vector3(float.Parse(leveldata[i]["rotation"].Attributes["x"].Value), float.Parse(leveldata[i]["rotation"].Attributes["y"].Value), float.Parse(leveldata[i]["rotation"].Attributes["z"].Value));
                loadGO.GetComponent<IDGetSet>().objectID = int.Parse(leveldata[i]["IDGetSet"].Attributes["objectID"].Value);
                loadGO.tag = "LyingItem";
                loadGO.layer = 8;
                loadGO.AddComponent<Rigidbody>();
                loadGO.AddComponent<DroppedItem>();
                loadGO.GetComponent<SphereCollider>().enabled = true;
                dic.Add(leveldata[i], loadGO);
            }
            else
            if ((leveldata[i].Name.Contains("Squad") && !leveldata[i].Name.Contains("SquadTent")))
            {
                GameObject loadGO = Instantiate(Resources.Load("SquadComposition"), new Vector3(float.Parse(leveldata[i]["position"].Attributes["x"].Value), float.Parse(leveldata[i]["position"].Attributes["y"].Value), float.Parse(leveldata[i]["position"].Attributes["z"].Value)), Quaternion.identity) as GameObject;
                loadGO.transform.localEulerAngles = new Vector3(float.Parse(leveldata[i]["rotation"].Attributes["x"].Value), float.Parse(leveldata[i]["rotation"].Attributes["y"].Value), float.Parse(leveldata[i]["rotation"].Attributes["z"].Value));
                loadGO.name = leveldata[i].Name;
                if (loadGO.GetComponent<IDGetSet>())
                    loadGO.GetComponent<IDGetSet>().objectID = int.Parse(leveldata[i]["IDGetSet"].Attributes["objectID"].Value);
                dic.Add(leveldata[i], loadGO);
            }
            else
            if (leveldata[i].Name == "Camera")
            {
                Camera.main.transform.position = new Vector3(float.Parse(leveldata[i]["position"].Attributes["x"].Value), float.Parse(leveldata[i]["position"].Attributes["y"].Value), float.Parse(leveldata[i]["position"].Attributes["z"].Value));
                Camera.main.transform.localEulerAngles = new Vector3(float.Parse(leveldata[i]["rotation"].Attributes["x"].Value), float.Parse(leveldata[i]["rotation"].Attributes["y"].Value), float.Parse(leveldata[i]["rotation"].Attributes["z"].Value));
                dic.Add(leveldata[i], Camera.main.gameObject);
            } else {
                GameObject loadGO = Instantiate(Resources.Load(leveldata[i].Name), new Vector3(float.Parse(leveldata[i]["position"].Attributes["x"].Value), float.Parse(leveldata[i]["position"].Attributes["y"].Value), float.Parse(leveldata[i]["position"].Attributes["z"].Value)), Quaternion.identity) as GameObject;
                loadGO.transform.localEulerAngles = new Vector3(float.Parse(leveldata[i]["rotation"].Attributes["x"].Value), float.Parse(leveldata[i]["rotation"].Attributes["y"].Value), float.Parse(leveldata[i]["rotation"].Attributes["z"].Value));
                loadGO.name = leveldata[i].Name;
                if (loadGO.GetComponent<IDGetSet>())
                    loadGO.GetComponent<IDGetSet>().objectID = int.Parse(leveldata[i]["IDGetSet"].Attributes["objectID"].Value);
                dic.Add(leveldata[i], loadGO);
            }
        } // загрузили все GO

        foreach(XmlNode key in dic.Keys)
        {
            switch (dic[key].tag)
            {
                case "Player":
                    Camera3dPerson.hero = dic[key];
                    LoadHero(dic, key);
                    break;
                case "MainCamera":
                    LoadCamera(dic, key);
                    break;
                case "Unit":
                    LoadUnit(dic, key);
                    break;
                case "Lumberjack":
                    LoadLumberjack(dic, key);
                    break;
                case "NPC":
                    LoadNPC(dic, key);
                    break;
                case "SquadGO":
                    LoadSquadGO(dic, key);
                    break;
                case "QuestExplore":
                    LoadQuestExplore(dic, key);
                    break;
                case "FriendBuild":
                    LoadBuild(dic, key);
                    break;
                case "Enemy":
                    LoadEnemy(dic, key);
                    break;
                case "EnemyBuild":
                    LoadEnemyBuild(dic, key);
                    break;
                case "SquadTent":
                    LoadTent(dic, key);
                    break;
                case "Mine":
                    LoadMine(dic, key);
                    break;
                case "Tree":
                    LoadTree(dic, key);
                    break;
                case "Quarry":
                    LoadQuarry(dic, key);
                    break;
                case "Stone":
                    LoadStone(dic, key);
                    break;
                case "LyingItem":
                    LoadLyingItem(dic, key);
                    break;
            }
        }

        GameObject.FindGameObjectWithTag("FoWProjector").GetComponent<RealFoW>().LoadTexture();

        loading = false;
    }

    void LoadInfo(object whatToLoad, string name, Dictionary<XmlNode, GameObject> dic, XmlNode key)
    {
        foreach (FieldInfo a in whatToLoad.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
        {
            if (a.FieldType == typeof(Text) || a.FieldType == typeof(NavMeshAgent) || a.FieldType == typeof(AnimationClip) || a.FieldType == typeof(Transform) || a.FieldType == typeof(UIController) || a.FieldType == typeof(Projector))
                continue;

            if (a.FieldType == typeof(List<GameObject>))
                if (key[name][a.Name] != null && key[name][a.Name].ChildNodes.Count > 0)
                {
                    List<GameObject> listOfGO = new List<GameObject>();
                    foreach (XmlNode node in key[name][a.Name].ChildNodes)
                        listOfGO.Add(FindGameObjectInDictionary(dic, int.Parse(node.Attributes[0].Value)));
                    a.SetValue(whatToLoad, listOfGO);
                }

            if (a.FieldType == typeof(GameObject)) 
                if(key[name].Attributes[a.Name] != null && key[name].Attributes[a.Name].Value != null)
                    a.SetValue(whatToLoad, FindGameObjectInDictionary(dic, int.Parse(key[name].Attributes[a.Name].Value)));

            if (a.FieldType == typeof(List<float>))
                if (key[name][a.Name] != null && key[name][a.Name].ChildNodes.Count > 0)
                    a.SetValue(whatToLoad, LoadList<float>(key[name][a.Name]));
            if (a.FieldType == typeof(List<int>))
                if (key[name][a.Name] != null && key[name][a.Name].ChildNodes.Count > 0)
                    a.SetValue(whatToLoad, LoadList<int>(key[name][a.Name]));
            if (a.FieldType == typeof(List<string>))
                if (key[name][a.Name] != null && key[name][a.Name].ChildNodes.Count > 0)
                    a.SetValue(whatToLoad, LoadList<string>(key[name][a.Name]));


            if (a.FieldType == typeof(int))
                a.SetValue(whatToLoad, int.Parse(key[name].Attributes[a.Name].Value));
            if (a.FieldType == typeof(float))
                a.SetValue(whatToLoad, float.Parse(key[name].Attributes[a.Name].Value));
            if (a.FieldType == typeof(string))
                a.SetValue(whatToLoad, key[name].Attributes[a.Name].Value);
            if (a.FieldType == typeof(bool))
                a.SetValue(whatToLoad, bool.Parse(key[name].Attributes[a.Name].Value));

            if (a.FieldType == typeof(Quest))
                    a.SetValue(whatToLoad, LoadQuest(key[name][a.Name], dic));
            if (a.FieldType == typeof(List<Quest>)) 
                a.SetValue(whatToLoad, LoadQuestList(key[name][a.Name].ChildNodes, dic));

            if (a.FieldType == typeof(Vector3))
                a.SetValue(whatToLoad, getVector3(key[name].Attributes[a.Name].Value));

            if (a.FieldType == typeof(Item))
            {
                a.SetValue(whatToLoad, LoadOneItem(key[name][a.Name], dic));
            }
            if(a.FieldType == typeof(List<Item>))
            {
                a.SetValue(whatToLoad, LoadItemList(key[name].ChildNodes,dic));
            }
        }
    }

    Item LoadOneItem(XmlNode key, Dictionary<XmlNode, GameObject> dic)
    {
        Item it = new Item();
        foreach (FieldInfo a in it.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
        {
            if (a.FieldType == typeof(int))
                a.SetValue(it, int.Parse(key.Attributes[a.Name].Value));
            else
            if (a.FieldType == typeof(float))
                a.SetValue(it, float.Parse(key.Attributes[a.Name].Value));
            else
            if (a.FieldType == typeof(string))
                a.SetValue(it, key.Attributes[a.Name].Value);
            else
            if (a.FieldType == typeof(bool))
                a.SetValue(it, bool.Parse(key.Attributes[a.Name].Value));
        }
        return Item.CopyItem(it);
    }

    List<Item> LoadItemList(XmlNodeList items, Dictionary<XmlNode, GameObject> dic)
    {
        List<Item> listItems = new List<Item>();
        foreach (XmlNode item in items[0].ChildNodes)
            listItems.Add(LoadOneItem(item, dic));
        return listItems;
    }

    List<Quest> LoadQuestList(XmlNodeList quests, Dictionary<XmlNode, GameObject> dic)
    {
        List<Quest> listQuest = new List<Quest>();
        foreach (XmlNode node in quests)
            listQuest.Add(LoadQuest(node, dic));
        return listQuest;
    }

    Quest LoadQuest(XmlNode key, Dictionary<XmlNode, GameObject> dic)
    {
        Quest loadQuest = new Quest();
        foreach (FieldInfo a in loadQuest.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
        {
            if (a.FieldType == typeof(int))
                a.SetValue(loadQuest, int.Parse(key.Attributes[a.Name].Value));
            else
            if (a.FieldType == typeof(float))
                a.SetValue(loadQuest, float.Parse(key.Attributes[a.Name].Value));
            else
            if (a.FieldType == typeof(string))
                a.SetValue(loadQuest, key.Attributes[a.Name].Value);
            else
            if (a.FieldType == typeof(bool))
                a.SetValue(loadQuest, bool.Parse(key.Attributes[a.Name].Value));
            else
            if (a.FieldType == typeof(Quest.QuestType))
                a.SetValue(loadQuest, (Quest.QuestType)int.Parse(key.Attributes[a.Name].Value));
            else
            if (a.FieldType == typeof(Quest.RunTo))
                a.SetValue(loadQuest, (Quest.RunTo)int.Parse(key.Attributes[a.Name].Value));
            else
            if (a.FieldType == typeof(Quest.WhoWillCome))
                a.SetValue(loadQuest, (Quest.WhoWillCome)int.Parse(key.Attributes[a.Name].Value));
            else
            if (a.FieldType == typeof(GameObject))
                if(key.Attributes[a.Name] != null)
                    a.SetValue(loadQuest, FindGameObjectInDictionary(dic, int.Parse(key.Attributes[a.Name].Value)));
            else
            if (a.FieldType == typeof(List<Vector3>))
                if (key[a.Name] != null)
                    a.SetValue(loadQuest, LoadListVector3(key));
        }

        return loadQuest;
    }

    List<Tp> LoadList<Tp>(XmlNode nodes)
    {
        if (typeof(Tp) == typeof(int))
        {
            var list = new List<int>();
            foreach (XmlNode node in nodes)
                list.Add(int.Parse(node.Attributes[0].Value));
            return list as List<Tp>;
        }
        if (typeof(Tp) == typeof(float))
        {
            var list = new List<float>();
            foreach (XmlNode node in nodes)
                list.Add(float.Parse(node.Attributes[0].Value));
            return list as List<Tp>;
        }
        if (typeof(Tp) == typeof(string))
        {
            var list = new List<string>();
            foreach (XmlNode node in nodes)
                list.Add(node.Attributes[0].Value);
            return list as List<Tp>;
        }
        if (typeof(Tp) == typeof(bool))
        {
            var list = new List<bool>();
            foreach (XmlNode node in nodes)
                list.Add(bool.Parse(node.Attributes[0].Value));
            return list as List<Tp>;
        }
        return new List<Tp>();
    }

    List<Vector3> LoadListVector3(XmlNode nodes)
    {
        List<Vector3> list = new List<Vector3>();

        foreach (XmlNode node in nodes)
            list.Add(getVector3(node.Attributes[0].Value));

        return list;
    }

    Vector3 getVector3(string rString)
    {
        string[] temp = rString.Substring(1, rString.Length - 2).Split(',');
        float x = float.Parse(temp[0]);
        float y = float.Parse(temp[1]);
        float z = float.Parse(temp[2]);
        Vector3 rValue = new Vector3(x, y, z);
        return rValue;
    }

    /// <summary>
    /// Ищет в словаре GameObject по его ID
    /// </summary>
    /// <param name="dic"> словарь, в котором ищем</param>
    /// <param name="findID"> ID того, кого ищем </param>
    /// <returns></returns>
    GameObject FindGameObjectInDictionary(Dictionary<XmlNode, GameObject> dic, int findID)
    {
        foreach(GameObject loadedGO in dic.Values)
            if (loadedGO && loadedGO.GetComponent<IDGetSet>() && loadedGO.GetComponent<IDGetSet>().objectID == findID)
                return loadedGO;
        return null;
    }

    void LoadHero(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<NavMeshAgent>().destination, "destination", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Parameters>(), "Parameters", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Builder>(), "Builder", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Fighter>(), "Fighter", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<ClickToMove>(), "ClickToMove", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Gold>(), "Gold", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<QuestReceiver>(), "QuestReceiver", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<InventoryUnit>(), "InventoryUnit", dic, heroData);
    }

    void LoadCamera(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<Camera3dPerson>(), "Camera3dPerson", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Squads>(), "Squads", dic, heroData);
        for(int i = 0; i < 12; i++)
        {
            if (heroData["UnitFlags"].ChildNodes[i].ChildNodes.Count > 0)
            {
                GameObject flg = Instantiate(Resources.Load("UnitFlag"),
                    new Vector3(float.Parse(heroData["UnitFlags"].ChildNodes[i]["position"].Attributes["x"].Value), float.Parse(heroData["UnitFlags"].ChildNodes[i]["position"].Attributes["y"].Value), float.Parse(heroData["UnitFlags"].ChildNodes[i]["position"].Attributes["z"].Value)), 
                    Quaternion.identity) as GameObject;
                flg.transform.localEulerAngles = new Vector3(float.Parse(heroData["UnitFlags"].ChildNodes[i]["rotation"].Attributes["x"].Value), float.Parse(heroData["UnitFlags"].ChildNodes[i]["rotation"].Attributes["y"].Value), float.Parse(heroData["UnitFlags"].ChildNodes[i]["rotation"].Attributes["z"].Value));
                flg.name = "UnitFlag";
                Squads.visibleFlags[i] = flg;
                LoadInfo(flg.GetComponent<FriendUnitFlag>(), "FriendUnitFlag", dic, heroData["UnitFlags"].ChildNodes[i]);
                LoadInfo(flg.GetComponent<IDGetSet>(), "IDGetSet", dic, heroData["UnitFlags"].ChildNodes[i]);
            }
        }
        LoadInfo(dic[heroData].GetComponent<GameResources>(), "GameResources", dic, heroData);
    }

    void LoadUnit(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<NavMeshAgent>().destination, "destination", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Parameters>(), "Parameters", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<FriendUnit>(), "FriendUnit", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Gold>(), "Gold", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<InventoryUnit>(), "InventoryUnit", dic, heroData);
    }

    void LoadLumberjack(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<NavMeshAgent>().destination, "destination", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Parameters>(), "Parameters", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Lumberjack>(), "Lumberjack", dic, heroData);
    }

    void LoadNPC(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {        
        LoadInfo(dic[heroData].GetComponent<NavMeshAgent>().destination, "destination", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Parameters>(), "Parameters", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<QuestGiver>(), "QuestGiver", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<NPC>(), "NPC", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<DialoguesTalker>(), "DialoguesTalker", dic, heroData);
    }
    void LoadSquadGO(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<Squad>(), "Squad", dic, heroData);
        dic[heroData].GetComponent<Squad>().notNeed = false;
        if (dic[heroData].GetComponent<Squad>().isGoingToFlag)
            dic[heroData].GetComponent<Squad>().GoToFlag(dic[heroData].GetComponent<Squad>().whereToGo, dic[heroData].GetComponent<Squad>().type);
    }

    void LoadQuestExplore(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].transform.GetChild(0).GetComponent<QuestWhereToGoCheck>(), "QuestWhereToGoCheck", dic, heroData);
    }

    void LoadBuild(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<Parameters>(), "Parameters", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Construction>(), "Construction", dic, heroData);

        if (heroData["BuildBuild"] != null)
            LoadInfo(dic[heroData].GetComponent<BuildBuild>(), "BuildBuild", dic, heroData);
        else
        {
            Destroy(dic[heroData].GetComponent<BuildBuild>());
            Parameters.MakeColor(dic[heroData], Color.white);
        }
        if (dic[heroData].GetComponent<Kazarma>())
            LoadInfo(dic[heroData].GetComponent<Kazarma>(), "Kazarma", dic, heroData);
        if (dic[heroData].GetComponent<DefenceTower>())
            LoadInfo(dic[heroData].GetComponent<DefenceTower>(), "DefenceTower", dic, heroData);
        if (dic[heroData].GetComponent<Shop>())
            LoadInfo(dic[heroData].GetComponent<Shop>(), "Shop", dic, heroData);
        if (dic[heroData].GetComponent<TownHall>())
            LoadInfo(dic[heroData].GetComponent<TownHall>(), "TownHall", dic, heroData);
    }
    
    void LoadEnemyBuild(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<Parameters>(), "Parameters", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<EnemySpawn>(), "EnemySpawn", dic, heroData);
    }

    void LoadEnemy(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<Parameters>(), "Parameters", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Enemy>(), "Enemy", dic, heroData);
    }

    void LoadTent(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<SquadTent>(), "SquadTent", dic, heroData);
        LoadInfo(dic[heroData].GetComponent<Construction>(), "Construction", dic, heroData);
    }

    void LoadMine(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<Mine>(), "Mine", dic, heroData);
    }

    void LoadTree(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<Tree>(), "Tree", dic, heroData);
    }
    void LoadQuarry(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<Quarry>(), "Quarry", dic, heroData);
    }

    void LoadStone(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<Stone>(), "Stone", dic, heroData);
    }

    void LoadLyingItem(Dictionary<XmlNode, GameObject> dic, XmlNode heroData)
    {
        LoadInfo(dic[heroData].GetComponent<DroppedItem>(), "DroppedItem", dic, heroData);
    }
}
