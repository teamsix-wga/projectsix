﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

public class FadeText : MonoBehaviour {
    private static GameObject ui;
    public static Dictionary<string, Color> text;
    public bool textOnScreen = false;

    void Awake()
    {
        ui = GameObject.Find("InfoUI").transform.FindChild("Text").gameObject;
        text = new Dictionary<string, Color>();
    }

    /// <summary>
    /// Функция задания текста на экране
    /// </summary>
    /// <param name="txt"> Текст </param>
    /// <param name="col"> Цвет текста </param>
    /// <param name="speed"> Сколько секунд текст будет на экране</param>
    /// <returns></returns>
    public static IEnumerator FadeTxt(string txt, Color col, float speed)
    {
        float fadeSpeed = speed;

        text.Add(txt, col);
        ui.GetComponent<FadeText>().textOnScreen = true;

        while (fadeSpeed > 0)
        {
            if (fadeSpeed > 1)
                fadeSpeed -= Time.fixedDeltaTime;
            if (fadeSpeed <= 1 && fadeSpeed > 0)
            {
                fadeSpeed -= Time.fixedDeltaTime;
                Color cl = text[txt];
                text[txt] = new Color(cl.r, cl.g, cl.b, fadeSpeed);
            }
            yield return new WaitForEndOfFrame();
        }
        text.Remove(txt);
        if (text.Count == 0)
        {
            ui.GetComponent<FadeText>().textOnScreen = false;
            ui.GetComponent<Text>().text = "";
        }
        yield return null;
    }

    void Update()
    {
        if (textOnScreen)
        {
            ui.GetComponent<Text>().text = "";
            foreach (string txt in text.Keys)
            {
                Color32 c = text[txt];
                string r = Convert.ToString(Mathf.FloorToInt(c.r), 16);
                if (r.Length == 1)
                    r = "0" + r;
                string g = Convert.ToString(Mathf.FloorToInt(c.g), 16);
                if (g.Length == 1)
                    g = "0" + g;
                string b = Convert.ToString(Mathf.FloorToInt(c.b), 16);
                if (b.Length == 1)
                    b = "0" + b;
                string a = Convert.ToString(Mathf.FloorToInt(c.a), 16);
                if (a.Length == 1)
                    a = "0" + a;
                ui.GetComponent<Text>().text += "<color=#" + r + g + b + a + ">" + txt + "</color>\n";
            }
        }
    }
}
