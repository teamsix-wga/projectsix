﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class ShopItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    public Item item;
    public int amount;
    public int slot;
    public Shop inventory;

    private InventoryTooltip tooltip;
    private Vector2 offset;

    void Start()
    {
        tooltip = GameObject.Find("Inventory").GetComponent<InventoryTooltip>();
    }

    void BuyItem(GameObject whoBuy)
    {
        Transform ui = GameObject.Find("ShopUI").transform.FindChild("Inventory Panel").transform.FindChild("Slot Panel");
        int slt = 0;
        for (int i = 0; i < ui.childCount; i++)
        {
            if (ui.GetChild(i).childCount == 0)
            {
                slt = i;
                break;
            }
        }

        whoBuy.GetComponent<Gold>().currentGold -= item.buyCost;

        Item itemToAdd = Item.CopyItem(item); 
        itemToAdd.slotID = slt;

        whoBuy.GetComponent<InventoryUnit>().items.Add(itemToAdd);
        whoBuy.GetComponent<InventoryUnit>().items[whoBuy.GetComponent<InventoryUnit>().items.Count - 1].slotID = slt;

        inventory.items[slt] = itemToAdd;
        if(inventory.uniqueAssortiment.Exists(delegate (Item it) { return it == item;})){
            tooltip.Deactivate();
            inventory.uniqueAssortiment.Remove(item);
        }
        inventory.UpdateAssortiment();
    }

    void SellItem(GameObject whoSell)
    {
        whoSell.GetComponent<Gold>().currentGold += item.sellCost; 

        if(item.slotID > Camera3dPerson.hero.GetComponent<InventoryUnit>().slotsCount-1)
            ShopSlots.UpdateParameters(whoSell, item, -1);
        whoSell.GetComponent<InventoryUnit>().items.Remove(whoSell.GetComponent<InventoryUnit>().items.Find(delegate (Item it) { return it.slotID == item.slotID; }));
        tooltip.Deactivate();
        inventory.items[item.slotID] = new Item();
        if (item.quality == Item.ItemQuality.Rare || item.quality == Item.ItemQuality.Good){
            inventory.uniqueAssortiment.Add(item);
        }
        inventory.UpdateAssortiment();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (item != null && slot < Camera3dPerson.hero.GetComponent<InventoryUnit>().slotsCount+5)
        {
            offset = eventData.position - new Vector2(this.transform.position.x, this.transform.position.y);
            this.transform.SetParent(this.transform.parent.parent);
            this.transform.position = eventData.position - offset;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (item != null && slot < Camera3dPerson.hero.GetComponent<InventoryUnit>().slotsCount+5)
        {
            this.transform.position = eventData.position - offset;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (item != null && slot < Camera3dPerson.hero.GetComponent<InventoryUnit>().slotsCount + 5)
        {
            this.transform.SetParent(inventory.slots[slot].transform);
            this.transform.position = inventory.slots[slot].transform.position;
            GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (Shop.shopOpened)
        {
            tooltip.transform.parent = GameObject.Find("ShopUI").transform;
        }
        tooltip.Activate(item);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tooltip.Deactivate();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (Shop.shopOpened && eventData.button == PointerEventData.InputButton.Right)
        {
            GameObject hero = GameObject.FindGameObjectWithTag("Player");

            if (item.slotID >= Camera3dPerson.hero.GetComponent<InventoryUnit>().slotsCount + 5 && hero.GetComponent<Gold>().currentGold >= item.buyCost)
                BuyItem(hero);
            else
            if (item.slotID < Camera3dPerson.hero.GetComponent<InventoryUnit>().slotsCount + 5)
                SellItem(hero);
        }
    }
}
