﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Globalization;
using UnityEngine.EventSystems;

public class Kazarma : MonoBehaviour {
	// куда идти юниту после создания
	public Vector3 UnitFlag;
    // ставим ли флаг
    public bool SettingFlag;
    // gameobject созданного юнита
    GameObject unit;
    public static bool flagSetted;
    float destroyedTime;
    public Vector3 spawnPoint;
    GameObject visibleFlag;
    public int warriorCostInGold = 200;
    public float warriorTimeToCreate = 10;
    GameObject hero;

    private GameObject campfireGO, warriorGO;

    void Awake() {
        warriorGO = Resources.Load("Warrior") as GameObject;
        campfireGO = Resources.Load("Campfire") as GameObject;
        hero = GameObject.FindGameObjectWithTag("Player");
        flagSetted = SettingFlag = false;
        UnitFlag = transform.position;
        UnitFlag.y = 0f;
    }

    void Update() {
        if(GetComponent<Parameters>().died && destroyedTime <= Time.time)
        {
            Destroy(gameObject);
            if (GetComponent<Construction>().buildEffect)
                Destroy(GetComponent<Construction>().buildEffect);
            Destroy(this);
        }

        if (!GetComponent<Parameters>().died && GetComponent<Construction>().builded)
        {
            // если нажата ЛКМ и мы ставим флаг
            if (Input.GetMouseButtonDown(0) && SettingFlag)
            {
                int mask = ~(1 << 8);
                mask = ~(mask << 2);
                RaycastHit hit = Builder.CastFromCursorMask(mask);
                NavMeshPath path = new NavMeshPath();
                NavMesh.CalculatePath(spawnPoint, hit.point, NavMesh.AllAreas, path);
                if (path.status == NavMeshPathStatus.PathComplete)
                {
                    // сбрасываем флаг
                    SettingFlag = false;
                    hero.GetComponent<ClickToMove>().settingFlag = false;
                    UnitFlag = hit.point;
                    flagSetted = true;
                    Debug.Log("Flag set at" + UnitFlag);
                    visibleFlag.transform.position = UnitFlag;
                }
            }
            if (Input.GetMouseButtonUp(0) && flagSetted)
            {
                flagSetted = false;
            }
        }
    }

    public void ObjectSelected()
    {
        if (!visibleFlag && GetComponent<Construction>().builded)
        {
            visibleFlag = Instantiate(campfireGO, UnitFlag, Quaternion.identity) as GameObject;
            visibleFlag.transform.parent = null;
            visibleFlag.transform.localEulerAngles = new Vector3(-90f, -180f, 0f);
        }
    }
    public void ObjectUnselected()
    {
        Destroy(visibleFlag);
    }

    public void MakeUnit(UnitType type)
    {
        if (type == UnitType.warrior)
        {
            if (hero.GetComponent<Gold>().currentGold >= warriorCostInGold)
            {
                GameObject lumber =
                    Camera.main.GetComponent<Camera3dPerson>()
                        .lumberjacks.Find(lumb => (!lumb.GetComponent<Lumberjack>().working));

                if (lumber)
                {
                    lumber.GetComponent<Lumberjack>().state = LumberState.Warrior;
                    lumber.GetComponent<Lumberjack>().GoToWork(spawnPoint, gameObject);

                    var unitButton = Instantiate(Resources.Load("UI/Kazarma_Warrior_Queue")) as GameObject;
                    var butScale = unitButton.transform.localScale;

                    unitButton.transform.parent =
                        GetComponent<Construction>().recruitmentMenu.transform.FindChild("ProductionQueue");
                    unitButton.transform.localScale = butScale;
                    unitButton.transform.localPosition = Vector3.zero;
                    unitButton.transform.localEulerAngles = Vector3.zero;
                    unitButton.GetComponent<RecruitmentQueueButton>().savedWorker = lumber;
                    unitButton.transform.FindChild("ProgressBar/CountDown").GetComponent<TextMesh>().text =
                        warriorTimeToCreate.ToString();
                    lumber.GetComponent<Lumberjack>().beWarriorQueueButton = unitButton;
                    unitButton.GetComponent<SpriteRenderer>().color = Color.black;
                }
                else
                    StartCoroutine(FadeText.FadeTxt("Отсутствуют незанятые рабочие!", Color.red, 3));
            }
            else
                StartCoroutine(FadeText.FadeTxt("Нужно больше золота!", Color.red, 3));
        }
    }
    
    public void MakeWarrior(GameObject worker, GameObject unitButton){
        if (hero.GetComponent<Gold>().currentGold >= warriorCostInGold)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<Gold>().currentGold -= warriorCostInGold;
            unitButton.GetComponent<RecruitmentQueueButton>().returnMoneyIfCancel = warriorCostInGold;
            unitButton.GetComponent<RecruitmentQueueButton>().unitCoroutine = StartCoroutine(waitMakeWarrior(unitButton));

            Camera.main.GetComponent<Camera3dPerson>().lumberjacks.Remove(worker);
            worker.SetActive(false);
        }
        else
        {
            StartCoroutine(FadeText.FadeTxt("Нужно больше золота!", Color.red, 3));
            Destroy(unitButton);
            worker.GetComponent<Lumberjack>().GoBack();
        }
    }

    IEnumerator waitMakeWarrior(GameObject button)
    {
        float time = warriorTimeToCreate;
        float oneTimeMeasure = warriorTimeToCreate/100;
        float oneColorMeasure = 1f/100;
        while (time > 0)
        {
            button.transform.FindChild("ProgressBar/CountDown").GetComponent<TextMesh>().text = time.ToString("#");
            button.GetComponent<SpriteRenderer>().color = Color.Lerp(button.GetComponent<SpriteRenderer>().color, Color.white, oneColorMeasure);
            time -= oneTimeMeasure;
            yield return new WaitForSeconds(oneTimeMeasure);
        }
        
        unit = Instantiate(warriorGO, spawnPoint, Quaternion.identity) as GameObject;
        unit.GetComponent<NavMeshAgent>().destination = UnitFlag;
        unit.GetComponent<FriendUnit>().cameFrom = UnitFlag;
        unit.GetComponent<FriendUnit>().type = UnitType.warrior;
        unit.GetComponent<FriendUnit>().SetParentBuilding(gameObject);
        hero.SendMessage("CheckCreatedUnit", unit.name);
        Destroy(button);
        yield return null;
    }

    public void SetFlag() {
        SettingFlag = true;
        hero.GetComponent<ClickToMove>().settingFlag = true;
    }

    public void GetHit(AttackStructure atStr) {
        float damage = atStr.damage;
        if (GetComponent<Parameters>().defence <= damage)
            GetComponent<Parameters>().health -= damage - GetComponent<Parameters>().defence;

        GetComponent<Parameters>().health = GetComponent<Parameters>().health;
        if (!GetComponent<Parameters>().died && GetComponent<Parameters>().health <= 0)
        {
            hero.GetComponent<Builder>().listBuild.Remove(gameObject);
            destroyedTime = Time.time;
            GetComponent<Parameters>().died = true;
            SettingFlag = false;
            if (GetComponent<Parameters>().selected)
                Destroy(visibleFlag);
            Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Remove(gameObject);
        }
    }
}
