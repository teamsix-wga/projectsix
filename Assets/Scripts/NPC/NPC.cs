﻿using UnityEngine;
using System.Collections;

public class NPC : MonoBehaviour {
    GameObject who; // за кем бежим
    public bool isRunning = false; // надо ли бежать?
    public bool canWalk = true; // можем ли ходить
    public float distance = 10; // расстояние, на котором держимся от цели
    public float freeWalkDistance = 15; // расстояние, на которое нпс может отходить от своей точки
    public bool useAnimations = true;
    public AnimationClip idle, walk;
    NavMeshAgent agent;
    Vector3 startPosition; // начальная точка нпс
    

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        startPosition = transform.position;
        StartCoroutine("FreeWalk");
    }

	void Update () {
        if(GetComponent<Parameters>() && GetComponent<Parameters>().died)
            return;

	    if (useAnimations)
	    {
            if (Vector3.Distance(agent.destination, transform.position) > agent.stoppingDistance)
                if(GetComponent<Animation>())
                    GetComponent<Animation>().CrossFade(walk.name);
                else
                    GetComponent<Animator>().Play(walk.name);
            else
            if (GetComponent<Animation>())
                GetComponent<Animation>().CrossFade(idle.name);
            else
                GetComponent<Animator>().Play(idle.name);
        }

        if (isRunning && ((GetComponent<FriendUnit>() && !GetComponent<FriendUnit>().Opponent)||(GetComponent<Enemy>() && !GetComponent<Enemy>().Opponent)||(!GetComponent<FriendUnit>() && !GetComponent<Enemy>())))
	    {
            if(Vector3.Distance(who.transform.position, transform.position) > distance)
                agent.destination = who.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
            else
                agent.destination = transform.position;
        }
	}

    public void SetTarget(GameObject who, bool run)
    {
        this.who = who;
        isRunning = run;

        if(who && who.GetComponent<NavMeshAgent>() && GetComponent<Parameters>())
            GetComponent<NavMeshAgent>().speed = run ? who.GetComponent<NavMeshAgent>().speed : GetComponent<Parameters>().speed;
        else 
        if (run && GetComponent<Parameters>())
            GetComponent<NavMeshAgent>().speed = GetComponent<Parameters>().speed;
    }

    public void UpdateStartPosition(Vector3 pos)
    {
        startPosition = pos;
    }

    IEnumerator FreeWalk()
    {
        while (true) // если не за кем не бежим
        {
            if (!canWalk || isRunning)
                break;

            yield return new WaitForSeconds(Random.Range(5, 15));

            if (!canWalk || isRunning)
                break;

            if (Act1Startup.animPlaying)
                continue;

            Vector3 randomDirection = (Random.insideUnitSphere * freeWalkDistance) + startPosition;
            NavMeshHit hit;
            NavMesh.SamplePosition(randomDirection, out hit, freeWalkDistance, 1);
            while (Vector3.Distance(hit.position, startPosition) > freeWalkDistance)
            {
                randomDirection = (Random.insideUnitSphere * freeWalkDistance) + startPosition;
                NavMesh.SamplePosition(randomDirection, out hit, freeWalkDistance, 1);
            }
            agent.SetDestination(hit.position);
            while (Vector3.Distance(transform.position, hit.position) > agent.stoppingDistance)
                yield return new WaitForSeconds(1);
        }
    }
}
