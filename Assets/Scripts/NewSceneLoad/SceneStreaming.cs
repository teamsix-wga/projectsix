﻿using UnityEngine;
using System.Collections;

public class SceneStreaming : MonoBehaviour
{

    public string sceneName = "";
    public bool load = true;
    bool loaded = false;

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player") && load && !loaded)
            StartCoroutine(LoadScene());
        if (col.CompareTag("Player") && !load && loaded)
            UnloadScene();

    }

    IEnumerator LoadScene()
    {
        AsyncOperation async  = Application.LoadLevelAdditiveAsync(sceneName);
        while (!async.isDone)
        {
            yield return new WaitForEndOfFrame();
        }
        Camera.main.transform.position = GameObject.Find("CameraStartPoint").transform.position;
        GameObject hero = GameObject.FindGameObjectWithTag("Player");
        hero.transform.position = GameObject.Find("PlayerStartPoint").transform.position;
        hero.GetComponent<Parameters>().agent.destination = GameObject.Find("PlayerStartPoint").transform.position;
        hero.GetComponent<ClickToMove>().waitPosition = GameObject.Find("PlayerStartPoint").transform.position;
        Camera.main.transform.LookAt(hero.transform);
        loaded = true;
        yield return async;
    }


    void UnloadScene()
    {
        loaded = false;
        Application.UnloadLevel(sceneName);
    }
}
