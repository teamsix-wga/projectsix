﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SkillTooltip : MonoBehaviour {
    private Skill skill;
    private string data;
    private GameObject tooltip;
    bool act = false;

    void Start()
    {
        tooltip = GameObject.Find("Tooltip").transform.FindChild("TooltipBar").gameObject;
        tooltip.SetActive(false);
    }

    void Update()
    {
        if (act)
        {
            tooltip.transform.position = Input.mousePosition + new Vector3(tooltip.GetComponent<RectTransform>().sizeDelta.x/2, tooltip.GetComponent<RectTransform>().sizeDelta.y / 2);
            float byY = tooltip.GetComponent<RectTransform>().position.y - tooltip.GetComponent<RectTransform>().rect.height;
            if (byY < 0)
            {
                tooltip.transform.position = new Vector3(tooltip.transform.position.x, tooltip.transform.position.y - byY, tooltip.transform.position.z);
            }
            float byX = tooltip.GetComponent<RectTransform>().position.x + tooltip.GetComponent<RectTransform>().rect.width;
            if (byX > Screen.width)
            {
                tooltip.transform.position = new Vector3(Screen.width - tooltip.GetComponent<RectTransform>().rect.width, tooltip.transform.position.y, tooltip.transform.position.z);
            }
        }
    }

    public void Activate(Skill skill)
    {
        if (tooltip.activeInHierarchy)
            return;
        this.skill = skill;
        ConstructDataString();
        tooltip.SetActive(true);
        act = true;
    }

    public void Deactivate()
    {
        tooltip.SetActive(false);
        act = false;
    }

    public void ConstructDataString()
    {
        data = "<color=#FFA000><b>" + skill.name + "</b></color>\n\n";


        data += skill.description+"\n\n";
        if (skill.damage != 0)
            data += "Урон: <b>"+skill.damage.ToString() +"</b>\n";
        if (skill.distance != 0)
            data += "Дистанция до цели: <b>" + skill.distance.ToString() + "</b>\n";
        if (skill.range!= 0)
            data += "Радиус умения: <b>" + skill.range.ToString() + "</b>\n";
        if (skill.cooldown != 0)
            data += "Откат: <b>" + skill.cooldown.ToString() + "</b>\n";
        if (skill.lifetime!= 0)
            data += "Длительность действия: <b>" + skill.lifetime.ToString() + "</b>\n";
        if (skill.healingInPercent != 0)
            data += "Восстанавливает здоровья (в процентах): <b>" + skill.healingInPercent.ToString() + "</b>\n";
        data += "\n";

        tooltip.transform.GetChild(0).GetComponent<Text>().text = data;
    }
}
