﻿using UnityEngine;
using System.Collections;
using FMOD.Studio;

public class whoUseSpell
{
    public GameObject who;
    public Skill skill;
    public GameObject Opponent;
    public Vector3 where;
}

public class SkillsFunctions : MonoBehaviour
{
    public void HeroSkill_Shield(whoUseSpell spellStruct)
    {
        var shield = Instantiate(Resources.Load("Spell1")) as GameObject;
        shield.transform.position = spellStruct.who.transform.position + new Vector3(0, 0.2f, 0);
        shield.transform.parent = spellStruct.who.transform;
        shield.transform.localPosition = Vector3.zero + new Vector3(0, 0.2f, 0);
        shield.transform.localScale = Vector3.one * spellStruct.skill.range;

        float mult = 100 / spellStruct.skill.healingInPercent;
        float totalHPRevive = spellStruct.who.GetComponent<Parameters>().maxHealth / mult;
        StartCoroutine(HealHim(spellStruct.who, totalHPRevive/spellStruct.skill.lifetime, spellStruct.skill.lifetime, true));
        StartCoroutine(DestroyAfterSec(shield, spellStruct.skill.lifetime));
    }

    public void HeroSkill_AoESword(whoUseSpell spellStruct)
    {
        var aoeSword = Instantiate(Resources.Load("AoESwordSpell"), spellStruct.where + new Vector3(0,1,0), Quaternion.identity) as GameObject;
        // запрещаем двигаться 
        ClickToMove.attacking = true;
        StartCoroutine(HeroSkill_AoESword_Coroutine(spellStruct, aoeSword));
        StartCoroutine(DestroyAfterSec(aoeSword, spellStruct.skill.lifetime));
    }

    IEnumerator HeroSkill_AoESword_Coroutine(whoUseSpell spellStruct, GameObject sword)
    {
        sword.GetComponent<FMODUnity.StudioEventEmitter>().Play();
        sword.GetComponent<FMODUnity.StudioEventEmitter>().SetParameter("Vol", 1);

        yield return new WaitForSeconds(4);

        Collider[] hitColliders = Physics.OverlapSphere(spellStruct.where, spellStruct.skill.range / 2);
        for (int i = 0; i < hitColliders.Length; i++)
            if (hitColliders[i].CompareTag("Enemy") || hitColliders[i].CompareTag("EnemyBuild"))
                hitColliders[i].SendMessage("GetHit", new AttackStructure()
                {
                    damage = spellStruct.skill.damage + spellStruct.who.GetComponent<Parameters>().damage,
                    whoHit = spellStruct.who
                });

        for (int j = 0; j < 9; j++)
        {
            if(sword)
                sword.GetComponent<FMODUnity.StudioEventEmitter>().SetParameter("Vol", 1-j/10);
            yield return new WaitForSeconds(0.1f);
        }
    }

    public void SimpleHit(whoUseSpell spellStruct)
    {
        spellStruct.Opponent.SendMessage("GetHit", new AttackStructure { damage = spellStruct.skill.damage + spellStruct.who.GetComponent<Parameters>().damage, whoHit = spellStruct.who });
    }

    public void HeroSkill_Arrow(whoUseSpell spellStruct)
    {
        GameObject grenade = Instantiate(Resources.Load("Arrow"), new Vector3(spellStruct.who.transform.position.x, spellStruct.who.GetComponent<Collider>().bounds.center.y, spellStruct.who.transform.position.z), Quaternion.identity) as GameObject;
        grenade.transform.parent = null;
        grenade.GetComponent<TowerGrenade>().SetOpponent(spellStruct.Opponent, spellStruct.skill.damage + spellStruct.who.GetComponent<Parameters>().damage, spellStruct.who);
    }

    IEnumerator HealHim(GameObject who, float HPS, float time, bool invulnerability)
    {
        var endTime = Time.time + time;
        while (Time.time < endTime)
        {
            if (HPS + who.GetComponent<Parameters>().health <= who.GetComponent<Parameters>().maxHealth)
                who.GetComponent<Parameters>().health += HPS;
            else
                who.GetComponent<Parameters>().health = who.GetComponent<Parameters>().maxHealth;

            if (who.GetComponent<Parameters>().selected)
            {
            }
            if(invulnerability)
                who.GetComponent<Parameters>().invulnerability = true;
            yield return new WaitForSeconds(1);
        }
        if(invulnerability)
            who.GetComponent<Parameters>().invulnerability = false;
    }

    IEnumerator DestroyAfterSec(GameObject what, float when)
    {
        yield return new WaitForSeconds(when);
        Destroy(what);
    }

    void Friend_Mage_FrozenShield(whoUseSpell spellStruct)
    {
        var shield = Instantiate(Resources.Load("Spells/FrozenShield")) as GameObject;
        shield.transform.parent = spellStruct.who.transform;
        shield.transform.localPosition = new Vector3(0, 3.5f, 0);

        spellStruct.who.GetComponent<Parameters>().defence += 60;

        StartCoroutine(Cor_Friend_Mage_FrozenShield(shield, spellStruct.skill.lifetime, spellStruct));
    }

    IEnumerator Cor_Friend_Mage_FrozenShield(GameObject shield, float whenDestroy, whoUseSpell spellStruct)
    {
        yield return new WaitForSeconds(whenDestroy);
        Destroy(shield);
        spellStruct.who.GetComponent<Parameters>().defence -= 60;
    }

    void Enemy_Goblin_Vertyshka(whoUseSpell spellStruct)
    {
        Collider[] hitColliders = Physics.OverlapSphere(spellStruct.who.transform.position, spellStruct.skill.range / 2);
        for (int i = 0; i < hitColliders.Length; i++)
            if (hitColliders[i].CompareTag("Player") || hitColliders[i].CompareTag("Unit") || hitColliders[i].CompareTag("FriendBuild") || hitColliders[i].CompareTag("Lumberjack"))
                hitColliders[i].SendMessage("GetHit", new AttackStructure()
                {
                    damage = spellStruct.skill.damage + spellStruct.who.GetComponent<Parameters>().damage,
                    whoHit = spellStruct.who
                });
    }

    void Enemy_Demonitsa_Warp(whoUseSpell spellStruct)
    {
        Collider[] hitColliders = Physics.OverlapSphere(spellStruct.who.transform.position, spellStruct.skill.range);
        foreach (Collider col in hitColliders)
        {
            if (col.CompareTag("Unit") || col.CompareTag("FriendBuild") || col.CompareTag("Lumberjack"))
                spellStruct.who.GetComponent<NavMeshAgent>().Warp(Act1Startup.FindNearestFreePos(spellStruct.who, col.gameObject.transform.position, 7));
            break;
        }
    }

    void Enemy_Demonitsa_ImbaHit(whoUseSpell spellStruct)
    {
        spellStruct.Opponent.SendMessage("GetHit", new AttackStructure()
        {
            damage = spellStruct.skill.damage + spellStruct.who.GetComponent<Parameters>().damage,
            whoHit = spellStruct.who
        });
    }

    void Enemy_Gulia_Topot(whoUseSpell spellStruct)
    {
        spellStruct.Opponent.SendMessage("GetHit", new AttackStructure()
        {
            damage = spellStruct.skill.damage + spellStruct.who.GetComponent<Parameters>().damage,
            whoHit = spellStruct.who
        });
    }

    void Friend_Angel_Heal(whoUseSpell spellStruct)
    {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Parameters>().health =
            GameObject.FindGameObjectWithTag("Player").GetComponent<Parameters>().maxHealth;
    }

    void Friend_Angel_Warp(whoUseSpell spellStruct)
    {
        Collider[] hitColliders = Physics.OverlapSphere(spellStruct.who.transform.position, spellStruct.skill.range);
        foreach (Collider col in hitColliders)
        {
            if (col.CompareTag("Enemy") || col.CompareTag("EnemyBuild"))
                spellStruct.who.GetComponent<NavMeshAgent>().Warp(Act1Startup.FindNearestFreePos(spellStruct.who, col.gameObject.transform.position, 7));
            break;
        }
    }

    void Enemy_Koldyn_Shield(whoUseSpell spellStruct)
    {
        var shield = Instantiate(Resources.Load("Spells/FrozenShield")) as GameObject;
        shield.transform.parent = spellStruct.who.transform;
        shield.transform.localPosition = new Vector3(0, 3.5f, 0);

        spellStruct.who.GetComponent<Parameters>().defence += 20;

        StartCoroutine(Cor_Friend_Mage_FrozenShield(shield, spellStruct.skill.lifetime, spellStruct));
    }
}