﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;

public class SkillsLoader : MonoBehaviour {
   
    public List<Skill> LoadSkills(string name)
    {
        List<Skill> skills = new List<Skill>();

        string filepath = Application.dataPath + "\\StreamingAssets\\Skills\\"+name+".xml";

        XmlDocument xmlDoc = new XmlDocument();
        XmlNode rootNode = null;
        // загружаем файл или выходим
        if (File.Exists(filepath))
        {
            xmlDoc.Load(filepath);
            rootNode = xmlDoc.DocumentElement;
        }
        else
            return skills;

        // получили все квесты из XML файла
        XmlNodeList skillsData = rootNode.ChildNodes;

        for(int i = 0; i < skillsData.Count; i++)
        {
            string skillName = "";
            string description = "";
            float damage = 0;
            string clipName = "";
            float distance = 0;
            float range = 0;
            float cooldown = 0;
            float lifetime = 0;
            float healingInPercent = 0;
            float castTime = 0;
            string func = "";
            bool needOpponent = true;
            int priority = -1;
            Sprite icon = new Sprite();
            Skill.SkillType type = Skill.SkillType.AoE;
            XmlAttributeCollection skillInfo = skillsData[i].Attributes;
            for (int j = 0; j < skillInfo.Count; j++)
            {
                switch (skillInfo[j].Name)
                {
                    case "name":
                        skillName = skillInfo[j].Value;
                        break;
                    case "description":
                        description = skillInfo[j].Value;
                        break;
                    case "damage":
                        damage = float.Parse(skillInfo[j].Value);
                        break;
                    case "clipName":
                        clipName = skillInfo[j].Value;
                        break;
                    case "distance":
                        distance = float.Parse(skillInfo[j].Value);
                        break;
                    case "range":
                        range = float.Parse(skillInfo[j].Value);
                        break;
                    case "cooldown":
                        cooldown = float.Parse(skillInfo[j].Value);
                        break;
                    case "lifetime":
                        lifetime = float.Parse(skillInfo[j].Value);
                        break;
                    case "healingInPercent":
                        healingInPercent = float.Parse(skillInfo[j].Value);
                        break;
                    case "type":
                        type = (Skill.SkillType)int.Parse(skillInfo[j].Value);
                        break;
                    case "icon":
                        icon = Resources.Load<Sprite>("SkillsIcons/" + skillInfo[j].Value);
                        break;
                    case "castTime":
                        castTime = float.Parse(skillInfo[j].Value);
                        break;
                    case "skillFunc":
                        func = skillInfo[j].Value;
                        break;
                    case "needOpponent":
                        needOpponent = bool.Parse(skillInfo[j].Value);
                        break;
                    case "priority":
                        priority = int.Parse(skillInfo[j].Value);
                        break;
                }
            }
            skills.Add(new Skill(skillName, description, damage, clipName, distance,range,cooldown,lifetime,healingInPercent, type, icon, castTime, func, needOpponent, priority));
        }

        return skills;
    }
}


[System.Serializable]
public class Skill
{
    public enum SkillType
    {
        Damage = 0,
        Shield = 1,
        AoE = 2
    }

    public string name; // название
    public string description; // описание
    public float damage; // урон
    public string clipName; // анимация
    public float distance; // дистанция для использования
    public float range; // радиус скилла
    public float cooldown; // откат
    public float lifetime; // длительность действия
    public float healingInPercent; // сколько хилит (в процентах)
    public SkillType type; // тип скилла
    public Sprite icon; // иконка
    public float castLength; // длительность каста
    public string skillFunc; // название функции скилла
    public bool needOpponent; // нужен ли выделенный враг
    public int priority; // приоритет использования (для мобов)


    public Skill()
    {
        name = "";
        description = "";
        damage = 0;
        clipName = "";
        distance = 0;
        range = 0;
        cooldown = 0;
        lifetime = 0;
        healingInPercent = 0;
        type = SkillType.Damage;
        icon = new Sprite();
        castLength = 0;
        skillFunc = "";
        needOpponent = true;
        priority = -1;
    }

    public Skill(string name, string description, float damage, string clipName, float distance, float range, float cooldown, float lifetime, float healingInPercent, SkillType type, Sprite icon, float cast, string func, bool needOpponent, int prior)
    {
        this.name = name;
        this.description = description;
        this.damage = damage;
        this.clipName = clipName;
        this.distance = distance;
        this.range = range;
        this.cooldown = cooldown;
        this.lifetime = lifetime;
        this.healingInPercent = healingInPercent;
        this.type = type;
        this.icon = icon;
        castLength = cast;
        skillFunc = func;
        this.needOpponent = needOpponent;
        priority = prior;
    }

    public Skill(Skill skill)
    {
        name = skill.name;
        description = skill.description;
        damage = skill.damage;
        clipName = skill.clipName;
        distance = skill.distance;
        range = skill.range;
        cooldown = skill.cooldown;
        lifetime = skill.lifetime;
        healingInPercent = skill.healingInPercent;
        type = skill.type;
        icon = skill.icon;
        castLength = skill.castLength;
        skillFunc = skill.skillFunc;
        needOpponent = skill.needOpponent;
        priority = skill.priority;
    }
}
