﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class SkillButton : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler
{
    private Skill skill;
    private KeyCode skillKeyCode;
    private GameObject hero;
    private SkillTooltip tooltip;

    void Start()
    {
        hero = GameObject.FindGameObjectWithTag("Player");
        tooltip = GameObject.FindGameObjectWithTag("SpellPanel").GetComponent<SkillTooltip>();
    }

    void Update()
    {
        if (Input.GetKeyDown(skillKeyCode) && hero.GetComponent<Parameters>().selected && !GameObject.Find("EscMenu").GetComponentsInChildren<Button>()[0].GetComponent<EscMenu>().menuActive &&
            !CommandLine.enableConsole)
        {
            Camera3dPerson.hero.GetComponent<Fighter>().SetUsedSkill(skill);
        }
    }

    public void SetSkill(Skill skill, int numButton)
    {
        this.skill = skill;

        switch (numButton)
        {
            case 0:
                skillKeyCode = KeyCode.Alpha0;
                break;
            case 1:
                skillKeyCode = KeyCode.Alpha1;
                break;
            case 2:
                skillKeyCode = KeyCode.Alpha2;
                break;
            case 3:
                skillKeyCode = KeyCode.Alpha3;
                break;
            case 4:
                skillKeyCode = KeyCode.Alpha4;
                break;
            case 5:
                skillKeyCode = KeyCode.Alpha5;
                break;
            case 6:
                skillKeyCode = KeyCode.Alpha6;
                break;
            case 7:
                skillKeyCode = KeyCode.Alpha7;
                break;
            case 8:
                skillKeyCode = KeyCode.Alpha8;
                break;
            case 9:
                skillKeyCode = KeyCode.Alpha9;
                break;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        tooltip.Activate(skill);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if(eventData.button == PointerEventData.InputButton.Left)
            Camera3dPerson.hero.GetComponent<Fighter>().SetUsedSkill(skill);
        if (eventData.button == PointerEventData.InputButton.Right)
            Camera3dPerson.hero.GetComponent<Fighter>().SetAutoattackSkill(skill);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        tooltip.Deactivate();
    }
}
