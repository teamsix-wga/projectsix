﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using ProgressBar;

public class BuildBuild : MonoBehaviour {
    float timer;
    GameObject lumbArea, buildPopUp, lumbEffect;
    float maxArea;
    public List<GameObject> workingBuilders;
    bool closedPopUp;
    TextMesh countLumbers;
    GameObject percentOfReady;
    float floatPercentOfReady = 0;
    GameObject buildPopupGO;

	void Awake () {
        buildPopupGO = Resources.Load("BuildPopUp") as GameObject;
        closedPopUp = false;
        workingBuilders = new List<GameObject>();
        maxArea = 100f;
	}
	
	void Update () {
        if (GetComponent<Construction>().placed && !closedPopUp && Vector3.Distance(Camera.main.transform.position, transform.position) <= maxArea+30f)
        {
            if (!buildPopUp)
            {
                buildPopUp = Instantiate(buildPopupGO, new Vector3(GetComponent<BoxCollider>().bounds.center.x, GetComponent<BoxCollider>().bounds.max.y, GetComponent<BoxCollider>().bounds.center.z), Quaternion.identity) as GameObject;
                buildPopUp.transform.localScale = new Vector3(20, 20, 5);
                Button[] btns = buildPopUp.GetComponentsInChildren<Button>();
                btns[0].onClick.RemoveAllListeners();
                btns[0].onClick.AddListener(AddLumberjacks);
                btns[1].onClick.RemoveAllListeners();
                btns[1].onClick.AddListener(DeleteLumberjack);
                btns[2].onClick.RemoveAllListeners();
                btns[2].onClick.AddListener(ClosePopUp);
                countLumbers = buildPopUp.transform.GetChild(0).FindChild("Working/Text").GetComponent<TextMesh>();
                countLumbers.text = "Строителей нет";
                percentOfReady = buildPopUp.transform.GetChild(0).FindChild("Progress/ProgressBar").gameObject;
                percentOfReady.GetComponent<ProgressBarBehaviour>().SetFillerSizeAsPercentage(Mathf.Round(floatPercentOfReady * 100));
            }
            if(!buildPopUp.activeInHierarchy)
                buildPopUp.SetActive(true);
            buildPopUp.transform.LookAt(Camera.main.transform);
            buildPopUp.transform.localScale = new Vector3(-20, 20, 5);
        } else
        if(Vector3.Distance(Camera.main.transform.position, transform.position) > maxArea + 30f)
        {
            closedPopUp = false;
            if (buildPopUp && buildPopUp.activeInHierarchy)
                buildPopUp.SetActive(false);
        }
    }
    
    public void DeleteFlag(){
        if(buildPopUp)
            Destroy(buildPopUp);
        for(int i = workingBuilders.Count - 1; i >= 0 ; i--)
        {
            workingBuilders[i].GetComponent<Lumberjack>().GoBack();
            workingBuilders.RemoveAt(workingBuilders.Count - 1);
        }
        workingBuilders.Clear();
        Destroy(this);
    }


    public void AddLumberjacks()
    {
        GameObject lumber = Camera.main.GetComponent<Camera3dPerson>().lumberjacks.Find(lumb => (!lumb.GetComponent<Lumberjack>().working));
        if (lumber)
        {
            lumber.GetComponent<Lumberjack>().state = LumberState.Builder;

            Collider col = gameObject.GetComponent<Collider>();
            Vector3 randomPoint = new Vector3(
                            Random.Range(col.bounds.min.x, col.bounds.max.x),
                            col.bounds.min.y,
                            Random.Range(col.bounds.min.z, col.bounds.max.z)); // берём случайную точку на нижней границе коллайдера

            

            float nearestX = (Mathf.Abs(randomPoint.x - col.bounds.min.x) > Mathf.Abs(randomPoint.x - col.bounds.max.x)) ? col.bounds.max.x : col.bounds.min.x, // выясняем, какая точка по X ближе к найденной точке
                nearestZ = (Mathf.Abs(randomPoint.z - col.bounds.min.z) > Mathf.Abs(randomPoint.z - col.bounds.max.z)) ? col.bounds.max.z : col.bounds.min.z; // выясняем, какая точка по Z ближе к найденной точке

            Vector3 workHere;

            if (nearestX < nearestZ) 
                workHere = new Vector3(nearestX, randomPoint.y, randomPoint.z);
            else
                workHere = new Vector3(randomPoint.x, randomPoint.y, nearestZ);


            lumber.GetComponent<Lumberjack>().GoToWork(
                workHere,
                gameObject);
            workingBuilders.Add(lumber);
            countLumbers.text = "Сейчас работа" + WordWorkEnding(workingBuilders.Count) + "\n" + workingBuilders.Count+ " строител" + WordCountEnding(workingBuilders.Count); 
        }
    }

    public void DeleteLumberjack()
    {
        if (workingBuilders.Count > 0)
        {
            workingBuilders[workingBuilders.Count - 1].GetComponent<Lumberjack>().GoBack();
            workingBuilders.RemoveAt(workingBuilders.Count - 1);
            if (workingBuilders.Count > 0)
                 countLumbers.text = "Сейчас работа" + WordWorkEnding(workingBuilders.Count) + "\n" + workingBuilders.Count + " строител" + WordCountEnding(workingBuilders.Count);
            else
                countLumbers.text = "Строителей нет";
        }
    }

    public void DeleteLumberjack(GameObject lumb)
    {
        if (workingBuilders.Count > 0)
        {
            lumb.GetComponent<Lumberjack>().GoBack();
            workingBuilders.Remove(lumb);
            if (buildPopUp)
                if (workingBuilders.Count > 0)
                    countLumbers.text = "Сейчас работа"+ WordWorkEnding(workingBuilders.Count) +"\n" + workingBuilders.Count + " строител" + WordCountEnding(workingBuilders.Count);
                else
                    countLumbers.text = "Строителей нет";
        }
    }

    string WordCountEnding(int count)
    {
        var num = 0;
        if (count > 0 && count < 10)
            num = count;
        else
            num = count%10;

        if (num == 1 && count != 11)
            return "ь";
        if (count >= 2 && count <= 4)
            return "я";
        return "ей";
    }

    string WordWorkEnding(int count)
    {
        var num = 0;
        if (count > 0 && count < 10)
            num = count;
        else
            num = count % 10;

        if (num == 1 && count != 11)
            return "ет";
        return "ют";
    }

    public void UpdateProgressBar(float progress)
    {
        floatPercentOfReady = progress;
        if(percentOfReady)
            percentOfReady.GetComponent<ProgressBarBehaviour>().SetFillerSizeAsPercentage(Mathf.Round(floatPercentOfReady * 100));
        //percentOfReady.text = (Mathf.Round(floatPercentOfReady*100)).ToString() + "%";
    }

    public void ClosePopUp()
    {
        Destroy(buildPopUp);
        closedPopUp = true;
    }

    void OnMouseDown()
    {
        closedPopUp = false;
    }
}
