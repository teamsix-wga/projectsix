﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LumberjacksChecker : MonoBehaviour {
    List<GameObject> items = new List<GameObject>();

    void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("LyingItem") && col.GetComponent<DroppedItem>().item.type >= (Item.ItemType)12 && col.GetComponent<DroppedItem>().item.type <= (Item.ItemType)15) // если это лежащая вещь и это орудие труда
        {
            items.Add(col.gameObject);
            transform.parent.gameObject.SendMessage("CheckItems", items);
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.CompareTag("LyingItem") && col.GetComponent<DroppedItem>().item.type >= (Item.ItemType)12 && col.GetComponent<DroppedItem>().item.type <= (Item.ItemType)15) // если это лежащая вещь и это орудие труда
        {
            items.Remove(col.gameObject);
        }
    }
}
