﻿using UnityEngine;
using System.Collections;

public class DungeoTrigger : MonoBehaviour
{

    GameObject cutsceneManager;
    public bool inCave;
    public static bool canEnter = false;

    void Start ()
    {
        cutsceneManager = GameObject.Find("CutScenesManager");
	}

    void OnTriggerEnter(Collider col)
    {
        if(col.CompareTag("Player") && canEnter)
            if (inCave)
            {
                if (col.GetComponent<QuestReceiver>().quests.Exists(q => q.ID == 31))
                {
                    col.transform.FindChild("VisibleSelection").GetComponent<Renderer>().material.SetFloat("_Outline", col.GetComponent<Parameters>().outlineWidth);
                    ClickToMove.canUnselect = true;
                    Camera3dPerson.canMoveCam = true;
                    cutsceneManager.SendMessage("HeroWantToExitDungeon");
                    Destroy(gameObject);
                }
            }
            else
            {
                if (col.GetComponent<QuestReceiver>().quests.Exists(q => q.ID == 30))
                {
                    col.transform.FindChild("VisibleSelection")
                        .GetComponent<Renderer>()
                        .material.SetFloat("_Outline", 0.09f);
                    ClickToMove.canUnselect = false;
                    Camera3dPerson.canMoveCam = false;
                    Camera.main.GetComponent<Camera3dPerson>().SwitchSelect();
                    if (!Camera.main.GetComponent<Camera3dPerson>().UnitList.Exists(unit => unit.CompareTag("Player")))
                        Camera.main.GetComponent<Camera3dPerson>().UnitList.Add(col.gameObject);
                    cutsceneManager.SendMessage("HeroNearDungeon");
                }
            }
    }
    

}
