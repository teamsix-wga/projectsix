﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions.Comparers;
using UnityEngine.UI;

public class Fighter : MonoBehaviour
{
    // враг, которого мы выделяем
    public static GameObject Opponent;
    // использованный скилл
    public static int usedskill;
    // флаг использования аое
    public bool aoe;
    GameObject aoearea;
    public List<float> cooldownTimer;
    GameObject whohitted;
    public int autoattackSkill;
    public bool manualUsedSkill;

    public Transform hand;
    public Transform secondHand;
    public Transform bootsL;
    public Transform bootsR;
    public Transform armor;
    public Transform handL;
    public Transform handR;

    public List<Skill> skills;
    
    public GameObject playerUI;
    GameObject spellPanel;
    private bool aoeButtonPressed;
    private GameObject skillFuncManager;
    private GameObject skillButtonGO;
    Vector3 aoePos = Vector3.zero;
    bool savedUnselectState = true;
    GameObject inventoryOpenButton;
    UnityEngine.Object AoEImage, AoERestrictedImage;

    private bool _canAttack;

    public bool canAttack
    {
        get { return _canAttack; }
        set
        {
            _canAttack = value;
            OnCanAttackChange();
        }
    }

    void Awake()
    {
        inventoryOpenButton = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/SkillPanel/Inventory").gameObject;
        AoEImage = Resources.Load("Images/AoE");
        AoERestrictedImage = Resources.Load("Images/AoECross");
        skillFuncManager = GameObject.Find("SkillsManager");
        skillButtonGO = Resources.Load("SkillButton") as GameObject;
        manualUsedSkill = false;
        
        playerUI = GameObject.Find("SkillPanel");

        skills = GameObject.Find("SkillsManager").GetComponent<SkillsLoader>().LoadSkills("Hero");
        cooldownTimer = new List<float>();

        spellPanel = GameObject.FindGameObjectWithTag("SpellPanel");

        ResetSkills();

        aoe = false;
        SetAutoattackSkill(skills[autoattackSkill - 1]);
        ObjectUnselected();
        canAttack = true;
    }

    public void ResetSkills()
    {
        var interactable = true;
        if (spellPanel.transform.childCount > 0)
            interactable = spellPanel.transform.GetChild(0).GetComponent<Button>().interactable;

        cooldownTimer = new List<float>();
        for (int i = 0; i < spellPanel.transform.childCount; i++)
            Destroy(spellPanel.transform.GetChild(i).gameObject);

        for (int i = 0; i < skills.Count; i++)
        {
            cooldownTimer.Add(0);

            GameObject spell = Instantiate(skillButtonGO);
            spell.GetComponent<SkillButton>().SetSkill(skills[i], i + 1);
            spell.GetComponent<Image>().sprite = skills[i].icon;
            spell.transform.SetParent(spellPanel.transform);
            spell.transform.localScale = new Vector3(1, 1, 1);
            spell.GetComponent<Button>().interactable = interactable;
        }
    }

    public void ObjectSelected()
    {
        for (int i = 0; i < spellPanel.transform.childCount; i++)
        {
            spellPanel.transform.GetChild(i).gameObject.SetActive(true);
        }
        spellPanel.GetComponent<Image>().color = new Color(spellPanel.GetComponent<Image>().color.r, spellPanel.GetComponent<Image>().color.g,
                                                            spellPanel.GetComponent<Image>().color.b, 1);

        if (Camera.main.GetComponent<Camera3dPerson>().SelectedUnit.Count == 1 &&
    Camera.main.GetComponent<Camera3dPerson>().SelectedUnit[0] == gameObject)
        {
            inventoryOpenButton.GetComponent<Button>().onClick.RemoveAllListeners();
            inventoryOpenButton.GetComponent<Button>().onClick.AddListener(GetComponent<InventoryUnit>().OpenOrClose);
        }
    }

    public void ObjectUnselected()
    {
        for (int i = 0; i < spellPanel.transform.childCount; i++)
        {
            spellPanel.transform.GetChild(i).gameObject.SetActive(false);
        }
        spellPanel.GetComponent<Image>().color = new Color(spellPanel.GetComponent<Image>().color.r, spellPanel.GetComponent<Image>().color.g,
                                                    spellPanel.GetComponent<Image>().color.b, 0.58f);
    }

    public void Update()
    {
        for (int i = 0; i < skills.Count; i++)
        {
            GameObject image = playerUI.transform.GetChild(0).GetChild(i).FindChild("Image").gameObject;
            if (cooldownTimer[i] > 0)
            {
                cooldownTimer[i] -= Time.deltaTime;
                if (!image.activeInHierarchy)
                    image.SetActive(true);
                image.transform.GetChild(0).GetComponent<Text>().text = Math.Round(cooldownTimer[i], 1).ToString();
            }
            if (cooldownTimer[i] < 0)
            {
                cooldownTimer[i] = 0;
                image.SetActive(false);
            }
        }

        if (Opponent && (Opponent.GetComponent<Parameters>().health <= 0 || !canAttack))
            Opponent = null;
        // Если мы не мертвы
        if (!GetComponent<Parameters>().died)
        {
            // если выбираем область аое
            if (aoe)
            {
                RaycastHit hit;
                int mask = ~((1 << 8) | (1 << 10) | (1 << 2));
                // пускаем луч из мышки
                var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                // смотрим, куда попали
                Physics.Raycast(ray, out hit, 1000, mask);
                aoearea.transform.position = new Vector3(hit.point.x, hit.point.y + 15f, hit.point.z);

                if (Vector3.Distance(transform.position, hit.point) > skills[usedskill - 1].distance)
                {
                    aoearea.GetComponent<Projector>().material.SetTexture("_ShadowTex", AoERestrictedImage as Texture);
                    aoearea.GetComponent<Projector>().material.color = new Color32(255, 0, 0, 255); // красный
                }
                else
                {
                    aoearea.GetComponent<Projector>().material.SetTexture("_ShadowTex", AoEImage as Texture);
                    aoearea.GetComponent<Projector>().material.color = new Color32(255, 217, 66, 255); // светло-жёлтый
                }

                if (Input.GetMouseButtonDown(0) && !aoeButtonPressed)
                {
                    if (Vector3.Distance(transform.position, hit.point) > skills[usedskill - 1].distance)
                        return;
                    aoePos = hit.point;
                    aoe = false;
                    Destroy(aoearea);
                    StopCoroutine("WaitCast");
                    ClickToMove.canUnselect = false;
                    ClickToMove.attacking = false;
                    StartCoroutine("WaitCast");
                }

                if (Input.GetKey(KeyCode.Escape) || Input.GetMouseButtonDown(1))
                {
                    // сбрасываем аое флаг
                    aoe = false;
                    // уничтожаем круг
                    Destroy(aoearea);
                    ClickToMove.canUnselect = savedUnselectState;
                }
            }

            if (((skills[autoattackSkill - 1].needOpponent && Opponent) || !skills[autoattackSkill - 1].needOpponent) && !ClickToMove.attacking && !aoe)
                AutoAttack();

            if (Input.GetMouseButtonUp(0) && aoeButtonPressed)
                aoeButtonPressed = false;
        }
    }

    public void SetUsedSkill(Skill skill)
    {
        if (ClickToMove.attacking)
            return;

        usedskill = skills.IndexOf(skill);
        usedskill++;

        StopCoroutine("WaitCast");
        ClickToMove.attacking = false;
        if (skill.type == Skill.SkillType.AoE)
        {
            savedUnselectState = ClickToMove.canUnselect;
            ClickToMove.canUnselect = false;
            aoe = true;
            GetComponent<Animator>().Play("IDLE");
            // создаём клон префаба круга аое
            if(!aoearea)
                aoearea = Instantiate(Resources.Load("AoEArea"), transform.position, Quaternion.identity) as GameObject;
            aoearea.GetComponent<Projector>().orthographicSize = skills[usedskill - 1].range;
            aoearea.GetComponent<Projector>().material.SetTexture("_ShadowTex", AoEImage as Texture);
            aoearea.GetComponent<Projector>().material.color = new Color32(255, 217, 66, 255); // светло-жёлтый
            aoearea.transform.localEulerAngles = new Vector3(90,0,0);
            aoeButtonPressed = true;
        }
        if (!aoe)
            StartCoroutine("WaitCast");
    }

    public void SetAutoattackSkill(Skill skill)
    {
        playerUI.transform.GetChild(0).GetChild(autoattackSkill - 1).FindChild("AutoAttack").gameObject.SetActive(false);
        autoattackSkill = skills.FindIndex(sk => sk == skill) + 1;
        playerUI.transform.GetChild(0).GetChild(autoattackSkill - 1).FindChild("AutoAttack").gameObject.SetActive(true);
    }

    public IEnumerator WaitCast()
    {
        if (cooldownTimer[usedskill - 1] > 0)
            goto end;
        if (!canAttack)
            goto end;
        if (skills[usedskill - 1].needOpponent && !Opponent)
            goto end;
        if (skills[usedskill - 1].needOpponent && Vector3.Distance(GetComponent<Collider>().ClosestPointOnBounds(Opponent.transform.position),
                Opponent.GetComponent<Collider>().ClosestPointOnBounds(transform.position)) >
            GetComponent<NavMeshAgent>().stoppingDistance + skills[usedskill - 1].distance)
        {
            goto end;
        }

        GetComponent<NavMeshAgent>().destination = transform.position;
        ClickToMove.attacking = true;
        GetComponent<Animator>().Play(skills[usedskill - 1].clipName);
        yield return new WaitForSeconds(skills[usedskill - 1].castLength);
        ClickToMove.attacking = false;
        GetComponent<Animator>().Play("IDLE");
        Use_Skill();
        end:
        StartCoroutine(AllowUnselectPlayer());
        yield return null;
    }

    IEnumerator AllowUnselectPlayer()
    {
        if (aoearea)
            yield break;
        yield return new WaitForSeconds(0.5f);
        ClickToMove.canUnselect = savedUnselectState;
    }

    public void Use_Skill()
    {
        if (!skills[usedskill - 1].needOpponent) 
        {
            if (aoePos == Vector3.zero)
                aoePos = transform.position;
            skillFuncManager.SendMessage(skills[usedskill - 1].skillFunc, new whoUseSpell() { who = gameObject, skill = skills[usedskill - 1], Opponent = null, where = aoePos });
            cooldownTimer[usedskill - 1] = skills[usedskill - 1].cooldown - GetComponent<Parameters>().concentration;
        }
        else if (skills[usedskill - 1].needOpponent && Opponent)
        {
            if (
                Vector3.Distance(GetComponent<Collider>().ClosestPointOnBounds(Opponent.transform.position),
                    Opponent.GetComponent<Collider>().ClosestPointOnBounds(transform.position)) <=
                GetComponent<NavMeshAgent>().stoppingDistance + skills[usedskill - 1].distance)
            {
                transform.LookAt(Opponent.transform.position);
                skillFuncManager.SendMessage(skills[usedskill - 1].skillFunc,
                    new whoUseSpell() {who = gameObject, skill = skills[usedskill - 1], Opponent = Opponent});
                cooldownTimer[usedskill - 1] = skills[usedskill - 1].cooldown - GetComponent<Parameters>().concentration;
            }
            else
            {
                GetComponent<NavMeshAgent>().destination = Opponent.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
            }
        }
        aoePos = Vector3.zero;
        ClickToMove.attacking = false;
    }

    // функция получения урона
    public void GetHit(AttackStructure atStr)
    {
        if (GetComponent<Parameters>().invulnerability)
            return;
        float damage = atStr.damage;
        if (damage >= GetComponent<Parameters>().defence)
            // уменьшаем здоровье
            GetComponent<Parameters>().health -= damage - GetComponent<Parameters>().defence;
        // если здоровье меньше 0 
        if (GetComponent<Parameters>().health <= 0)
            // умираем
            StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        if (Act2.lastBattle)
        {
            GameObject.Find("CutScenesManager").SendMessage("HeroLostBattle");
            GetComponent<Parameters>().invulnerability = true;
            yield break;
        }    
        float waitTime = 0;
        GetComponent<Parameters>().selected = false;
        Camera.main.GetComponent<Camera3dPerson>().UnitList.Remove(gameObject);
        GetComponent<Parameters>().died = true;
        if (GetComponent<Animator>())
        {
            GetComponent<Animator>().Play("Spear_Die_fall_backward");
            waitTime = GetComponent<Animator>().GetCurrentAnimatorClipInfo(0)[0].clip.length;
        }
        GetComponent<NavMeshAgent>().enabled = false;
        Opponent = null;
        if (aoearea)
            Destroy(aoearea);
        aoe = false;
        Camera.main.GetComponent<Camera3dPerson>().heroDied = true;
        Camera.main.SendMessage("HeroDied");
        GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/EscMenu").GetComponent<Canvas>().enabled = true;
        yield return new WaitForSeconds(waitTime);
        Time.timeScale = 0;
        GetComponent<Animator>().Stop();
    }

    public void AutoAttack()
    {
        usedskill = autoattackSkill;
        StopCoroutine("WaitCast");
        ClickToMove.attacking = false;
        StartCoroutine("WaitCast");
    }

    void OnCanAttackChange()
    {
        if (!_canAttack)
        {
            Opponent = null;
            for (int i = 0; i < spellPanel.transform.childCount; i++)
                spellPanel.transform.GetChild(i).GetComponent<Button>().interactable = false;
        }
        else
        {
            for (int i = 0; i < spellPanel.transform.childCount; i++)
                spellPanel.transform.GetChild(i).GetComponent<Button>().interactable = true;
        }
    }
}
