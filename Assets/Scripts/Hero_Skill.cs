﻿using UnityEngine;
using System.Collections;

public class Hero_Skill : MonoBehaviour {

	// урон скиллов
	public static int[] damage = {20, 40, 100};
	
	// анимации скиллов
	public static AnimationClip[] clips = {GameObject.FindGameObjectWithTag ("Player").GetComponent<Animation> ().GetClip ("SwordSwingHighRight"), //1 скилл
		GameObject.FindGameObjectWithTag ("Player").GetComponent<Animation> ().GetClip ("BowAim"), //начало 2 скилл
		GameObject.FindGameObjectWithTag ("Player").GetComponent<Animation> ().GetClip ("BowShootArrow")}; // конец 2 скилл
	
	
	// расстояние до врага
	public static int[] distance = {10, 50, 50};
	
	// радиус действия
	public static int[] range = {0,0,40};
}
