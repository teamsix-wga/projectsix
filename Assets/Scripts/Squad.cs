﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum FlagType
{
    Attack = 1,
    Defence = 2,
    Scout = 3,
    Free = 0
}

/// <summary>
/// Скрипт управления отрядом
/// </summary>
public class Squad : MonoBehaviour {
    public int numSquad = -1; // номер отряда, к которому привязан скрипт
    public List<GameObject> units = new List<GameObject>(); // список юнитов в отряде
    public bool isAttacking = false; // атакует ли отряд врага?
    public bool isGoingToFlag; // идёт ли отряд к флагу
    public Vector3 whereToGo; // куда идёт отряд
    public bool isSquadReady = false; // готов ли отряд?
    public GameObject squadCamp; // палатка отряда
    public FlagType type; // тип флага
    public bool isComingBack = false;
    public bool notNeed = false;
    public bool squadActive = true;
    public string squadName = "";
    bool defencing = false;
    private GameObject hero, unitPanelController;
    public GameObject squadButtonUI, squadButton;
    GameObject squadMinimapPlane;
    NavMeshPath squadPath;
    float goldPool = 0;
    int unitsAtTaskStart = 0;
    public static bool canSquadBeReady = true; 
    
    void Start ()
    {
        squadMinimapPlane = Instantiate(Resources.Load("SquadMinimapPlane")) as GameObject;
        StartCoroutine(SetPositionAtMinimap());
        squadButtonUI = GameObject.Find("SquadButtonsUI");
        unitPanelController = GameObject.Find("UI").transform.FindChild("CanBeTurnedOff/UnitPanelUI").gameObject;
        hero = GameObject.FindGameObjectWithTag("Player");
        if (units.Count > 0 && !notNeed) 
        {
            notNeed = true;
            Squads.squads.Add(units);
            Camera.main.GetComponent<Camera3dPerson>().MakeNewSquad(units, gameObject);
            Squads.squads.Sort((a, b) => a[0].GetComponent<FriendUnit>().numSquad.CompareTo(b[0].GetComponent<FriendUnit>().numSquad));
            foreach (GameObject unit in units)
                unit.GetComponent<FriendUnit>().squadGO = gameObject;
        }
        if (!Camera3dPerson.squadsGO.Exists((GameObject squad) => (squad == gameObject)))
        {
            Camera3dPerson.squadsGO.Add(gameObject);
            Camera3dPerson.squadsGO.Sort((a,b)=> int.Parse(a.name.Remove(0, 5)).CompareTo(int.Parse(b.name.Remove(0, 5))));
        }

        squadButton = Instantiate(Resources.Load("UI/SquadSprite")) as GameObject;
        squadButton.transform.SetParent(squadButtonUI.transform);
        var unitButtonRes = Resources.Load("UI/UnitInSquadButton") as GameObject;
        squadButton.GetComponent<SquadFoldoutButton>().NewSquad(units);
        CheckSquadReadyToGo();
        hero.SendMessage("CheckCreatedSquad", units.Count);
	}

    void Update()
    {
        if (!isAttacking && isGoingToFlag && !CheckMarauding() && isSquadNearFlag()) // если отряд идёт к флагу и уже пришёл
        {
            isGoingToFlag = false;
            if (type == FlagType.Attack || type == FlagType.Scout || type == FlagType.Free)
            {
                if (Squads.visibleFlags[numSquad - 1])
                {
                    Destroy(Squads.visibleFlags[numSquad - 1]);
                }
                GoToCamp();
            }
            else
            if(type == FlagType.Defence)
            {
                StartCoroutine("DefenceArea");
            }
        }
    }

    IEnumerator SetPositionAtMinimap()
    {
        while (true)
        {
            var vectorSum = Vector3.zero;
            foreach (var unit in units)
                if(unit)
                    vectorSum += unit.transform.position;
            var unitsCenter = vectorSum/units.Count;
            if(squadMinimapPlane)
                squadMinimapPlane.transform.position = unitsCenter + new Vector3(0,60,0);
            yield return new WaitForSeconds(0.7f);
        }
    }

    IEnumerator DefenceArea()
    {
        defencing = true;
        yield return new WaitForSeconds(30);
        defencing = false;
        if (Squads.visibleFlags[numSquad - 1])
        {
            Destroy(Squads.visibleFlags[numSquad - 1]);
        }
        GoToCamp();
    }

    public float GetFlagCost(FlagType flagType)
    {
        var taskCost = 0;
        if (flagType == FlagType.Attack)
            taskCost = units.Count * 150;
        if (flagType == FlagType.Defence)
            taskCost = units.Count * 50;
        if (flagType == FlagType.Scout)
            taskCost = units.Count * 100;
        return taskCost;
    }

    /// <summary>
    /// Функция назначения конечной точки (флага)
    /// </summary>
    /// <param name="flagPosition"> Позиция флага </param>
    public void GoToFlag(Vector3 flagPosition, FlagType flagType)
    {
        if (!isGoingToFlag)
        {
            var taskCost = 0;
            if (flagType == FlagType.Attack)
                taskCost = units.Count * 150;
            if (flagType == FlagType.Defence)
                taskCost = units.Count * 50;
            if (flagType == FlagType.Scout)
                taskCost = units.Count * 100;
            if (flagType == FlagType.Free)
                taskCost = 0;

            if (hero.GetComponent<Gold>().currentGold < taskCost)
            {
                StartCoroutine(FadeText.FadeTxt("Недостаточно золота!", Color.red, 3));
                return;
            }

            hero.GetComponent<Gold>().currentGold -= taskCost;
            goldPool = taskCost;
            unitsAtTaskStart = units.Count;
        }

        type = flagType;
        whereToGo = flagPosition;
        transform.position = whereToGo;

        RaycastHit hit;
        Physics.Raycast(transform.position + new Vector3(0, 0.1f, 0), transform.up * -1, out hit, 10); // делаем рэйкаст "под собой"
        transform.localEulerAngles = Quaternion.FromToRotation(transform.right, hit.normal).eulerAngles + new Vector3(0, 0, 90); // получаем угол между нормалью к поверхности и плоскости, параллельной земли

        for (int i = 0; i < units.Count; i++)
        {
            NavMeshPath path = new NavMeshPath();
            if (!units[i].GetComponent<FriendUnit>().agent.CalculatePath(transform.GetChild(i).position, path))
                units[i].GetComponent<NavMeshAgent>().destination = flagPosition;
            else
                units[i].GetComponent<NavMeshAgent>().destination = transform.GetChild(i).position;
            units[i].GetComponent<FriendUnit>().controllingBySquad = true;
            units[i].GetComponent<FriendUnit>().needToStopHealing = true;
        }
        
        isSquadReady = false;
        if (!isGoingToFlag)
            foreach (GameObject unit in units)
                unit.GetComponent<FriendUnit>().cameFrom = unit.transform.position;
        isGoingToFlag = true;
    }

    /// <summary>
    /// Функция назначения врага для атаки
    /// </summary>
    /// <param name="enemy"> Враг </param>
    public void AttackEnemy(GameObject enemy)
    {
        isAttacking = isEveryoneAttackingEnemy();
        if (!isAttacking && enemy.GetComponent<Parameters>().health > 0)
        {
            isSquadReady = false;
            isAttacking = true; // отряд атакует врага
            SetDestinationForSquad(enemy.transform.position); // конечный пункт назначения отряда - враг
            foreach (GameObject unit in units)
            {
                unit.SendMessage("SetOpponent", enemy); // говорим, кого надо атаковать
                unit.GetComponent<FriendUnit>().needToStopHealing = true;
            }
        }
    }

    bool isEveryoneAttackingEnemy()
    {
        if (units[0].GetComponent<FriendUnit>().Opponent)
            return true;
        else
            return false;
    }

    /// <summary>
    /// Функция остановки атаки
    /// </summary>
    public void StopAttackingEnemy()
    {
        isAttacking = false;
        foreach (GameObject unit in units)
            unit.SendMessage("SetNullOpponent");
        if (!CheckMarauding())
        {
            if (isGoingToFlag)
                GoToFlag(whereToGo, type);
            if (isComingBack)
                GoToCamp();
        }
    }

    bool CheckMarauding()
    {
        foreach(GameObject unit in units)
        {
            if (unit.GetComponent<FriendUnit>().goingToThisItem)
            {
                WaitIfNoMarauding();
                return true;
            }
        }
        return false;
    }

    void WaitIfNoMarauding()
    {
        foreach (GameObject unit in units)
        {
            if (!unit.GetComponent<FriendUnit>().goingToThisItem)
            {
                unit.GetComponent<NavMeshAgent>().destination = unit.transform.position;
            }
        }
    }

    /// <summary>
    /// Функция назначения конечной точки пути для отряда
    /// </summary>
    /// <param name="destination"> конечная точка пути </param>
    public void SetDestinationForSquad(Vector3 destination)
    {
        foreach (GameObject unit in units)
        {
            unit.GetComponent<FriendUnit>().needToStopHealing = true;
            unit.GetComponent<FriendUnit>().controllingBySquad = true;
            unit.GetComponent<NavMeshAgent>().destination = destination; // каждому юниту ставим конечную точку
        }
    }

    public bool isSquadNearFlag()
    {
        if (!isGoingToFlag)
            return false;
        int count = 0;
        for(int i = 0; i < units.Count; i++)
            if (Vector3.Distance(units[i].transform.position, units[i].GetComponent<NavMeshAgent>().destination) <= units[i].GetComponent<NavMeshAgent>().stoppingDistance + 0.1f)
                count++;
        if(count == units.Count)
            return true;
        return false;
    }

    /// <summary>
    /// Функция возвращения юнитов в палатку
    /// </summary>
    public void GoToCamp()
    {
        isSquadReady = false;
        foreach (GameObject unit in units)
        {
            isComingBack = true;
            unit.GetComponent<NavMeshAgent>().destination = unit.GetComponent<FriendUnit>().cameFrom; // каждому юниту ставим конечную точку
        }
    }

    void GiveRewardForSquad()
    {
        var reward = goldPool/unitsAtTaskStart;
        foreach (GameObject unit in units)
        {
            unit.GetComponent<Gold>().currentGold += reward;
            goldPool -= reward;
            unit.SendMessage("CheckShop");
        }
        hero.GetComponent<Gold>().currentGold += goldPool;
        goldPool = 0;
    }

    bool SomeoneSelected()
    {
        foreach (GameObject unit in units)
        {
            if (unit.GetComponent<Parameters>().selected)
                return true;
        }
        return false;
    }

    /// <summary>
    /// Функция установления номера отряда
    /// </summary>
    /// <param name="number"> Номер отряда </param>
    public void SetNumberOfSquad(int number, List<GameObject> sqd, GameObject camp)
    {
        numSquad = number;
        isSquadReady = false;
        units = new List<GameObject>();
        if (units.Count == 0 && !notNeed)
        {
            notNeed = true;
            for (int i = 0; i < sqd.Count; i++)
            {
                units.Add(sqd[i]);
            }
            if (!Squads.squads.Exists((List<GameObject> sq) => (sq == sqd)))
                Squads.squads.Add(sqd);
        }
        else
        if (!Squads.squads.Exists((List<GameObject> sq) => (sq == sqd)))
            Squads.squads.Add(sqd);

        Squads.squads.Sort((a, b) => a[0].GetComponent<FriendUnit>().numSquad.CompareTo(b[0].GetComponent<FriendUnit>().numSquad));
        squadCamp = camp;
    }

    public void CheckSquadReadyToGo()
    {
        if (defencing)
            return;

        if (!canSquadBeReady)
        {
            unitPanelController.GetComponent<UnitPanelController>().SetSquadTaskUnavailable();
            isSquadReady = false;
            return;
        }

        int count = 0;
        foreach (GameObject unit in units)
            if (unit.GetComponent<FriendUnit>().isReady())
                count++;
        if (count == units.Count)
        {
            isSquadReady = true;
            if (isComingBack)
            {
                isComingBack = false;
                GiveRewardForSquad();
            }
            if (!isAttacking && !isGoingToFlag)
                foreach (GameObject unit in units)
                    unit.GetComponent<FriendUnit>().controllingBySquad = false;

            if (SomeoneSelected())
                unitPanelController.GetComponent<UnitPanelController>().SetSquadTaskAvailable();
        }
        else
        {
            if(unitPanelController)
                unitPanelController.GetComponent<UnitPanelController>().SetSquadTaskUnavailable();
            isSquadReady = false;
        }
    }

    public void ChangeSquadActiveStatus(bool status)
    {
        squadActive = status;
        foreach (var unit in units)
        {
            unit.SetActive(status);
        }
        squadButton.GetComponent<Button>().interactable = status;
        squadMinimapPlane.SetActive(status);
    }

    void OnDestroy()
    {
        if(squadMinimapPlane)
            Destroy(squadMinimapPlane);
    }

    public void UnitDie()
    {
        unitPanelController.SendMessage("ResetSelectedUnits");
        if (units.Count > 0 && squadButton)
            squadButton.GetComponent<SquadFoldoutButton>().NewSquad(units);
        else
            if(squadButton)
                Destroy(squadButton);
    }
}
