﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShopInventory : MonoBehaviour {
    GameObject ui;
    public GameObject inventorySlot;
    public GameObject inventoryItem;
    GameObject inventoryPanel;
    GameObject slotPanel;
    int slotsAmount;
    public List<Item> items = new List<Item>();
    public List<Item> shopItems = new List<Item>();
    public List<GameObject> slots = new List<GameObject>();
    public List<GameObject> shopSlots = new List<GameObject>();

    void Start () {
        ui = gameObject;
        ui.GetComponent<Canvas>().enabled = false;
        slotsAmount = 21;

        GameObject hero = Camera3dPerson.hero;
        inventoryPanel = ui.transform.FindChild("Inventory Panel").gameObject;
        slotPanel = inventoryPanel.transform.FindChild("Slot Panel").gameObject;
        for (int i = 0; i < hero.GetComponent<InventoryUnit>().slotsCount; i++)
        {
            items.Add(new Item());
            GameObject instSlot = Instantiate(inventorySlot);
            slots.Add(instSlot);
            slots[i].GetComponent<ShopSlots>().id = i;
            slots[i].transform.SetParent(slotPanel.transform);
            instSlot.transform.localScale = Vector3.one;
        }
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("Gloves").gameObject);
        slots[slots.Count - 1].GetComponent<ShopSlots>().id = hero.GetComponent<InventoryUnit>().slotsCount;
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("Armor").gameObject);
        slots[slots.Count - 1].GetComponent<ShopSlots>().id = hero.GetComponent<InventoryUnit>().slotsCount + 1;
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("Boots").gameObject);
        slots[slots.Count - 1].GetComponent<ShopSlots>().id = hero.GetComponent<InventoryUnit>().slotsCount + 2;
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("Weapon").gameObject);
        slots[slots.Count - 1].GetComponent<ShopSlots>().id = hero.GetComponent<InventoryUnit>().slotsCount + 3;
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("SecondWeapon").gameObject);
        slots[slots.Count - 1].GetComponent<ShopSlots>().id = hero.GetComponent<InventoryUnit>().slotsCount + 4;
        items.Add(new Item());
        slots.Add(inventoryPanel.transform.FindChild("Trash").gameObject);
        slots[slots.Count - 1].GetComponent<ShopSlots>().id = hero.GetComponent<InventoryUnit>().slotsCount + 5;

        inventoryPanel = ui.transform.FindChild("Shop Panel").gameObject;
        slotPanel = inventoryPanel.transform.FindChild("Slot Panel").gameObject;
        for (int i = 0; i < slotsAmount; i++)
        {
            shopItems.Add(new Item());
            GameObject instSlot = Instantiate(inventorySlot);
            shopSlots.Add(instSlot);
            
            shopSlots[i].GetComponent<ShopSlots>().id = i + hero.GetComponent<InventoryUnit>().slotsCount + 5;
            shopSlots[i].transform.SetParent(slotPanel.transform);
            instSlot.transform.localScale = Vector3.one;
        }
        
    }
}
