﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


/// <summary>
/// Скрипт для лежащих на земле предметов (как в Diablo)
/// </summary>
public class DroppedItem : MonoBehaviour {
    public Item item; // что это за предмет
    GameObject windowWithName; // табличка с названием предмета (загружается из ресурсов)
    GameObject itemNameWindow; // клонированная табличка
    public float pickupRange = 10; // на каком расстоянии можно подобрать вещь
    public bool someoneGoingHere = false; // кто-то хочет подобрать вещь 
    Color itemColor;
    public string modelName="";
    GameObject outline;
    bool mouseOver = false, altPressed;

	void Start () {
        windowWithName = Resources.Load("ItemNamePopup") as GameObject;
	    outline = transform.Find("VisibleSelection").gameObject;
        outline.GetComponent<Renderer>().material.shader = Shader.Find("Outlined/Silhouette Only");
        outline.GetComponent<Renderer>().material.SetColor("_OutlineColor", itemColor);
        outline.GetComponent<Renderer>().material.SetFloat("_Outline", 0.003f);
        outline.SetActive(false);
	}
	
	void Update () {
        if (itemNameWindow)
        {
            itemNameWindow.transform.position = transform.position + new Vector3(0, 10, 0);
            itemNameWindow.transform.LookAt(Camera.main.transform);
        }
        // подсвечиваем объект
	    if (Input.GetKeyDown(KeyCode.LeftAlt))
	    {
            altPressed = true;
            if (!itemNameWindow) // если нажат левый альт и предмет виден камере и табличка ещё не создана
	        {
	            if (!itemNameWindow)
	                LoadItemNameWindow();
	            if (outline)
	                outline.SetActive(true);
	        }
	    }
	    if (Input.GetKeyUp(KeyCode.LeftAlt))
	    {
            altPressed = false;
            if (itemNameWindow && !mouseOver)
	        {
	            if (itemNameWindow)
	                Destroy(itemNameWindow);
	            if (outline)
	                outline.SetActive(false);
	        }
	    }
	}

    void LoadItemNameWindow()
    {
        itemNameWindow = Instantiate(windowWithName, transform.position + new Vector3(0, 10, 0), Quaternion.identity) as GameObject;
        itemNameWindow.transform.LookAt(Camera.main.transform);
        itemNameWindow.transform.FindChild("Name").FindChild("Text").transform.localScale = new Vector3(-1 * itemNameWindow.transform.FindChild("Name").FindChild("Text").transform.localScale.x, itemNameWindow.transform.FindChild("Name").FindChild("Text").transform.localScale.y, itemNameWindow.transform.FindChild("Name").FindChild("Text").transform.localScale.z);
        itemNameWindow.transform.SetParent(gameObject.transform);
        itemNameWindow.transform.FindChild("Name").GetComponent<Button>().onClick.RemoveAllListeners();
        itemNameWindow.transform.FindChild("Name").GetComponent<Button>().onClick.AddListener(PickupItem);
        itemNameWindow.transform.FindChild("Name").FindChild("Text").GetComponent<Text>().text = item.Name;
        if (itemColor == new Color(255, 255, 255))
            itemNameWindow.transform.FindChild("Name").FindChild("Text").GetComponent<Text>().color = new Color(180,180,180, 255);
        else
            itemNameWindow.transform.FindChild("Name").FindChild("Text").GetComponent<Text>().color = itemColor;
    }

    /// <summary>
    /// Функция подбора предмета по клику на вещь, или на табличку вещи
    /// </summary>
    public void PickupItem()
    {
        if (item.ID == -1) // если вещь не назначена - выходим
            return;
        int cnt = 0; // счётчик для слотов
        GameObject hero = GameObject.FindGameObjectWithTag("Player"); // ищем героя
        if (Vector3.Distance(hero.transform.position, GetComponent<Collider>().ClosestPointOnBounds(hero.transform.position)) > pickupRange) //  если герой далеко
            return; // выходим
        
        for (int i = 0; i < hero.GetComponent<InventoryUnit>().slotsCount; i++) // по каждому слоту в инвентаре
        {
            if (hero.GetComponent<InventoryUnit>().items.Exists(delegate (Item it) { return it.slotID == i; })) // если слот занят
                cnt++; // увеличиваем счётчик
            else { item.slotID = cnt; break; } // иначе присваиваем слоту вещи свободный слот
        }

        if (cnt == hero.GetComponent<InventoryUnit>().slotsCount) // если нет свободного места
        {
            GetComponent<Rigidbody>().velocity = (transform.up + new Vector3(Random.Range(0, 0.3f), 0, Random.Range(0, 0.3f)) * 10); // вещь подпрыгивает
            return;
        }
        hero.GetComponent<InventoryUnit>().items.Add(item); // добавляем вещь герою
        Destroy(gameObject); // удаляемся
        Destroy(this);
    }

    /// <summary>
    /// Функция для задания предмета
    /// </summary>
    /// <param name="itm"> предмет </param>
    void SetItem(Item itm)
    {
        item = Item.CopyItem(itm);
        if(!outline)
            outline = transform.Find("VisibleSelection").gameObject;
        switch (item.quality)
        {
            case Item.ItemQuality.Broken:
                itemColor = new Color(255, 0, 0);
                break;
            case Item.ItemQuality.Damaged:
                itemColor = new Color(255, 160, 0);
                break;
            case Item.ItemQuality.Common:
                itemColor = new Color(255, 255, 255);
                break;
            case Item.ItemQuality.Good:
                itemColor = new Color(0, 255, 0);
                break;
            case Item.ItemQuality.Rare:
                itemColor = new Color(139, 0, 255);
                break;
        }
        if(outline)
            outline.GetComponent<Renderer>().material.SetColor("_OutlineColor", itemColor);
    }
    
    void OnMouseDown()
    {
        PickupItem();
    }
    void OnMouseEnter()
    {
        mouseOver = true;
        if (!itemNameWindow)
            LoadItemNameWindow();
        if (outline)
            outline.SetActive(true);
    }
    void OnMouseExit()
    {
        mouseOver = false;
        if (altPressed)
            return;
        if(itemNameWindow)
            Destroy(itemNameWindow);
        if(outline)
            outline.SetActive(false);
    }
}
