﻿using UnityEngine;
using System.Collections;

public class IDGetSet : MonoBehaviour {
    public int objectID = -1; // ID объекта.
    public int creatureID = -1; // ID существа (не объекта). Изменяется из редактора. Герой - 0, казарма - 1, 
                           // рабочий - 2, TalkingTom - 3, ТХ - 4, башня защитная - 5, магазин - 6, Warrior - 7, дерево - 8, враг - 9

    void Start () {
        if(objectID == -1)
            objectID = gameObject.GetHashCode();
    }
}
