﻿using UnityEngine;
using System.Collections;
using UnityEditor;

//[CustomEditor(typeof(GameObject))]
public class PlaceTree : Editor {
    public static bool canPlace = false;
    
    void OnSceneGUI()
    {
        if (canPlace && Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition); ;
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, 1000.0f))
            {
                var go = PrefabUtility.InstantiatePrefab(Resources.Load("Tree")) as GameObject;
                go.transform.position = hit.point;
            }
        }
    }
}

public class PlaceTreeEditorWindow : EditorWindow
{
    static PlaceTreeEditorWindow window;

    [MenuItem("Window/Place Trees")]
    private static void Init()
    {
        window = (PlaceTreeEditorWindow)EditorWindow.GetWindow(typeof(PlaceTreeEditorWindow));
        window.titleContent.text = "Place Trees";
        window.autoRepaintOnSceneChange = true;
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal();
        
        PlaceTree.canPlace = EditorGUILayout.Toggle("Сажаем деревья?", PlaceTree.canPlace);

        GUILayout.EndHorizontal();
    }

    private void OnDestroy()
    {
        PlaceTree.canPlace = false;
    }
}