﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;

public class TakeDamageTest : MonoBehaviour {

    /*[Test]
    public void HeroTakeDamage()
    {
        var fighter = Instantiate(Resources.Load("Hero"), Vector3.zero, Quaternion.identity) as GameObject; // создаём героя

        fighter.GetComponent<Parameters>().health = fighter.GetComponent<Parameters>().maxHealth = 200;
        fighter.GetComponent<Parameters>().defence = 0;

        AttackStructure atk = new AttackStructure();
        int damage = 50;
        atk.damage = damage;
        atk.whoHit = null;

        fighter.GetComponent<Fighter>().GetHit(atk);
        Assert.AreEqual(fighter.GetComponent<Parameters>().maxHealth - damage, fighter.GetComponent<Parameters>().health); // проверка, корректно ли работает нанесение урона 

        fighter.GetComponent<Parameters>().defence = 10; // повышаем защиту

        fighter.GetComponent<Fighter>().GetHit(atk);
        Assert.AreEqual(fighter.GetComponent<Parameters>().maxHealth - damage * 2 + 10, fighter.GetComponent<Parameters>().health);// проверка, корректно ли защита при нанесении урона

        atk.damage = 2000;

        fighter.GetComponent<Fighter>().GetHit(atk);
        Assert.AreEqual(true, fighter.GetComponent<Parameters>().died); // проверка, умер ли герой
    }

    [Test]
    public void FriendWarriorTakeDamage()
    {
        var fighter = Instantiate(Resources.Load("Warrior"), Vector3.zero, Quaternion.identity) as GameObject; // создаём героя

        fighter.GetComponent<Parameters>().health = fighter.GetComponent<Parameters>().maxHealth = 200;
        fighter.GetComponent<Parameters>().defence = 0;

        AttackStructure atk = new AttackStructure();
        int damage = 50;
        atk.damage = damage;
        atk.whoHit = null;

        fighter.GetComponent<FriendUnit>().GetHit(atk);
        Assert.AreEqual(fighter.GetComponent<Parameters>().maxHealth - damage, fighter.GetComponent<Parameters>().health); // проверка, корректно ли работает нанесение урона 

        fighter.GetComponent<Parameters>().defence = 10; // повышаем защиту

        fighter.GetComponent<FriendUnit>().GetHit(atk);
        Assert.AreEqual(fighter.GetComponent<Parameters>().maxHealth - damage * 2 + 10, fighter.GetComponent<Parameters>().health);// проверка, корректно ли защита при нанесении урона

        atk.damage = 2000;

        fighter.GetComponent<FriendUnit>().GetHit(atk);
        Assert.AreEqual(true, fighter.GetComponent<Parameters>().died); // проверка, умер ли герой
    }*/
}
