﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System;
using System.Text;
using System.IO;


public class MakeBuild {
    static string[] SCENES = FindEnabledEditorScenes();

    static string APP_NAME = "ProjectSix";
    static string TARGET_DIR = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
    
    static void PerformWindows64Build()
    {
        GenericBuild(SCENES, TARGET_DIR + "\\" + APP_NAME, BuildTarget.StandaloneWindows64, BuildOptions.None);
    }

    private static string[] FindEnabledEditorScenes()
    {
        List<string> EditorScenes = new List<string>();
        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            if (!scene.enabled) continue;
            EditorScenes.Add(scene.path);
        }
        return EditorScenes.ToArray();
    }

    static void GenericBuild(string[] scenes, string target_dir, BuildTarget build_target, BuildOptions build_options)
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(build_target);
        string res = BuildPipeline.BuildPlayer(scenes, target_dir, build_target, build_options);
        if (res.Length > 0)
        {
			File.WriteAllText(TARGET_DIR+"\\UNITY_ERROR.txt",res);
            throw new Exception("BuildPlayer failure: " + res);
        }
    }
}
