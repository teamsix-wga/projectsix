using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System.Text;

public class ItemsEditor : EditorWindow
{
    private JsonData itemData;
    public List<Item> items = new List<Item>();

    private List<string> names;
    private int selectedItem;
    private string infoString = "Info box";

    static ItemsEditor window;
    [MenuItem("Window/Item Editor")]
    static void Init()
    {
        window = (ItemsEditor)EditorWindow.GetWindow(typeof(ItemsEditor));
        window.titleContent.text = "Items Editor";
        window.autoRepaintOnSceneChange = true;
    }

    private void OnEnable()
    {
        names = new List<string>();
        LoadFile();
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Добавить новый предмет"))
        {
            
            Item newItem = new Item();
            newItem.Name = "Новый предмет";
            newItem.ID = items.Count;
            items.Add(newItem);
            names.Add(newItem.Name);
            selectedItem = items.Count-1;
            infoString = "Добавлен новый предмет";
        }

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        selectedItem = EditorGUILayout.Popup("Предметы: ", selectedItem, names.ToArray());

        if (GUILayout.Button("Удалить выбранный предмет"))
        {
            if (items.Count > 1)
            {
                items.RemoveAt(selectedItem);
                names.RemoveAt(selectedItem);
                for(int i = selectedItem; i < items.Count; i++){
                    items[i].ID--;
                }
                selectedItem = 0;
                infoString = "Предмет удалён!";
            }
            else
            {
                infoString = "Can't delete last item in list";
            }
        }

        GUILayout.EndHorizontal();

        // Поле ID предмета
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("ID: "+ items[selectedItem].ID.ToString());
        GUILayout.EndHorizontal();
        
        // Название предмета
        GUILayout.BeginHorizontal();
        names[selectedItem] = items[selectedItem].Name = EditorGUILayout.TextField("Название: ", items[selectedItem].Name);
        GUILayout.EndHorizontal();
        
        // Описание предмета
        GUILayout.BeginHorizontal();
        items[selectedItem].Description = EditorGUILayout.TextField("Описание: ", items[selectedItem].Description);
        GUILayout.EndHorizontal();

        // урон от предмета
        GUILayout.BeginHorizontal();
        items[selectedItem].Damage = EditorGUILayout.IntField("Урон: ", items[selectedItem].Damage);
        GUILayout.EndHorizontal();
        
        // концентрация
        GUILayout.BeginHorizontal();
        items[selectedItem].Concentration = EditorGUILayout.FloatField("Концентрация: ", items[selectedItem].Concentration);
        GUILayout.EndHorizontal();
                
        // здоровье от предмета        
        GUILayout.BeginHorizontal();
        items[selectedItem].Health = EditorGUILayout.IntField("Здоровье: ", items[selectedItem].Health);
        GUILayout.EndHorizontal();
        
        // Защита
        GUILayout.BeginHorizontal();
        items[selectedItem].Defence = EditorGUILayout.IntField("Защита: ", items[selectedItem].Defence);
        GUILayout.EndHorizontal();
        
        // скорость передвижения
        GUILayout.BeginHorizontal();
        items[selectedItem].MovingSpeed = EditorGUILayout.FloatField("Скорость передвижения: ", items[selectedItem].MovingSpeed);
        GUILayout.EndHorizontal();
        
        // уровень предмета
        GUILayout.BeginHorizontal();
        items[selectedItem].ItemLevel = EditorGUILayout.IntPopup("Уровень предмета: ", items[selectedItem].ItemLevel, new string[3]{"1","2","3"}, new int[3]{1,2,3});
        GUILayout.EndHorizontal();

        // название файла со скиллами
        GUILayout.BeginHorizontal();
        items[selectedItem].skillFileName = EditorGUILayout.TextField("Файл скиллов: ", items[selectedItem].skillFileName);
        GUILayout.EndHorizontal();

        // 
        GUILayout.BeginHorizontal();
        int itType = 0;
        switch (items[selectedItem].type)
        {
            case Item.ItemType.Armor:
                itType = 0;
                break;
            case Item.ItemType.Gloves:
                itType = 1;
                break;
            case Item.ItemType.Boots:
                itType = 2;
                break;
            case Item.ItemType.Weapon:
                itType = 3;
                break;
            case Item.ItemType.SecondWeapon:
                itType = 4;
                break;
            case Item.ItemType.Potion:
                itType = 5;
                break;
        }
        
        itType = EditorGUILayout.Popup("Тип предмета: ", itType, new string[]{"Броня", "Перчатки","Ботинки","Оружие","Второстепенное оружие", "Зелье"});
        switch (itType)
        {
            case 0:
                items[selectedItem].type = Item.ItemType.Armor;
                break;
            case 1:
                items[selectedItem].type = Item.ItemType.Gloves;
                break;
            case 2:
                items[selectedItem].type = Item.ItemType.Boots;
                break;
            case 3:
                items[selectedItem].type = Item.ItemType.Weapon;
                break;
            case 4:
                items[selectedItem].type = Item.ItemType.SecondWeapon;
                break;
            case 5:
                items[selectedItem].type = Item.ItemType.Potion;
                break;
        }
        GUILayout.EndHorizontal();
        
        // качество
        GUILayout.BeginHorizontal();
        int itQual = (int) items[selectedItem].quality;
        itQual = EditorGUILayout.Popup("Качество предмета: ", itQual, new string[]{"Сломанное", "Повреждённое","Обычное","Хорошее","Редкое"});
        items[selectedItem].quality = (Item.ItemQuality)itQual;
        GUILayout.EndHorizontal();
        
        // stackable        
        GUILayout.BeginHorizontal();
        items[selectedItem].Stackable = EditorGUILayout.Toggle("Стакается (stackable): ", items[selectedItem].Stackable);
        GUILayout.EndHorizontal();
        
        // стоимость покупки в магазине
        GUILayout.BeginHorizontal();
        items[selectedItem].buyCost = EditorGUILayout.IntField("Стоимость покупки в магазине: ", items[selectedItem].buyCost);
        GUILayout.EndHorizontal();

        // стоимость продажи в магазин
        GUILayout.BeginHorizontal();
        items[selectedItem].sellCost = EditorGUILayout.IntField("Стоимость продажи в магазин: ", items[selectedItem].sellCost);
        GUILayout.EndHorizontal();

        // стоимость продажи в магазин
        GUILayout.BeginHorizontal();
        items[selectedItem].sprite = (Sprite)EditorGUILayout.ObjectField("Иконка: ", items[selectedItem].sprite, typeof(Sprite), true);
        if (items[selectedItem].sprite)
            items[selectedItem].iconName = items[selectedItem].sprite.name;
        GUILayout.EndHorizontal();

        // стоимость продажи в магазин
        GUILayout.BeginHorizontal();
        items[selectedItem].model = EditorGUILayout.ObjectField("Модель предмета: ", items[selectedItem].model, typeof(Object), true);
        if (items[selectedItem].model)
            items[selectedItem].modelName = items[selectedItem].model.name;
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Перезагрузить файл"))
        {
            LoadFile();
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Сохранить файл"))
        {
            SaveFile();
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.HelpBox(infoString, MessageType.Info);
        GUILayout.EndHorizontal();
    }

    private void SaveFile()
    {
        string outString = "[\n\t";
        
        for(int i = 0; i < items.Count; i++){
            outString += "{\n\t\t";
            
            outString += "\"id\": "+ items[i].ID.ToString()+",\n\t\t";
            outString += "\"name\": \""+items[i].Name+"\",\n\t\t";
            outString += "\"description\": \""+items[i].Description+"\",\n\t\t";
            outString += "\"damage\": "+items[i].Damage.ToString()+",\n\t\t";
            if(items[i].Concentration % 10 != 0)
                outString += "\"concentration\": "+items[i].Concentration.ToString()+",\n\t\t";
            else
                outString += "\"concentration\": "+items[i].Concentration.ToString()+".0,\n\t\t";
            outString += "\"health\": "+ items[i].Health.ToString()+",\n\t\t";
            outString += "\"defence\": "+items[i].Defence.ToString()+",\n\t\t";
            if(items[i].MovingSpeed % 10 != 0)
                outString += "\"movingSpeed\": "+items[i].MovingSpeed.ToString()+",\n\t\t";
            else
                outString += "\"movingSpeed\": "+items[i].MovingSpeed.ToString()+".0,\n\t\t";
            outString += "\"itemLevel\": "+items[i].ItemLevel.ToString()+",\n\t\t";
            outString += "\"itemType\": "+((int)items[i].type).ToString()+",\n\t\t";
            outString += "\"itemQuality\": "+((int)items[i].quality).ToString()+",\n\t\t";
            outString += "\"stackable\": "+items[i].Stackable.ToString().ToLower()+",\n\t\t";
            outString += "\"iconName\": \"" + items[i].iconName.ToString() + "\",\n\t\t";
            outString += "\"modelName\": \"" + items[i].modelName.ToString() + "\",\n\t\t";
            outString += "\"sellCost\": " + items[i].sellCost.ToString() + ",\n\t\t";
            outString += "\"buyCost\": " + items[i].buyCost.ToString() + ",\n\t\t";
            if(items[i].skillFileName != "")
                outString += "\"skillFileName\": \"" + items[i].skillFileName + "\"\n\t";
            else
                outString += "\"skillFileName\": \"\"\n\t";
            if (i != items.Count -1)
                outString += "},\n\t";
            else
                outString += "}\n";
        }
        outString += "]\n";
        File.WriteAllText(Application.dataPath + "/StreamingAssets/ItemDB/Items.json", outString);
        infoString = "Файл сохранён";
    }

    private void LoadFile()
    {
        itemData = JsonMapper.ToObject(File.ReadAllText(Application.dataPath + "/StreamingAssets/ItemDB/Items.json"));
        for (int i = 0; i < itemData.Count; i++)
        {
            int id = (int)(itemData[i]["id"]);
            string name = itemData[i]["name"].ToString();
            string descr = itemData[i]["description"].ToString();
            int damage = (int)itemData[i]["damage"];
            double con = (double)itemData[i]["concentration"];
            int hlth = (int)itemData[i]["health"];
            int def = (int)itemData[i]["defence"];
            double movSpeed = (double)itemData[i]["movingSpeed"];
            int lvl = (int)itemData[i]["itemLevel"];
            Item.ItemType type = (Item.ItemType)((int)itemData[i]["itemType"]);
            Item.ItemQuality quality = (Item.ItemQuality)((int)itemData[i]["itemQuality"]);
            int sellCost = (int)itemData[i]["sellCost"];
            int buyCost = (int)itemData[i]["buyCost"];
            string fileName = itemData[i]["skillFileName"].ToString();
            items.Add(new Item(
                id,
                name,
                descr,
                damage,
                (float)con,
                hlth,
                def,
                (float)movSpeed,
                lvl,
                type,
                quality,
                (bool)itemData[i]["stackable"],
                itemData[i]["iconName"].ToString(),
                itemData[i]["modelName"].ToString(),
                sellCost,
                buyCost,
                fileName));
            names.Add(name);
        }
        
        infoString = "Файл загружен!";
    }
}