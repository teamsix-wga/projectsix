using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;
using System.Text;
using System.Xml;

public class DialoguesEditor : EditorWindow
{
    public List<Dialogue> dialoguesDB = new List<Dialogue>();
    private List<string> names;
    private int selectedItem;
    private string infoString = "Info box";
    Vector2 scrollPos;
    Dictionary<Phrase, bool> foldoutOp;
    static DialoguesEditor window;
    [MenuItem("Window/Dialogues Editor")]
    static void Init()
    {
        window = (DialoguesEditor)EditorWindow.GetWindow(typeof(DialoguesEditor));
        window.titleContent.text = "Dialogues Editor";
        window.autoRepaintOnSceneChange = true;
    }

    private void OnEnable()
    {
        names = new List<string>();
        foldoutOp = new Dictionary<Phrase, bool>();
        LoadFile();
    }

    private void OnGUI()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);


        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Добавить новый диалог"))
        {
            Dialogue dial = new Dialogue(dialoguesDB.Count, new Phrase(), "");
            dialoguesDB.Add(dial);
            names.Add("Новый диалог");
            selectedItem = dialoguesDB.Count - 1;
            infoString = "Добавлен новый диалог";
            dial.rootPhrase.type = Dialogue.Type.NPCSay;
            foldoutOp.Add(dial.rootPhrase, false);
        }

        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();

        selectedItem = EditorGUILayout.Popup("Диалоги: ", selectedItem, names.ToArray());

        if (GUILayout.Button("Удалить выбранный диалог"))
        {
            if (dialoguesDB.Count > 1)
            {
                dialoguesDB.RemoveAt(selectedItem);
                names.RemoveAt(selectedItem);
                for(int i = selectedItem; i < dialoguesDB.Count; i++){
                    dialoguesDB[i].dialID--;
                }
                selectedItem = 0;
                infoString = "Диалог удалён!";
            }
            else
            {
                infoString = "Невозможно удалить последний элемент списка";
            }
        }

        GUILayout.EndHorizontal();
        
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("ID: " + dialoguesDB[selectedItem].dialID.ToString());
        GUILayout.EndHorizontal();
        
        GUILayout.BeginHorizontal();
        names[selectedItem] = EditorGUILayout.TextField("Описание диалога: ", names[selectedItem]);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        dialoguesDB[selectedItem].rootPhrase.phrase = EditorGUILayout.TextField("Начальная фраза: ", dialoguesDB[selectedItem].rootPhrase.phrase);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        dialoguesDB[selectedItem].eventName = EditorGUILayout.TextField("Название event: ", dialoguesDB[selectedItem].eventName);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        dialoguesDB[selectedItem].rootPhrase.parameterVal = EditorGUILayout.IntField("Значение параметра FMOD", dialoguesDB[selectedItem].rootPhrase.parameterVal);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        foldoutOp[dialoguesDB[selectedItem].rootPhrase] = EditorGUILayout.Foldout(foldoutOp[dialoguesDB[selectedItem].rootPhrase], "Варианты ответов (" + dialoguesDB[selectedItem].rootPhrase.answers.Count.ToString() + "): ");
        if (GUILayout.Button("Добавить новый ответ"))
        {
            dialoguesDB[selectedItem].rootPhrase.answers.Add(new Phrase());
            foldoutOp.Add(dialoguesDB[selectedItem].rootPhrase.answers[dialoguesDB[selectedItem].rootPhrase.answers.Count -1], false);
        }
        GUILayout.EndHorizontal();
        if (foldoutOp[dialoguesDB[selectedItem].rootPhrase])
            ShowFoldoutAnswers(dialoguesDB[selectedItem].rootPhrase.answers, 1);
        
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Перезагрузить файл"))
        {
            LoadFile();
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Сохранить файл"))
        {
            SaveFile();
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        EditorGUILayout.HelpBox(infoString, MessageType.Info);
        GUILayout.EndHorizontal();

        EditorGUILayout.EndScrollView();
    }

    void ShowFoldoutAnswers(List<Phrase> phrases, int depth)
    {
        bool needRemove = false;
        Phrase needToRemove = new Phrase();
        foreach (Phrase ph in phrases)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("", GUILayout.Width(depth * 10));
            if (GUILayout.Button("Удалить", GUILayout.Width(100)))
            {
                needRemove = true;
                needToRemove = ph;
            }
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.Label("", GUILayout.Width(depth * 10));
            GUILayout.Label("Фраза", GUILayout.Width("Фраза".ToCharArray().Length * 10));
            ph.phrase = EditorGUILayout.TextField(ph.phrase);
            GUILayout.EndHorizontal();           

            GUILayout.BeginHorizontal();
            GUILayout.Label("", GUILayout.Width(depth * 10));
            GUILayout.Label("Действие", GUILayout.Width("Действие".ToCharArray().Length * 10));
            ph.action = (Dialogue.Action)EditorGUILayout.EnumPopup("",ph.action, GUILayout.Width(100));
            GUILayout.EndHorizontal();
            if (ph.action == Dialogue.Action.TakeQuest)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(depth * 10));
                GUILayout.Label("ID квеста", GUILayout.Width("ID квеста".ToCharArray().Length * 10));
                ph.questID = EditorGUILayout.IntField(ph.questID, GUILayout.Width(100));
                GUILayout.EndHorizontal();
            }
            if (ph.action == Dialogue.Action.ExitAndSendMessage)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(depth * 10));
                GUILayout.Label("Кому отправляем сообщение", GUILayout.Width("Кому отправляем сообщение".ToCharArray().Length * 10));
                ph.whoWillSendMessage = EditorGUILayout.TextField(ph.whoWillSendMessage);
                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(depth * 10));
                GUILayout.Label("Что за сообщение отправляем", GUILayout.Width("Что за сообщение отправляем".ToCharArray().Length * 10));
                ph.whatMessageWeWillSend = EditorGUILayout.TextField(ph.whatMessageWeWillSend);
                GUILayout.EndHorizontal();
            }
            GUILayout.BeginHorizontal();
            GUILayout.Label("", GUILayout.Width(depth * 10));
            GUILayout.Label("Значение параметра FMOD", GUILayout.Width("Значение параметра FMOD".ToCharArray().Length * 10));
            ph.parameterVal = EditorGUILayout.IntField(ph.parameterVal);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("", GUILayout.Width(depth * 10));
            GUILayout.Label("Тип фразы", GUILayout.Width("Тип фразы".ToCharArray().Length * 10));
            ph.type = (Dialogue.Type)EditorGUILayout.EnumPopup(ph.type, GUILayout.Width(100));
            GUILayout.EndHorizontal();           

            if (ph.answers.Count > 0)
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(depth * 10));
                foldoutOp[ph] = EditorGUILayout.Foldout(foldoutOp[ph], "Варианты ответов (" + ph.answers.Count.ToString() + "): ");
                if (GUILayout.Button("Добавить новый ответ"))
                {
                    ph.answers.Add(new Phrase());
                    foldoutOp.Add(ph.answers[ph.answers.Count-1], false);
                }
                GUILayout.EndHorizontal();
                if (foldoutOp[ph])
                    ShowFoldoutAnswers(ph.answers, depth + 1);
            } else
            {
                GUILayout.BeginHorizontal();
                GUILayout.Label("", GUILayout.Width(depth * 10));
                foldoutOp[ph] = EditorGUILayout.Foldout(foldoutOp[ph], "Варианты ответов (" + ph.answers.Count.ToString() + "): ");
                if (GUILayout.Button("Добавить новый ответ"))
                {
                    ph.answers.Add(new Phrase());
                    foldoutOp.Add(ph.answers[ph.answers.Count - 1], false);
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.BeginHorizontal();
            GUILayout.Label("", GUILayout.Width(depth * 10));
            GUILayout.EndHorizontal();
        }
        if (needRemove)
            phrases.Remove(needToRemove);
        EditorGUILayout.Space();
    }

    private void SaveFile()
    {
        // создаём новый XML документ
        XmlDocument xmlDoc = new XmlDocument();
        // корневой элемент
        XmlNode rootNode = null;
        // создать новый корневой элемент  
        rootNode = xmlDoc.CreateElement("Dialogues");
        // добавить элемент
        xmlDoc.AppendChild(rootNode);

        for(int i = 0; i < dialoguesDB.Count; i++) // по каждому диалогу
        {
            Dialogue dlg = dialoguesDB[i];
            XmlElement dialogueXML = xmlDoc.CreateElement("Dialogue");


            XmlAttribute dialId = xmlDoc.CreateAttribute("ID"); // записываем ID
            dialId.Value = dlg.dialID.ToString();
            dialogueXML.SetAttributeNode(dialId);

            XmlAttribute descr = xmlDoc.CreateAttribute("descr"); // записываем описание диалога
            descr.Value = names[i];
            dialogueXML.SetAttributeNode(descr);

            XmlAttribute eventName = xmlDoc.CreateAttribute("eventName"); // записываем описание диалога
            eventName.Value = dlg.eventName;
            dialogueXML.SetAttributeNode(eventName);

            WritePhrases(dialoguesDB[i].rootPhrase, dialogueXML, xmlDoc);


            rootNode.AppendChild(dialogueXML);
        }

        xmlDoc.Save(Application.dataPath + "/StreamingAssets/DialoguesDB/Dialogues.xml");

        infoString = "Файл сохранён";
    }

    void WritePhrases(Phrase phrase, XmlElement dialogueXML, XmlDocument xmlDoc)
    {
        XmlElement phr = null;
        if (phrase.type == Dialogue.Type.NPCSay) // если фразу говорит NPC
        {
            phr = xmlDoc.CreateElement("NPCSay");
            XmlAttribute phraseXML = xmlDoc.CreateAttribute("phrase"); // записываем фразу
            phraseXML.Value = phrase.phrase;
            phr.SetAttributeNode(phraseXML);

            XmlAttribute param = xmlDoc.CreateAttribute("Parameter"); // записываем действие
            param.Value = phrase.parameterVal.ToString();
            phr.SetAttributeNode(param);
        } 
        if(phrase.type == Dialogue.Type.answer)
        {
            phr = xmlDoc.CreateElement("answer");
            XmlAttribute phraseXML = xmlDoc.CreateAttribute("phrase"); // записываем фразу
            phraseXML.Value = phrase.phrase;
            phr.SetAttributeNode(phraseXML);

            XmlAttribute action = xmlDoc.CreateAttribute("action"); // записываем действие
            action.Value = ((int)phrase.action).ToString();
            phr.SetAttributeNode(action);

            if(phrase.action == Dialogue.Action.TakeQuest)
            {
                XmlAttribute QuestID = xmlDoc.CreateAttribute("QuestID"); // записываем действие
                QuestID.Value = phrase.questID.ToString();
                phr.SetAttributeNode(QuestID);
            }
            if (phrase.action == Dialogue.Action.ExitAndSendMessage)
            {
                XmlAttribute whoMessage = xmlDoc.CreateAttribute("whoMessage"); // записываем действие
                whoMessage.Value = phrase.whoWillSendMessage;
                phr.SetAttributeNode(whoMessage);
                XmlAttribute whatMessage = xmlDoc.CreateAttribute("whatMessage"); // записываем действие
                whatMessage.Value = phrase.whatMessageWeWillSend;
                phr.SetAttributeNode(whatMessage);
            }
            XmlAttribute param = xmlDoc.CreateAttribute("Parameter"); // записываем действие
            param.Value = phrase.parameterVal.ToString();
            phr.SetAttributeNode(param);
        }
        foreach(Phrase ph in phrase.answers) 
            WritePhrases(ph, phr, xmlDoc);

        dialogueXML.AppendChild(phr);
    }

    private void LoadFile()
    {
        dialoguesDB = new List<Dialogue>();
        selectedItem = 0;
        names = new List<string>();
        XmlDocument xmlDoc = new XmlDocument();
        XmlNode rootNode = null;

        // загружаем файл или выходим
        if (File.Exists(Application.dataPath + "/StreamingAssets/DialoguesDB/Dialogues.xml"))
        {
            xmlDoc.Load(Application.dataPath + "/StreamingAssets/DialoguesDB/Dialogues.xml");
            rootNode = xmlDoc.DocumentElement;
        }
        else
            return;

        // получили все диалоги из XML файла
        XmlNodeList dialData = rootNode.ChildNodes;

        // по каждому диалогу в файле
        for (int i = 0; i < dialData.Count; i++)
        {
            int dialID = 0;
            string eventName = "";
            dialID = int.Parse(dialData[i].Attributes[0].Value); // получаем ID диалога
            if (dialData[i].Attributes["eventName"] != null)
                eventName = dialData[i].Attributes["eventName"].Value;
            names.Add(dialData[i].Attributes[1].Value);
            XmlNodeList nodes = dialData[i].ChildNodes; // получаем все дочерние элементы
            Phrase rootPhrase = new Phrase(); // начальная фраза
            rootPhrase.type = Dialogue.Type.NPCSay;
            if (nodes[0].Attributes.Count == 1) // если эта фраза не выдаёт квест
                rootPhrase.phrase = nodes[0].Attributes[0].Value;
            else
            { // если фраза выдаёт квест
                rootPhrase.phrase = nodes[0].Attributes[0].Value;
                rootPhrase.questID = int.Parse(nodes[0].Attributes[1].Value);
            }

            LoadDBNodes(rootPhrase, nodes);

            dialoguesDB.Add(new Dialogue(dialID, rootPhrase.answers[0], eventName));
        }
        
        infoString = "Файл загружен!";
    }



    void LoadDBNodes(Phrase phrase, XmlNodeList nodes)
    {
        foreach (XmlNode node in nodes)
        {
            // разбираем ноду
            Phrase newPhrase = new Phrase();
            if (node.Name == "NPCSay")
            {
                newPhrase.type = Dialogue.Type.NPCSay;
                newPhrase.phrase = node.Attributes[0].Value;
                if (node.Attributes["Parameter"] != null)
                    newPhrase.parameterVal = int.Parse(node.Attributes["Parameter"].Value);
            }
            else
            if (node.Name == "answer")
            {
                newPhrase.type = Dialogue.Type.answer;
                newPhrase.action = (Dialogue.Action)int.Parse(node.Attributes["action"].Value);
                newPhrase.phrase = node.Attributes["phrase"].Value;
                if (node.Attributes["QuestID"] != null)
                    newPhrase.questID = int.Parse(node.Attributes["QuestID"].Value);
                if (node.Attributes["whoMessage"] != null)
                    newPhrase.whoWillSendMessage = node.Attributes["whoMessage"].Value;
                if (node.Attributes["whatMessage"] != null)
                    newPhrase.whatMessageWeWillSend = node.Attributes["whatMessage"].Value;
                if (node.Attributes["Parameter"] != null)
                    newPhrase.parameterVal = int.Parse(node.Attributes["Parameter"].Value);
            }
            foldoutOp.Add(newPhrase, false);

            if (node.HasChildNodes) // если есть дочерние элементы
            {
                LoadDBNodes(newPhrase, node.ChildNodes);
            }
            phrase.answers.Add(newPhrase);
        }
    }
}