﻿Shader "Custom/FogShader" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		Pass
	{
		Lighting Off

		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#include "UnityCG.cginc"
#pragma multi_compile_fog

		sampler2D _MainTex;
	float4 _QOffset;
	float _Dist;
	uniform float4 _MainTex_ST;

	struct v2f {
		float4 pos : SV_POSITION;
		float2 uv : TEXCOORD0;
		UNITY_FOG_COORDS(1)
	};

	v2f vert(appdata_base v)
	{
		v2f o = (v2f)0;
		o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
		o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
		UNITY_TRANSFER_FOG(o,o.pos);
		return o;
	}

	half4 frag(v2f i) : COLOR
	{
		fixed4 c = tex2D(_MainTex, i.uv);
	UNITY_APPLY_FOG(i.fogCoord, c);
	return c;
	}
		ENDCG
	}
	}
		FallBack "Diffuse"
}