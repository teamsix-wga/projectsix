﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Helper : MonoBehaviour
{
    public string tagOfGameObject = ""; // название GO, которые будут использованы 
    public float timeToDie = 3; // время, через которое уничтожатся GO
    public GameObject whoItWillBe; // что мы будем создавать
    private List<GameObject> listOfGO; // пары объектов: ГО и уничтожается ли он

    void Start ()
    {
        listOfGO = GameObject.FindGameObjectsWithTag(tagOfGameObject).ToList();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B) && listOfGO.Count > 0)
        { 
            listOfGO.Remove(listOfGO[0]);
            StartCoroutine(KillGameObjects(listOfGO[0]));
        }
    }

    IEnumerator KillGameObjects(GameObject who)
    {
        GameObject temp = Instantiate(whoItWillBe);
        temp.transform.parent = who.transform;
        temp.transform.position = Vector3.zero;

        yield return new WaitForSeconds(timeToDie);
        Destroy(who);
    }
}
