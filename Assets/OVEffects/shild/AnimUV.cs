﻿using UnityEngine;
using System.Collections;

public class AnimUV : MonoBehaviour {
    public float ScrollSpeedY = 1f;
    public float ScrollSpeedX = 0.2f;
    public bool X = false;
    public bool Y = true;
    public float offsetY;
    public float offsetX;
    public Material mat;

    // Use this for initialization
    void Start()
    {
        mat = GetComponent<Renderer>().material;

    }

    // Update is called once per frame
    void Update()
    {
        mat = GetComponent<Renderer>().material;
        offsetY = Time.time * ScrollSpeedY % 1;
        offsetX = Time.time * ScrollSpeedX % 1;
        if (X & Y)
        {
            mat.mainTextureOffset = new Vector2(offsetX, offsetY);
        }
        else if (X)
        {
            mat.mainTextureOffset = new Vector2(offsetX, 0);
        }
        else if (Y)
        {
            mat.mainTextureOffset = new Vector2(0, offsetY);

        }

        
    }

}
