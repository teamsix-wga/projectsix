﻿using UnityEngine;
using System.Collections;

public class MatSvap : MonoBehaviour
{
    public Material mat, mat1, mat2, mat3;
    public int i;
    
    public float rememberedTime;
    public float ASpeedConst = 0.2f;

    // Use this for initialization
    void Start()
    {
        mat = GetComponent<Renderer>().material;
        rememberedTime = Time.time;
        i = 0;
    }

    // Update is called once per frame
    void Update()
    {

        
        if (rememberedTime + ASpeedConst <= Time.time)
        {
            rememberedTime = Time.time;
            
            if (i == 0)
            {
                GetComponent<Renderer>().material = mat;
            }
            if (i == 1)
            {
                GetComponent<Renderer>().material = mat1;
            }
            if (i == 2)
            {
                GetComponent<Renderer>().material = mat2;
            }
            if (i == 3)
            {
                GetComponent<Renderer>().material = mat3;
            }
            i++;//Random.Range(0, 4);
            if (i > 3) i = 0;
        }

    }
}
