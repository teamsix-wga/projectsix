﻿
using UnityEngine; 
using System.Collections; 

public class SwordDown : MonoBehaviour

{
    Vector3 earth;

    void Awake()
    {
        earth = GameObject.Find("Terrain").GetComponent<Collider>().ClosestPointOnBounds(transform.position);
    }

    void Update()
    {
        while (transform.position.y > earth.y)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - 30f, transform.position.z);
        }
    }
}