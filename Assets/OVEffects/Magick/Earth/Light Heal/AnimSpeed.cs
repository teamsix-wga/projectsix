﻿using UnityEngine;
using System.Collections;

public class AnimSpeed : MonoBehaviour {
    public float Aspeed = 1;
    public AnimationClip Clip;
	// Use this for initialization
	void Start () {
        GetComponent<Animation>()[Clip.name].speed = Aspeed;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
