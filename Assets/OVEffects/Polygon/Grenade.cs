﻿using UnityEngine;
using System.Collections;

public class Grenade : MonoBehaviour
{


    public GameObject opponent;
    public GameObject obj;
    public float speed;

    void Update()
    {
        if (opponent)
        {
            Vector3 distance = opponent.GetComponent<Collider>().ClosestPointOnBounds(transform.position) - GetComponent<Collider>().ClosestPointOnBounds(opponent.transform.position);
            if (Mathf.Abs(distance.x) <= 0.5 && Mathf.Abs(distance.y) <= 0.5 && Mathf.Abs(distance.z) <= 0.5)
            {
                Destroy(gameObject);
                Destroy(this);
            }
            
            transform.LookAt(opponent.transform);
            transform.position += transform.forward * Time.deltaTime * speed;
            transform.localEulerAngles = new Vector3(0f, 0f, 0f);

        }
        else
        {
            transform.LookAt(transform.forward);
            transform.position += transform.forward * Time.deltaTime * speed;
        }
    }

    public void Shoot(GameObject opp, float spd)
    {
        opponent = opp;
        speed = spd;
    }
}
