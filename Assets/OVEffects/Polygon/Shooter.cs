﻿using UnityEngine;
using System.Collections;

public class Shooter : MonoBehaviour {

    public GameObject opponent;
    public GameObject obj;
    public float speed;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject gren = Instantiate(obj);
            gren.transform.parent = transform.parent;
            gren.transform.position = transform.position + new Vector3(0, 0, 0);
            
            gren.GetComponent<Grenade>().Shoot(opponent, speed);
        }
    }
}